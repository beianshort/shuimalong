package com.xincheng.shuimalong.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.xincheng.library.glide.GlideCircleTransform;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.UIUtil;
import com.xincheng.library.widget.alertview.AlertView;
import com.xincheng.library.widget.alertview.OnAlertItemClickListener;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.mydetail.ReanNameAuthenticationActivity;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.recept.Acceptance;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by xiaote on 2017/5/24.
 */

public abstract class BaseFragment extends Fragment {
    public LinearLayout mTitleBarLayout;
    public TextView mCitySelectTv;
    public TabLayout mTabLayout;
    public TextView mTitleTv;
    public ImageView mSearchIv;
    public ImageView mMessageIv;
    public ImageView mFilterIv;
    public TextView mAddTv;

    public int mState = 0;

    private Unbinder mUnbinder;

    private void initTitleBar() {
        mTitleBarLayout = UIUtil.findViewById(mRootView, R.id.common_titlebar_layout);
        mCitySelectTv = UIUtil.findViewById(mRootView, R.id.common_city_select_tv);
        mTabLayout = UIUtil.findViewById(mRootView, R.id.common_tab_layout);
        mTitleTv = UIUtil.findViewById(mRootView, R.id.common_title_tv);
        mSearchIv = UIUtil.findViewById(mRootView, R.id.common_search_iv);
        mMessageIv = UIUtil.findViewById(mRootView, R.id.common_message_iv);
        mFilterIv = UIUtil.findViewById(mRootView, R.id.common_filter_iv);
        mAddTv = UIUtil.findViewById(mRootView, R.id.common_add_tv);
    }

    public abstract int getFragmentLayoutId();

    public abstract void initData();

    public abstract void addListener();

    public View mRootView;

    private void initView() {
        mUnbinder = ButterKnife.bind(this, mRootView);
        initData();
        addListener();
    }

    private void initTabLayout(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(arr[i]));
        }
    }

    private void initTabLayout(List<CommonConfig> carTypes) {
        for (int i = 0; i < carTypes.size(); i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(carTypes.get(i).getName()));
        }
    }

    public void initHomeTitleBar(String[] arr) {
        initTitleBar();
        initTabLayout(arr);
        mSearchIv.setVisibility(View.VISIBLE);
        mMessageIv.setVisibility(View.VISIBLE);
    }

    public void initMarketTitleBar(List<CommonConfig> carTypes) {
        initTitleBar();
        initTabLayout(carTypes);
        mFilterIv.setVisibility(View.VISIBLE);
        mSearchIv.setVisibility(View.VISIBLE);
    }

    public void initInsuranceTitleBar() {
        initTitleBar();
        mCitySelectTv.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.GONE);
        mTitleTv.setVisibility(View.VISIBLE);
        mAddTv.setVisibility(View.VISIBLE);
        mMessageIv.setVisibility(View.VISIBLE);
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(getFragmentLayoutId(), null);
        initView();
        return mRootView;
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    public void showToastMsg(String msg) {
        CommonUtil.showToast(getActivity(), msg);
    }

    public void showIdentifyAlertView() {
        new AlertView(getActivity(), null, "您的账号尚未进行实名认证，是否去认证？",
                getString(R.string.cancel), new String[]{getString(R.string.confirm)}, null,
                AlertView.Style.Alert, new OnAlertItemClickListener() {
            @Override
            public void onItemClick(AlertView alertView, int position) {
                switch (position) {
                    case 0:
                        ShowActivity.showActivity(getActivity(), ReanNameAuthenticationActivity.class);
                        break;
                }
            }
        }).show();
    }


}
