package com.xincheng.shuimalong.fragment.config;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.config.CitySelectActivity;
import com.xincheng.shuimalong.adapter.config.CommonConfigGridAdapter;
import com.xincheng.shuimalong.adapter.config.CityAdapter;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.fragment.BaseFragment;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.view.SideLetterBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by xiaote on 2017/6/16.
 */

public class CityFragment extends BaseFragment {
    @BindView(R.id.city_hot_rv)
    MRecyclerView mHotCityRv;
    @BindView(R.id.city_list_rv)
    MRecyclerView mCityListRv;
    @BindView(R.id.city_letterbar)
    SideLetterBar mCityLetterBar;

    private List<CommonConfig> mHotCitys;
    private CommonConfigGridAdapter mHotCityAdapter;
    private List<RegionData> mCityList;
    private CityAdapter mCityAdapter;
    private LinearLayoutManager mLayoutManager;
    private RegionData mProvince;

    @Override
    public int getFragmentLayoutId() {
        return R.layout.fragment_city;
    }

    private void initHotCityData() {
        mHotCityRv.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mHotCitys = ConfigUtil.getCommonConfigList(getActivity(), PrefKey.HOT_CITYS);
        mHotCityAdapter = new CommonConfigGridAdapter(getActivity(), mHotCitys);
        mHotCityRv.setAdapter(mHotCityAdapter);
    }

    @Override
    public void initData() {
        initHotCityData();
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCityListRv.setLayoutManager(mLayoutManager);
        initCity();
        mCityAdapter = new CityAdapter(getActivity(), mCityList);
        mCityListRv.setAdapter(mCityAdapter);
    }

    private void initCity() {
        List<RegionData> allCityList = ConfigUtil.getRegionDataList(getActivity(), PrefKey.CITY_LIST);
        mProvince = ((CitySelectActivity) getActivity()).getProvince();
        if(mProvince != null) {
            mCityList = new ArrayList<>();
            RegionData cityAll = new RegionData();
            cityAll.setParentId(0);
            cityAll.setId(0);
            cityAll.setName("全部");
            cityAll.setLetter("");
            mCityList.add(cityAll);
            int size = allCityList.size();
            for (int i = 0; i < size; i++) {
                if (mProvince.getId() == allCityList.get(i).getParentId()) {
                    mCityList.add(allCityList.get(i));
                }
            }
        } else {
            mCityList = allCityList;
        }
    }

    @Override
    public void addListener() {
        mCityLetterBar.setOnLetterChangedListener(new MyOnLetterChangedListener());
        mCityListRv.setOnItemClickListener(new MyOnItemClickListener());
        mHotCityRv.setOnItemClickListener(new HotCityItemClickListener());
    }

    private class MyOnLetterChangedListener implements SideLetterBar.OnLetterChangedListener {

        @Override
        public void onLetterChanged(String letter) {
            if (!TextUtils.isEmpty(letter)) {
                int position = mCityAdapter.getLetterPosition(letter);
                if (position != -1) {
                    mLayoutManager.scrollToPositionWithOffset(position, 0);
                }
            }
        }
    }

    private class HotCityItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            Intent intent = new Intent();
            CommonConfig cityData = mHotCityAdapter.getItem(position);
            intent.putExtra(IntentKey.HOT_CITY, cityData);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }

    private class MyOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            RegionData cityData = mCityAdapter.getItem(position);
            Intent intent = new Intent();
            if(mProvince != null) {
                intent.putExtra(IntentKey.PROVINCE,mProvince);
            }
            intent.putExtra(IntentKey.CITY, cityData);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }
    }
}
