package com.xincheng.shuimalong.fragment.home;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.util.UIUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.MainActivity;
import com.xincheng.shuimalong.activity.mydetail.ReanNameAuthenticationActivity;
import com.xincheng.shuimalong.activity.recept.FillReceiptInfoActivity;
import com.xincheng.shuimalong.activity.recept.MyAcceptanceActivity;
import com.xincheng.shuimalong.adapter.viewpager.HomeViewPagerAdapter;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RefreshStatus;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.entity.user.UserInfo;
import com.xincheng.shuimalong.fragment.BaseFragment;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

/**
 * Created by xiaote on 2017/6/29.
 */

public class ViewPagerFragment extends BaseFragment implements MainActivity.OrderCallBack, MainActivity.ConfigCallBack {
    @BindView(R.id.home_viewpager_tab_layout)
    TabLayout mHomeTabLayout;
    @BindView(R.id.home_viewpager)
    ViewPager mHomeViewPager;
    @BindView(R.id.home_viewpager_bill_btn)
    StateButton mBillBtn;

    private List<Fragment> mFragments;
    private int mOrderConfig;
    private int mCityId = 213;
    private int mProvinceId;
    private String mKeyword = "";

    @Override
    public int getFragmentLayoutId() {
        return R.layout.fragment_home_viewpager;
    }

    private void initFragment() {
        if (mFragments == null) {
            mFragments = new ArrayList<>();
            for (int i = 0; i < OrderConfig.ORDER_STATUS_LENGTH; i++) {
                mFragments.add(new OrderFragment());
            }
        }
    }

    @Override
    public void initData() {
        initFragment();
        mHomeTabLayout.setupWithViewPager(mHomeViewPager);
        mOrderConfig = getArguments().getInt(IntentKey.ORDER_CONFIG);
        ((MainActivity) getActivity()).registerOrderCallBack(this);
        ((MainActivity) getActivity()).registerConfigCallBack(this);
        String[] homeListTitleArr = null;
        switch (mOrderConfig) {
            case OrderConfig.RECEIPT:
                homeListTitleArr = getResources().getStringArray(R.array.home_receive_arr);
                mBillBtn.setText(R.string.issue_receipt_bill);
                break;
            case OrderConfig.SELL:
                homeListTitleArr = getResources().getStringArray(R.array.home_sell_arr);
                mBillBtn.setText(R.string.sell_bill);
                break;
        }
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getChildFragmentManager(), homeListTitleArr,
                mFragments, mOrderConfig, mProvinceId, mCityId, mKeyword);
        mHomeViewPager.setAdapter(adapter);

    }

    @OnClick(R.id.home_viewpager_bill_btn)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home_viewpager_bill_btn:
                switch (mOrderConfig) {
                    case OrderConfig.RECEIPT:
                        getUserInfo();
                        break;
                    case OrderConfig.SELL:
                        RadioGroup radioGroup = UIUtil.findViewById(getActivity(), R.id.radio_group);
                        if (radioGroup != null) {
                            radioGroup.check(R.id.radio_market);
                        }
                        break;
                }
                break;
        }
    }

    private void getUserInfo() {
        String token = PrefUtil.getString(getActivity(), PrefKey.TOKEN, "");
        String userId = PrefUtil.getString(getActivity(), PrefKey.LOGIN_USER_ID, "");
        RetroSubscrube.getInstance().getSubscrube(GetParam.getUserInfo(token, userId),
                new BaseObserver(getActivity(), getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            UserInfo userInfo = GsonUtil.parseData(data, UserInfo.class);
                            if (userInfo.isCert()) {
                                ShowActivity.showActivity(getActivity(), MyAcceptanceActivity.class);
                            } else {
                                showIdentifyAlertView();
                            }
                        } else {
                            isGoFillReceiptInfo();
                        }

                    }

                    @Override
                    protected void onHandleError(String msg) {
                        isGoFillReceiptInfo();
                    }
                });
    }

    private void isGoFillReceiptInfo() {
        boolean isCert = PrefUtil.getBoolean(getActivity(), PrefKey.LOGIN_CERT, false);
        if(isCert) {
            ShowActivity.showActivity(getActivity(), MyAcceptanceActivity.class);
        } else {
            showIdentifyAlertView();
        }
    }

    @Override
    public void addListener() {

    }

    @Override
    public void quoteSuccess() { //报价成功
        XLog.e("quoteSuccess...");
        mHomeViewPager.setCurrentItem(1);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(1);
        orderFragment.orderSuccess();
    }

    @Override
    public void issueSuccess() {//出单成功
        mHomeViewPager.setCurrentItem(2);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(2);
        orderFragment.orderSuccess();
    }

    @Override
    public void signReceiveSuccess() {//回单成功
        mHomeViewPager.setCurrentItem(3);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(3);
        orderFragment.orderSuccess();
    }

    @Override
    public void paySuccess() {
        XLog.e("paySuccess");
        mHomeViewPager.setCurrentItem(2);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(2);
        orderFragment.orderSuccess();
    }

    @Override
    public void evalueteSucess() {
        mHomeViewPager.setCurrentItem(4);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(4);
        orderFragment.orderSuccess();
    }

    @Override
    public void refuseSuccess() {
        mHomeViewPager.setCurrentItem(0);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(0);
        orderFragment.orderSuccess();
    }

    @Override
    public void republishSuccess() {
        mHomeViewPager.setCurrentItem(0);
        OrderFragment orderFragment = (OrderFragment) mFragments.get(0);
        orderFragment.orderSuccess();
    }


    @Override
    public void parseCity(RegionData province, RegionData city, CommonConfig hotCity) {
        TextView cityTv = (TextView) getActivity().findViewById(R.id.common_city_select_tv);
        if (province != null) {
            mProvinceId = province.getId();
            mCityId = 0;
            ((MainActivity) getActivity()).setProvinceId(province.getId());
            if (mProvinceId == 0) {
                if (cityTv != null) {
                    cityTv.setText(province.getName());
                }
            } else {
                if (city != null) {
                    mCityId = city.getId();
                    ((MainActivity) getActivity()).setCityId(city.getId());
                    if (cityTv != null) {
                        if (mCityId == 0) {
                            cityTv.setText(province.getName());
                        } else {
                            cityTv.setText(city.getName());
                        }
                    }
                }
            }
        } else if (hotCity != null) {
            mCityId = hotCity.getId();
            ((MainActivity) getActivity()).setCityId(hotCity.getId());
            if (cityTv != null) {
                cityTv.setText(hotCity.getName());
            }
        }
        OrderFragment fragment = (OrderFragment) mFragments.get(mHomeViewPager.getCurrentItem());
        fragment.orderConfig(mProvinceId, mCityId, mKeyword);
    }

    @Override
    public void parseFilter(boolean showNewCar, int newCarId, int min, int max, int useage) {

    }

    @Override
    public void parseCompany(CommonConfig company) {

    }

    @Override
    public void parseKeyWord(String keyword) {
        OrderFragment fragment = (OrderFragment) mFragments.get(mHomeViewPager.getCurrentItem());
        fragment.orderConfig(mProvinceId, mCityId, mKeyword);
    }

}
