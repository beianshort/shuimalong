package com.xincheng.shuimalong.fragment.config;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.config.CitySelectActivity;
import com.xincheng.shuimalong.adapter.config.RegionDataGridAdapter;
import com.xincheng.shuimalong.adapter.config.RegionDataListAdapter;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.Region;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.fragment.BaseFragment;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.GsonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by xiaote on 2017/6/16.
 */

public class ProvinceFragment extends BaseFragment {
    @BindView(R.id.province_current_tv)
    TextView mProvinceCurrentTv;
    @BindView(R.id.province_selected_rv)
    MRecyclerView mProvinceSelectRv;
    @BindView(R.id.province_list_rv)
    MRecyclerView mProvinceListRv;

    private List<RegionData> mProvinceList;
    private RegionDataListAdapter mProvinceAdapter;

    private List<RegionData> mProvinceSelectList;
    private RegionDataGridAdapter mProvinceGridAdapter;

    @Override
    public int getFragmentLayoutId() {
        return R.layout.fragment_province;
    }

    @Override
    public void initData() {
        initProvince();
        initProvinceSelect();
    }

    private void initProvince() {
        mProvinceListRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mProvinceList = ConfigUtil.getRegionDataList(getActivity(), PrefKey.PROVINCE_LIST);
        RegionData regionData = new RegionData();
        regionData.setId(0);
        regionData.setName("全国");
        regionData.setLetter("");
        regionData.setParentId(0);
        mProvinceList.add(0,regionData);
        mProvinceAdapter = new RegionDataListAdapter(getActivity(), mProvinceList);
        mProvinceListRv.setAdapter(mProvinceAdapter);
    }

    private void initProvinceSelect() {
        String provinceSave = PrefUtil.getString(getActivity(), PrefKey.PROVINCE_SAVE, "");
        if (!CommonUtil.isEmpty(provinceSave)) {
            mProvinceSelectList = ConfigUtil.getRegionDataList(getActivity(), PrefKey.PROVINCE_SAVE);
        } else {
            mProvinceSelectList = new ArrayList<>();
        }
        mProvinceSelectRv.setLayoutManager(new GridLayoutManager(getActivity(),
                Constant.PROVINCE_SELECT_ROW_ITEMS_SIZE));
        mProvinceGridAdapter = new RegionDataGridAdapter(getActivity(), mProvinceSelectList);
        mProvinceSelectRv.setAdapter(mProvinceGridAdapter);
    }

    @Override
    public void addListener() {
        mProvinceListRv.setOnItemClickListener(new MyOnItemClickListener());
        mProvinceSelectRv.setOnItemClickListener(new MyOnGridItemClickListener());
    }

    private class MyOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            RegionData province = mProvinceAdapter.getItem(position);
            selectProvince(province);
        }
    }

    private class MyOnGridItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            RegionData province = mProvinceGridAdapter.getItem(position);
            selectProvince(province);
        }
    }

    private void selectProvince(RegionData province) {
        mProvinceCurrentTv.setText(province.getName());
        saveProvince(province);
        if(province.getId() == 0) {
            Intent intent = new Intent();
            intent.putExtra(IntentKey.PROVINCE, province);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        } else {
            ((CitySelectActivity) getActivity()).setProvince(province);
            TabLayout tabLayout = ((CitySelectActivity) getActivity()).getTabLayoyt();
            if(tabLayout != null) {
                tabLayout.getTabAt(1).select();
            }
        }

    }

    private void saveProvince(RegionData province) {
        if (mProvinceSelectList == null) {
            mProvinceSelectList = new ArrayList<>();
            mProvinceSelectList.add(province);
        } else {
            for (int i = 0; i < mProvinceSelectList.size(); i++) {
                if (province.getId() == mProvinceSelectList.get(i).getId()) {
                    return;
                }
            }
            if (mProvinceSelectList.size() < Constant.PROVINCE_SELECT_ROW_ITEMS_SIZE) {
                mProvinceSelectList.add(province);
            } else {
                mProvinceSelectList.remove(0);
                mProvinceSelectList.add(province);
            }
        }
        PrefUtil.putString(getActivity(), PrefKey.PROVINCE_SAVE, GsonUtil.toJson(mProvinceSelectList));
    }
}
