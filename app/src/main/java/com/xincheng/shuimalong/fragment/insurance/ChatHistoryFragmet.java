package com.xincheng.shuimalong.fragment.insurance;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.XRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.insurance.ChatActivity;
import com.xincheng.shuimalong.adapter.friend.ChatHistoryAdapter;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.dialog.FriendDialog;
import com.xincheng.shuimalong.entity.friend.ChatMessage;
import com.xincheng.shuimalong.entity.friend.Friend;
import com.xincheng.shuimalong.entity.friend.HistoryChat;
import com.xincheng.shuimalong.fragment.BaseFragment;
import com.xincheng.shuimalong.listener.OnMessageListener;
import com.xincheng.shuimalong.util.GreenUtil;
import com.xincheng.shuimalong.util.RongUtil;
import com.xincheng.shuimalong.util.ShowActivity;
import com.xincheng.shuimalong.view.ListItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;

/**
 * Created by 许浩 on 2017/6/30.
 */

public class ChatHistoryFragmet extends BaseFragment {

    @BindView(R.id.history_recycler)
    XRecyclerView historyRecycler;
    ChatHistoryAdapter adapter;
    List<HistoryChat> conversations;

    public static ChatHistoryFragmet newInstance() {
        ChatHistoryFragmet fragment = new ChatHistoryFragmet();
        return fragment;
    }

    @Override
    public int getFragmentLayoutId() {
        return R.layout.fragment_chat_history;
    }

    @Override
    public void initData() {
        conversations = new ArrayList<>();
        historyRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        historyRecycler.addItemDecoration(new ListItemDecoration());
        adapter = new ChatHistoryAdapter(getContext(), conversations);
        adapter.setOnItemLongClickListener(new ChatHistoryAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(ChatHistoryAdapter.HistoryHolder viewHolder, HistoryChat conversation, int
                    position) {
                showDialog(conversation);
                return false;
            }
        });
        historyRecycler.setAdapter(adapter);
        getConversation();
    }

    private void getConversation() {
        /**
         * 获取本地私聊会话
         */
        GreenUtil.getInstances().loadAll(new GreenUtil.ResultCallback<List<HistoryChat>>() {
            @Override
            public void onSuccess(List<HistoryChat> chathistorys) {
                XLog.e(chathistorys);
                conversations.addAll(chathistorys);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {

            }
        });
    }
    public void RushList() {
        XLog.e("rushList..");
        XLog.e("conversations==>" + conversations);
        if (conversations != null) {
            conversations.clear();
        }
        getConversation();
    }

    private void showDialog(final HistoryChat conversation) {
        final FriendDialog dialog = FriendDialog.createDialog(getActivity());
        dialog.setTvMoveGone();
        dialog.setTvDeleteListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GreenUtil.getInstances().deleteHisory(conversation.getTergid(),
                        new GreenUtil.ResultCallback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean aBoolean) {
                                if (aBoolean) {
                                    showToastMsg("删除成功");
                                    conversations.remove(conversation);
                                    adapter.notifyDataSetChanged();
                                }

                            }

                            @Override
                            public void onError(String message) {
                                showToastMsg(message);
                            }
                        }
                );
                RongIMClient.getInstance().removeConversation(
                        Conversation.ConversationType.PRIVATE,
                        conversation.getTergid(),
                        new RongIMClient.ResultCallback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean aBoolean) {

                            }

                            @Override
                            public void onError(RongIMClient.ErrorCode errorCode) {

                            }
                        });
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void addListener() {
        historyRecycler.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerHolder holder, View view, int position) {
                HistoryChat historyChat = conversations.get(position - 1);
                Friend.Data data = new Friend.Data();
                data.setUser_id(historyChat.getTergid());
                data.setNick_name(historyChat.getNick_name());
                data.setFace(historyChat.getFace());
                Bundle bundle=new Bundle();
                bundle.putSerializable(IntentKey.FRIEND_BEAN,data);
                ShowActivity.showActivityForResult(getActivity(), ChatActivity.class,bundle, RequestCodeKey.UP_FRIEND_LIST);
            }
        });
    }
}
