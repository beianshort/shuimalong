package com.xincheng.shuimalong.fragment.insurance;

import android.content.Context;
import android.os.Bundle;

import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.activity.insurance.UserHomeActivity;
import com.xincheng.shuimalong.adapter.friend.ChatMessageAdapter;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.util.ShowActivity;

import io.rong.imkit.fragment.ConversationFragment;
import io.rong.imkit.widget.adapter.MessageListAdapter;

/**
 * Created by 许浩 on 2017/7/31.
 */

public class ChatFragment extends ConversationFragment {
    @Override
    public MessageListAdapter onResolveAdapter(Context context) {
        ChatMessageAdapter adapter = new ChatMessageAdapter(context);
        final String user_id = getActivity().getIntent().getData().getQueryParameter("targetId");
        adapter.setOnAvatarClick(new ChatMessageAdapter.OnAvatarClick() {
            @Override
            public void onAvatarClick(boolean isMyself) {
                Bundle extras1 = new Bundle();
                if (isMyself) {
                    extras1.putString(IntentKey.USER_ID,
                            PrefUtil.getString(getContext(), PrefKey.LOGIN_USER_ID, ""));
                } else {
                    extras1.putString(IntentKey.USER_ID, user_id);
                }
                extras1.putInt(IntentKey.FROM, 1);
                ShowActivity.showActivity(getActivity(), UserHomeActivity.class, extras1);
            }
        });
        return adapter;
    }
}
