package com.xincheng.shuimalong.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.SearchActivity;
import com.xincheng.shuimalong.activity.MainActivity;
import com.xincheng.shuimalong.activity.config.CitySelectActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.fragment.home.ViewPagerFragment;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xuhao on 2017/6/13.
 * 首页
 */

public class HomeFragment extends BaseFragment {
    @BindView(R.id.home_broadcast_message_layout)
    LinearLayout mBroadcastMessageLayout;
    @BindView(R.id.home_broadcast_message_tv)
    TextView mBroadcastMessageTv;
    @BindView(R.id.home_broadcast_messgae_clear_iv)
    ImageView mBroadcastClearIv;
    @BindView(R.id.home_user_headicon_iv)
    ImageView mHeadIconIv;
    @BindView(R.id.home_user_name_tv)
    TextView mUsernameTv;
    @BindView(R.id.home_user_message_tv)
    TextView mUserMessageTv;


    @Override
    public int getFragmentLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initData() {
        String[] arr = getResources().getStringArray(R.array.home_title_arr);
        String mobile = PrefUtil.getString(getActivity(), PrefKey.LOGIN_NICKNAME, "");
        String face = PrefUtil.getString(getActivity(), PrefKey.LOGIN_FACE, "");
        initHomeTitleBar(arr);
        changeFragment(OrderConfig.RECEIPT);
        mUsernameTv.setText(getString(R.string.hi_user, mobile));
        mBroadcastMessageTv.setSelected(true);
        Glide.with(getActivity()).load(face)
                .apply(GlideOptionsManager.getInstance().getRequestOptions()).into(mHeadIconIv);
    }

    @OnClick({R.id.common_city_select_tv, R.id.home_broadcast_messgae_clear_iv, R.id.common_search_iv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.common_city_select_tv:
                ShowActivity.showActivityForResult(getActivity(), CitySelectActivity.class,
                        RequestCodeKey.GO_CITY_SELECT);
                break;
            case R.id.home_broadcast_messgae_clear_iv:
                mBroadcastMessageLayout.setVisibility(View.GONE);
                break;
            case R.id.common_search_iv:
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 1);
                ShowActivity.showActivityForResult(getActivity(), SearchActivity.class, extras,
                        RequestCodeKey.GO_SEARCH);
                break;
        }
    }

    @Override
    public void addListener() {
        mTabLayout.addOnTabSelectedListener(new MyOnTabSelectedListener());
    }

    private void changeFragment(int orderConfig) {
        Fragment fragment = new ViewPagerFragment();
        Bundle extras = new Bundle();
        extras.putInt(IntentKey.ORDER_CONFIG, orderConfig);
        fragment.setArguments(extras);
        getChildFragmentManager().beginTransaction().replace(R.id.home_content_layout, fragment)
                .commitAllowingStateLoss();
    }

    private class MyOnTabSelectedListener implements TabLayout.OnTabSelectedListener {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            XLog.e("onTabSelected");
            int orderConfig = 0;
            switch (tab.getPosition()) {
                case 0:
                    orderConfig = OrderConfig.RECEIPT;
                    break;
                case 1:
                    orderConfig = OrderConfig.SELL;
                    break;
            }
            changeFragment(orderConfig);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            XLog.e("onTabUnselected");
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            XLog.e("onTabReselected");
            int orderConfig = 0;
            switch (tab.getPosition()) {
                case 0:
                    orderConfig = OrderConfig.RECEIPT;
                    break;
                case 1:
                    orderConfig = OrderConfig.SELL;
                    break;
            }
            changeFragment(orderConfig);
        }
    }
}
