package com.xincheng.shuimalong.application;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.util.XLogUtil;
import com.xincheng.shuimalong.common.AppConfig;
import com.xincheng.shuimalong.dao.DaoMaster;
import com.xincheng.shuimalong.dao.DaoSession;
import com.xincheng.shuimalong.notifination.ChatNotifination;
import com.xincheng.shuimalong.util.GreenUtil;
import com.xincheng.shuimalong.util.PgyUtil;
import com.xincheng.shuimalong.util.RongUtil;
import com.xincheng.shuimalong.util.UmengUtil;


/**
 * Created by xiaote on 2017/5/24.
 */

public class BaseApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        XLogUtil.init(AppConfig.TAG, AppConfig.IS_DEBUG, AppConfig.IS_LOG_SHOW_BOARDER);
        PrefUtil.setPrefTag(AppConfig.TAG);
        PgyUtil.registerCrash(this, !AppConfig.IS_DEBUG);
        RongUtil.getInstances().initRongImClient(this);
        ChatNotifination.getInstance().init(this);
        UmengUtil.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
