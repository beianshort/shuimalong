package com.xincheng.shuimalong.common;

import com.xincheng.shuimalong.api.AppUrl;

import java.io.File;
import java.text.MessageFormat;

/**
 * Created by xiaote on 2017/6/19.
 */

public class Constant {

    public static final int ITEM_POSITION_MINUS = 1;
    public static final int ITEM_POSITION_MINUS_WITH_HEAD = 2;
    public static final int DATA_LENGTH_DEFAULT = 10;//
    public static final int EXIT_INTERVAL_TIME = 2000;
    public static final int YEAR_BEGIN = 2001;
    public static final int YEAR_END = YEAR_BEGIN + 50;
    public static final int CAMERA_SUCESS = 0x1001;
    public static final String[] CAMERA_OR_LOCAL = {"拍照", "从手机相册选择"};
    public static final String[] CALL_WAY = {"呼叫", "复制号码", "添加到手机通讯录"};
    public static final int UPLOAD_IMAGE_SIZE_DEFAULT = 540;

    public static final String BASE_DIR_NAME = "shuimalong";
    public static final String ID_CARD_PATH = BASE_DIR_NAME + File.separator + "idcard";

    public static final String PASSWORD_SECRET = "shuimalong_2017";

    public static final int COMPANY_SELECT_ROW_ITEMS_SIZE = 3;
    public static final int PROVINCE_SELECT_ROW_ITEMS_SIZE = 4;
    public static final int VCITPE_SELECT_SIZE_DEFAULT = 4;
    public static final String[] MARKET_SORT_ORDER = new String[]{"", "score", "vci_rb", "bid_num"};

    public static final String RONG_APPKEY = "pwe86ga5phxl6";//融云APPKEY
    public static final String RONG_APPSCREET = "DIM0Dy0Sfslk";//融云APPSCREET
    public static boolean SUCCESS = false;

    public static final String getPercentData(int value) {
        return MessageFormat.format("{0}%", value);
    }

    public static final String getPercentData(String value) {
        return MessageFormat.format("{0}%", value);
    }
}
