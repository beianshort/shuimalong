package com.xincheng.shuimalong.common;

/**
 * Created by xiaote on 2017/6/19.
 */

public class RequestCodeKey {
    public static final int GO_CITY_SELECT = 1;
    public static final int GO_INSURANCE_COMPAY_SELECT = 2;
    public static final int GO_PUBLISH_ACCEPTANCE = 3;
    public static final int GO_MARKET_FILTER = 4;
    public static final int GO_SELECT_VCI_TYPE = 5;
    public static final int GO_RECEIPT_QUOTE = 6;
    public static final int GO_ADD_ADDRESS = 7;
    public static final int GO_GET_ADDRESS = 8;
    public static final int INDENTIFY_FRONT = 9;
    public static final int INDENTIFY_CONTRARY = 10;
    public static final int CAR_IDENTIFY_FRONT = 11;
    public static final int CAR_IDENTIFY_CONTRARY = 12;
    public static final int TAKE_PHOTO_CAMERA = 13;
    public static final int TAKE_PHOTO_LOCAL = 14;
    public static final int GO_UPLOAD_IMG = 15;
    public static final int GO_ADD_CUSTOMER = 16;
    public static final int GO_UPDATE_NICKNAME = 17;
    public static final int GO_SEARCH = 18;
    public static final int GO_ADD_BANKCARD = 19;
    public static final int GO_RECEIPT_ISSUE = 20;
    public static final int GO_SIGN_RECEIVE = 21;
    public static final int GO_GET_BANKCARD = 22;
    public static final int UP_FRIEND_LIST = 23;
    public static final int UP_GROUPLIST = 24;
    public static final int GO_PAY = 25;
    public static final int EVALUATE = 26;
    public static final int GO_DESPOSE = 27;
    public static final int GO_REPUBISH_BILL = 28;
}
