package com.xincheng.shuimalong.common;

import com.xincheng.shuimalong.entity.friend.Friend;

/**
 * Created by xiaote on 2017/6/19.
 */

public class IntentKey {
    public static final String TITLE_MESSAGE = "title_message";
    public static final String SUCCESS_MESSAGE = "success_message";
    public static final String PROVINCE = "province";
    public static final String CITY = "city";
    public static final String HOT_CITY = "hot_city";
    public static final String INSURANCE_COMPANY = "insurance_company";
    public static final String SUCCESS = "success";
    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String SHOW_NEW_CAR = "show_new_car";
    public static final String NEW_CAR_ID = "new_car_id";
    public static final String VCI_TYPE_LIST = "vci_type_list";
    public static final String MARKET_DATA = "market_data";
    public static final String BID_ID = "bid_id";
    public static final String VCI_RB = "vci_rb";
    public static final String TCI_RB = "tci_rb";
    public static final String QUOTE_SUCCESS = "quote_success";
    public static final String ORDER_CONFIG = "order_config";
    public static final String REFUSE_REASON="refuse_reason";
    public static final String REFUSE_STATUS="refuse_status";
    public static final String REFUSE_BAK="refuse_bak";

    public static final String STATE = "state";
    public static final String PLATE_NO = "plate_no";
    public static final String TOTAL_MONEY = "total_money";
    public static final String ADDRESS = "address";
    public static final String ADDRESS_ID = "address_id";
    public static final String FROM = "from";
    public static final String FRIEND_BEAN = "friend";
    public static final String CONVERSATION= "conversation";
    public static final String CUSTOMER = "customer";
    public static final String ORDER_DETAIL = "order_detail";
    public static final String UPLOAD_IMG_PATH1 = "upload_img_path1";
    public static final String UPLOAD_IMG_PATH2 = "upload_img_path2";
    public static final String UPLOAD_IMG_BASE641= "upload_img_base641";
    public static final String UPLOAD_IMG_BASE642 = "upload_img_base642";
    public static final String ORDER_STATUS = "order_status";
    public static final String CREATE_TIME = "create_time";
    public static final String FILE_STATUS = "file_status";
    public static final String GUARANTEE_IMAGE = "guarantee_image";
    public static final String MOBILE = "mobile";
    public static final String MOBILE_TOKEN = "mobile_token";
    public static final String CAN_UPDATE = "can_update";
    public static final String NICK_NAME = "nick_name";
    public static final String AVATAR="avatar";
    public static final String ISSUING_NUMBER = "issuing_number";
    public static final String VCI_PERFORMANCE = "vci_performance";
    public static final String TCI_PERFORMANCE = "tci_performance";
    public static final String KEYWORD = "keyword";
    public static final String NEW_MESSAGE = "new_message";
    public static final String BANK_CARD_NO = "bank_card_no";
    public static final String BANK_NAME = "bank_name";
    public static final String BANK_LATTICE = "bank_lattice";
    public static final String BANK_CARD = "bank_card";
    public static final String BANK_CARD_NAME = "bank_card_name";
    public static final String USER_NAME = "username";
    public static final String ID_CARD_NO = "id_card_no";
    public static final String UPLOAD_FROM_FILL_INFO = "upload_from_fill_info";
    public static final String ISSUE_SUCCESS = "issue_success";
    public static final String SIGN_RECEIVE_SUCCESS = "sign_receive_success";
    public static final String MODIFY_FOR_PAYPWD = "modify_for_paypwd";
    public static final String FRIEND_IDS="friend_ids";
    public static final String USER_ID = "user_id";
    public static final String PAY_SUCCESS = "pay_success";
    public static final String EVALUATE = "evaluate";
    public static final String WITHDRAW_SUCCESS = "withdraw_success";
    public static final String REFUSE_SUCCESS = "refuse_success";
    public static final String USER_CERT = "user_cert";
    public static final String REGISTER_TIME = "register_time";
    public static final String REPUBLISH_SUCCESS = "republish_success" ;
    public static final String USE_AGE = "use_age";
    public static final String LOG_ID = "log_id";
    public static final String LOG_TYPE = "log_type";
    public static final String LOG_REDUCE_STATUS = "log_reduce_status";
    public static final String IS_COMMENT = "is_comment";
    public static final String IS_DISABLED = "is_disabled";
    public static final String INTENT_FLAG="intent_flag";
}
