package com.xincheng.shuimalong.common;

/**
 * Created by xiaote1988 on 2017/8/16.
 */

public class UserConfig {
    public static final int MONTH_RECEIPT_STATUS = 1;
    public static final int MONTH_SELL_STATUS = 2;
    public static final int TOTAL_RECEIPT_STATUS = 3;
    public static final int TOTAL_SELL_STATUS = 4;
}
