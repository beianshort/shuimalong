package com.xincheng.shuimalong.common;

/**
 * Created by xiaote on 2017/6/29.
 */

public class OrderConfig {
    public static final int RECEIPT = 1;
    public static final int SELL = 2;

    public static final int[] STATE_ARR = new int[]{0, 1, 2, 3, 4, 5, 6};
    public static final int ORDER_STATUS_LENGTH = 7;
}
