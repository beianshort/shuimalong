package com.xincheng.shuimalong.common;

/**
 * Created by 许浩 on 2017/9/3.
 */

public class MessageConfig {
    public static final int ABOUT_US = 1;
    public static final int CONTACTUS = 2;
    public static final int LAW = 3;
}
