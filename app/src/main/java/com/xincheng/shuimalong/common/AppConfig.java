package com.xincheng.shuimalong.common;

/**
 * Created by xiaote on 2017/5/24.
 */

public class AppConfig {
    public static final String TAG = "shuimalong";
    public static final boolean IS_DEBUG = false;
    public static final boolean IS_LOG_SHOW_BOARDER = false;
}
