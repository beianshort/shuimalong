package com.xincheng.shuimalong.common;

/**
 * Created by xiaote on 2017/6/19.
 */

public class PrefKey {
    public static final String LOGIN_MOBILE = "login_mobile";
    public static final String LOGIN_PASSWORD = "login_password";
    public static final String LOGIN_FACE = "login_face";
    public static final String LOGIN_NICKNAME = "login_nickname";
    public static final String LOGIN_CERT_REASON = "login_cert_reason";
    public static final String TOKEN = "token";
    public static final String IM_TOKEN = "im_token";
    public static final String LOGIN_USER_ID = "user_id";
    public static final String IS_LOGIN = "is_login";
    public static final String LOGIN_SET_PAYPWD = "set_paypwd";
    public static final String LOGIN_OPEN_NOTICE = "open_notice_";
    public static final String LOGIN_CERT = "login_cert";
    public static final String MY_WALLET_MONEY = "my_wallet_money";
    //Save
    public static final String COMPANY_SAVE = "company_save";
    public static final String PROVINCE_SAVE = "province_save";

    //Config
    public static final String EXPRESS_COMPANYS = "expressCompanys";//快递公司
    public static final String EXPRESS_TYPE = "expressType";//保单办理方式
    public static final String COMPANY_LIST = "companyList";//保险公司
    public static final String COMPANY_LOGO_LIST = "companyLogoList";//保险公司LOGO
    public static final String VCI_TYPES = "vciTypes";//商业险类型
    public static final String CAR_TYPE = "carType";   //车型
    public static final String TRUCK_WEIGHT_TYPES = "truckWeightTypes";//货车重量
    public static final String NEW_CAR_TYPE = "newCarType";//车辆新旧
    public static final String ACCEPT_PRICE = "acceptPrice";//30分钟内报价
    public static final String ACCEPT_ISSUE = "acceptIssue";//当天出单
    public static final String BUYER_ISSUE = "buyerIssue";//两天内寄回订单
    public static final String HOT_CITYS = "hotCitys";//热门城市
    public static final String CITY_LIST = "cityList";//城市列表
    public static final String PROVINCE_LIST = "provinceList";//省份列表
    public static final String USAGE = "usage";//是否营运
    public static final String FILE_STATUS = "fileStatus";//文件签收状态
    public static final String REFUSE_REASON = "refuseReason";//退单原因

    public static final String ABOUT_US = "abouyt_us";
    public static final String LAW = "law";
    public static final String CONTACTUS = "contactus";

    //SEARCH_HISTORY
    public static final String MARKET_SEARCH_KEYWORDS = "market_search_keywords";
    public static final String CUSTOMER_SEARCH_KEYWORDS = "customer_search_keywords";
    public static final String HOME_SEARCH_KEYWORDS = "home_search_keywords";

}
