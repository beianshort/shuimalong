package com.xincheng.shuimalong.common;

/**
 * Created by xuhao on 2017/7/19.
 */

public class ApplyFriendCommon {
    public static final int AGREE = 1;
    public static final int REJECT = 2;
    public static final int NOMAL = 0;
    public static final int FRIEND = 3;
    public static final int DELETE = 4;
    public static final int CHAT = 5;
}
