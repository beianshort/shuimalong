package com.xincheng.shuimalong.common;

/**
 * Created by xiaote on 2017/7/22.
 */

public class ChatKey {
    public static final String MESSAGE_TYPE = "message_type";   //消息类型，1：发送消息 0接受消息
    public static final String FROM = "from";                   //发送人ID
    public static final String TO = "to";                      //接收人ID
    public static final String NICKNAME = "nickname";          //发送人昵称
    public static final String FACE = "face";                  //发送人头像
    public static final String CONTENT = "content";            //消息内容
    public static final String TIME = "time";                 //发送时间戳
}
