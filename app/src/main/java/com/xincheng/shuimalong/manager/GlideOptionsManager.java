package com.xincheng.shuimalong.manager;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.xincheng.library.glide.GlideCircleTransform;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;

/**
 * Created by xuhao on 2017/7/18.
 */

public class GlideOptionsManager {

    private static GlideOptionsManager instance;

    private RequestOptions mOptions;

    private GlideOptionsManager() {

    }


    public static GlideOptionsManager getInstance() {
        if(instance == null) {
            synchronized (GlideOptionsManager.class) {
                if(instance == null) {
                    instance = new GlideOptionsManager();
                }
            }
        }
        return instance;
    }

    public RequestOptions getRequestOptions() {
        if(mOptions == null) {
            mOptions = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.icon_avatar)
                    .error(R.drawable.icon_avatar)
                    .priority(Priority.HIGH)
                    .transform(new GlideCircleTransform());
        }
        return mOptions;
    }
}
