package com.xincheng.shuimalong.manager;

import android.app.Activity;

import com.xincheng.library.manager.ExitManager;
import com.xincheng.library.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiaote on 2017/7/10.
 */

public class OrderExitManager {
    private static OrderExitManager instance;

    private List<Activity> activityList;

    private OrderExitManager() {
        activityList = new ArrayList<>();
    }

    public static OrderExitManager getInstance() {
        if (instance == null) {
            synchronized (ExitManager.class) {
                if (instance == null) {
                    instance = new OrderExitManager();
                }
            }
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        this.activityList.add(activity);
    }

    public void exitActivities() {
        if (!CommonUtil.isEmpty(activityList)) {
            for (int i = 0; i < activityList.size(); i++) {
                activityList.get(i).finish();
            }
            activityList.clear();
        }
    }

    public void clearActivies() {
        if(!CommonUtil.isEmpty(activityList)) {
            activityList.clear();
        }
    }
}