package com.xincheng.shuimalong.entity;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xuhao on 2017/8/10.
 */

public class HistoryOrder implements Parcelable {
    private int id;
    private int bid_id;
    private String order_no;
    private String total_money;
    private long update_time;
    private String nick_name;
    private int ac_user_id;
    private int bill_user_id;
    private String avatar;
    private String mobile;
    private String output_volume;
    private String curb_weight;
    private int seat_num;
    private String plate_no;
    private String car_owner;

    public HistoryOrder(Parcel in) {
        id=in.readInt();
        bid_id = in.readInt();
        order_no = in.readString();
        total_money = in.readString();
        update_time = in.readLong();
        nick_name = in.readString();
        ac_user_id = in.readInt();
        bill_user_id = in.readInt();
        avatar = in.readString();
        mobile = in.readString();
        output_volume = in.readString();
        curb_weight = in.readString();
        seat_num = in.readInt();
        plate_no = in.readString();
        car_owner = in.readString();
    }


    public int getBid_id() {
        return bid_id;
    }

    public void setBid_id(int bid_id) {
        this.bid_id = bid_id;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getTotal_money() {
        return total_money;
    }

    public void setTotal_money(String total_money) {
        this.total_money = total_money;
    }

    public long getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(long update_time) {
        this.update_time = update_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAc_user_id() {
        return ac_user_id;
    }

    public void setAc_user_id(int ac_user_id) {
        this.ac_user_id = ac_user_id;
    }

    public int getBill_user_id() {
        return bill_user_id;
    }

    public void setBill_user_id(int bill_user_id) {
        this.bill_user_id = bill_user_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOutput_volume() {
        return output_volume;
    }

    public void setOutput_volume(String output_volume) {
        this.output_volume = output_volume;
    }

    public String getCurb_weight() {
        return curb_weight;
    }

    public void setCurb_weight(String curb_weight) {
        this.curb_weight = curb_weight;
    }

    public int getSeat_num() {
        return seat_num;
    }

    public void setSeat_num(int seat_num) {
        this.seat_num = seat_num;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getCar_owner() {
        return car_owner;
    }

    public void setCar_owner(String car_owner) {
        this.car_owner = car_owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(bid_id);
        dest.writeString(order_no);
        dest.writeString(total_money);
        dest.writeLong(update_time);
        dest.writeString(nick_name);
        dest.writeInt(ac_user_id);
        dest.writeInt(bill_user_id);
        dest.writeString(avatar);
        dest.writeString(mobile);
        dest.writeString(output_volume);
        dest.writeString(curb_weight);
        dest.writeInt(seat_num);
        dest.writeString(plate_no);
        dest.writeString(car_owner);
    }
    public static final Creator<HistoryOrder> CREATOR = new Creator<HistoryOrder>() {
        @Override
        public HistoryOrder createFromParcel(Parcel in) {
            return new HistoryOrder(in);
        }

        @Override
        public HistoryOrder[] newArray(int size) {
            return new HistoryOrder[size];
        }
    };
}
