package com.xincheng.shuimalong.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;

/**
 * Created by xiaote on 2017/6/28.
 */

public class OrderDetail implements Parcelable {

    @SerializedName("bid_id")
    private int bidId;
    @SerializedName("tci_rb")
    private int tciRb;
    @SerializedName("vci_rb")
    private int vciRb;
    private String status;
    @SerializedName("order_no")
    private String orderNo;
    @SerializedName("update_time")
    private String updateTime;
    @SerializedName("create_time")
    private String createTime;
    @SerializedName("pay_time")
    private String payTime;
    @SerializedName("shipping_type")
    private String shippingType;
    @SerializedName("shipping_company")
    private String shippingCompany;
    @SerializedName("shipping_no")
    private String shippingNo;
    @SerializedName("r_shipping_type")
    private String rShippingType;
    @SerializedName("r_shipping_company")
    private String rShippingCompany;
    @SerializedName("r_shipping_no")
    private String rShippingNo;
    @SerializedName("vci_date")
    private String vciDate;
    @SerializedName("tci_date")
    private String tciDate;
    @SerializedName("car_owner")
    private String carOwner;
    @SerializedName("idcard_no")
    private String idcardNo;
    private String vin;
    @SerializedName("engine_no")
    private String engineNo;
    @SerializedName("plate_no")
    private String plateNo;
    @SerializedName("car_register_date")
    private String carRegisterDate;
    @SerializedName("useage")
    private String useAge;
    @SerializedName("new_car")
    private int newCar;
    @SerializedName("car_type")
    private int carType;
    @SerializedName("transfer_car")
    private int transferCar;
    @SerializedName("vci_settings")
    private String vciSettings;
    @SerializedName("car_bak")
    private String carBak;
    @SerializedName("output_volume")
    private String outputVolume;
    @SerializedName("curb_weight")
    private String curbWeight;
    @SerializedName("seat_num")
    private String seatNum;
    private String bak;
    @SerializedName("idcard_photo1")
    private String idcardPhoto1;
    @SerializedName("idcard_photo2")
    private String idcardPhoto2;
    @SerializedName("dl_photo1")
    private String dlPhoto1;
    @SerializedName("dl_photo2")
    private String dlPhoto2;
    @SerializedName("tci_no")
    private String tciNo;
    @SerializedName("vci_no")
    private String vciNo;
    @SerializedName("tci_ct")
    private String tciCt;
    @SerializedName("vci_ct")
    private String vciCt;
    @SerializedName("tci_money")
    private String tciMoney;
    @SerializedName("vci_money")
    private String vciMoney;
    @SerializedName("vvt_money")
    private String vvtMoney;
    @SerializedName("tci_return_money")
    private String tciReturnMoney;
    @SerializedName("vci_return_money")
    private String vciReturnMoney;
    @SerializedName("company")
    private int companyId;
    @SerializedName("province")
    private int provinceId;
    @SerializedName("city")
    private int cityId;
    @SerializedName("mobile_phone")
    private String mobilePhone;
    @SerializedName("reason_status")
    private int reasonStatus;
    @SerializedName("confirm_accept")
    private int confirmAccept;
    @SerializedName("confirm_time")
    private String confirmTime;

    public static final Creator<OrderDetail> CREATOR = new Creator<OrderDetail>() {
        @Override
        public OrderDetail createFromParcel(Parcel in) {
            return new OrderDetail(in);
        }

        @Override
        public OrderDetail[] newArray(int size) {
            return new OrderDetail[size];
        }
    };

    public OrderDetail() {
    }

    protected OrderDetail(Parcel in) {
        bidId = in.readInt();
        tciRb = in.readInt();
        vciRb = in.readInt();
        status = in.readString();
        orderNo = in.readString();
        updateTime = in.readString();
        createTime = in.readString();
        payTime = in.readString();
        shippingType = in.readString();
        shippingCompany = in.readString();
        shippingNo = in.readString();
        rShippingType = in.readString();
        rShippingCompany = in.readString();
        rShippingNo = in.readString();
        vciDate = in.readString();
        tciDate = in.readString();
        carOwner = in.readString();
        idcardNo = in.readString();
        vin = in.readString();
        engineNo = in.readString();
        plateNo = in.readString();
        carRegisterDate = in.readString();
        useAge = in.readString();
        newCar = in.readInt();
        carType = in.readInt();
        transferCar = in.readInt();
        vciSettings = in.readString();
        carBak = in.readString();
        outputVolume = in.readString();
        curbWeight = in.readString();
        seatNum = in.readString();
        bak = in.readString();
        idcardPhoto1 = in.readString();
        idcardPhoto2 = in.readString();
        dlPhoto1 = in.readString();
        dlPhoto2 = in.readString();
        tciNo = in.readString();
        vciNo = in.readString();
        tciCt = in.readString();
        vciCt = in.readString();
        tciMoney = in.readString();
        vciMoney = in.readString();
        vvtMoney = in.readString();
        tciReturnMoney = in.readString();
        vciReturnMoney = in.readString();
        companyId = in.readInt();
        provinceId = in.readInt();
        cityId = in.readInt();
        mobilePhone = in.readString();
        reasonStatus = in.readInt();
        confirmAccept = in.readInt();
        confirmTime = in.readString();
    }

    public int getReasonStatus() {
        return reasonStatus;
    }

    public void setReasonStatus(int reasonStatus) {
        this.reasonStatus = reasonStatus;
    }

    public int getBidId() {
        return bidId;
    }

    public void setBidId(int bidId) {
        this.bidId = bidId;
    }

    public int getTciRb() {
        return tciRb;
    }

    public void setTciRb(int tciRb) {
        this.tciRb = tciRb;
    }

    public int getVciRb() {
        return vciRb;
    }

    public void setVciRb(int vciRb) {
        this.vciRb = vciRb;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(String shippingCompany) {
        this.shippingCompany = shippingCompany;
    }

    public String getShippingNo() {
        return shippingNo;
    }

    public String getShippingMsg() {
        if (CommonUtil.isEmpty(getShippingType())) {
            return "";
        }
        if ("当面办理".equals(getShippingType())) {
            return getShippingType();
        } else if ("快递邮寄".equals(getShippingType())) {
            return getShippingCompany() + "  " + getShippingNo();
        } else {
            return null;
        }
    }

    public void setShippingNo(String shippingNo) {
        this.shippingNo = shippingNo;
    }

    public String getrShippingType() {
        return rShippingType;
    }

    public void setrShippingType(String rShippingType) {
        this.rShippingType = rShippingType;
    }

    public String getrShippingCompany() {
        return rShippingCompany;
    }

    public void setrShippingCompany(String rShippingCompany) {
        this.rShippingCompany = rShippingCompany;
    }

    public String getrShippingNo() {
        return rShippingNo;
    }

    public void setrShippingNo(String rShippingNo) {
        this.rShippingNo = rShippingNo;
    }

    public String getrShippingMsg() {
        if (CommonUtil.isEmpty(getrShippingType())) {
            return "";
        }
        if ("当面办理".equals(getrShippingType())) {
            return getrShippingType();
        } else if ("快递邮寄".equals(getrShippingType())) {
            return getrShippingCompany() + "  " + getrShippingNo();
        } else {
            return null;
        }
    }

    public String getVciDate() {
        return vciDate;
    }

    public void setVciDate(String vciDate) {
        this.vciDate = vciDate;
    }

    public String getTciDate() {
        return tciDate;
    }

    public void setTciDate(String tciDate) {
        this.tciDate = tciDate;
    }

    public String getCarOwner() {
        return carOwner;
    }

    public void setCarOwner(String carOwner) {
        this.carOwner = carOwner;
    }

    public String getIdcardNo() {
        return idcardNo;
    }

    public void setIdcardNo(String idcardNo) {
        this.idcardNo = idcardNo;
    }

    public String getCarBak() {
        return carBak;
    }

    public void setCarBak(String carBak) {
        this.carBak = carBak;
    }

    public String getOutputVolume() {
        return outputVolume;
    }

    public void setOutputVolume(String outputVolume) {
        this.outputVolume = outputVolume;
    }

    public String getCurbWeight() {
        return curbWeight;
    }

    public void setCurbWeight(String curbWeight) {
        this.curbWeight = curbWeight;
    }

    public String getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(String seatNum) {
        this.seatNum = seatNum;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getCarRegisterDate() {
        return carRegisterDate;
    }

    public void setCarRegisterDate(String carRegisterDate) {
        this.carRegisterDate = carRegisterDate;
    }

    public String getUseAge() {
        return useAge;
    }

    public void setUseAge(String useAge) {
        this.useAge = useAge;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getUseAgeMsg() {
        if (CommonUtil.isEmpty(useAge)) {
            return "";
        }
        try {
            int useAgeInt = Integer.parseInt(this.useAge);
            switch (useAgeInt) {
                case 1:
                    return "营运";
                case 2:
                    return "非营运";
            }
        } catch (Exception e) {
        }
        return "";
    }

    public int getNewCar() {
        return newCar;
    }

    public void setNewCar(int newCar) {
        this.newCar = newCar;
    }

    public String getNewCarMsg() {
        switch (newCar) {
            case 1:
                return "新车";
            case 2:
                return "旧车";
            default:
                return "未知";
        }
    }

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }

    public int getTransferCar() {
        return transferCar;
    }

    public void setTransferCar(int transferCar) {
        this.transferCar = transferCar;
    }

    public String getVciSettings() {
        return vciSettings;
    }

    public void setVciSettings(String vciSettings) {
        this.vciSettings = vciSettings;
    }

    public String getIdcardPhoto1() {
        return idcardPhoto1;
    }

    public void setIdcardPhoto1(String idcardPhoto1) {
        this.idcardPhoto1 = idcardPhoto1;
    }

    public String getIdcardPhoto2() {
        return idcardPhoto2;
    }

    public void setIdcardPhoto2(String idcardPhoto2) {
        this.idcardPhoto2 = idcardPhoto2;
    }

    public String getDlPhoto1() {
        return dlPhoto1;
    }

    public void setDlPhoto1(String dlPhoto1) {
        this.dlPhoto1 = dlPhoto1;
    }

    public String getDlPhoto2() {
        return dlPhoto2;
    }

    public void setDlPhoto2(String dlPhoto2) {
        this.dlPhoto2 = dlPhoto2;
    }

    public String getTciNo() {
        return tciNo;
    }

    public void setTciNo(String tciNo) {
        this.tciNo = tciNo;
    }

    public String getVciNo() {
        return vciNo;
    }

    public void setVciNo(String vciNo) {
        this.vciNo = vciNo;
    }

    public String getTciCt() {
        return tciCt;
    }

    public void setTciCt(String tciCt) {
        this.tciCt = tciCt;
    }

    public String getVciCt() {
        return vciCt;
    }

    public void setVciCt(String vciCt) {
        this.vciCt = vciCt;
    }

    public String getTciMoney() {
        return tciMoney;
    }


    public void setTciMoney(String tciMoney) {
        this.tciMoney = tciMoney;
    }

    public String getVciMoney() {
        return vciMoney;
    }

    public void setVciMoney(String vciMoney) {
        this.vciMoney = vciMoney;
    }

    public String getVvtMoney() {
        return vvtMoney;
    }

    public void setVvtMoney(String vvtMoney) {
        this.vvtMoney = vvtMoney;
    }

    public String getTciReturnMoney() {
        return tciReturnMoney;
    }

    public void setTciReturnMoney(String tciReturnMoney) {
        this.tciReturnMoney = tciReturnMoney;
    }

    public String getVciReturnMoney() {
        return vciReturnMoney;
    }

    public void setVciReturnMoney(String vciReturnMoney) {
        this.vciReturnMoney = vciReturnMoney;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getBak() {
        return bak;
    }

    public void setBak(String bak) {
        this.bak = bak;
    }

    public int getConfirmAccept() {
        return confirmAccept;
    }

    public void setConfirmAccept(int confirmAccept) {
        this.confirmAccept = confirmAccept;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getTotalMoney() {
        double vciMoney = Double.parseDouble(getVciMoney());
        double tciMoney = Double.parseDouble(getTciMoney());
        double vvtMoney = Double.parseDouble(getVvtMoney());
        return CommonUtil.getMoneyStr(vciMoney + tciMoney + vvtMoney);
    }

    public String getFeeTotalMoney() {
        double vciReturnMoney = Double.parseDouble(getVciReturnMoney());
        double tciReturnMoney = Double.parseDouble(getTciReturnMoney());
        return CommonUtil.getMoneyStr(vciReturnMoney + tciReturnMoney);
    }

    public boolean isCanPay() {
        return (!CommonUtil.isEmpty(idcardPhoto1) || !CommonUtil.isEmpty(idcardPhoto2)) &&
                (!CommonUtil.isEmpty(dlPhoto1) || !CommonUtil.isEmpty(dlPhoto2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bidId);
        dest.writeInt(tciRb);
        dest.writeInt(vciRb);
        dest.writeString(status);
        dest.writeString(orderNo);
        dest.writeString(updateTime);
        dest.writeString(createTime);
        dest.writeString(payTime);
        dest.writeString(shippingType);
        dest.writeString(shippingCompany);
        dest.writeString(shippingNo);
        dest.writeString(rShippingType);
        dest.writeString(rShippingCompany);
        dest.writeString(rShippingNo);
        dest.writeString(vciDate);
        dest.writeString(tciDate);
        dest.writeString(carOwner);
        dest.writeString(idcardNo);
        dest.writeString(vin);
        dest.writeString(engineNo);
        dest.writeString(plateNo);
        dest.writeString(carRegisterDate);
        dest.writeString(useAge);
        dest.writeInt(newCar);
        dest.writeInt(carType);
        dest.writeInt(transferCar);
        dest.writeString(vciSettings);
        dest.writeString(carBak);
        dest.writeString(outputVolume);
        dest.writeString(curbWeight);
        dest.writeString(seatNum);
        dest.writeString(bak);
        dest.writeString(idcardPhoto1);
        dest.writeString(idcardPhoto2);
        dest.writeString(dlPhoto1);
        dest.writeString(dlPhoto2);
        dest.writeString(tciNo);
        dest.writeString(vciNo);
        dest.writeString(tciCt);
        dest.writeString(vciCt);
        dest.writeString(tciMoney);
        dest.writeString(vciMoney);
        dest.writeString(vvtMoney);
        dest.writeString(tciReturnMoney);
        dest.writeString(vciReturnMoney);
        dest.writeInt(companyId);
        dest.writeInt(provinceId);
        dest.writeInt(cityId);
        dest.writeString(mobilePhone);
        dest.writeInt(reasonStatus);
        dest.writeInt(confirmAccept);
        dest.writeString(confirmTime);
    }
}
