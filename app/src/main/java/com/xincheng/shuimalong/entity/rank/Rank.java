package com.xincheng.shuimalong.entity.rank;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaote1988 on 2017/7/29.
 */

public class Rank {
    private int num;
    @SerializedName("totalmoney")
    private String totalMoney;
    @SerializedName("shoudan")
    private List<Data> receiptList;
    @SerializedName("maidan")
    private List<Data> sellList;

    public Rank() {
    }

    public List<Data> getReceiptList() {
        return receiptList;
    }

    public void setReceiptList(List<Data> receiptList) {
        this.receiptList = receiptList;
    }

    public List<Data> getSellList() {
        return sellList;
    }

    public void setSellList(List<Data> sellList) {
        this.sellList = sellList;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "receiptList=" + receiptList +
                ", sellList=" + sellList +
                '}';
    }

    public static class Data {
        private String money;
        private String num;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("nick_name")
        private String nickName;
        @SerializedName("region_id")
        private int regionId;
        private String avatar;

        public Data() {
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public int getRegionId() {
            return regionId;
        }

        public void setRegionId(int regionId) {
            this.regionId = regionId;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "money='" + money + '\'' +
                    ", num='" + num + '\'' +
                    ", userId=" + userId +
                    ", nickName='" + nickName + '\'' +
                    ", regionId=" + regionId +
                    '}';
        }
    }
}
