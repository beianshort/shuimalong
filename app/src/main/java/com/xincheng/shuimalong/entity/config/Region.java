package com.xincheng.shuimalong.entity.config;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaote on 2017/6/21.
 */

public class Region {
    @SerializedName("city")
    private List<RegionData> cityList;
    @SerializedName("province")
    private List<RegionData> provinceList;

    public Region() {
    }

    public Region(List<RegionData> cityList, List<RegionData> provinceList) {
        this.cityList = cityList;
        this.provinceList = provinceList;
    }

    public List<RegionData> getCityList() {
        return cityList;
    }

    public void setCityList(List<RegionData> cityList) {
        this.cityList = cityList;
    }

    public List<RegionData> getProvinceList() {
        return provinceList;
    }

    public void setProvinceList(List<RegionData> provinceList) {
        this.provinceList = provinceList;
    }

    @Override
    public String toString() {
        return "City{" + "cityList=" + cityList + '}';
    }

}
