package com.xincheng.shuimalong.entity.friend;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by xuhao on 2017/8/10.
 */

public class Commeny implements Parcelable {
    private String id;
    private int bid_id;
    private String bill_user;
    private float accept_star;
    private String accept_price;
    private String accept_issue;
    private long bill_comment_time;
    private String accept_note;
    private String nick_name;
    private String avatar;
    private String ac_user;
    private float buyer_star;
    private String buyer_issue;
    private long ac_comment_time;
    private String buyer_note;

    public Commeny() {

    }

    protected Commeny(Parcel in) {
        id = in.readString();
        bid_id = in.readInt();
        bill_user = in.readString();
        accept_star = in.readFloat();
        accept_price = in.readString();
        accept_issue = in.readString();
        bill_comment_time = in.readLong();
        accept_note = in.readString();
        nick_name = in.readString();
        avatar = in.readString();
        ac_user = in.readString();
        buyer_star = in.readFloat();
        buyer_issue = in.readString();
        ac_comment_time = in.readLong();
        buyer_note = in.readString();
    }

    public static final Creator<Commeny> CREATOR = new Creator<Commeny>() {
        @Override
        public Commeny createFromParcel(Parcel in) {
            return new Commeny(in);
        }

        @Override
        public Commeny[] newArray(int size) {
            return new Commeny[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getBid_id() {
        return bid_id;
    }

    public void setBid_id(int bid_id) {
        this.bid_id = bid_id;
    }

    public String getBill_user() {
        return bill_user;
    }

    public void setBill_user(String bill_user) {
        this.bill_user = bill_user;
    }

    public float getAccept_star() {
        return accept_star;
    }

    public void setAccept_star(float accept_star) {
        this.accept_star = accept_star;
    }

    public String getAccept_price() {
        return accept_price;
    }

    public void setAccept_price(String accept_price) {
        this.accept_price = accept_price;
    }

    public String getAccept_issue() {
        return accept_issue;
    }

    public void setAccept_issue(String accept_issue) {
        this.accept_issue = accept_issue;
    }

    public long getBill_comment_time() {
        return bill_comment_time;
    }

    public void setBill_comment_time(long bill_comment_time) {
        this.bill_comment_time = bill_comment_time;
    }

    public String getAccept_note() {
        return accept_note;
    }

    public void setAccept_note(String accept_note) {
        this.accept_note = accept_note;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAc_user() {
        return ac_user;
    }

    public void setAc_user(String ac_user) {
        this.ac_user = ac_user;
    }

    public float getBuyer_star() {
        return buyer_star;
    }

    public void setBuyer_star(float buyer_star) {
        this.buyer_star = buyer_star;
    }

    public String getBuyer_issue() {
        return buyer_issue;
    }

    public void setBuyer_issue(String buyer_issue) {
        this.buyer_issue = buyer_issue;
    }

    public long getAc_comment_time() {
        return ac_comment_time;
    }

    public void setAc_comment_time(long ac_comment_time) {
        this.ac_comment_time = ac_comment_time;
    }

    public String getBuyer_note() {
        return buyer_note;
    }

    public void setBuyer_note(String buyer_note) {
        this.buyer_note = buyer_note;
    }

    @Override
    public String toString() {
        return "Commeny{" +
                "id='" + id + '\'' +
                ", bid_id=" + bid_id +
                ", bill_user='" + bill_user + '\'' +
                ", accept_star=" + accept_star +
                ", accept_price='" + accept_price + '\'' +
                ", accept_issue='" + accept_issue + '\'' +
                ", bill_comment_time=" + bill_comment_time +
                ", accept_note='" + accept_note + '\'' +
                ", nick_name='" + nick_name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", ac_user='" + ac_user + '\'' +
                ", buyer_star=" + buyer_star +
                ", buyer_issue='" + buyer_issue + '\'' +
                ", ac_comment_time=" + ac_comment_time +
                ", buyer_note='" + buyer_note + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(bid_id);
        dest.writeString(bill_user);
        dest.writeFloat(accept_star);
        dest.writeString(accept_price);
        dest.writeString(accept_issue);
        dest.writeLong(bill_comment_time);
        dest.writeString(accept_note);
        dest.writeString(nick_name);
        dest.writeString(avatar);
        dest.writeString(ac_user);
        dest.writeFloat(buyer_star);
        dest.writeString(buyer_issue);
        dest.writeLong(ac_comment_time);
        dest.writeString(buyer_note);
    }
}
