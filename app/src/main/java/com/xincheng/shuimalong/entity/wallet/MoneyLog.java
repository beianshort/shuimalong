package com.xincheng.shuimalong.entity.wallet;

import com.bumptech.glide.load.model.DataUrlLoader;
import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by xiaote on 2017/7/20.
 */

public class MoneyLog {
    @SerializedName("total_increase")
    private String totalIncrease;
    @SerializedName("total_reduce")
    private String totalReduce;
    @SerializedName("loglist")
    private List<Data> logList;

    public MoneyLog() {

    }

    public String getTotalIncrease() {
        return totalIncrease;
    }

    public void setTotalIncrease(String totalIncrease) {
        this.totalIncrease = totalIncrease;
    }

    public String getTotalReduce() {
        return totalReduce;
    }

    public void setTotalReduce(String totalReduce) {
        this.totalReduce = totalReduce;
    }

    public List<Data> getLogList() {
        return logList;
    }

    public void setLogList(List<Data> logList) {
        this.logList = logList;
    }

    @Override
    public String toString() {
        return "MoneyLog{" +
                "logList=" + logList +
                '}';
    }

    public static class Data {
        @SerializedName("log_id")
        String logId;
        @SerializedName("order_sn")
        private String orderSn;
        private int type;
        private String price;
        @SerializedName("create_time")
        private String createTime;
        private int status;
        @SerializedName("pay_method")
        private int payMethod;
        @SerializedName("all_money")
        private String allMoney;
        @SerializedName("pay_money_from_amount")
        private String payMoneyFromAmount;
        @SerializedName("withdraw_process")
        private int withdrawProcess;
        @SerializedName("withdraw_status")
        private int withdrawStatus;
        private int increase;
        private int reduce;
        private Object process;
        private String bak;

        public Data() {

        }

        public String getLogId() {
            return logId;
        }

        public void setLogId(String logId) {
            this.logId = logId;
        }

        public String getOrderSn() {
            return orderSn;
        }

        public void setOrderSn(String orderSn) {
            this.orderSn = orderSn;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getPayMethod() {
            return payMethod;
        }

        public void setPayMethod(int payMethod) {
            this.payMethod = payMethod;
        }

        public String getAllMoney() {
            return allMoney;
        }

        public void setAllMoney(String allMoney) {
            this.allMoney = allMoney;
        }

        public String getPayMoneyFromAmount() {
            return payMoneyFromAmount;
        }

        public void setPayMoneyFromAmount(String payMoneyFromAmount) {
            this.payMoneyFromAmount = payMoneyFromAmount;
        }

        public Object getProcess() {
            return process;
        }

        public void setProcess(Object process) {
            this.process = process;
        }

        public String getBak() {
            return bak;
        }

        public void setBak(String bak) {
            this.bak = bak;
        }

        public String getDateTime() {
            if (CommonUtil.isEmpty(createTime)) {
                return "";
            }
            long time = Long.parseLong(createTime) * 1000;
            long todayTime = DateUtil.getTodayZeroTime();
            if (time >= todayTime) {
                return "今天" + "\n" + DateUtil.getHourMinute(time);
            } else {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(time);
                return DateUtil.getWeekDay(c) + "\n" + DateUtil.getMonthDay(time);
            }
        }

        public String getTypeMsg() {
            String str = "";
            switch (type) {
                case 1:
                    str = "保证押金";
                    break;
                case 2:
                    str = "充值";
                    break;
                case 3:
                    str = "交易酬金";
                    break;
                case 4:
                    str = "提现";
                    break;
                case 5:
                    str = "交易净费";
                    break;
                case 6:
                    str = "交易奖励";
                    break;
                default:
                    str = "其他";
                    break;
            }
            return str + allMoney + "元";
        }

        public String getStatusMsg() {
            switch (status) {
                case -1:
                    return "操作失败";
                case -2:
                    return "放弃";
                case 0:
                    return "未完成";
                case 1:
                    return "操作中";
                case 2:
                    return "已完成";
            }
            return "";
        }

        public int getWithdrawProcess() {
            return withdrawProcess;
        }

        public void setWithdrawProcess(int withdrawProcess) {
            this.withdrawProcess = withdrawProcess;
        }

        public int getWithdrawStatus() {
            return withdrawStatus;
        }

        public void setWithdrawStatus(int withdrawStatus) {
            this.withdrawStatus = withdrawStatus;
        }

        public int getIncrease() {
            return increase;
        }

        public void setIncrease(int increase) {
            this.increase = increase;
        }

        public int getReduce() {
            return reduce;
        }

        public void setReduce(int reduce) {
            this.reduce = reduce;
        }

        public String getReduceMsg() {
            if (increase == 1 && reduce == 0) {
                return "收";
            }
            if (increase == 0 && reduce == 1) {
                return "支";
            }
            return "";
        }

        public int getReduceStatus() {
            if (increase == 1 && reduce == 0) {
                return 1;
            }
            if (increase == 0 && reduce == 1) {
                return 2;
            }
            return 0;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "orderSn='" + orderSn + '\'' +
                    ", type='" + type + '\'' +
                    ", price='" + price + '\'' +
                    ", createTime=" + createTime +
                    ", status=" + status +
                    ", payMethod=" + payMethod +
                    ", allMoney='" + allMoney + '\'' +
                    ", payMoneyFromAmount='" + payMoneyFromAmount + '\'' +
                    ", process=" + process +
                    ", bak='" + bak + '\'' +
                    '}';
        }

    }
}
