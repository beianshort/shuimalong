package com.xincheng.shuimalong.entity.address;

/**
 * Created by xiaote on 2017/6/30.
 */

public enum AddressStatus {
    GET,SET_DEFAULT, DELETE;
}
