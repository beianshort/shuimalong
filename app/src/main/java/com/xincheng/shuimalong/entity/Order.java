package com.xincheng.shuimalong.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.OrderConfig;

import java.util.List;

/**
 * Created by xiaote on 2017/6/27.
 * 收卖单方订单
 */

public class Order {
    @SerializedName("list")
    private List<Data> orderList;

    public List<Data> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Data> orderList) {
        this.orderList = orderList;
    }

    public static class Data implements Parcelable {

        private int id;
        @SerializedName("bid_id")
        private int bidId;
        @SerializedName("ac_id")
        private int acId;
        @SerializedName("bill_id")
        private int billId;
        @SerializedName("tci_money")
        private String tciMoney;
        @SerializedName("vci_money")
        private String vciMoney;
        @SerializedName("vvt_money")
        private String vvtMoney;
        @SerializedName("update_time")
        private long updateTime;
        private int status;
        @SerializedName("file_status")
        private int fileStatus;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("nick_name")
        private String nickName;
        @SerializedName("real_name")
        private String realName;
        private String mobile;
        private String avatar;
        @SerializedName("plate_no")
        private String plateNo;
        @SerializedName("car_owner")
        private String carOwner;
        @SerializedName("car_register_date")
        private String carRegisterDate;
        @SerializedName("vci_date")
        private String vciDate;
        @SerializedName("province")
        private int provinceId;
        @SerializedName("city")
        private int cityId;
        @SerializedName("company")
        private int companyId;
        @SerializedName("car_type")
        private int carTypeId;
        @SerializedName("new_car")
        private int newCarId;
        @SerializedName("tci_rb")
        private int tciRb;
        @SerializedName("vci_rb")
        private int vciRb;
        @SerializedName("car_weight")
        private int carWeightId;
        @SerializedName("seat_num")
        private int seatNum;
        private String bak;
        @SerializedName("car_bak")
        private String carBak;
        @SerializedName("create_time")
        private long createTime;
        @SerializedName("refuse_status")
        private int refuseStatus;
        @SerializedName("refuse_reason")
        private int refuseReason;
        @SerializedName("refuse_bak")
        private String refuseBak;
        @SerializedName("idcard_photo1")
        private String idCardPhoto1;
        @SerializedName("idcard_photo2")
        private String idCardPhoto2;
        @SerializedName("dl_photo1")
        private String dlPhoto1;
        @SerializedName("dl_photo2")
        private String dlPhoto2;
        @SerializedName("ac_comment")
        private int acComment;
        @SerializedName("bill_comment")
        private int billComment;
        private int disabled;

        public Data() {
        }

        protected Data(Parcel in) {
            id = in.readInt();
            bidId = in.readInt();
            acId = in.readInt();
            billId = in.readInt();
            tciMoney = in.readString();
            vciMoney = in.readString();
            vvtMoney = in.readString();
            updateTime = in.readLong();
            status = in.readInt();
            fileStatus = in.readInt();
            userId = in.readInt();
            userName = in.readString();
            nickName = in.readString();
            realName = in.readString();
            mobile = in.readString();
            avatar = in.readString();
            plateNo = in.readString();
            carOwner = in.readString();
            carRegisterDate = in.readString();
            vciDate = in.readString();
            provinceId = in.readInt();
            cityId = in.readInt();
            companyId = in.readInt();
            carTypeId = in.readInt();
            newCarId = in.readInt();
            tciRb = in.readInt();
            vciRb = in.readInt();
            carWeightId = in.readInt();
            seatNum = in.readInt();
            bak = in.readString();
            carBak = in.readString();
            createTime = in.readLong();
            refuseStatus = in.readInt();
            refuseReason = in.readInt();
            refuseBak = in.readString();
            idCardPhoto1 = in.readString();
            idCardPhoto2 = in.readString();
            dlPhoto1 = in.readString();
            dlPhoto2 = in.readString();
            acComment = in.readInt();
            billComment = in.readInt();
            disabled = in.readInt();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public int getRefuseStatus() {
            return refuseStatus;
        }

        public void setRefuseStatus(int refuseStatus) {
            this.refuseStatus = refuseStatus;
        }

        public int getRefuseReason() {
            return refuseReason;
        }

        public void setRefuseReason(int refuseReason) {
            this.refuseReason = refuseReason;
        }

        public String getRefuseBak() {
            return refuseBak;
        }

        public void setRefuseBak(String refuseBak) {
            this.refuseBak = refuseBak;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getBidId() {
            return bidId;
        }

        public void setBidId(int bidId) {
            this.bidId = bidId;
        }

        public int getAcId() {
            return acId;
        }

        public void setAcId(int acId) {
            this.acId = acId;
        }

        public int getBillId() {
            return billId;
        }

        public void setBillId(int billId) {
            this.billId = billId;
        }

        public String getTciMoney() {
            return tciMoney;
        }

        public void setTciMoney(String tciMoney) {
            this.tciMoney = tciMoney;
        }

        public String getVciMoney() {
            return vciMoney;
        }

        public void setVciMoney(String vciMoney) {
            this.vciMoney = vciMoney;
        }

        public String getVvtMoney() {
            return vvtMoney;
        }

        public void setVvtMoney(String vvtMoney) {
            this.vvtMoney = vvtMoney;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMsg(int orderConfig) {
            switch (status) {
                case 0:
                    switch (orderConfig) {
                        case OrderConfig.RECEIPT:
                            return "待报价";
                        case OrderConfig.SELL:
                            if(refuseStatus == 1) {
                                return "已驳回";
                            } else {
                                return "待报价";
                            }
                    }
                case 1:
                    return "待付款";
                case 2:
                    switch (orderConfig) {
                        case OrderConfig.RECEIPT:
                            switch (fileStatus) {
                                case 0:
                                    return "待出单";
                                case 1:
                                    return "待签收";
                                case 2:
                                    return "待回单";
                                case 3:
                                    return "待签收";
                                case 4:
                                    if(acComment == 1) {
                                        return "已完成";
                                    }
                                    return "待评价";
                            }
                            break;
                        case OrderConfig.SELL:
                            switch (fileStatus) {
                                case 0:
                                    return "待出单";
                                case 1:
                                    return "待签收";
                                case 2:
                                    return "待回单";
                                case 3:
                                    return "待确认";
                                case 4:
                                    if(billComment == 1) {
                                        return "已完成";
                                    }
                                    return "待评价";
                            }
                    }
            }
            return "";
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getFileStatus() {
            return fileStatus;
        }

        public void setFileStatus(int fileStatus) {
            this.fileStatus = fileStatus;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public String getCarOwner() {
            return carOwner;
        }

        public void setCarOwner(String carOwner) {
            this.carOwner = carOwner;
        }

        public String getCarRegisterDate() {
            return carRegisterDate;
        }

        public void setCarRegisterDate(String carRegisterDate) {
            this.carRegisterDate = carRegisterDate;
        }

        public String getVciDate() {
            return vciDate;
        }

        public void setVciDate(String vciDate) {
            this.vciDate = vciDate;
        }

        public int getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(int provinceId) {
            this.provinceId = provinceId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        public int getCarTypeId() {
            return carTypeId;
        }

        public void setCarTypeId(int carTypeId) {
            this.carTypeId = carTypeId;
        }

        public int getNewCarId() {
            return newCarId;
        }

        public void setNewCarId(int newCarId) {
            this.newCarId = newCarId;
        }

        public int getTciRb() {
            return tciRb;
        }

        public void setTciRb(int tciRb) {
            this.tciRb = tciRb;
        }

        public int getVciRb() {
            return vciRb;
        }

        public void setVciRb(int vciRb) {
            this.vciRb = vciRb;
        }

        public int getCarWeightId() {
            return carWeightId;
        }

        public void setCarWeightId(int carWeightId) {
            this.carWeightId = carWeightId;
        }

        public int getSeatNum() {
            return seatNum;
        }

        public void setSeatNum(int seatNum) {
            this.seatNum = seatNum;
        }

        public String getBak() {
            return bak;
        }

        public void setBak(String bak) {
            this.bak = bak;
        }

        public String getCarBak() {
            return CommonUtil.isEmpty(carBak) ? "车型未知" : carBak;
        }

        public void setCarBak(String carBak) {
            this.carBak = carBak;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getTakeEffectDate() {
            return vciDate + " 生效";
        }

        public String getIdCardPhoto1() {
            return idCardPhoto1;
        }

        public void setIdCardPhoto1(String idCardPhoto1) {
            this.idCardPhoto1 = idCardPhoto1;
        }

        public String getIdCardPhoto2() {
            return idCardPhoto2;
        }

        public void setIdCardPhoto2(String idCardPhoto2) {
            this.idCardPhoto2 = idCardPhoto2;
        }

        public String getDlPhoto1() {
            return dlPhoto1;
        }

        public void setDlPhoto1(String dlPhoto1) {
            this.dlPhoto1 = dlPhoto1;
        }

        public String getDlPhoto2() {
            return dlPhoto2;
        }

        public void setDlPhoto2(String dlPhoto2) {
            this.dlPhoto2 = dlPhoto2;
        }

        public int getAcComment() {
            return acComment;
        }

        public void setAcComment(int acComment) {
            this.acComment = acComment;
        }

        public int getBillComment() {
            return billComment;
        }

        public void setBillComment(int billComment) {
            this.billComment = billComment;
        }

        public int getDisabled() {
            return disabled;
        }

        public void setDisabled(int disabled) {
            this.disabled = disabled;
        }

        /**
         * @return 投保总金额
         */
        public String getTotalMoney() {
            double vciMoney = Double.parseDouble(getVciMoney());
            double tciMoney = Double.parseDouble(getTciMoney());
            double vvtMoney = Double.parseDouble(getVvtMoney());
            return CommonUtil.getMoneyStr(vciMoney + tciMoney + vvtMoney);
        }

        /**
         * 酬金
         * @return
         */
        public String getPromotionMoney() {
            double vciMoney = Double.parseDouble(getVciMoney());
            double tciMoney = Double.parseDouble(getTciMoney());
            double promotionMoney = vciMoney * getVciRb() / 100 + tciMoney * getTciRb() / 100;
            return CommonUtil.getMoneyStr(promotionMoney);
        }

        public boolean isCanPay() {
            return (!CommonUtil.isEmpty(idCardPhoto1) || !CommonUtil.isEmpty(idCardPhoto2)) &&
                    (!CommonUtil.isEmpty(dlPhoto1) || !CommonUtil.isEmpty(dlPhoto2));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeInt(bidId);
            parcel.writeInt(acId);
            parcel.writeInt(billId);
            parcel.writeString(tciMoney);
            parcel.writeString(vciMoney);
            parcel.writeString(vvtMoney);
            parcel.writeLong(updateTime);
            parcel.writeInt(status);
            parcel.writeInt(fileStatus);
            parcel.writeInt(userId);
            parcel.writeString(userName);
            parcel.writeString(nickName);
            parcel.writeString(realName);
            parcel.writeString(mobile);
            parcel.writeString(avatar);
            parcel.writeString(plateNo);
            parcel.writeString(carOwner);
            parcel.writeString(carRegisterDate);
            parcel.writeString(vciDate);
            parcel.writeInt(provinceId);
            parcel.writeInt(cityId);
            parcel.writeInt(companyId);
            parcel.writeInt(carTypeId);
            parcel.writeInt(newCarId);
            parcel.writeInt(tciRb);
            parcel.writeInt(vciRb);
            parcel.writeInt(carWeightId);
            parcel.writeInt(seatNum);
            parcel.writeString(bak);
            parcel.writeString(carBak);
            parcel.writeLong(createTime);
            parcel.writeInt(refuseStatus);
            parcel.writeInt(refuseReason);
            parcel.writeString(refuseBak);
            parcel.writeString(idCardPhoto1);
            parcel.writeString(idCardPhoto2);
            parcel.writeString(dlPhoto1);
            parcel.writeString(dlPhoto2);
            parcel.writeInt(acComment);
            parcel.writeInt(billComment);
            parcel.writeInt(disabled);
        }
    }
}
