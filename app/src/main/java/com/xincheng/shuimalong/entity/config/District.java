package com.xincheng.shuimalong.entity.config;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.widget.pickerview.model.IPickerViewData;

import java.util.List;

/**
 * Created by xiaote on 2017/6/29.
 * 区域
 */

public class District implements IPickerViewData {
    @SerializedName("menu_id")
    private int menuId;
    private String name;

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPickerViewText() {
        return name;
    }
}
