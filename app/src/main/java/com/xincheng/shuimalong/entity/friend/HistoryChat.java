package com.xincheng.shuimalong.entity.friend;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by 许浩 on 2017/7/22.
 */
@Entity
public class HistoryChat {
    @Id
    private String tergid;
    private String nick_name;
    private String face;
    private long time;
    private String lasstmessage;
    private int unbumber;

    @Generated(hash = 926782203)
    public HistoryChat(String tergid, String nick_name, String face, long time,
            String lasstmessage, int unbumber) {
        this.tergid = tergid;
        this.nick_name = nick_name;
        this.face = face;
        this.time = time;
        this.lasstmessage = lasstmessage;
        this.unbumber = unbumber;
    }
    @Generated(hash = 745125694)
    public HistoryChat() {
    }

    public String getNick_name() {
        return this.nick_name;
    }
    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }
    public String getFace() {
        return this.face;
    }
    public void setFace(String face) {
        this.face = face;
    }
    public String getTergid() {
        return this.tergid;
    }
    public void setTergid(String tergid) {
        this.tergid = tergid;
    }
    public long getTime() {
        return this.time;
    }
    public void setTime(long time) {
        this.time = time;
    }
    public String getLasstmessage() {
        return this.lasstmessage;
    }
    public void setLasstmessage(String lasstmessage) {
        this.lasstmessage = lasstmessage;
    }
    public int getUnbumber() {
        return this.unbumber;
    }
    public void setUnbumber(int unbumber) {
        this.unbumber = unbumber;
    }
}
