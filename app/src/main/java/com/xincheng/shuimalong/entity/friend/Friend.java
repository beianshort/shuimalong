package com.xincheng.shuimalong.entity.friend;

import com.xincheng.library.base.BaseItemData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 许浩 on 2017/6/29.
 */

public class Friend extends BaseItemData implements Serializable {
        private String group_name;
        private String group_id;
        private int checked;
        private List<Data> list;
        private List<Friend>groupList;

    public List<Friend> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Friend> groupList) {
        this.groupList = groupList;
    }

    @Override
        public String toString() {
            return "Friend{" +
                    "groupName='" + group_name + '\'' +
                    ", groupId='" + group_id + '\'' +
                    ", checked=" + checked +
                    ", friends=" + list +
                    '}';
        }
        public boolean getChecked() {
            return checked == 1;
        }

        public void setChecked(int checked) {
            this.checked = checked;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }

        public List<Data> getList() {
            return list;
        }

        public void setList(List<Data> list) {
            this.list = list;
        }

        public List<Data> getFriends() {
            return list;
        }

        public void setFriends(List<Data> friends) {
            this.list = friends;
        }

    public static class Data extends BaseItemData implements Serializable {
        private String nick_name;
        private String location;
        private String face;
        private String dealNum;
        private String user_id;
        private String frend_id;
        private String id;

        public String getFrend_id() {
            return frend_id;
        }

        public void setFrend_id(String frend_id) {
            this.frend_id = frend_id;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getFace() {
            return face;
        }

        public void setFace(String face) {
            this.face = face;
        }

        public String getDealNum() {
            return dealNum;
        }

        public void setDealNum(String dealNum) {
            this.dealNum = dealNum;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "nick_name='" + nick_name + '\'' +
                    ", location='" + location + '\'' +
                    ", face='" + face + '\'' +
                    ", dealNum='" + dealNum + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", frend_id='" + frend_id + '\'' +
                    ", id='" + id + '\'' +
                    '}';
        }
    }
}
