package com.xincheng.shuimalong.entity.config;

/**
 * Created by xiaote on 2017/6/21.
 */

public class ConfigName {
    /**
     * expressCompanys : 快递公司
     * expressType : 保单办理方式
     * companyList : 保险公司
     * vciTypes : 商业险类型
     * carType : 车辆类型
     * truckWeightTypes : 货车重量选项
     * newCarType : 车辆新旧选项
     * acceptPrice : 30分钟内报价
     * acceptIssue : 当天出单
     * buyerIssue : 两天内回寄订单
     * hotCitys : 热门城市
     * "orderstatus": "订单状态",
     * "usage": "是否营运",
     * "fileStatus": "文件签收状态"
     *  "refuseReason": "退单原因"
     */

    private String expressCompanys;
    private String expressType;
    private String companyList;
    private String vciTypes;
    private String carType;
    private String truckWeightTypes;
    private String newCarType;
    private String acceptPrice;
    private String acceptIssue;
    private String buyerIssue;
    private String hotCitys;
    private String orderStatus;
    private String usage;
    private String fileStatus;
    private String refuseReason;

    public ConfigName() {
    }

    public String getExpressCompanys() {
        return expressCompanys;
    }

    public void setExpressCompanys(String expressCompanys) {
        this.expressCompanys = expressCompanys;
    }

    public String getExpressType() {
        return expressType;
    }

    public void setExpressType(String expressType) {
        this.expressType = expressType;
    }

    public String getCompanyList() {
        return companyList;
    }

    public void setCompanyList(String companyList) {
        this.companyList = companyList;
    }

    public String getVciTypes() {
        return vciTypes;
    }

    public void setVciTypes(String vciTypes) {
        this.vciTypes = vciTypes;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getTruckWeightTypes() {
        return truckWeightTypes;
    }

    public void setTruckWeightTypes(String truckWeightTypes) {
        this.truckWeightTypes = truckWeightTypes;
    }

    public String getNewCarType() {
        return newCarType;
    }

    public void setNewCarType(String newCarType) {
        this.newCarType = newCarType;
    }

    public String getAcceptPrice() {
        return acceptPrice;
    }

    public void setAcceptPrice(String acceptPrice) {
        this.acceptPrice = acceptPrice;
    }

    public String getAcceptIssue() {
        return acceptIssue;
    }

    public void setAcceptIssue(String acceptIssue) {
        this.acceptIssue = acceptIssue;
    }

    public String getBuyerIssue() {
        return buyerIssue;
    }

    public void setBuyerIssue(String buyerIssue) {
        this.buyerIssue = buyerIssue;
    }

    public String getHotCitys() {
        return hotCitys;
    }

    public void setHotCitys(String hotCitys) {
        this.hotCitys = hotCitys;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }

    @Override
    public String toString() {
        return "ConfigName{" +
                "expressCompanys='" + expressCompanys + '\'' +
                ", expressType='" + expressType + '\'' +
                ", companyList='" + companyList + '\'' +
                ", vciTypes='" + vciTypes + '\'' +
                ", carType='" + carType + '\'' +
                ", truckWeightTypes='" + truckWeightTypes + '\'' +
                ", newCarType='" + newCarType + '\'' +
                ", acceptPrice='" + acceptPrice + '\'' +
                ", acceptIssue='" + acceptIssue + '\'' +
                ", buyerIssue='" + buyerIssue + '\'' +
                ", hotCitys='" + hotCitys + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", usage='" + usage + '\'' +
                ", fileStatus='" + fileStatus + '\'' +
                ", refuseReason='" + refuseReason + '\'' +
                '}';
    }
}
