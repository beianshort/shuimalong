package com.xincheng.shuimalong.entity.friend;

/**
 * Created by xuhao on 2017/7/17.
 */

public class ImToken {
    private String userId;
    private String token;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "ImToken{" +
                " userId='" + userId + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
