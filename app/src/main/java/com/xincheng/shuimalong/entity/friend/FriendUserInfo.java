package com.xincheng.shuimalong.entity.friend;

import com.xincheng.shuimalong.entity.user.InsuranceCompany;

import java.util.List;

/**
 * Created by xuhao on 2017/8/10.
 * "month_totalMoney": 4100,
 * "month_tciMoney": 2500,
 * "month_vciMoney": 1000,
 */

public class FriendUserInfo {
    private String user_id;
    private String nick_name;
    private String real_name;
    private String user_name;
    private String avatar;
    private int user_cert;
    private String register_time;
    private String num;
    private String month_num;
    private String month_totalMoney;
    private String month_vciMoney;
    private String month_tciMoney;
    private float star;
    private String accept_price;
    private String accept_issue;
    private float buyer_star;
    private String buyer_issue;
    private int small_car_num;
    private int big_car_num;
    private List<InsuranceCompany> companyNum;
    private List<Commeny> commenylist;
    private int isFriend;

    public boolean getIsFriend() {
        return isFriend == 1;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public String getMonth_vciMoney() {
        return month_vciMoney;
    }

    public void setMonth_vciMoney(String month_vciMoney) {
        this.month_vciMoney = month_vciMoney;
    }

    public String getMonth_tciMoney() {
        return month_tciMoney;
    }

    public void setMonth_tciMoney(String month_tciMoney) {
        this.month_tciMoney = month_tciMoney;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_cert() {
        return user_cert == 1 ? "已认证" : "未认证";
    }

    public void setUser_cert(int user_cert) {
        this.user_cert = user_cert;
    }

    public String getRegister_time() {
        return register_time;
    }

    public void setRegister_time(String register_time) {
        this.register_time = register_time;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getMonth_num() {
        return month_num;
    }

    public void setMonth_num(String month_num) {
        this.month_num = month_num;
    }

    public String getMonth_totalMoney() {
        return month_totalMoney;
    }

    public void setMonth_totalMoney(String month_totalMoney) {
        this.month_totalMoney = month_totalMoney;
    }

    public float getStar() {
        return star;
    }

    public void setStar(float star) {
        this.star = star;
    }

    public String getAccept_price() {
        return accept_price;
    }

    public void setAccept_price(String accept_price) {
        this.accept_price = accept_price;
    }

    public String getAccept_issue() {
        return accept_issue;
    }

    public void setAccept_issue(String accept_issue) {
        this.accept_issue = accept_issue;
    }

    public float getBuyer_star() {
        return buyer_star;
    }

    public void setBuyer_star(float buyer_star) {
        this.buyer_star = buyer_star;
    }

    public String getBuyer_issue() {
        return buyer_issue;
    }

    public void setBuyer_issue(String buyer_issue) {
        this.buyer_issue = buyer_issue;
    }

    public int getSmall_car_num() {
        return small_car_num;
    }

    public void setSmall_car_num(int small_car_num) {
        this.small_car_num = small_car_num;
    }

    public int getBig_car_num() {
        return big_car_num;
    }

    public void setBig_car_num(int big_car_num) {
        this.big_car_num = big_car_num;
    }

    public List<InsuranceCompany> getCompanyNum() {
        return companyNum;
    }

    public void setCompanyNum(List<InsuranceCompany> companyNum) {
        this.companyNum = companyNum;
    }

    public List<Commeny> getCommenylist() {
        return commenylist;
    }

    public void setCommenylist(List<Commeny> commenylist) {
        this.commenylist = commenylist;
    }
}
