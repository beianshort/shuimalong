package com.xincheng.shuimalong.entity.friend;

import java.util.List;

/**
 * Created by xuhao on 2017/7/19.
 */

public class ApplyFriendBean {
    private List<data>list;

    public List<data> getList() {
        return list;
    }

    public void setList(List<data> list) {
        this.list = list;
    }

    public static class data{
        private int id;
        private int user_id;
        private int friend_user_id;
        private int status;
        private long register_time;
        private String avatar;
        private String nick_name;
        private int  is_friend;

        public int getIs_friend() {
            return is_friend;
        }

        public void setIs_friend(int is_friend) {
            this.is_friend = is_friend;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getFriend_user_id() {
            return friend_user_id;
        }

        public void setFriend_user_id(int friend_user_id) {
            this.friend_user_id = friend_user_id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getRegister_time() {
            return register_time;
        }

        public void setRegister_time(long register_time) {
            this.register_time = register_time;
        }
    }
}
