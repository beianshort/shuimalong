package com.xincheng.shuimalong.entity.friend;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.xincheng.shuimalong.util.GsonUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * Created by 许浩 on 2017/7/1.
 */
@MessageTag(value = "app:custom", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatMessage extends MessageContent  {
    private int msg_type;//1：发送消息 0接受消息
    private String from;//发送人ID
    private String to;//接受人ID
    private String nickname;//发送人昵称
    private String face;//发送人头像
    private String content;//消息
    private long time;//发送时间戳
    private int messageid;
    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {

        @Override
        public ChatMessage createFromParcel(Parcel source) {
            return new ChatMessage(source);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    public ChatMessage(){

    }

    public int getMessageid() {
        return messageid;
    }

    public void setMessageid(int messageid) {
        this.messageid = messageid;
    }

    public boolean getMsg_type() {
        return msg_type==1;
    }

    public void setMsg_type(int msg_type) {
        this.msg_type = msg_type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public byte[] encode() {
        try {
            return new Gson().toJson(this).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ChatMessage(Parcel in){
        nickname=ParcelUtils.readFromParcel(in);
        face=ParcelUtils.readFromParcel(in);
        content=ParcelUtils.readFromParcel(in);
        msg_type=ParcelUtils.readIntFromParcel(in);
        from=ParcelUtils.readFromParcel(in);
        to=ParcelUtils.readFromParcel(in);
        time=ParcelUtils.readLongFromParcel(in);
        messageid=ParcelUtils.readIntFromParcel(in);
    }
    public ChatMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.has("msg_type"))
                msg_type = jsonObj.optInt("msg_type");
            if (jsonObj.has("from"))
                from = jsonObj.optString("from");
            if (jsonObj.has("to"))
                to = jsonObj.optString("to");
            if (jsonObj.has("nickname"))
                nickname = jsonObj.optString("nickname");
            if (jsonObj.has("face"))
                face = jsonObj.optString("face");
            if (jsonObj.has("content"))
                content = jsonObj.optString("content");
            if (jsonObj.has("time"))
                time = jsonObj.optLong("time");
            if(jsonObj.has("messageid")){
                messageid=jsonObj.optInt("messageid");
            }
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, nickname);
        ParcelUtils.writeToParcel(dest, face);
        ParcelUtils.writeToParcel(dest, content);
        ParcelUtils.writeToParcel(dest, msg_type);
        ParcelUtils.writeToParcel(dest, from);
        ParcelUtils.writeToParcel(dest, to);
        ParcelUtils.writeToParcel(dest, time);
        ParcelUtils.writeToParcel(dest, messageid);
    }

    @Override
    public String toString() {
        return "{" +
                "msg_type=" + msg_type +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", nickname='" + nickname + '\'' +
                ", face='" + face + '\'' +
                ", content='" + content + '\'' +
                ", time=" + time +
                ", messageid=" + messageid +
                '}';
    }
}
