package com.xincheng.shuimalong.entity;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by xuhao on 2017/6/12.
 */

public class BaseEntity implements Serializable {
    private int code;
    private String msg;
    private Object data;

    public boolean isSuccess() {
        return code == 1;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return msg;
    }

    public void setMessage(String message) {
        this.msg = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}