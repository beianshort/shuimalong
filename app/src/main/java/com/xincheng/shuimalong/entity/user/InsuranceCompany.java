package com.xincheng.shuimalong.entity.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xiaote on 2017/8/3.
 */

public class InsuranceCompany implements Parcelable {
    private int id;
    private int num;
    private String company;

    protected InsuranceCompany(Parcel in) {
        id = in.readInt();
        num = in.readInt();
        company = in.readString();
    }

    public static final Creator<InsuranceCompany> CREATOR = new Creator<InsuranceCompany>() {
        @Override
        public InsuranceCompany createFromParcel(Parcel in) {
            return new InsuranceCompany(in);
        }

        @Override
        public InsuranceCompany[] newArray(int size) {
            return new InsuranceCompany[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(num);
        parcel.writeString(company);
    }

    @Override
    public String toString() {
        return "InsuranceCompany{" +
                "id=" + id +
                ", num=" + num +
                ", company='" + company + '\'' +
                '}';
    }
}
