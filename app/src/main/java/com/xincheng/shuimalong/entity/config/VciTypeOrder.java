package com.xincheng.shuimalong.entity.config;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;

import java.util.List;

/**
 * Created by xiaote on 2017/7/19.
 */

public class VciTypeOrder {
    private int id;
    private String title;
    private String name;
    private boolean ndis;
    private String option = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return CommonUtil.isEmpty(title) ? name : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNdis() {
        return ndis;
    }

    public void setNdis(boolean ndis) {
        this.ndis = ndis;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "VciTypeOrder{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", ndis=" + ndis +
                ", option='" + option + '\'' +
                '}';
    }
}
