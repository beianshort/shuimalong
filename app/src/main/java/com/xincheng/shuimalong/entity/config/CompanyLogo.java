package com.xincheng.shuimalong.entity.config;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xiaote1988 on 2017/8/20.
 * 保险公司LOGO类
 */

public class CompanyLogo implements Parcelable {
    private int id;
    private String logo;

    public static final Creator<CompanyLogo> CREATOR = new Creator<CompanyLogo>() {
        @Override
        public CompanyLogo createFromParcel(Parcel in) {
            return new CompanyLogo(in);
        }

        @Override
        public CompanyLogo[] newArray(int size) {
            return new CompanyLogo[size];
        }
    };

    public CompanyLogo() {

    }

    protected CompanyLogo(Parcel in) {
        id = in.readInt();
        logo = in.readString();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(logo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "CompanyLogo{" +
                "id=" + id +
                ", logo='" + logo + '\'' +
                '}';
    }
}
