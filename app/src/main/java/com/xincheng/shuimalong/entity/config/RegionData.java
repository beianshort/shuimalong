package com.xincheng.shuimalong.entity.config;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.widget.pickerview.model.IPickerViewData;

/**
 * Created by xiaote on 2017/6/21.
 */

public class RegionData implements Parcelable ,IPickerViewData {
    private int id;
    private String name;
    private String letter;
    @SerializedName("parent_id")
    private int parentId;

    public static final Creator<RegionData> CREATOR = new Creator<RegionData>() {
        @Override
        public RegionData createFromParcel(Parcel in) {
            return new RegionData(in);
        }

        @Override
        public RegionData[] newArray(int size) {
            return new RegionData[size];
        }
    };

    public RegionData() {
    }

    public RegionData(int id, String name, String letter, int parentId) {
        this.id = id;
        this.name = name;
        this.letter = letter;
        this.parentId = parentId;
    }

    protected RegionData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        letter = in.readString();
        parentId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(letter);
        dest.writeInt(parentId);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", letter='" + letter + '\'' +
                ", parentId='" + parentId + '\'' +
                '}';
    }

    @Override
    public String getPickerViewText() {
        return name;
    }
}
