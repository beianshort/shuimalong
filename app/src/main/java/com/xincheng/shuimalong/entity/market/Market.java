package com.xincheng.shuimalong.entity.market;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaote on 2017/6/26.
 */

public class Market {
    @SerializedName("list")
    private List<Data> marketList;

    public Market() {
    }

    public Market(List<Data> marketList) {
        this.marketList = marketList;
    }

    public List<Data> getMarketList() {
        return marketList;
    }

    public void setMarketList(List<Data> marketList) {
        this.marketList = marketList;
    }

    @Override
    public String toString() {
        return "Market{" +
                "marketList=" + marketList +
                '}';
    }

    public static class Data implements Parcelable {
        @SerializedName("ac_id")
        private int acId;
        @SerializedName("province")
        private int provinceId;
        @SerializedName("city")
        private int cityId;
        @SerializedName("company")
        private int companyId;
        @SerializedName("car_type")
        private int carTypeId;
        @SerializedName("new_car")
        private int newCarId;
        @SerializedName("tci_rb")
        private int tciRb;
        @SerializedName("vci_rb")
        private int vciRb;
        @SerializedName("long_term")
        private int longTerm;
        @SerializedName("start_date")
        private String startDate;
        @SerializedName("end_date")
        private String endDate;
        @SerializedName("car_weight")
        private int carWeight;
        @SerializedName("seat_num")
        private int seatNum;
        private String bak;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("create_time")
        private long createTime;
        @SerializedName("update_time")
        private long updateTime;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("nick_name")
        private String nickName;
        private String avatar;
        @SerializedName("real_name")
        private String realName;
        @SerializedName("useage")
        private int useAge;
        @SerializedName("bid_num")
        private int bidNum;
        @SerializedName("month_num")
        private int monthNum;

        public Data() {

        }

        public Data(int acId, int provinceId, int cityId, int companyId, int carTypeId, int newCarId,
                    int tciRb, int vciRb, int longTerm, String startDate, String endDate, int carWeight,
                    int seatNum, String bak, int userId, long createTime, long updateTime, String userName,
                    String nickName, String avatar, String realName, int useAge, int bidNum, int monthNum) {
            this.acId = acId;
            this.provinceId = provinceId;
            this.cityId = cityId;
            this.companyId = companyId;
            this.carTypeId = carTypeId;
            this.newCarId = newCarId;
            this.tciRb = tciRb;
            this.vciRb = vciRb;
            this.longTerm = longTerm;
            this.startDate = startDate;
            this.endDate = endDate;
            this.carWeight = carWeight;
            this.seatNum = seatNum;
            this.bak = bak;
            this.userId = userId;
            this.createTime = createTime;
            this.updateTime = updateTime;
            this.userName = userName;
            this.nickName = nickName;
            this.avatar = avatar;
            this.realName = realName;
            this.useAge = useAge;
            this.bidNum = bidNum;
            this.monthNum = monthNum;
        }

        protected Data(Parcel in) {
            acId = in.readInt();
            provinceId = in.readInt();
            cityId = in.readInt();
            companyId = in.readInt();
            carTypeId = in.readInt();
            newCarId = in.readInt();
            tciRb = in.readInt();
            vciRb = in.readInt();
            longTerm = in.readInt();
            startDate = in.readString();
            endDate = in.readString();
            carWeight = in.readInt();
            seatNum = in.readInt();
            bak = in.readString();
            userId = in.readInt();
            createTime = in.readLong();
            updateTime = in.readLong();
            userName = in.readString();
            nickName = in.readString();
            avatar = in.readString();
            realName = in.readString();
            useAge = in.readInt();
            bidNum = in.readInt();
            monthNum = in.readInt();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public int getAcId() {
            return acId;
        }

        public void setAcId(int acId) {
            this.acId = acId;
        }

        public int getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(int provinceId) {
            this.provinceId = provinceId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        public int getCarTypeId() {
            return carTypeId;
        }

        public void setCarTypeId(int carTypeId) {
            this.carTypeId = carTypeId;
        }

        public int getNewCarId() {
            return newCarId;
        }

        public void setNewCarId(int newCarId) {
            this.newCarId = newCarId;
        }

        public int getTciRb() {
            return tciRb;
        }

        public void setTciRb(int tciRb) {
            this.tciRb = tciRb;
        }

        public int getVciRb() {
            return vciRb;
        }

        public void setVciRb(int vciRb) {
            this.vciRb = vciRb;
        }

        public int getLongTerm() {
            return longTerm;
        }

        public void setLongTerm(int longTerm) {
            this.longTerm = longTerm;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public int getCarWeight() {
            return carWeight;
        }

        public void setCarWeight(int carWeight) {
            this.carWeight = carWeight;
        }

        public int getSeatNum() {
            return seatNum;
        }

        public void setSeatNum(int seatNum) {
            this.seatNum = seatNum;
        }

        public String getBak() {
            return bak;
        }

        public void setBak(String bak) {
            this.bak = bak;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public String getBidNumStr() {
            StringBuffer buf = new StringBuffer();
            buf.append("总单").append(" ").append(bidNum).append("\t").append("月单").append(" ")
                    .append(monthNum);
            return buf.toString();
        }

        public String getUseAgeStr() {
            switch (useAge) {
                case 1:
                    return "营运";
                case 2:
                    return "非营运";
            }
            return "非营运";
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(acId);
            dest.writeInt(provinceId);
            dest.writeInt(cityId);
            dest.writeInt(companyId);
            dest.writeInt(carTypeId);
            dest.writeInt(newCarId);
            dest.writeInt(tciRb);
            dest.writeInt(vciRb);
            dest.writeInt(longTerm);
            dest.writeString(startDate);
            dest.writeString(endDate);
            dest.writeInt(carWeight);
            dest.writeInt(seatNum);
            dest.writeString(bak);
            dest.writeInt(userId);
            dest.writeLong(createTime);
            dest.writeLong(updateTime);
            dest.writeString(userName);
            dest.writeString(nickName);
            dest.writeString(avatar);
            dest.writeString(realName);
            dest.writeInt(useAge);
            dest.writeInt(bidNum);
            dest.writeInt(monthNum);
        }

        @Override
        public String toString() {
            return "Data{" +
                    "acId=" + acId +
                    ", provinceId=" + provinceId +
                    ", cityId=" + cityId +
                    ", companyId=" + companyId +
                    ", carTypeId=" + carTypeId +
                    ", newCarId=" + newCarId +
                    ", tciRb=" + tciRb +
                    ", vciRb=" + vciRb +
                    ", longTerm=" + longTerm +
                    ", startDate='" + startDate + '\'' +
                    ", endDate='" + endDate + '\'' +
                    ", carWeight=" + carWeight +
                    ", seatNum=" + seatNum +
                    ", bak='" + bak + '\'' +
                    ", userId=" + userId +
                    ", createTime=" + createTime +
                    ", updateTime=" + updateTime +
                    ", userName='" + userName + '\'' +
                    ", nickName='" + nickName + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", realName='" + realName + '\'' +
                    ", useAge=" + useAge +
                    ", bidNum=" + bidNum +
                    ", monthNum=" + monthNum +
                    '}';
        }
    }
}
