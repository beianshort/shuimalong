package com.xincheng.shuimalong.entity.address;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaote on 2017/6/29.
 */

public class Address implements Parcelable {
    private int id;
    @SerializedName("address_id")
    private int addressId;
    @SerializedName("region_id")
    private int regionId;
    private String province;
    private String city;
    private String area;
    private String address;
    private String name;
    private String mobile;
    @SerializedName("is_default")
    private int isDefault;
    @SerializedName("list_order")
    private int listOrder;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("create_time")
    private long createTime;
    @SerializedName("is_del")
    private int isDel;
    private boolean select;

    public Address() {

    }

    protected Address(Parcel in) {
        id = in.readInt();
        addressId = in.readInt();
        regionId = in.readInt();
        province = in.readString();
        city = in.readString();
        area = in.readString();
        address = in.readString();
        name = in.readString();
        mobile = in.readString();
        isDefault = in.readInt();
        listOrder = in.readInt();
        userId = in.readInt();
        createTime = in.readLong();
        isDel = in.readInt();
        select = in.readByte() != 0;
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public int getListOrder() {
        return listOrder;
    }

    public void setListOrder(int listOrder) {
        this.listOrder = listOrder;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getIsDel() {
        return isDel;
    }

    public void setIsDel(int isDel) {
        this.isDel = isDel;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getDetailAddress() {
        StringBuffer buf = new StringBuffer();
        buf.append(province).append(" ").append(city).append(" ").append(area).append(" ").append(address);
        return buf.toString();
    }

    public boolean isDefault() {
        return isDefault == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(addressId);
        parcel.writeInt(regionId);
        parcel.writeString(province);
        parcel.writeString(city);
        parcel.writeString(area);
        parcel.writeString(address);
        parcel.writeString(name);
        parcel.writeString(mobile);
        parcel.writeInt(isDefault);
        parcel.writeInt(listOrder);
        parcel.writeInt(userId);
        parcel.writeLong(createTime);
        parcel.writeInt(isDel);
        parcel.writeByte((byte) (select ? 1 : 0));
    }
}
