package com.xincheng.shuimalong.entity.config;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;

import java.util.List;

/**
 * Created by xiaote on 2017/6/21.
 * 商业险类型
 */

public class VciType implements Parcelable {

    private int id;
    private String title;
    private boolean ndis;
    @SerializedName("Options")
    private List<String> options;
    private String option = "";
    private boolean selected;

    public VciType() {

    }

    public VciType(int id, String title, boolean ndis, List<String> options, String option, boolean selected,
                   boolean ndisChecked) {
        this.id = id;
        this.title = title;
        this.ndis = ndis;
        this.options = options;
        this.option = option;
        this.selected = selected;
    }

    protected VciType(Parcel in) {
        id = in.readInt();
        title = in.readString();
        ndis = in.readByte() != 0;
        options = in.createStringArrayList();
        option = in.readString();
        selected = in.readByte() != 0;
    }

    public static final Creator<VciType> CREATOR = new Creator<VciType>() {
        @Override
        public VciType createFromParcel(Parcel in) {
            return new VciType(in);
        }

        @Override
        public VciType[] newArray(int size) {
            return new VciType[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isNdis() {
        return ndis;
    }

    public void setNdis(boolean ndis) {
        this.ndis = ndis;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void selectToggle() {
        this.selected = !selected;
    }

    public void ndisToggle() {
        this.setNdis(!ndis);
    }

    public int getSelectOptionIndex() {
        if (id == 2) {
            for (int i = 0; i < options.size(); i++) {
                if ("50万".equals(options.get(i))) {
                    return i;
                }
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "VciType{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", ndis=" + ndis +
                ", option=" + option +
                '}';
    }

    public boolean canNdis() {
        return id >= 1 && id <= 7;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeByte((byte) (ndis ? 1 : 0));
        parcel.writeStringList(options);
        parcel.writeString(option);
        parcel.writeByte((byte) (selected ? 1 : 0));
    }

}
