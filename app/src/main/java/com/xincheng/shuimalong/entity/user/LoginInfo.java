package com.xincheng.shuimalong.entity.user;

import com.xincheng.shuimalong.entity.BaseEntity;

/**
 * Created by xuhao on 2017/6/12.
 */

public class LoginInfo {
    private String token; //用户身份
    private String user_name;//用户名
    private String user_id;//用户ID
    private String nick_name;//用户昵称
    private String real_name;//用户真实名字
    private String face;//用户头像
    private String mobile;//用户手机
    private int user_cert;//实名认证
    private String reason;//被拒绝理由
    private int pay_pwd;//是否设置交易密码

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public int getUser_cert() {
        return user_cert;
    }

    public void setUser_cert(int user_cert) {
        this.user_cert = user_cert;
    }

    public boolean isCert() {
        return user_cert == 1;
    }

    public int getPay_pwd() {
        return pay_pwd;
    }

    public void setPay_pwd(int pay_pwd) {
        this.pay_pwd = pay_pwd;
    }

    public boolean isSetPayPwd() {
        return pay_pwd == 1;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "LoginInfo{" +
                "token='" + token + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_id='" + user_id + '\'' +
                ", nick_name='" + nick_name + '\'' +
                ", real_name='" + real_name + '\'' +
                ", face='" + face + '\'' +
                ", mobile='" + mobile + '\'' +
                ", user_cert=" + user_cert +
                ", reason='" + reason + '\'' +
                ", pay_pwd=" + pay_pwd +
                '}';
    }
}
