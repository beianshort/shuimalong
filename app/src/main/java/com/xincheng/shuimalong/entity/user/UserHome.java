package com.xincheng.shuimalong.entity.user;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.HistoryOrder;

import java.util.List;

/**
 * Created by xiaote1988 on 2017/7/30.
 * 收单方主页
 */

public class UserHome implements Parcelable {

    @SerializedName("user_id")
    private int userId;
    @SerializedName("nick_name")
    private String nickName;
    @SerializedName("real_name")
    private String realName;
    @SerializedName("user_name")
    private String userName;
    private String avatar;
    private String amount;
    @SerializedName("user_cert")
    private int userCert;
    @SerializedName("register_time")
    private String registerTime;
    private int num;
    @SerializedName("month_num")
    private int monthNum;
    @SerializedName("total_money")
    private String totalMoney;
    @SerializedName("month_totalMoney")
    private String monthTotalMoney;
    @SerializedName("month_tciMoney")
    private String monthTciMoney;
    @SerializedName("total_tciMoney")
    private String totalTciMoney;
    @SerializedName("month_vciMoney")
    private String monthVciMoney;
    @SerializedName("total_vciMoney")
    private String totalVciMoney;
    private String reason;

    @SerializedName("month_orderList")
    private List<HistoryOrder> monthOrderList;
    private List<HistoryOrder> orderList;

    public UserHome() {

    }

    protected UserHome(Parcel in) {
        userId = in.readInt();
        nickName = in.readString();
        realName = in.readString();
        userName = in.readString();
        avatar = in.readString();
        amount = in.readString();
        userCert = in.readInt();
        registerTime = in.readString();
        num = in.readInt();
        monthNum = in.readInt();
        totalMoney = in.readString();
        monthTotalMoney = in.readString();
        monthTciMoney = in.readString();
        totalTciMoney = in.readString();
        monthVciMoney = in.readString();
        totalVciMoney = in.readString();
        reason = in.readString();
        monthOrderList = in.createTypedArrayList(HistoryOrder.CREATOR);
        orderList = in.createTypedArrayList(HistoryOrder.CREATOR);
    }

    public static final Creator<UserHome> CREATOR = new Creator<UserHome>() {
        @Override
        public UserHome createFromParcel(Parcel in) {
            return new UserHome(in);
        }

        @Override
        public UserHome[] newArray(int size) {
            return new UserHome[size];
        }
    };

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getUserCert() {
        return userCert;
    }

    public void setUserCert(int userCert) {
        this.userCert = userCert;
    }

    public String getCertMsg() {
        switch (userCert) {
            case 0:
                if(CommonUtil.isEmpty(reason)) {
                    return "未认证";
                } else {
                    return "认证不通过";
                }
            case 1:
                return "已认证";
        }
        return "未认证";
    }

    public boolean isCert() {
        return userCert == 1;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getMonthNum() {
        return monthNum;
    }

    public void setMonthNum(int monthNum) {
        this.monthNum = monthNum;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getMonthTotalMoney() {
        return monthTotalMoney;
    }

    public void setMonthTotalMoney(String monthTotalMoney) {
        this.monthTotalMoney = monthTotalMoney;
    }

    public String getMonthTciMoney() {
        return monthTciMoney;
    }

    public void setMonthTciMoney(String monthTciMoney) {
        this.monthTciMoney = monthTciMoney;
    }

    public String getTotalTciMoney() {
        return totalTciMoney;
    }

    public void setTotalTciMoney(String totalTciMoney) {
        this.totalTciMoney = totalTciMoney;
    }

    public String getMonthVciMoney() {
        return monthVciMoney;
    }

    public void setMonthVciMoney(String monthVciMoney) {
        this.monthVciMoney = monthVciMoney;
    }

    public String getTotalVciMoney() {
        return totalVciMoney;
    }

    public void setTotalVciMoney(String totalVciMoney) {
        this.totalVciMoney = totalVciMoney;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<HistoryOrder> getMonthOrderList() {
        return monthOrderList;
    }

    public void setMonthOrderList(List<HistoryOrder> monthOrderList) {
        this.monthOrderList = monthOrderList;
    }

    public List<HistoryOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<HistoryOrder> orderList) {
        this.orderList = orderList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeString(nickName);
        dest.writeString(realName);
        dest.writeString(userName);
        dest.writeString(avatar);
        dest.writeString(amount);
        dest.writeInt(userCert);
        dest.writeString(registerTime);
        dest.writeInt(num);
        dest.writeInt(monthNum);
        dest.writeString(totalMoney);
        dest.writeString(monthTotalMoney);
        dest.writeString(monthTciMoney);
        dest.writeString(totalTciMoney);
        dest.writeString(monthVciMoney);
        dest.writeString(totalVciMoney);
        dest.writeString(reason);
        dest.writeTypedList(monthOrderList);
        dest.writeTypedList(orderList);
    }


}
