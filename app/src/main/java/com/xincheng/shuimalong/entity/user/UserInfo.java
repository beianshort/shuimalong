package com.xincheng.shuimalong.entity.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaote on 2017/8/1.
 */

public class UserInfo {
    @SerializedName("user_id")
    private int userId;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("user_pwd")
    private String userPwd;
    @SerializedName("group_id")
    private int groupId;
    private String encrypt;
    private String email;
    private String mobile;
    @SerializedName("nick_name")
    private String nickName;
    @SerializedName("real_name")
    private String realName;
    private String sex;
    private String birthday;
    private String signature;
    @SerializedName("register_ip")
    private String registerIp;
    @SerializedName("last_login_ip")
    private String lastLoginIp;
    @SerializedName("register_time")
    private long registerTime;
    @SerializedName("last_login_time")
    private long lastLoginTime;
    @SerializedName("login_num")
    private int logiNum;
    @SerializedName("region_id")
    private int regionId;
    private String address;
    private String amount;
    private int point;
    @SerializedName("model_id")
    private int modelId;
    @SerializedName("have_message")
    private int haveMessage;
    @SerializedName("is_lock")
    private int isLock;
    private int vip;
    @SerializedName("overdue_date")
    private long overdueDate;
    private String avatar;
    @SerializedName("site_id")
    private int siteId;
    @SerializedName("registered_from")
    private int registeredFrom;
    @SerializedName("disable_form_login")
    private int disableFormLogin;
    @SerializedName("parent_user_id")
    private int parentUserId;
    @SerializedName("mobile_cert")
    private int mobileCert;
    @SerializedName("user_cert")
    private int userCert;
    @SerializedName("open_notice")
    private int openNotice;
    @SerializedName("is_friend")
    private int isFriend;
    private String reason;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(String encrypt) {
        this.encrypt = encrypt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public long getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(long registerTime) {
        this.registerTime = registerTime;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public int getLogiNum() {
        return logiNum;
    }

    public void setLogiNum(int logiNum) {
        this.logiNum = logiNum;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getHaveMessage() {
        return haveMessage;
    }

    public void setHaveMessage(int haveMessage) {
        this.haveMessage = haveMessage;
    }

    public int getIsLock() {
        return isLock;
    }

    public void setIsLock(int isLock) {
        this.isLock = isLock;
    }

    public int getVip() {
        return vip;
    }

    public void setVip(int vip) {
        this.vip = vip;
    }

    public long getOverdueDate() {
        return overdueDate;
    }

    public void setOverdueDate(long overdueDate) {
        this.overdueDate = overdueDate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getRegisteredFrom() {
        return registeredFrom;
    }

    public void setRegisteredFrom(int registeredFrom) {
        this.registeredFrom = registeredFrom;
    }

    public int getDisableFormLogin() {
        return disableFormLogin;
    }

    public void setDisableFormLogin(int disableFormLogin) {
        this.disableFormLogin = disableFormLogin;
    }

    public int getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(int parentUserId) {
        this.parentUserId = parentUserId;
    }

    public int getMobileCert() {
        return mobileCert;
    }

    public void setMobileCert(int mobileCert) {
        this.mobileCert = mobileCert;
    }

    public int getUserCert() {
        return userCert;
    }

    public void setUserCert(int userCert) {
        this.userCert = userCert;
    }

    public boolean isCert() {
        return userCert == 1;
    }

    public String getCertMsg() {
        return userCert == 1 ? "已认证" : "未认证";
    }

    public int getOpenNotice() {
        return openNotice;
    }

    public void setOpenNotice(int openNotice) {
        this.openNotice = openNotice;
    }

    public int getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public boolean isFriend() {
        return isFriend == 1;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
