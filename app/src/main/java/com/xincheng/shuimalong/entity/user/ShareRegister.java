package com.xincheng.shuimalong.entity.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaote on 2017/7/10.
 */

public class ShareRegister {

    private String qrcode;
    private String link;

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
