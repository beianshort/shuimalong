package com.xincheng.shuimalong.entity.config;

import android.os.Parcel;
import android.os.Parcelable;

import com.xincheng.library.widget.pickerview.model.IPickerViewData;

/**
 * Created by xiaote on 2017/6/21.
 * expressCompanys : 快递公司
 * expressType : 保单办理方式
 * companyList : 保险公司
 * carType : 车辆类型
 * truckWeightTypes : 货车重量选项
 * newCarType : 车辆新旧选项
 * acceptPrice : 30分钟内报价
 * acceptIssue : 当天出单
 * buyerIssue : 两天内回寄订单
 * hotCitys : 热门城市
 */

public class CommonConfig implements Parcelable, IPickerViewData {
    private int id;
    private String name;

    public static final Creator<CommonConfig> CREATOR = new Creator<CommonConfig>() {
        @Override
        public CommonConfig createFromParcel(Parcel in) {
            return new CommonConfig(in);
        }

        @Override
        public CommonConfig[] newArray(int size) {
            return new CommonConfig[size];
        }
    };

    public CommonConfig() {

    }

    public CommonConfig(int id, String name) {
        this.id = id;
        this.name = name;
    }

    protected CommonConfig(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
    }

    @Override
    public String getPickerViewText() {
        return name;
    }
}
