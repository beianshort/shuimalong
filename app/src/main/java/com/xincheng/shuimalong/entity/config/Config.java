package com.xincheng.shuimalong.entity.config;

import java.util.List;

/**
 * Created by xiaote on 2017/6/21.
 */

public class Config {
    private ConfigName configNames;
    private List<CommonConfig> expressCompanys;
    private List<CommonConfig> expressType;
    private List<CommonConfig> companyList;
    private List<VciType> vciTypes;
    private List<CommonConfig> carType;
    private List<CommonConfig> truckWeightTypes;
    private List<CommonConfig> newCarType;
    private List<CommonConfig> acceptPrice;
    private List<CommonConfig> acceptIssue;
    private List<CommonConfig> buyerIssue;
    private List<CommonConfig> hotCitys;
    private List<CommonConfig> usage;
    private List<CommonConfig> fileStatus;
    private List<CommonConfig> refuseReason;
    private List<CompanyLogo> companyLogoList;
    private String aboutus;
    private String law;
    private String contactus;
    public Config() {

    }

    public String getAboutus() {
        return aboutus;
    }

    public void setAboutus(String aboutus) {
        this.aboutus = aboutus;
    }

    public String getLaw() {
        return law;
    }

    public void setLaw(String law) {
        this.law = law;
    }

    public String getContactus() {
        return contactus;
    }

    public void setContactus(String contactus) {
        this.contactus = contactus;
    }

    public ConfigName getConfigNames() {
        return configNames;
    }

    public void setConfigNames(ConfigName configNames) {
        this.configNames = configNames;
    }

    public List<CommonConfig> getExpressCompanys() {
        return expressCompanys;
    }

    public void setExpressCompanys(List<CommonConfig> expressCompanys) {
        this.expressCompanys = expressCompanys;
    }

    public List<CommonConfig> getExpressType() {
        return expressType;
    }

    public void setExpressType(List<CommonConfig> expressType) {
        this.expressType = expressType;
    }

    public List<CommonConfig> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<CommonConfig> companyList) {
        this.companyList = companyList;
    }

    public List<VciType> getVciTypes() {
        return vciTypes;
    }

    public void setVciTypes(List<VciType> vciTypes) {
        this.vciTypes = vciTypes;
    }

    public List<CommonConfig> getCarType() {
        return carType;
    }

    public void setCarType(List<CommonConfig> carType) {
        this.carType = carType;
    }

    public List<CommonConfig> getTruckWeightTypes() {
        return truckWeightTypes;
    }

    public void setTruckWeightTypes(List<CommonConfig> truckWeightTypes) {
        this.truckWeightTypes = truckWeightTypes;
    }

    public List<CommonConfig> getNewCarType() {
        return newCarType;
    }

    public void setNewCarType(List<CommonConfig> newCarType) {
        this.newCarType = newCarType;
    }

    public List<CommonConfig> getAcceptPrice() {
        return acceptPrice;
    }

    public void setAcceptPrice(List<CommonConfig> acceptPrice) {
        this.acceptPrice = acceptPrice;
    }

    public List<CommonConfig> getAcceptIssue() {
        return acceptIssue;
    }

    public void setAcceptIssue(List<CommonConfig> acceptIssue) {
        this.acceptIssue = acceptIssue;
    }

    public List<CommonConfig> getBuyerIssue() {
        return buyerIssue;
    }

    public void setBuyerIssue(List<CommonConfig> buyerIssue) {
        this.buyerIssue = buyerIssue;
    }

    public List<CommonConfig> getHotCitys() {
        return hotCitys;
    }

    public void setHotCitys(List<CommonConfig> hotCitys) {
        this.hotCitys = hotCitys;
    }

    public List<CommonConfig> getUsage() {
        return usage;
    }

    public void setUsage(List<CommonConfig> usage) {
        this.usage = usage;
    }

    public List<CommonConfig> getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(List<CommonConfig> fileStatus) {
        this.fileStatus = fileStatus;
    }

    public List<CommonConfig> getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(List<CommonConfig> refuseReason) {
        this.refuseReason = refuseReason;
    }

    public List<CompanyLogo> getCompanyLogoList() {
        return companyLogoList;
    }

    public void setCompanyLogoList(List<CompanyLogo> companyLogoList) {
        this.companyLogoList = companyLogoList;
    }
}
