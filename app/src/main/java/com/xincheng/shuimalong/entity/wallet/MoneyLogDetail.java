package com.xincheng.shuimalong.entity.wallet;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaote1988 on 2017/8/19.
 */

public class MoneyLogDetail {

    @SerializedName("all_money")
    private String allMoney;
    private String bak;
    @SerializedName("create_time")
    private String createTime;
    @SerializedName("bid")
    private BidData bidData;
    private WithDraw withdraw;

    public String getAllMoney() {
        return allMoney;
    }

    public void setAllMoney(String allMoney) {
        this.allMoney = allMoney;
    }

    public String getBak() {
        return bak;
    }

    public void setBak(String bak) {
        this.bak = bak;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public BidData getBidData() {
        return bidData;
    }

    public void setBidData(BidData bidData) {
        this.bidData = bidData;
    }

    public WithDraw getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(WithDraw withdraw) {
        this.withdraw = withdraw;
    }

    public static class BidData {

        @SerializedName("bid_id")
        private int bidId;
        @SerializedName("bill_user_id")
        private String billUserId;
        @SerializedName("ac_user_id")
        private String acUserId;
        @SerializedName("total_money")
        private String totalMoney;
        @SerializedName("order_no")
        private String orderNo;
        @SerializedName("pay_time")
        private String payTime;
        @SerializedName("plate_no")
        private String plateNo;
        @SerializedName("tci_rb")
        private int tciRb;
        @SerializedName("vci_rb")
        private int vciRb;
        @SerializedName("tci_money")
        private String tciMoney;
        @SerializedName("vci_money")
        private String vciMoney;
        @SerializedName("vvt_money")
        private String vvtMoney;

        public int getBidId() {
            return bidId;
        }

        public void setBidId(int bidId) {
            this.bidId = bidId;
        }

        public String getBillUserId() {
            return billUserId;
        }

        public void setBillUserId(String billUserId) {
            this.billUserId = billUserId;
        }

        public String getAcUserId() {
            return acUserId;
        }

        public void setAcUserId(String acUserId) {
            this.acUserId = acUserId;
        }

        public String getTotalMoney() {
            return totalMoney;
        }

        public void setTotalMoney(String totalMoney) {
            this.totalMoney = totalMoney;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public int getTciRb() {
            return tciRb;
        }

        public void setTciRb(int tciRb) {
            this.tciRb = tciRb;
        }

        public int getVciRb() {
            return vciRb;
        }

        public void setVciRb(int vciRb) {
            this.vciRb = vciRb;
        }

        public String getTciMoney() {
            return tciMoney;
        }

        public void setTciMoney(String tciMoney) {
            this.tciMoney = tciMoney;
        }

        public String getVciMoney() {
            return vciMoney;
        }

        public void setVciMoney(String vciMoney) {
            this.vciMoney = vciMoney;
        }

        public String getVvtMoney() {
            return vvtMoney;
        }

        public void setVvtMoney(String vvtMoney) {
            this.vvtMoney = vvtMoney;
        }

        @Override
        public String toString() {
            return "BidData{" +
                    "bidId=" + bidId +
                    ", billUserId='" + billUserId + '\'' +
                    ", acUserId='" + acUserId + '\'' +
                    ", totalMoney='" + totalMoney + '\'' +
                    ", orderNo='" + orderNo + '\'' +
                    ", payTime='" + payTime + '\'' +
                    ", plateNo='" + plateNo + '\'' +
                    ", tciRb=" + tciRb +
                    ", vciRb=" + vciRb +
                    ", tciMoney='" + tciMoney + '\'' +
                    ", vciMoney='" + vciMoney + '\'' +
                    ", vvtMoney='" + vvtMoney + '\'' +
                    '}';
        }
    }

    public static class WithDraw {
        private String id;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("bank_id")
        private String bankId;
        private String money;
        private int process;
        private int status;
        private String bak;
        @SerializedName("apply_time")
        private String applyTime;
        @SerializedName("process_time")
        private String processTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getBankId() {
            return bankId;
        }

        public void setBankId(String bankId) {
            this.bankId = bankId;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

               public int getProcess() {
            return process;
        }

        public void setProcess(int process) {
            this.process = process;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getBak() {
            return bak;
        }

        public void setBak(String bak) {
            this.bak = bak;
        }

        public String getApplyTime() {
            return applyTime;
        }

        public void setApplyTime(String applyTime) {
            this.applyTime = applyTime;
        }

        public String getProcessTime() {
            return processTime;
        }

        public void setProcessTime(String processTime) {
            this.processTime = processTime;
        }

        @Override
        public String toString() {
            return "WithDraw{" +
                    "id='" + id + '\'' +
                    ", userId='" + userId + '\'' +
                    ", bankId='" + bankId + '\'' +
                    ", money='" + money + '\'' +
                    ", process=" + process +
                    ", status=" + status +
                    ", bak='" + bak + '\'' +
                    ", applyTime='" + applyTime + '\'' +
                    ", processTime='" + processTime + '\'' +
                    '}';
        }
    }
}
