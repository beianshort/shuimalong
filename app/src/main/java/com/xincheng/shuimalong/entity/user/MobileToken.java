package com.xincheng.shuimalong.entity.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaote on 2017/7/11.
 */

public class MobileToken {
    @SerializedName("mobiletoken")
    private String mobileToken;

    public String getMobileToken() {
        return mobileToken;
    }

    public void setMobileToken(String mobileToken) {
        this.mobileToken = mobileToken;
    }
}
