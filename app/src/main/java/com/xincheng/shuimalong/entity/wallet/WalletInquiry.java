package com.xincheng.shuimalong.entity.wallet;

/**
 * Created by xiaote on 2017/7/19.
 */

public class WalletInquiry {
    private int id;
    private String name;
    private boolean selected;

    public WalletInquiry() {
    }

    public WalletInquiry(int id, String name, boolean selected) {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
