package com.xincheng.shuimalong.entity.recept;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.xincheng.library.util.CommonUtil;

import java.util.List;

/**
 * Created by xiaote on 2017/6/20.
 * 收单
 */

public class Acceptance {
    @SerializedName("list")
    private List<Data> acceptanceList;

    public List<Data> getAcceptanceList() {
        return acceptanceList;
    }

    public void setAcceptanceList(List<Data> acceptanceList) {
        this.acceptanceList = acceptanceList;
    }

    @Override
    public String toString() {
        return "Acceptance{" +
                "acceptanceList=" + acceptanceList +
                '}';
    }

    public static class Data {

        @SerializedName("ac_id")
        private int acId;
        @SerializedName("province")
        private int provinceId;
        @SerializedName("city")
        private int cityId;
        @SerializedName("company")
        private int companyId;
        @SerializedName("car_type")
        private int carTypeId;
        @SerializedName("new_car")
        private int newCarId;
        @SerializedName("tci_rb")
        private int tciRb;
        @SerializedName("vci_rb")
        private int vciRb;
        @SerializedName("long_term")
        private int longTerm;
        @SerializedName("start_date")
        private String startDate;
        @SerializedName("end_date")
        private String endDate;
        @SerializedName("car_weight")
        private int carWeight;
        @SerializedName("seat_num")
        private int seatNum;
        private String bak;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("create_time")
        private long createTime;
        @SerializedName("update_time")
        private long updateTime;
        private int usage;

        public Data() {
        }

        public Data(int acId, int provinceId, int cityId, int companyId, int carTypeId, int newCarId, int tciRb,
                    int vciRb, int longTerm, String startDate, String endDate, int carWeight, int seatNum, String bak,
                    int userId, long createTime, long updateTime,int usage) {
            this.acId = acId;
            this.provinceId = provinceId;
            this.cityId = cityId;
            this.companyId = companyId;
            this.carTypeId = carTypeId;
            this.newCarId = newCarId;
            this.tciRb = tciRb;
            this.vciRb = vciRb;
            this.longTerm = longTerm;
            this.startDate = startDate;
            this.endDate = endDate;
            this.carWeight = carWeight;
            this.seatNum = seatNum;
            this.bak = bak;
            this.userId = userId;
            this.createTime = createTime;
            this.updateTime = updateTime;
            this.usage = usage;
        }

        public int getAcId() {
            return acId;
        }

        public void setAcId(int acId) {
            this.acId = acId;
        }

        public int getTciRb() {
            return tciRb;
        }

        public void setTciRb(int tciRb) {
            this.tciRb = tciRb;
        }

        public int getVciRb() {
            return vciRb;
        }

        public void setVciRb(int vciRb) {
            this.vciRb = vciRb;
        }

        public int getLongTerm() {
            return longTerm;
        }

        public void setLongTerm(int longTerm) {
            this.longTerm = longTerm;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public int getCarWeight() {
            return carWeight;
        }

        public void setCarWeight(int carWeight) {
            this.carWeight = carWeight;
        }

        public int getSeatNum() {
            return seatNum;
        }

        public void setSeatNum(int seatNum) {
            this.seatNum = seatNum;
        }

        public String getBak() {
            return bak;
        }

        public int getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(int provinceId) {
            this.provinceId = provinceId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        public int getCarTypeId() {
            return carTypeId;
        }

        public void setCarTypeId(int carTypeId) {
            this.carTypeId = carTypeId;
        }

        public int getNewCarId() {
            return newCarId;
        }

        public void setNewCarId(int newCarId) {
            this.newCarId = newCarId;
        }

        public void setBak(String bak) {
            this.bak = bak;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getUsage() {
            return usage;
        }

        public void setUsage(int usage) {
            this.usage = usage;
        }

        public String getUseAgeMsg() {
            return usage == 1 ? "营运" : "非营运";
        }

        public String getActivityDuration() {
           return CommonUtil.isEmpty(startDate) ? "" : startDate + " 至 " + endDate;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "acId=" + acId +
                    ", provinceId=" + provinceId +
                    ", cityId=" + cityId +
                    ", companyId=" + companyId +
                    ", carTypeId=" + carTypeId +
                    ", newCarId=" + newCarId +
                    ", tciRb=" + tciRb +
                    ", vciRb=" + vciRb +
                    ", longTerm=" + longTerm +
                    ", startDate='" + startDate + '\'' +
                    ", endDate='" + endDate + '\'' +
                    ", carWeight=" + carWeight +
                    ", seatNum=" + seatNum +
                    ", bak='" + bak + '\'' +
                    ", userId=" + userId +
                    ", createTime=" + createTime +
                    ", updateTime=" + updateTime +
                    ", usgae=" + usage +
                    '}';
        }

    }
}
