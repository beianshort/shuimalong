package com.xincheng.shuimalong.entity.rank;

/**
 * Created by xiaote1988 on 2017/7/29.
 */

public class RankData {
    public static final int TYPE_TITLE = 0;
    public static final int TYPE_CONTENT = 1;

    private Rank.Data data;
    private String title;
    private int top;
    private int type;//0-标题 2-内容

    public RankData() {
    }

    public RankData(String title, int top, int type) {
        this.title = title;
        this.top = top;
        this.type = type;
    }

    public RankData(Rank.Data data, int type) {
        this.data = data;
        this.type = type;
    }

    public Rank.Data getData() {
        return data;
    }

    public void setData(Rank.Data data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "RankData{" +
                "data=" + data +
                ", title='" + title + '\'' +
                ", top=" + top +
                ", type=" + type +
                '}';
    }
}
