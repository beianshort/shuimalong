package com.xincheng.shuimalong.entity.bank;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaote on 2017/7/11.
 */

public class BankCard {
    @SerializedName("banklist")
    private List<Data> bankList;

    public List<Data> getBankList() {
        return bankList;
    }

    public void setBankList(List<Data> bankList) {
        this.bankList = bankList;
    }

    @Override
    public String toString() {
        return "BankCard{" + "bankList=" + bankList + '}';
    }

    public static class Data implements Parcelable {
        private String id;
        private String name;
        @SerializedName("idcard_no")
        private String idcardNo;
        private String bank;
        @SerializedName("bankcard_no")
        private String bankcardNo;
        @SerializedName("region_id")
        private int regionId;
        @SerializedName("lattice_point")
        private String latticePoint;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("create_time")
        private long createTime;
        @SerializedName("list_order")
        private int listOrder;
        @SerializedName("is_default")
        private int isDefault;
        @SerializedName("is_del")
        private int isDel;
        @SerializedName("is_select")
        private int isSelect;
        private String money;
        private int status; //1- 钱包 2- 银行卡 ，0-添加

        public Data() {
        }

        protected Data(Parcel in) {
            id = in.readString();
            name = in.readString();
            idcardNo = in.readString();
            bank = in.readString();
            bankcardNo = in.readString();
            regionId = in.readInt();
            latticePoint = in.readString();
            userId = in.readInt();
            createTime = in.readLong();
            listOrder = in.readInt();
            isDefault = in.readInt();
            isDel = in.readInt();
            money = in.readString();
            status = in.readInt();
        }


        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public boolean getIsSelect() {
            return isSelect==1;
        }

        public void setIsSelect(int isSelect) {
            this.isSelect = isSelect;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIdcardNo() {
            return idcardNo;
        }

        public void setIdcardNo(String idcardNo) {
            this.idcardNo = idcardNo;
        }

        public String getBank() {
            return bank;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }

        public String getBankcardNo() {
            return bankcardNo;
        }

        public String getBankcardNoLast() {
            return "尾号" + bankcardNo.substring(bankcardNo.length() - 4, bankcardNo.length());
        }

        public void setBankcardNo(String bankcardNo) {
            this.bankcardNo = bankcardNo;
        }

        public int getRegionId() {
            return regionId;
        }

        public void setRegionId(int regionId) {
            this.regionId = regionId;
        }

        public String getLatticePoint() {
            return latticePoint;
        }

        public void setLatticePoint(String latticePoint) {
            this.latticePoint = latticePoint;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getListOrder() {
            return listOrder;
        }

        public void setListOrder(int listOrder) {
            this.listOrder = listOrder;
        }

        public int getIsDefault() {
            return isDefault;
        }

        public boolean isDefault() {
            return isDefault == 1;
        }

        public void setIsDefault(int isDefault) {
            this.isDefault = isDefault;
        }

        public int getIsDel() {
            return isDel;
        }

        public void setIsDel(int isDel) {
            this.isDel = isDel;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(idcardNo);
            dest.writeString(bank);
            dest.writeString(bankcardNo);
            dest.writeInt(regionId);
            dest.writeString(latticePoint);
            dest.writeInt(userId);
            dest.writeLong(createTime);
            dest.writeInt(listOrder);
            dest.writeInt(isDefault);
            dest.writeInt(isDel);
            dest.writeInt(isSelect);
            dest.writeString(money);
            dest.writeInt(status);
        }

        @Override
        public int describeContents() {
            return 0;
        }
        @Override
        public String toString() {
            return "Data{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", idcardNo='" + idcardNo + '\'' +
                    ", bank='" + bank + '\'' + ", bankcardNo='" + bankcardNo + '\'' + ", regionId=" + regionId +
                    ", latticePoint='" + latticePoint + '\'' + ", userId=" + userId + ", createTime=" + createTime +
                    ", listOrder=" + listOrder + ", isDefault=" + isDefault + ", isDel=" + isDel + '}';
        }
    }
}
