package com.xincheng.shuimalong.entity.customer;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 许浩 on 2017/7/3.
 */

public class Customer implements Parcelable {
    private String customer_id;
    private String plate_no;
    private String car_owner;
    private String idcard_no;
    private String mobile_phone;
    private String car_register_date;
    private int transfer_car;
    private int new_car;
    private String vin;
    private String engine_no;
    private int tci_status;
    private String tci_date;
    private String vci_date;
    private String vci_settings;
    private String idcard_photo1;
    private String idcard_photo2;
    private String dl_photo1;
    private String dl_photo2;
    private String user_id;
    private String org_no;
    private String org_photo;

    private String d1_photo;
    private String d2_photo;
    private String idcartd1_photo;
    private String idcartd2_photo;
    private String org1_photo;
    private int useage;
    private String car_bak;
    private long create_time;
    private long update_time;
    private int car_type;
    private String output_volume;
    private String curb_weight;

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public Customer() {

    }

    protected Customer(Parcel in) {
        customer_id = in.readString();
        plate_no = in.readString();
        car_owner = in.readString();
        idcard_no = in.readString();
        mobile_phone = in.readString();
        car_register_date = in.readString();
        transfer_car = in.readInt();
        new_car = in.readInt();
        vin = in.readString();
        engine_no = in.readString();
        tci_status = in.readInt();
        tci_date = in.readString();
        vci_date = in.readString();
        vci_settings = in.readString();
        idcard_photo1 = in.readString();
        idcard_photo2 = in.readString();
        dl_photo1 = in.readString();
        dl_photo2 = in.readString();
        user_id = in.readString();
        org_no = in.readString();
        org_photo = in.readString();
        d1_photo = in.readString();
        d2_photo = in.readString();
        idcartd1_photo = in.readString();
        idcartd2_photo = in.readString();
        org1_photo = in.readString();
        useage = in.readInt();
        car_bak = in.readString();
        create_time = in.readLong();
        update_time = in.readLong();
        car_type = in.readInt();
        output_volume = in.readString();
        curb_weight = in.readString();
    }


    public String getD1_photo() {
        return d1_photo;
    }

    public void setD1_photo(String d1_photo) {
        this.d1_photo = d1_photo;
    }

    public String getD2_photo() {
        return d2_photo;
    }

    public void setD2_photo(String d2_photo) {
        this.d2_photo = d2_photo;
    }

    public String getIdcartd1_photo() {
        return idcartd1_photo;
    }

    public void setIdcartd1_photo(String idcartd1_photo) {
        this.idcartd1_photo = idcartd1_photo;
    }

    public String getIdcartd2_photo() {
        return idcartd2_photo;
    }

    public void setIdcartd2_photo(String idcartd2_photo) {
        this.idcartd2_photo = idcartd2_photo;
    }

    public int getCar_type() {
        return car_type;
    }

    public void setCar_type(int car_type) {
        this.car_type = car_type;
    }

    public String getCartypMsg() {
        switch (car_type) {
            case 1:
                return "轿车";
            case 2:
                return "货车";
        }
        return "未知";
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getCar_owner() {
        return car_owner;
    }

    public void setCar_owner(String car_owner) {
        this.car_owner = car_owner;
    }

    public String getIdcard_no() {
        return idcard_no;
    }

    public void setIdcard_no(String idcard_no) {
        this.idcard_no = idcard_no;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getCar_register_date() {
        return car_register_date;
    }

    public void setCar_register_date(String car_register_date) {
        this.car_register_date = car_register_date;
    }


    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEngine_no() {
        return engine_no;
    }

    public void setEngine_no(String engine_no) {
        this.engine_no = engine_no;
    }

    public int getTci_status() {
        return tci_status;
    }

    public void setTci_status(int tci_status) {
        this.tci_status = tci_status;
    }

    public String getTci_date() {
        return tci_date;
    }

    public void setTci_date(String tci_date) {
        this.tci_date = tci_date;
    }

    public String getVci_date() {
        return vci_date;
    }

    public void setVci_date(String vci_date) {
        this.vci_date = vci_date;
    }

    public String getVci_settings() {
        return vci_settings;
    }

    public void setVci_settings(String vci_settings) {
        this.vci_settings = vci_settings;
    }

    public String getIdcard_photo1() {
        return idcard_photo1;
    }

    public void setIdcard_photo1(String idcard_photo1) {
        this.idcard_photo1 = idcard_photo1;
    }

    public String getIdcard_photo2() {
        return idcard_photo2;
    }

    public void setIdcard_photo2(String idcard_photo2) {
        this.idcard_photo2 = idcard_photo2;
    }

    public String getDl_photo1() {
        return dl_photo1;
    }

    public void setDl_photo1(String dl_photo1) {
        this.dl_photo1 = dl_photo1;
    }

    public String getDl_photo2() {
        return dl_photo2;
    }

    public void setDl_photo2(String dl_photo2) {
        this.dl_photo2 = dl_photo2;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getTransfer_car() {
        return transfer_car;
    }

    public String getTransferCarMsg() {
        if(transfer_car == 1) {
            return "是";
        }
        return "否";
    }

    public void setTransfer_car(int transfer_car) {
        this.transfer_car = transfer_car;
    }

    public int getNew_car() {
        return new_car;
    }

    public String getNewCarMsg() {
        switch (new_car) {
            case 1:
                return "新车";
            case 2:
                return "旧车";
        }
        return "未知";
    }

    public void setNew_car(int new_car) {
        this.new_car = new_car;
    }

    public long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(long create_time) {
        this.create_time = create_time;
    }

    public long getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(long update_time) {
        this.update_time = update_time;
    }

    public String getOrg_no() {
        return org_no;
    }

    public void setOrg_no(String org_no) {
        this.org_no = org_no;
    }

    public String getOrg_photo() {
        return org_photo;
    }

    public void setOrg_photo(String org_photo) {
        this.org_photo = org_photo;
    }

    public String getOrg1_photo() {
        return org1_photo;
    }

    public void setOrg1_photo(String org1_photo) {
        this.org1_photo = org1_photo;
    }

    public int getUseage() {
        return useage;
    }

    public void setUseage(int useage) {
        this.useage = useage;
    }

    public String getUseageMsg() {
        switch (useage) {
            case 1:
                return "营运";
            case 2:
                return "非营运";
        }
        return "未知";
    }

    public String getCar_bak() {
        return car_bak;
    }

    public void setCar_bak(String car_bak) {
        this.car_bak = car_bak;
    }

    public String getOutput_volume() {
        return output_volume;
    }

    public void setOutput_volume(String output_volume) {
        this.output_volume = output_volume;
    }

    public String getCurb_weight() {
        return curb_weight;
    }

    public void setCurb_weight(String curb_weight) {
        this.curb_weight = curb_weight;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(customer_id);
        parcel.writeString(plate_no);
        parcel.writeString(car_owner);
        parcel.writeString(idcard_no);
        parcel.writeString(mobile_phone);
        parcel.writeString(car_register_date);
        parcel.writeInt(transfer_car);
        parcel.writeInt(new_car);
        parcel.writeString(vin);
        parcel.writeString(engine_no);
        parcel.writeInt(tci_status);
        parcel.writeString(tci_date);
        parcel.writeString(vci_date);
        parcel.writeString(vci_settings);
        parcel.writeString(idcard_photo1);
        parcel.writeString(idcard_photo2);
        parcel.writeString(dl_photo1);
        parcel.writeString(dl_photo2);
        parcel.writeString(user_id);
        parcel.writeString(org_no);
        parcel.writeString(org_photo);
        parcel.writeString(d1_photo);
        parcel.writeString(d2_photo);
        parcel.writeString(idcartd1_photo);
        parcel.writeString(idcartd2_photo);
        parcel.writeString(org1_photo);
        parcel.writeInt(useage);
        parcel.writeString(car_bak);
        parcel.writeLong(create_time);
        parcel.writeLong(update_time);
        parcel.writeInt(car_type);
        parcel.writeString(output_volume);
        parcel.writeString(curb_weight);
    }
}
