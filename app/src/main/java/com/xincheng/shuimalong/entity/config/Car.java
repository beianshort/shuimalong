package com.xincheng.shuimalong.entity.config;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaote on 2017/7/7.
 */

public class Car {
    @SerializedName("data")
    private List<Data> carDataList;

    public List<Data> getCarDataList() {
        return carDataList;
    }

    public void setCarDataList(List<Data> carDataList) {
        this.carDataList = carDataList;
    }

    public static class Data implements Parcelable {

        @SerializedName("car_id")
        private int carId;
        @SerializedName("make_name")
        private String makeName;
        @SerializedName("model_name")
        private String modelName;
        @SerializedName("type_name")
        private String typeName;
        private String transmission;

        public Data() {

        }

        public Data(int carId, String makeName, String modelName, String typeName, String transmission) {
            this.carId = carId;
            this.makeName = makeName;
            this.modelName = modelName;
            this.typeName = typeName;
            this.transmission = transmission;
        }

        protected Data(Parcel in) {
            carId = in.readInt();
            makeName = in.readString();
            modelName = in.readString();
            typeName = in.readString();
            transmission = in.readString();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public int getCarId() {
            return carId;
        }

        public void setCarId(int carId) {
            this.carId = carId;
        }

        public String getMakeName() {
            return makeName;
        }

        public void setMakeName(String makeName) {
            this.makeName = makeName;
        }

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public String getTransmission() {
            return transmission;
        }

        public void setTransmission(String transmission) {
            this.transmission = transmission;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(carId);
            parcel.writeString(makeName);
            parcel.writeString(modelName);
            parcel.writeString(typeName);
            parcel.writeString(transmission);
        }
    }
}
