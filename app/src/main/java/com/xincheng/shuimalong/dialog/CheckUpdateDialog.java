package com.xincheng.shuimalong.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.pgyersdk.javabean.AppBean;
import com.pgyersdk.update.PgyUpdateManager;
import com.pgyersdk.update.UpdateManagerListener;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.shuimalong.R;

public class CheckUpdateDialog {
    private Activity activity;
    private boolean isDebug;

    public CheckUpdateDialog(Activity activity, boolean isDebug) {
        this.activity = activity;
        this.isDebug = isDebug;
    }

    public void checkUpdate() {
        if (isDebug) {
            PgyUpdateManager.register(activity, new UpdateManagerListener() {
                @Override
                public void onUpdateAvailable(String result) {
                    final AppBean appBean = getAppBeanFromString(result);
                    if (Integer.parseInt(appBean.getVersionCode()) >
                            CommonUtil.getVersionCode(activity, activity.getPackageName())) {
                        new AlertDialog.Builder(activity).setTitle(R.string.update_hint)
                                .setMessage(appBean.getReleaseNote())
                                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        startDownloadTask(activity, appBean.getDownloadURL());
                                    }
                                })
                                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).create().show();
                    }

                }

                @Override
                public void onNoUpdateAvailable() {

                }
            });
        }
    }

}
