package com.xincheng.shuimalong.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.xincheng.shuimalong.R;


/**
 * Created by xuhao on 2016/12/14.
 */

public class FriendDialog extends Dialog {
    private static FriendDialog customProgressDialog = null;
    TextView tvMove;
    TextView tvDelete;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FriendDialog(Context context) {
        super(context);
    }

    public FriendDialog(Context context, int theme) {
        super(context, theme);
    }

    public static FriendDialog createDialog(Context context) {
        customProgressDialog = new FriendDialog(context,R.style.dialog);
        customProgressDialog.setContentView(R.layout.dialog_friend_layout);
        customProgressDialog.setCanceledOnTouchOutside(true);
        Window win = customProgressDialog.getWindow();
        win.setGravity(Gravity.BOTTOM);
        win.getDecorView().setPadding(0, 0, 0, 0); //消除边距
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        win.setAttributes(lp);
        win.setWindowAnimations(R.style.dialoganimstyle);
        win.findViewById(R.id.tv_cause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customProgressDialog.dismiss();
            }
        });
        return customProgressDialog;
    }
    public FriendDialog setTvMoveGone(){
        tvMove= (TextView) customProgressDialog.getWindow().findViewById(R.id.tv_move);
        tvMove.setVisibility(View.GONE);
        return  this;
    }
    public FriendDialog setTvMoveListener(View.OnClickListener listener){
        tvMove= (TextView) customProgressDialog.getWindow().findViewById(R.id.tv_move);
        tvMove.setOnClickListener(listener);
        return this;
    }
    public FriendDialog setTvDeleteListener(View.OnClickListener listener){
        tvDelete= (TextView) customProgressDialog.getWindow().findViewById(R.id.tv_delete);
        tvDelete.setOnClickListener(listener);
        return this;
    }
}
