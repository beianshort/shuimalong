package com.xincheng.shuimalong.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.shuimalong.R;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by xuhao on 2016/12/14.
 */

public class GroupDialog extends Dialog {
    TextView tvConfirm;
    ConfirmListener confirmListener;
    EditText etInputGroup;

    public GroupDialog setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
        return this;
    }

    public GroupDialog(Context context) {
        super(context);
        initView(context);
    }

    public GroupDialog(Context context, int theme) {
        super(context, theme);
        initView(context);
    }

    public static GroupDialog createDialog(Context context) {
        return new GroupDialog(context, R.style.dialog);
    }

    private void initView(final Context context) {
        setContentView(R.layout.dialog_addgroup);
        setCanceledOnTouchOutside(true);
        Window win = getWindow();
        win.setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        win.setAttributes(lp);
        etInputGroup= (EditText) win.findViewById(R.id.et_input_group);
        win.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        win.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmListener != null) {
                    String groupName=etInputGroup.getText().toString();
                    if(TextUtils.isEmpty(groupName)){
                        Toast.makeText(context,"请输入分组名",Toast.LENGTH_SHORT).show();
                    }else{
                        confirmListener.confirmListener(groupName);
                    }

                }
            }
        });
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                etInputGroup.setText("");
            }
        });
    }
    public interface ConfirmListener {
        void confirmListener(String message);
    }
}
