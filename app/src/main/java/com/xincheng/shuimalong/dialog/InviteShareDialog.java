package com.xincheng.shuimalong.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.umeng.socialize.bean.SHARE_MEDIA;
import com.xincheng.shuimalong.R;

/**
 * Created by 许浩 on 2017/7/25.
 */

public class InviteShareDialog extends Dialog implements View.OnClickListener {
    onShareClick shareClick;
    private int share_type;

    public void setShare_type(int share_type) {
        this.share_type = share_type;
    }

    public void setShareClick(onShareClick shareClick) {
        this.shareClick = shareClick;
    }

    public InviteShareDialog(@NonNull Context context) {
        super(context);
        createDialog(context);
    }

    public InviteShareDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        createDialog(context);
    }
    public void createDialog(Context context ) {
        setContentView(R.layout.share_dialog_layout);
        setCanceledOnTouchOutside(true);
        Window win = getWindow();
        win.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;   //设置宽度充满屏幕
        win.setAttributes(lp);
        win.setWindowAnimations(R.style.dialoganimstyle);
        win.findViewById(R.id.tv_share_qq).setOnClickListener(this);
        win.findViewById(R.id.tv_share_qzone).setOnClickListener(this);
        win.findViewById(R.id.tv_share_wx).setOnClickListener(this);
        win.findViewById(R.id.tv_share_wxf).setOnClickListener(this);
//        win.findViewById(R.id.tv_share_sina).setOnClickListener(this);
        win.findViewById(R.id.btn_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.tv_share_sina:
//                setShareCode(SHARE_MEDIA.SINA);
//                break;
            case R.id.tv_share_wxf:
                setShareCode(SHARE_MEDIA.WEIXIN_CIRCLE);
                break;
            case R.id.tv_share_wx:
                setShareCode(SHARE_MEDIA.WEIXIN);
                break;
            case R.id.tv_share_qzone:
                setShareCode(SHARE_MEDIA.QZONE);
                break;
            case R.id.tv_share_qq:
                setShareCode(SHARE_MEDIA.QQ);
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    private void  setShareCode(SHARE_MEDIA code){
        dismiss();
        if(shareClick!=null){
            shareClick.share(code,share_type);
        }
    }
    public interface onShareClick{
        void share(SHARE_MEDIA shareCode,int shareType);
    }
}
