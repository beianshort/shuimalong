package com.xincheng.shuimalong.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;

import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/6/20.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
