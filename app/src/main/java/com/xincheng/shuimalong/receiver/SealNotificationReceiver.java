package com.xincheng.shuimalong.receiver;

import android.content.Context;
import android.os.Bundle;

import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.insurance.ChatActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.entity.friend.ChatMessage;
import com.xincheng.shuimalong.entity.friend.Friend;
import com.xincheng.shuimalong.notifination.ChatNotifination;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import io.rong.imkit.RongIM;
import io.rong.push.notification.PushMessageReceiver;
import io.rong.push.notification.PushNotificationMessage;

/**
 * Created by xuhao on 2017/7/17.
 */

public class SealNotificationReceiver extends PushMessageReceiver {

    @Override
    public boolean onNotificationMessageArrived(Context context, PushNotificationMessage pushNotificationMessage) {
        // 返回 false, 会弹出融云 SDK 默认通知; 返回 true, 融云 SDK 不会弹通知, 通知需要由您自定义。
        return false;
    }

    @Override
    public boolean onNotificationMessageClicked(Context context, PushNotificationMessage pushNotificationMessage) {
        // 返回 false, 会走融云 SDK 默认处理逻辑, 即点击该通知会打开会话列表或会话界面; 返回 true, 则由您自定义处理逻辑。
       //打开自己的聊天页面
        XLog.e(pushNotificationMessage.getPushData());
        RongIM.getInstance().startPrivateChat(context,pushNotificationMessage.getTargetId(),pushNotificationMessage.getSenderName());
        return true;
    }
}
