package com.xincheng.shuimalong.activity.market;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.xincheng.library.widget.RangeSeekBar;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.common.IntentKey;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/16.
 * 市场筛选（是否新车、商业险推广费）
 */

public class MarketFilterActivity extends BaseActivity {
    @BindView(R.id.filter_new_cb)
    CheckedTextView mNewCheckTv;
    @BindView(R.id.filter_old_cb)
    CheckedTextView mOldCheckTv;
    @BindView(R.id.filter_range_tv)
    TextView mRangeTv;
    @BindView(R.id.filter_useage_rg)
    RadioGroup mUseAgeRg;
    @BindView(R.id.filter_range_seekbar)
    RangeSeekBar mRangeSeekBar;

    private boolean mNewChecked;
    private boolean mOldChecked;
    private int mMin;
    private int mMax;
    private int mUseAge;

    @Override
    public int getLayoutResId() {
        return R.layout.market_filter_layout;
    }

    @Override
    public void initData() {
        Bundle extras = getIntent().getExtras();
        mMin = extras.getInt(IntentKey.MIN, getResources().getInteger(R.integer.range_min_default));
        mMax = extras.getInt(IntentKey.MAX, getResources().getInteger(R.integer.range_max_default));
        mUseAge = extras.getInt(IntentKey.USE_AGE, 2);
        initTitleBar(getString(R.string.filter));
        mRangeTv.setText(getString(R.string.range_section, mMin + "%", mMax + "%"));
        mRangeSeekBar.setValue(mMin, mMax);
        int newCarId = extras.getInt(IntentKey.NEW_CAR_ID, 0);
        switch (newCarId) {
            case 0:
                mNewChecked = false;
                mOldChecked = false;
                break;
            case 1:
                mNewChecked = true;
                mOldChecked = false;
                break;
            case 2:
                mNewChecked = false;
                mOldChecked = true;
                break;
            case 3:
                mNewChecked = true;
                mOldChecked = true;
                break;
        }
        switch (mUseAge) {
            case 1:
                mUseAgeRg.check(R.id.filter_operation_rb);
                break;
            case 2:
                mUseAgeRg.check(R.id.filter_operation_not_rb);
                break;
        }
        mNewCheckTv.setChecked(mNewChecked);
        mOldCheckTv.setChecked(mOldChecked);
    }

    @OnClick({R.id.filter_new_layout, R.id.filter_old_layout})
    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.filter_new_layout:
                mNewChecked = !mNewChecked;
                mNewCheckTv.setChecked(mNewChecked);
                break;
            case R.id.filter_old_layout:
                mOldChecked = !mOldChecked;
                mOldCheckTv.setChecked(mOldChecked);
                break;
        }
    }

    @OnClick(R.id.filter_btn)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.filter_btn:
                boolean showNewCar;
                int newCarId;
                if (mNewChecked && mOldChecked) {
                    showNewCar = false;
                    newCarId = 3;
                } else if (mNewChecked) {
                    showNewCar = true;
                    newCarId = 1;
                } else if (mOldChecked) {
                    showNewCar = true;
                    newCarId = 2;
                } else {
                    showNewCar = false;
                    newCarId = 0;
                }
                Intent intent = new Intent();
                intent.putExtra(IntentKey.SHOW_NEW_CAR, showNewCar);
                intent.putExtra(IntentKey.NEW_CAR_ID, newCarId);
                intent.putExtra(IntentKey.MIN, mMin);
                intent.putExtra(IntentKey.MAX, mMax);
                intent.putExtra(IntentKey.USE_AGE, mUseAge);
                setResult(RESULT_OK, intent);
                finish();
                XLog.e(mMin + "," + mMax);
                break;
        }
    }

    @Override
    public void addListener() {
        mRangeSeekBar.setOnRangeChangedListener(new MyOnRangeChangedListener());
        mUseAgeRg.setOnCheckedChangeListener(new MyOnCheckedChangedListener());
    }

    private class MyOnRangeChangedListener implements RangeSeekBar.OnRangeChangedListener {

        @Override
        public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {
            mMin = (int) min;
            mMax = (int) max;
            mRangeTv.setText(getString(R.string.range_section, mMin + "%", mMax + "%"));
        }
    }

    private class MyOnCheckedChangedListener implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            switch (checkedId) {
                case R.id.filter_operation_rb:
                    mUseAge = 1;
                    break;
                case R.id.filter_operation_not_rb:
                    mUseAge = 2;
                    break;
            }
        }
    }
}
