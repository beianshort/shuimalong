package com.xincheng.shuimalong.activity.mydetail;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.xincheng.library.manager.ExitManager;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.FileUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.widget.alertview.AlertView;
import com.xincheng.library.widget.alertview.OnAlertItemClickListener;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.login.LoginActivity;
import com.xincheng.shuimalong.activity.mydetail.setting.BindPhoneActivity;
import com.xincheng.shuimalong.activity.mydetail.setting.MessageActivity;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.MessageConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import io.rong.imkit.RongIM;

/**
 * Created by 许浩 on 2017/6/18.
 */

public class SettingActivity extends BaseActivity {
    @BindView(R.id.switch_message)
    SwitchCompat switchMessage;
    @BindView(R.id.tv_cache_size)
    TextView tvCacheSize;
    @BindView(R.id.tv_version_name)
    TextView tvVersioName;

    private File mBaseFile;
    private String mMobile;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_setting_layout;
    }

    @Override
    public void initData() {
        initTitleBar("设置");
        mBaseFile = MyUtil.getOutputBaseFile(this, Constant.ID_CARD_PATH);
        ExitManager.getInstance().addActivity(this);
        mMobile = PrefUtil.getString(this, PrefKey.LOGIN_MOBILE, "");
        boolean isChecked = PrefUtil.getBoolean(this, PrefKey.LOGIN_OPEN_NOTICE + mMobile, true);
        switchMessage.setChecked(isChecked);
        try {
            String fileSize = FileUtil.formatFileSize(FileUtil.getFileSizes(mBaseFile));
            tvCacheSize.setText(fileSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvVersioName.setText(getString(R.string.version_name_value,
                CommonUtil.getVersionName(this, this.getPackageName())));
    }

    @Override
    public void addListener() {

    }

    @OnCheckedChanged(R.id.switch_message)
    public void messageCheckChanged(CompoundButton button, boolean isChecked) {
        PrefUtil.putBoolean(this, PrefKey.LOGIN_OPEN_NOTICE + mMobile, isChecked);
    }

    @OnClick({R.id.rl_change_pay_pwd, R.id.rl_change_pwd, R.id.rl_clear_cache, R.id.login_out, R.id.rl_about_us, R.id.rl_call_us, R.id.rl_clause})
    public void onViewClicked(View view) {
        Bundle extras = null;
        switch (view.getId()) {
            case R.id.rl_change_pay_pwd:
                extras = new Bundle();
                extras.putBoolean(IntentKey.MODIFY_FOR_PAYPWD, true);
                ShowActivity.showActivity(getContext(), BindPhoneActivity.class, extras);
                break;
            case R.id.rl_change_pwd:
                ShowActivity.showActivity(getContext(), BindPhoneActivity.class);
                break;
            case R.id.rl_clear_cache:
                showClearCacheAlert();
                break;
            case R.id.login_out:
                showExitAlert();
                break;
            case R.id.rl_about_us:
                extras = new Bundle();
                extras.putInt(IntentKey.INTENT_FLAG, MessageConfig.ABOUT_US);
                ShowActivity.showActivity(getContext(), MessageActivity.class, extras);
                break;
            case R.id.rl_call_us:
                extras = new Bundle();
                extras.putInt(IntentKey.INTENT_FLAG, MessageConfig.CONTACTUS);
                ShowActivity.showActivity(getContext(), MessageActivity.class, extras);
                break;
            case R.id.rl_clause:
                extras = new Bundle();
                extras.putInt(IntentKey.INTENT_FLAG, MessageConfig.LAW);
                ShowActivity.showActivity(getContext(), MessageActivity.class, extras);
                break;
        }
    }

    private void showClearCacheAlert() {
        new AlertView(this, null, "确定要清除缓存吗？", getString(R.string.cancel),
                new String[]{getString(R.string.confirm)}, null,
                AlertView.Style.Alert, new OnAlertItemClickListener() {
            @Override
            public void onItemClick(AlertView alertView, int position) {
                switch (position) {
                    case 0:
                        FileUtil.deleteFiles(mBaseFile);
                        tvCacheSize.setText(FileUtil.DEFAULT_FILE_SIZE);
                        break;
                }
            }
        }).show();
    }

    private void showExitAlert() {
        new AlertView(this, null, "确定要退出当前账号吗？", getString(R.string.cancel),
                new String[]{getString(R.string.confirm)}, null,
                AlertView.Style.Alert, new OnAlertItemClickListener() {
            @Override
            public void onItemClick(AlertView alertView, int position) {
                switch (position) {
                    case 0:
                        removeLoginInfo();
                        ExitManager.getInstance().exitActivities();
                        ShowActivity.showActivity(getContext(), LoginActivity.class);
                        RongIM.getInstance().logout();
                        finish();
                        break;
                }
            }
        }).show();
    }

    private void removeLoginInfo() {
        PrefUtil.removeKey(getContext(), PrefKey.IS_LOGIN);
        PrefUtil.removeKey(getContext(), PrefKey.LOGIN_NICKNAME);
        PrefUtil.removeKey(getContext(), PrefKey.LOGIN_PASSWORD);
        PrefUtil.removeKey(getContext(), PrefKey.TOKEN);
        PrefUtil.removeKey(getContext(), PrefKey.LOGIN_USER_ID);
        PrefUtil.removeKey(getContext(), PrefKey.LOGIN_FACE);
        PrefUtil.removeKey(getContext(), PrefKey.LOGIN_CERT);
        PrefUtil.removeKey(getContext(), PrefKey.LOGIN_SET_PAYPWD);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

}
