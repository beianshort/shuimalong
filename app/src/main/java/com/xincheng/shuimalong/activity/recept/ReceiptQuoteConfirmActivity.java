package com.xincheng.shuimalong.activity.recept;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.Order;
import com.xincheng.shuimalong.entity.OrderDetail;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

/**
 * Created by xiaote on 2017/6/28.
 * 收单信息确认报价
 */

public class ReceiptQuoteConfirmActivity extends BaseActivity {
    @BindView(R.id.quote_total_money_tv)
    TextView mTotalMoneyTv;
    @BindView(R.id.quote_vci_money_et)
    EditText mVciMoneyEt;
    @BindView(R.id.quote_tci_money_et)
    EditText mTciMoneyEt;
    @BindView(R.id.quote_vvt_money_et)
    EditText mVvtMoneyEt;
    @BindView(R.id.quote_putin_money_tv)
    TextView mPutinMoneyTv;
    @BindView(R.id.quote_vci_rb_tv)
    TextView mVciRbTv;
    @BindView(R.id.quote_tci_rb_tv)
    TextView mTciRbTv;

    private String mToken;
    private int mBidId;
    private int mVciRb;
    private int mTciRb;
    private String mVciMoney;
    private String mTciMoney;
    private String mVvtMoney;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_order_to_quote;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.order_quote));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        Bundle extras = getIntent().getExtras();
        mBidId = extras.getInt(IntentKey.BID_ID);
        mVciRb = extras.getInt(IntentKey.VCI_RB);
        mTciRb = extras.getInt(IntentKey.TCI_RB);
        mVciRbTv.setText(Constant.getPercentData(mVciRb));
        mTciRbTv.setText(Constant.getPercentData(mTciRb));
    }

    @OnTextChanged(value = R.id.quote_vci_money_et, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged1(Editable s) {
        double money = 0;
        double putInMoney = 0;
        if (s.length() >= 2 && s.toString().startsWith("0") && !s.toString().contains(".")) {
            showToast("首位不能为0");
            return;
        }
        if (!CommonUtil.isEmpty(s.toString()) && !s.toString().endsWith(".")) {
            double vicMoney = Double.parseDouble(s.toString());
            money += vicMoney;
            putInMoney += vicMoney * mVciRb / 100;
        } else {
            money = 0;
            putInMoney = 0;
        }
        if (!CommonUtil.isEmpty(mTciMoneyEt)) {
            double tciMoney = Double.parseDouble(mTciMoneyEt.getText().toString().trim());
            money += tciMoney;
            putInMoney += tciMoney * mTciRb / 100;
        }
        if (!CommonUtil.isEmpty(mVvtMoneyEt)) {
            money += Double.parseDouble(mVvtMoneyEt.getText().toString().trim());
        }
        if (money != 0) {
            mTotalMoneyTv.setText(getString(R.string.money_value, CommonUtil.getMoneyStr(money)));
        } else {
            mTotalMoneyTv.setText("");
        }
        if (putInMoney != 0) {
            mPutinMoneyTv.setText(getString(R.string.money_value, CommonUtil.getMoneyStr(putInMoney)));
        } else {
            mPutinMoneyTv.setText("");
        }
    }

    @OnTextChanged(value = R.id.quote_tci_money_et, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged2(Editable s) {
        double money = 0;
        double putInMoney = 0;
        if (s.length() >= 2 && s.toString().startsWith("0") && !s.toString().contains(".")) {
            showToast("首位不能为0");
            return;
        }
        if (!CommonUtil.isEmpty(s.toString()) && !s.toString().endsWith(".")) {
            double tciMoney = Double.parseDouble(s.toString());
            money += tciMoney;
            putInMoney += tciMoney * mTciRb / 100;
        }
        if (!CommonUtil.isEmpty(mVciMoneyEt)) {
            double vicMoney = Double.parseDouble(mVciMoneyEt.getText().toString().trim());
            money += vicMoney;
            putInMoney += vicMoney * mVciRb / 100;
        }
        if (!CommonUtil.isEmpty(mVvtMoneyEt)) {
            money += Double.parseDouble(mVvtMoneyEt.getText().toString().trim());
        }
        if (money != 0) {
            mTotalMoneyTv.setText(getString(R.string.money_value, CommonUtil.getMoneyStr(money)));
        } else {
            mTotalMoneyTv.setText("");
        }
        if (putInMoney != 0) {
            mPutinMoneyTv.setText(getString(R.string.money_value, CommonUtil.getMoneyStr(putInMoney)));
        } else {
            mPutinMoneyTv.setText("");
        }
    }

    @OnTextChanged(value = R.id.quote_vvt_money_et, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged3(Editable s) {
        double money = 0;
        if (s.length() >= 2 && s.toString().startsWith("0") && !s.toString().contains(".")) {
            showToast("首位不能为0");
            return;
        }
        if (!CommonUtil.isEmpty(s.toString()) && !s.toString().endsWith(".")) {
            money += Double.parseDouble(s.toString());
        }
        if (!CommonUtil.isEmpty(mVciMoneyEt)) {
            money += Double.parseDouble(mVciMoneyEt.getText().toString().trim());
        }
        if (!CommonUtil.isEmpty(mTciMoneyEt)) {
            money += Double.parseDouble(mTciMoneyEt.getText().toString().trim());
        }
        if (money != 0) {
            mTotalMoneyTv.setText(getString(R.string.money_value, CommonUtil.getMoneyStr(money)));
        } else {
            mTotalMoneyTv.setText("");
        }
    }

    private boolean checkData() {
        if (CommonUtil.isEmpty(mVciMoneyEt)) {
            showToast("请输入商业险金额");
            return false;
        }
        if (CommonUtil.isEmpty(mTciMoneyEt)) {
            showToast("请输入交强险金额");
            return false;
        }
        if (CommonUtil.isEmpty(mVvtMoneyEt)) {
            showToast("请输入车船税金额");
            return false;
        }
        return true;
    }

    @Override
    public void addListener() {

    }

    private void sendPrice() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.SEND_PRICE,
                PostParam.sendPrice(mToken, mBidId, mTciMoney, mVciMoney, mVvtMoney),
                new BaseObserver(this, getString(R.string.loading_submit)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg);
                        Intent intent = new Intent();
                        intent.putExtra(IntentKey.QUOTE_SUCCESS, true);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
    }

    @OnClick({R.id.quote_submit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.quote_submit_btn:
                if (checkData()) {
                    double vciMoney = Double.parseDouble(mVciMoneyEt.getText().toString().trim());
                    double tciMoney = Double.parseDouble(mTciMoneyEt.getText().toString().trim());
                    double vvtMoney = Double.parseDouble(mVvtMoneyEt.getText().toString().trim());
                    if (vciMoney == 0 && tciMoney == 0 && vvtMoney == 0) {
                        showToast("报价的金额不能为零");
                        return;
                    }
                    mVciMoney = CommonUtil.getMoneyStr(vciMoney);
                    mTciMoney = CommonUtil.getMoneyStr(tciMoney);
                    mVvtMoney = CommonUtil.getMoneyStr(vvtMoney);
                    sendPrice();
                }
                break;
        }
    }
}
