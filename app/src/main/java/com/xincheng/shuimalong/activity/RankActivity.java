package com.xincheng.shuimalong.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.xincheng.library.util.AESUtil;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.login.LoginActivity;
import com.xincheng.shuimalong.adapter.RankAdapter;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.AppConfig;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.dialog.CheckUpdateDialog;
import com.xincheng.shuimalong.entity.user.LoginInfo;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.entity.rank.Rank;
import com.xincheng.shuimalong.entity.rank.RankData;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote1988 on 2017/7/29.
 */

public class RankActivity extends BaseActivity {
    @BindView(R.id.rank_num_total_tv)
    TextView mNumTotalTv;
    @BindView(R.id.rank_money_total_tv)
    TextView mMoneyTotalTv;
    @BindView(R.id.rank_mrv)
    RecyclerView mRankRv;

    private boolean mIsLogin;
    private String mToken;
    private RankAdapter mAdapter;

    private String mMobile;
    private String mPassword;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_rank;
    }

    @Override
    public void initData() {
        new CheckUpdateDialog(this, !AppConfig.IS_DEBUG).checkUpdate();
        mIsLogin = PrefUtil.getBoolean(this, PrefKey.IS_LOGIN, false);
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        mRankRv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RankAdapter(this, GlideOptionsManager.getInstance().getRequestOptions());
        mRankRv.setAdapter(mAdapter);
        getRank();
    }

    private void getRank() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getRank(mToken),
                new BaseObserver(this, getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            Rank rank = GsonUtil.parseData(data, Rank.class);
                            mNumTotalTv.setText(String.valueOf(rank.getNum()));
                            double totalMoney = CommonUtil.isEmpty(rank.getTotalMoney()) ? 0 :
                                    Double.parseDouble(rank.getTotalMoney());
                            mMoneyTotalTv.setText(CommonUtil.getMoneyStr(totalMoney));
                            List<Rank.Data> receiptList = rank.getReceiptList();
                            List<Rank.Data> sellList = rank.getSellList();
                            initRank(receiptList, sellList);
                        }
                    }

                    @Override
                    protected void onHandleError(String msg) {
                        openActivity();
                    }
                });
    }

    private void initRank(List<Rank.Data> receiptList, List<Rank.Data> sellList) {
        List<RankData> rankDatas = new ArrayList<>();
        int size1 = receiptList.size() < 5 ? receiptList.size() : 5;
        RankData rankData1 = new RankData("收单排行", size1, RankData.TYPE_TITLE);
        rankDatas.add(rankData1);
        for (int i = 0; i < size1; i++) {
            RankData rankData = new RankData(receiptList.get(i), RankData.TYPE_CONTENT);
            rankDatas.add(rankData);
        }
        int size2 = sellList.size() < 5 ? sellList.size() : 5;
        RankData rankData2 = new RankData("卖单排行", size2, RankData.TYPE_TITLE);
        rankDatas.add(rankData2);
        for (int i = 0; i < size2; i++) {
            RankData rankData = new RankData(sellList.get(i), RankData.TYPE_CONTENT);
            rankDatas.add(rankData);
        }
        mAdapter.refreshData(rankDatas);
    }

    @Override
    public void addListener() {

    }

    @OnClick(R.id.rank_enter_btn)
    public void rankEnter() {
        openActivity();
    }

    private void openActivity() {
        if (!mIsLogin) {
            openLoginActivity();
        } else {
            login();
        }
    }

    private void login() {
        String mobile = PrefUtil.getString(this, PrefKey.LOGIN_MOBILE, "");
        String password = AESUtil.decrypt(PrefUtil.getString(this, PrefKey.LOGIN_PASSWORD, ""),
                Constant.PASSWORD_SECRET);
        RetroSubscrube.getInstance().postSubscrube(AppUrl.MOBILE_LOGIN, PostParam.getLoginParam(mobile, password),
                new BaseObserver(this, getString(R.string.loading_login)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            LoginInfo loginInfo = GsonUtil.parseData(data, LoginInfo.class);
                            saveLoginInfo(loginInfo);
                            openMainActivity();
                        }
                    }

                    @Override
                    protected void onHandleError(String msg) {
                        openLoginActivity();
                    }
                });
    }

    private void saveLoginInfo(LoginInfo loginInfo) {
        PrefUtil.putBoolean(getContext(), PrefKey.IS_LOGIN, true);
        PrefUtil.putString(getContext(), PrefKey.LOGIN_NICKNAME, loginInfo.getNick_name());
        PrefUtil.putString(getContext(), PrefKey.TOKEN, loginInfo.getToken());
        PrefUtil.putString(getContext(), PrefKey.LOGIN_USER_ID, loginInfo.getUser_id());
        PrefUtil.putString(getContext(), PrefKey.LOGIN_FACE, loginInfo.getFace());
        PrefUtil.putBoolean(getContext(), PrefKey.LOGIN_CERT, loginInfo.isCert());
        PrefUtil.putBoolean(getContext(), PrefKey.LOGIN_SET_PAYPWD, loginInfo.isSetPayPwd());
        if (!CommonUtil.isEmpty(loginInfo.getReason())) {
            PrefUtil.putString(getContext(), PrefKey.LOGIN_CERT_REASON, loginInfo.getReason());
        }
    }

    private void openLoginActivity() {
        ShowActivity.showActivity(getContext(), LoginActivity.class);
        finish();
    }

    private void openMainActivity() {
        ShowActivity.showActivity(getContext(), MainActivity.class);
        finish();
    }
}
