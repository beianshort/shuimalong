package com.xincheng.shuimalong.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.AppCompatRatingBar;
import android.text.Editable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.xincheng.library.manager.ExitManager;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.AppConfig;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.annotations.NonNull;

/**
 * Created by xiaote on 2017/7/6.
 * 评价
 */

public class EvaluateActivity extends BaseActivity {
    @BindView(R.id.evaluate_rating_bar)
    AppCompatRatingBar mRatingBar;
    @BindView(R.id.evaluate_receipt_layout)
    LinearLayout mReceiptLayout;
    @BindView(R.id.evaluate_mail_order_rg)
    RadioGroup mMailOrderRg;//两天内寄回订单
    @BindView(R.id.evaluate_sell_layout)
    LinearLayout mSellLayout;
    @BindView(R.id.evaluate_quote_thirty_minute_rg)
    RadioGroup mQuoteThirtyMinuteRg;
    @BindView(R.id.evaluate_payoff_issue_sameday_rg)
    RadioGroup mPayoffIssueSamedayRg;
    @BindView(R.id.evaluate_note_et)
    EditText mNoteEt;
    @BindView(R.id.evaluate_note_length_tv)
    TextView mNoteLengthTv;
    @BindView(R.id.evaluate_anonymity_cb)
    TextView mAnonymityCb;

    private String mToken;
    private int mBidId;
    private int mOrderConfig;
    private int mStar = 5;      // 星级
    private int mMailOrder = 1; //两天内寄回订单
    private int mQuote = 1;     //30分钟内报价
    private int mIssue = 1;     //当天出单
    private String mNote = "";
    private int mAnonymity = 0; //匿名
    private List<CommonConfig> mBuyerIssue;
    private List<CommonConfig> mAcceptPrice;
    private List<CommonConfig> mAcceptIssue;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_evaluate;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.evaluate));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        Bundle extras = getIntent().getExtras();
        mBidId = extras.getInt(IntentKey.BID_ID);
        mOrderConfig = extras.getInt(IntentKey.ORDER_CONFIG);
        switch (mOrderConfig) {
            case OrderConfig.RECEIPT:
                mReceiptLayout.setVisibility(View.VISIBLE);
                mBuyerIssue = ConfigUtil.getCommonConfigList(this, PrefKey.BUYER_ISSUE);
                XLog.e("buyerIssue=>" + mBuyerIssue.toString());
                break;
            case OrderConfig.SELL:
                mSellLayout.setVisibility(View.VISIBLE);
                mAcceptPrice = ConfigUtil.getCommonConfigList(this, PrefKey.ACCEPT_PRICE);
                mAcceptIssue = ConfigUtil.getCommonConfigList(this, PrefKey.ACCEPT_ISSUE);
                XLog.e("acceptPrice=>" + mAcceptPrice.toString());
                XLog.e("acceptIssue=>" + mAcceptIssue.toString());
                break;
        }
    }


    @OnCheckedChanged(R.id.evaluate_anonymity_cb)
    public void isAnonymity(CompoundButton button, boolean isChecked) {
        if (isChecked) {
            mAnonymity = 1;
        } else {
            mAnonymity = 0;
        }
    }

    @OnTextChanged(value = R.id.evaluate_note_et, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        mNoteLengthTv.setText(String.valueOf(getResources().getInteger(R.integer.edit_length_default_200) -
                s.length()));
    }

    @OnClick(R.id.evaluate_submit_btn)
    public void evaluateSubmit() {
        if (checkData()) {
            mNote = mNoteEt.getText().toString().trim();
            toComment();
        }
    }

    private boolean checkData() {
        if (mStar == 0) {
            showToast("请选择满意度");
            return false;
        }
        return true;
    }

    @Override
    public void addListener() {
        MyOnCheckedChangedListener listener = new MyOnCheckedChangedListener();
        mMailOrderRg.setOnCheckedChangeListener(listener);
        mQuoteThirtyMinuteRg.setOnCheckedChangeListener(listener);
        mPayoffIssueSamedayRg.setOnCheckedChangeListener(listener);
        mRatingBar.setOnRatingBarChangeListener(new MyOnRatingChangedListener());
    }

    /**
     * 去评价
     */
    private void toComment() {
        BaseObserver observer = new BaseObserver(this, getString(R.string.loading_submit)) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                showToast(msg);
                Constant.SUCCESS = true;
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 2);
                extras.putString(IntentKey.TITLE_MESSAGE, getString(R.string.evaluate_success));
                extras.putString(IntentKey.SUCCESS_MESSAGE, getString(R.string.evaluate_success_message));
                ShowActivity.showActivityForResult(getContext(), SucceedActivity.class, extras,
                        RequestCodeKey.EVALUATE);
            }
        };
        switch (mOrderConfig) {
            case OrderConfig.RECEIPT:
                RetroSubscrube.getInstance().postSubscrube(AppUrl.AC_COMMENT,
                        PostParam.acComment(mToken, mBidId, mStar, mMailOrder, mNote, mAnonymity), observer);
                break;
            case OrderConfig.SELL:
                RetroSubscrube.getInstance().postSubscrube(AppUrl.BILL_COMMENT,
                        PostParam.billComment(mToken, mBidId, mStar, mQuote, mIssue, mNote, mAnonymity), observer);
                break;
        }
    }

    private class MyOnRatingChangedListener implements RatingBar.OnRatingBarChangeListener {
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            if (fromUser) {
                mStar = (int) rating;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.EVALUATE:
                    Intent intent = new Intent();
                    intent.putExtra(IntentKey.EVALUATE,true);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class MyOnCheckedChangedListener implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkId) {
            switch (radioGroup.getId()) {
                case R.id.evaluate_mail_order_rg:
                    switch (checkId) {
                        case R.id.evaluate_mail_order_yes_rb:
                            mMailOrder = mBuyerIssue.get(0).getId();
                            break;
                        case R.id.evaluate_mail_order_no_rb:
                            mMailOrder = mBuyerIssue.get(1).getId();
                            break;
                    }
                    break;
                case R.id.evaluate_quote_thirty_minute_rg:
                    switch (checkId) {
                        case R.id.evaluate_quote_thirty_minute_yes_rb:
                            mQuote = mAcceptPrice.get(0).getId();
                            break;
                        case R.id.evaluate_quote_thirty_minute_no_rb:
                            mQuote = mAcceptPrice.get(1).getId();
                            break;
                    }
                    break;
                case R.id.evaluate_payoff_issue_sameday_rg:
                    switch (checkId) {
                        case R.id.evaluate_payoff_issue_sameday_yes_rb:
                            mIssue = mAcceptIssue.get(0).getId();
                            break;
                        case R.id.evaluate_payoff_issue_sameday_no_rb:
                            mIssue = mAcceptPrice.get(1).getId();
                            break;
                    }
                    break;
            }
        }
    }
}
