package com.xincheng.shuimalong.activity.sell;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.widget.alertview.AlertView;
import com.xincheng.library.widget.pickerview.OptionsPickerView;
import com.xincheng.library.widget.pickerview.TimePickerView;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.layoutmanager.FullyLinearLayoutManager;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.library.xswipemenurecyclerview.widget.DividerListItemDecoration;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.UploadImageActivity;
import com.xincheng.shuimalong.activity.config.VciListActivity;
import com.xincheng.shuimalong.adapter.config.VciTypeOrderAdapter;
import com.xincheng.shuimalong.adapter.config.VciTypeSelectAdapter;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.OrderDetail;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.entity.config.VciTypeOrder;
import com.xincheng.shuimalong.entity.market.Market;
import com.xincheng.shuimalong.helper.AllCapTransformationMethod;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.util.BitmapUtil;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by xiaote on 2017/6/19.
 * 填写保单
 */

public class OrderFillInsuranceActivity extends BaseActivity {
    @BindView(R.id.policy_headicon_iv)
    ImageView mHeadIconIv;
    @BindView(R.id.policy_username_tv)
    TextView mUsernameTv;
    @BindView(R.id.policy_bak_tv)
    TextView mBakTv;
    @BindView(R.id.policy_city_tv)
    TextView mCityTv;
    @BindView(R.id.policy_insurance_company_tv)
    TextView mInsuranceCompanyTv;
    @BindView(R.id.policy_new_car_tv)
    TextView mNewCarTv;
    @BindView(R.id.policy_car_owner_et)
    EditText mCarOwnerEt;
    @BindView(R.id.policy_car_plate_et)
    EditText mCarPlateEt;
    @BindView(R.id.fill_info_carowner_mobile_et)
    EditText mCarOwnerMobileEt;
    @BindView(R.id.fill_info_idcardno_et)
    EditText mIdCardNoEt;
    @BindView(R.id.policy_usage_rg)
    RadioGroup mUsAgeRg;
    @BindView(R.id.fill_info_engine_et)
    EditText mEngineEt;
    @BindView(R.id.fill_info_vin_et)
    EditText mVinEt;
    @BindView(R.id.fill_info_carweight_et)
    EditText mCarWeightEt;
    @BindView(R.id.fill_info_vehicle_et)
    EditText mCarVehicleEt;
    @BindView(R.id.policy_car_register_date_tv)
    TextView mCarRegisterDateTv;
    @BindView(R.id.policy_transfer_switch)
    SwitchCompat mPolicyTransferSwitch;
    @BindView(R.id.policy_tci_payment_switch)
    SwitchCompat mTciPaymentSwitch;
    @BindView(R.id.policy_tci_take_effect_date_tv)
    TextView mTciTakeEffectDateTv;
    @BindView(R.id.policy_vci_take_effect_date_tv)
    TextView mVciTakeEffectDateTv;
    @BindView(R.id.policy_vci_select_mrv)
    MRecyclerView mVciSelectRv;
    @BindView(R.id.insurance_bak_et)
    EditText mBakEt;
    @BindView(R.id.insurance_bak_length_tv)
    TextView mBakLengthTv;
    @BindView(R.id.fill_info_seatnum_et)
    EditText mSeatNumEt;

    private VciTypeSelectAdapter mAdapter;
    private ArrayList<VciType> mVciTypes;
    private TimePickerView mCarRegistenerDatePicker;
    private TimePickerView mTciTimePicker;
    private TimePickerView mVciTimePicker;

    private String mCarRegisterDate = "";
    private String mTciDate = "";
    private String mVciDate = "";
    private int mTransfer = 2;
    private int mTciStatus = 1;
    private String mCarOwner = "";
    private String mPlateNo = "";
    private String mCarOwnerMobile = "";
    private String mIdCardNo = "";
    private String mToken = "";
    private String mIdCardPhoto1 = "";
    private String mIdCardPhoto2 = "";
    private String mDlPhoto1 = "";
    private String mDlPhoto2 = "";
    private String mOrgPhoto = "";
    private String mOrgNo = "";
    private String mOutputVolume = "";
    private String mCubWeight = "";
    private String mVin = "";
    private String mEngineNo = "";
    private String mCarVehicle = "";
    private int mUsAge = 2;
    private String mBak = "";
    private String mSeatNum = "";

    private int mGoFrom = 0;
    private String mIdcardImgPath1 = "", mIdcardImgPath2 = "";
    private String mDlImgPath1 = "", mDlImgPath2 = "";

    private OrderDetail orderDetail;

    @Override
    public int getLayoutResId() {
        return R.layout.market_fill_insurance_policy_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.change_insurance_policy));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        setViewMessage();
        initPicker();
        setTransformMethod();
    }

    private void setTransformMethod() {
        AllCapTransformationMethod transformationMethod = new AllCapTransformationMethod();
        mCarPlateEt.setTransformationMethod(transformationMethod);
        mEngineEt.setTransformationMethod(transformationMethod);
        mVinEt.setTransformationMethod(transformationMethod);
    }

    private void setViewMessage() {
        Bundle extras = getIntent().getExtras();
        orderDetail = extras.getParcelable(IntentKey.ORDER_DETAIL);
        RegionData province = ConfigUtil.getProvince(this, orderDetail.getProvinceId());
        RegionData city = ConfigUtil.getCity(this, orderDetail.getCityId());
        Glide.with(this).load(extras.getString(IntentKey.AVATAR)).apply(GlideOptionsManager.getInstance().getRequestOptions()).into(mHeadIconIv);
        mCityTv.setText(province.getName() + city.getName());
        mInsuranceCompanyTv.setText(ConfigUtil.getCommonConfigById(this, PrefKey.COMPANY_LIST,
                orderDetail.getCompanyId()).getName());
        mNewCarTv.setText(ConfigUtil.getCommonConfigById(this, PrefKey.NEW_CAR_TYPE, orderDetail.getNewCar()).getName());
        mVciSelectRv.setLayoutManager(new FullyLinearLayoutManager(this));
        mVciSelectRv.addItemDecoration(new DividerListItemDecoration(this));
        mVciTypes = initVciTypes();
        mAdapter = new VciTypeSelectAdapter(this, mVciTypes);
        mAdapter.setItemButtonClickListener(new MyItemButtonClickListener(this));
        mVciSelectRv.setAdapter(mAdapter);
        mUsernameTv.setText(extras.getString(IntentKey.NICK_NAME));
        mBakTv.setText(orderDetail.getBak());
        switch (orderDetail.getCarType()) {
            case 1:
                mCarWeightEt.setHint(getString(R.string.output_volume_hint));
                mCarWeightEt.setText(orderDetail.getOutputVolume());
                break;
            case 2:
                mCarWeightEt.setHint(getString(R.string.curb_weight_hint));
                mCarWeightEt.setText(orderDetail.getCurbWeight());
                break;
        }
        mCarOwnerEt.setText(orderDetail.getCarOwner());
        mCarPlateEt.setText(orderDetail.getPlateNo());
        mCarOwnerMobileEt.setText(orderDetail.getMobilePhone());
        mIdCardNoEt.setText(orderDetail.getIdcardNo());
        mCarVehicleEt.setText(orderDetail.getCarBak());
        mSeatNumEt.setText(orderDetail.getSeatNum());
        mEngineEt.setText(orderDetail.getEngineNo());
        mVinEt.setText(orderDetail.getVin());
        mBakEt.setText(orderDetail.getBak());
        mCarRegisterDate = orderDetail.getCarRegisterDate();
        mVciDate = orderDetail.getVciDate();
        mTciDate = orderDetail.getTciDate();
        mCarRegisterDateTv.setText(orderDetail.getCarRegisterDate());
        mTciTakeEffectDateTv.setText(orderDetail.getTciDate());
        mVciTakeEffectDateTv.setText(orderDetail.getVciDate());
        switch (orderDetail.getTransferCar()) {
            case 1:
                mPolicyTransferSwitch.setChecked(true);
                break;
            case 2:
                mPolicyTransferSwitch.setChecked(false);
                break;
        }
    }

    private ArrayList<VciType> initVciTypes() {
        ArrayList<VciType> vciTypes = new ArrayList<>();
        List<VciType> list = ConfigUtil.getVciTypes(this);
        for (int i = 0; i < Constant.VCITPE_SELECT_SIZE_DEFAULT; i++) {
            VciType vciType = list.get(i);
            vciType.setOption(vciType.getOptions().get(vciType.getSelectOptionIndex()));
            if (vciType.getId() == 1) {
                vciType.setNdis(true);
            }
            vciTypes.add(vciType);
        }
        if (!CommonUtil.isEmpty(orderDetail.getVciSettings())) {
            List<VciTypeOrder> vciTypeList = GsonUtil.parseData(
                    CommonUtil.getBase64DecodeStr(orderDetail.getVciSettings()),
                        new TypeToken<List<VciTypeOrder>>() {
                        }.getType());
            XLog.e("vicTypeList==>" + vciTypeList);
            if (!CommonUtil.isEmpty(vciTypeList)) {
                for (VciType order : vciTypes) {
                    order.setSelected(isSelect(order.getId(), vciTypeList));
                }
                return vciTypes;
            }
        }
        return vciTypes;
    }

    public boolean isSelect(int id, List<VciTypeOrder> vciTypeList) {
        for (VciTypeOrder order : vciTypeList) {
            if (id == order.getId()) {
                return true;
            }
        }
        return false;
    }

    @OnCheckedChanged({R.id.policy_transfer_switch, R.id.policy_tci_payment_switch})
    public void onButtonChecked(CompoundButton button, boolean isChecked) {
        switch (button.getId()) {
            case R.id.policy_transfer_switch:
                if (isChecked) {
                    mTransfer = 1;
                } else {
                    mTransfer = 2;
                }
                break;
            case R.id.policy_tci_payment_switch:
                if (isChecked) {
                    mTciStatus = 1;
                } else {
                    mTciStatus = 0;
                }
                break;
        }
    }

    private void initPicker() {
        Calendar selectDate = Calendar.getInstance();
        if(!CommonUtil.isEmpty(orderDetail.getCarRegisterDate())) {
            selectDate.setTimeInMillis(DateUtil.getTimeInMillis(orderDetail.getCarRegisterDate()));
        }
        Calendar selectedDate1 = DateUtil.getTomrow();
        selectedDate1.setTimeInMillis(DateUtil.getTimeInMillis2(orderDetail.getVciDate()));
        Calendar selectedDate2 = DateUtil.getTomrow();
        selectedDate2.setTimeInMillis(DateUtil.getTimeInMillis2(orderDetail.getTciDate()));
        Calendar startDate = Calendar.getInstance();
        startDate.set(Constant.YEAR_BEGIN, 0, 1);
        Calendar endDate = Calendar.getInstance();
        endDate.set(Constant.YEAR_END, 11, 31);

        mCarRegistenerDatePicker = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                mCarRegisterDate = DateUtil.parseDate(date);
                mCarRegisterDateTv.setText(DateUtil.getDate(date));
            }
        }).setDate(selectDate).isCyclic(true).setRangDate(startDate, endDate).setTitleText(getString(R.string
                .car_register_date)).setType(new boolean[]{true, true, true, false, false, false}).isCenterLabel
                (false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .build();
        mTciTimePicker = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                if (DateUtil.isBefore(date)) {
                    showToast(getString(R.string.commit_date));
                } else {
                    mTciDate = DateUtil.parseDate2(date);
                    mTciTakeEffectDateTv.setText(mTciDate);
                }
            }
        }).setDate(selectedDate2).isCyclic(true).setRangDate(startDate, endDate).setTitleText(getString(R.string
                .take_effect_date))
                .setType(new boolean[]{true, true, true, true, false, false})
                .setLabel("年", "月", "日", "时", null, null).isCenterLabel(false).build();
        mVciTimePicker = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) { //选中事件回调
                if (DateUtil.isBefore(date)) {
                    showToast(getString(R.string.commit_date));
                } else {
                    mVciDate = DateUtil.parseDate2(date);
                    mVciTakeEffectDateTv.setText(mVciDate);
                }
            }
        }).setDate(selectedDate1).isCyclic(true)
                .setRangDate(startDate, endDate).setTitleText(getString(R.string
                        .take_effect_date))
                .setType(new boolean[]{true, true, true, true, false, false})
                .setLabel("年", "月", "日", "时", null, null).isCenterLabel(false).build();
    }


    @OnClick({R.id.policy_car_register_date_tv, R.id.policy_add_vci_iv,
            R.id.policy_tci_take_effect_date_tv, R.id.policy_vci_take_effect_date_tv,
            R.id.policy_submit_btn, R.id.upload_idcard_iv, R.id.upload_car_drivingcard_iv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.policy_car_register_date_tv:
                if (mCarRegistenerDatePicker != null) {
                    mCarRegistenerDatePicker.show();
                }
                break;
            case R.id.policy_tci_take_effect_date_tv:
                if (mTciPaymentSwitch.isChecked() && mTciTimePicker != null) {
                    mTciTimePicker.show();
                }
                break;
            case R.id.policy_add_vci_iv:
                Bundle extras = new Bundle();
                if (!CommonUtil.isEmpty(mVciTypes)) {
                    extras.putParcelableArrayList(IntentKey.VCI_TYPE_LIST, mVciTypes);
                }
                ShowActivity.showActivityForResult(this, VciListActivity.class, extras,
                        RequestCodeKey.GO_SELECT_VCI_TYPE);
                break;
            case R.id.policy_vci_take_effect_date_tv:
                if (mVciTimePicker != null) {
                    mVciTimePicker.show();
                }
                break;
            case R.id.upload_idcard_iv:
                extras = new Bundle();
                mGoFrom = 1;
                extras.putInt(IntentKey.FROM, 1);
                extras.putString(IntentKey.UPLOAD_IMG_PATH1, mIdcardImgPath1);
                extras.putString(IntentKey.UPLOAD_IMG_PATH2, mIdcardImgPath2);
                extras.putString(IntentKey.UPLOAD_IMG_BASE641, orderDetail.getIdcardPhoto1());
                extras.putString(IntentKey.UPLOAD_IMG_BASE642, orderDetail.getIdcardPhoto2());
                extras.putBoolean(IntentKey.UPLOAD_FROM_FILL_INFO, true);
                ShowActivity.showActivityForResult(this, UploadImageActivity.class, extras,
                        RequestCodeKey.GO_UPLOAD_IMG);
                break;
            case R.id.upload_car_drivingcard_iv:
                extras = new Bundle();
                mGoFrom = 2;
                extras.putInt(IntentKey.FROM, 2);
                extras.putString(IntentKey.UPLOAD_IMG_PATH1, mDlImgPath1);
                extras.putString(IntentKey.UPLOAD_IMG_PATH2, mDlImgPath2);
                extras.putString(IntentKey.UPLOAD_IMG_BASE641, orderDetail.getDlPhoto1());
                extras.putString(IntentKey.UPLOAD_IMG_BASE642, orderDetail.getDlPhoto2());
                extras.putBoolean(IntentKey.UPLOAD_FROM_FILL_INFO, true);
                ShowActivity.showActivityForResult(this, UploadImageActivity.class, extras,
                        RequestCodeKey.GO_UPLOAD_IMG);
                break;
            case R.id.policy_submit_btn:
                if (checkData()) {
                    mCarOwner = mCarOwnerEt.getText().toString().trim();
                    mPlateNo = mCarPlateEt.getText().toString().trim();
                    mCarOwnerMobile = mCarOwnerMobileEt.getText().toString().trim();
                    mVin = mVinEt.getText().toString().trim();
                    mEngineNo = mEngineEt.getText().toString().trim();
                    mIdCardNo = mIdCardNoEt.getText().toString().trim();
                    mCarVehicle = mCarVehicleEt.getText().toString().trim();
                    mSeatNum = mSeatNumEt.getText().toString().trim();
                    switch (orderDetail.getCarType()) {
                        case 1:
                            mOutputVolume = mCarWeightEt.getText().toString().trim();
                            break;
                        case 2:
                            mCubWeight = mCarWeightEt.getText().toString().trim();
                            break;
                    }
                    mBak = mBakEt.getText().toString().trim();
                    RetroSubscrube.getInstance().postSubscrube(AppUrl.REPUBLISH_BILL, PostParam.republishbill(mToken,
                            orderDetail.getBidId(), mPlateNo, mCarOwner, mCarOwnerMobile, mIdCardNo, mCarRegisterDate,
                            mTransfer, mTciStatus, mTciDate, mVciDate, mVciTypes, mIdCardPhoto1,
                            mIdCardPhoto2, mDlPhoto1, mDlPhoto2, mOrgPhoto, mOrgNo, mOutputVolume,
                            mCubWeight, mVin, mEngineNo, mUsAge, mCarVehicle, mBak, mSeatNum),
                            new BaseObserver(this, getString(R.string.loading_submit)) {
                                @Override
                                protected void onHandleSuccess(Object data, String msg) {
                                    CommonUtil.showToast(getContext(), msg);
                                    Intent intent = new Intent();
                                    intent.putExtra(IntentKey.QUOTE_SUCCESS, true);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            });
                }
                break;
        }
    }

    @OnTextChanged(value = R.id.insurance_bak_et, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        mBakLengthTv.setText(String.valueOf(getResources().getInteger(R.integer.edit_length_default_200)
                - s.length()));
    }

    private boolean checkData() {
        if (CommonUtil.isEmpty(mCarOwnerEt)) {
            showToast("请输入车主姓名");
            return false;
        }
        if (CommonUtil.isEmpty(mIdCardNoEt)) {
            showToast("请输入身份证号或社会信用代码");
            return false;
        }
        if (CommonUtil.isEmpty(mCarVehicleEt)) {
            showToast(getString(R.string.car_vehicle_hint));
            return false;
        }
        if (CommonUtil.isEmpty(mCarPlateEt)) {
            showToast("请输入车牌号");
            return false;
        }
        if (CommonUtil.isEmpty(mCarRegisterDateTv)) {
            showToast("请选择注册日期");
            return false;
        }
        if (CommonUtil.isEmpty(mVinEt)) {
            showToast("请输入车架号");
            return false;
        }
        if (mVinEt.length() < 17) {
            showToast("车架号不能低于17位");
            return false;
        }
        if (CommonUtil.isEmpty(mEngineEt)) {
            showToast("请输入发动机号");
            return false;
        }
        if (CommonUtil.isEmpty(mSeatNumEt) || !getEditString(mSeatNumEt).matches("[1-9]{1}")) {
            showToast("座位数输入错误");
            return false;
        }
        if (mTciPaymentSwitch.isChecked() && CommonUtil.isEmpty(mTciTakeEffectDateTv)) {
            AlertView alertView = new AlertView(this, null, "交强险/车船税未填写完成", getString(R.string.cancel),
                    new String[]{getString(R.string.confirm)}, null, AlertView.Style.Alert, null);
            alertView.show();
            return false;
        }
        if (CommonUtil.isEmpty(mVciTypes) || CommonUtil.isEmpty(mVciTakeEffectDateTv)) {
            AlertView alertView = new AlertView(this, null, "商业险未填写完成", getString(R.string.cancel),
                    new String[]{getString(R.string.confirm)}, null, AlertView.Style.Alert, null);
            alertView.show();
            return false;
        }
        return true;
    }

    @Override
    public void addListener() {
        mUsAgeRg.setOnCheckedChangeListener(new MyOnCheckedChangeListener());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_SELECT_VCI_TYPE:
                    mVciTypes = data.getParcelableArrayListExtra(IntentKey.VCI_TYPE_LIST);
                    for (int i = 0; i < mVciTypes.size(); i++) {
                        VciType vciType = mVciTypes.get(i);
                        vciType.setOption(vciType.getOptions().get(vciType.getSelectOptionIndex()));
                        if (vciType.getId() == 1) {
                            vciType.setNdis(true);
                        }
                    }
                    mAdapter.refreshData(mVciTypes);
                    break;
                case RequestCodeKey.GO_UPLOAD_IMG:
                    switch (mGoFrom) {
                        case 1:
                            mIdcardImgPath1 = data.getStringExtra(IntentKey.UPLOAD_IMG_PATH1);
                            mIdcardImgPath2 = data.getStringExtra(IntentKey.UPLOAD_IMG_PATH2);
                            break;
                        case 2:
                            mDlImgPath1 = data.getStringExtra(IntentKey.UPLOAD_IMG_PATH1);
                            mDlImgPath2 = data.getStringExtra(IntentKey.UPLOAD_IMG_PATH2);
                            break;
                    }
                    parseImage();
                    break;
            }
        }
    }

    private void parseImage() {
        if (!CommonUtil.isEmpty(mIdcardImgPath1)) {
            Glide.with(this).asBitmap().load(mIdcardImgPath1).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    Bitmap bitmap = resource;
                    if (resource != null && resource.getWidth() > Constant.UPLOAD_IMAGE_SIZE_DEFAULT) {
                        bitmap = BitmapUtil.zoomImg(resource, Constant.UPLOAD_IMAGE_SIZE_DEFAULT);
                    }
                    mIdCardPhoto1 = BitmapUtil.bitmapToBase64(bitmap);
                }
            });
        }
        if (!CommonUtil.isEmpty(mIdcardImgPath2)) {
            Glide.with(this).asBitmap().load(mIdcardImgPath2).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    Bitmap bitmap = resource;
                    if (resource != null && resource.getWidth() > Constant.UPLOAD_IMAGE_SIZE_DEFAULT) {
                        bitmap = BitmapUtil.zoomImg(resource, Constant.UPLOAD_IMAGE_SIZE_DEFAULT);
                    }
                    mIdCardPhoto2 = BitmapUtil.bitmapToBase64(bitmap);
                }
            });
        }
        if (!CommonUtil.isEmpty(mDlImgPath1)) {
            Glide.with(this).asBitmap().load(mDlImgPath1).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    Bitmap bitmap = resource;
                    if (resource != null && resource.getWidth() > Constant.UPLOAD_IMAGE_SIZE_DEFAULT) {
                        bitmap = BitmapUtil.zoomImg(resource, Constant.UPLOAD_IMAGE_SIZE_DEFAULT);
                    }
                    mDlPhoto1 = BitmapUtil.bitmapToBase64(bitmap);
                }
            });
        }
        if (!CommonUtil.isEmpty(mDlImgPath2)) {
            Glide.with(this).asBitmap().load(mDlImgPath2).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    Bitmap bitmap = resource;
                    if (resource != null && resource.getWidth() > Constant.UPLOAD_IMAGE_SIZE_DEFAULT) {
                        bitmap = BitmapUtil.zoomImg(resource, Constant.UPLOAD_IMAGE_SIZE_DEFAULT);
                    }
                    mDlPhoto2 = BitmapUtil.bitmapToBase64(bitmap);
                }
            });
        }
    }

    private class MyOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkId) {
            switch (radioGroup.getId()) {
                case R.id.policy_usage_rg:
                    switch (checkId) {
                        case R.id.policy_operation_rb:
                            mUsAge = 1;
                            break;
                        case R.id.policy_operation_not_rb:
                            mUsAge = 2;
                            break;
                    }
                    break;
            }
        }
    }


    private class MyItemButtonClickListener implements ItemButtonClickListener {
        private Context context;

        public MyItemButtonClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void buttonClick(int clickId, int position) {
            VciType vciType = mAdapter.getItem(position);
            switch (clickId) {
                case R.id.vci_type_select_checked_tv:
                    vciType.ndisToggle();
                    mAdapter.notifyItemChanged(position);
                    break;
                case R.id.vci_type_select_option_tv:
                    initPicker(vciType, position);
                    break;
            }
        }

        private void initPicker(final VciType vciType, final int position) {
            final List<String> options = vciType.getOptions();
            OptionsPickerView pickerView = new OptionsPickerView.Builder(context, new OptionsPickerView
                    .OnOptionsSelectListener() {
                @Override
                public void onOptionsSelect(int options1, int options2, int options3, View v) {
                    String option = options.get(options1);
                    vciType.setOption(option);
                    mAdapter.modifyItem(position, vciType);
                }
            }).setTitleText(vciType.getTitle()).build();
            pickerView.setPicker(options);
            pickerView.setSelectOptions(vciType.getSelectOptionIndex());
            pickerView.show();
        }

    }

}
