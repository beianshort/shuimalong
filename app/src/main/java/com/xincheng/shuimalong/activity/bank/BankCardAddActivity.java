package com.xincheng.shuimalong.activity.bank;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.bank.BankCard;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

/**
 * Created by xiaote on 2017/6/30.
 */

public class BankCardAddActivity extends BaseActivity {
    @BindView(R.id.bankcard_no_et)
    EditText mBankCardNoEt;
    @BindView(R.id.bankcard_name_et)
    EditText mBankNameEt;
    @BindView(R.id.bankcard_lattice_et)
    EditText mBankLatticeEt;

    private String mBankCardNo;
    private String mBankName;
    private String mBankLattice;
    private String mToken;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_bankcard_add;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.bankcard_add));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
    }

    @OnClick(R.id.bankcard_add_next_tv)
    public void addNext() {
        if (checkData()) {
            mBankCardNo = mBankCardNoEt.getText().toString().trim();
            mBankName=mBankNameEt.getText().toString().trim();
            searchBank();
        }
    }

    private boolean checkData() {
        if (CommonUtil.isEmpty(mBankCardNoEt)) {
            showToast("银行卡号不能为空");
            return false;
        }
        if(CommonUtil.isEmpty(mBankNameEt)){
            showToast("银行名子不能为空");
            return false;
        }
        mBankName=mBankNameEt.getText().toString().trim();
        if(CommonUtil.isEmpty(mBankLatticeEt)){
            showToast("银行网点不能为空");
            return false;
        }
        mBankLattice=mBankLatticeEt.getText().toString().trim();
        return true;
    }

    private void searchBank() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.searchBank(mToken, mBankCardNo),
                new BaseObserver(this, getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        Bundle extras = new Bundle();
                        BankCard.Data bankCard = GsonUtil.parseData(data, BankCard.Data.class);
                        if(bankCard != null) {
                            extras.putParcelable(IntentKey.BANK_CARD, bankCard);
                        }
                        extras.putString(IntentKey.BANK_CARD_NO, mBankCardNoEt.getText().toString().trim());
                        extras.putString(IntentKey.BANK_CARD_NAME, mBankName);
                        extras.putString(IntentKey.BANK_LATTICE, mBankLattice);
                        ShowActivity.showActivityForResult(getContext(), BankCardVerifyActivity.class, extras,
                                RequestCodeKey.GO_ADD_BANKCARD);
                    }
                });
    }

    @Override
    public void addListener() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_ADD_BANKCARD:
                    Intent intent = new Intent();
                    intent.putExtra(IntentKey.SUCCESS, data.getBooleanExtra(IntentKey.SUCCESS, false));
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
