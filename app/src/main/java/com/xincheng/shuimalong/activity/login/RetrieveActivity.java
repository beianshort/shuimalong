package com.xincheng.shuimalong.activity.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xuhao on 2017/6/7.
 * 找回密码
 */

public class RetrieveActivity extends BaseActivity {

    @BindView(R.id.btn_next)
    StateButton btnNext;
    @BindView(R.id.et_input_password)
    EditText etInputPassword;
    @BindView(R.id.et_again_input_password)
    EditText etAgainInputPassword;

    private String mobile, mobileToken;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_retrieve_pwd;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.input_pwd));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mobile = extras.getString(IntentKey.MOBILE);
            mobileToken = extras.getString(IntentKey.MOBILE_TOKEN);
        }
    }

    @Override
    public void addListener() {
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (CommonUtil.isEmpty(etInputPassword)) {
                    showToast("请输入密码", Toast.LENGTH_SHORT);
                    return;
                }
                if (CommonUtil.isEmpty(etAgainInputPassword)) {
                    showToast("请再次输入密码", Toast.LENGTH_SHORT);
                    return;
                }
                if (!getEditString(etInputPassword).equals(getEditString(etAgainInputPassword))) {
                    showToast(getString(R.string.second_pwd_err), Toast.LENGTH_SHORT);
                    return;
                }
                RetroSubscrube.getInstance().postSubscrube(AppUrl.GET_PWD,
                        PostParam.getPwd(mobile, getEditString(etInputPassword),
                                getEditString(etAgainInputPassword), mobileToken),
                        new BaseObserver(getContext(), getString(R.string.retrieve_loading)) {
                            @Override
                            protected void onHandleSuccess(Object data, String msg) {
                                showToast(msg, Toast.LENGTH_SHORT);
                                finish();
                            }
                        }
                );
                break;
        }
    }
}
