package com.xincheng.shuimalong.activity.login;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.util.EditTextUtil;
import com.xincheng.shuimalong.util.MyCountDownTimer;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by xuhao on 2017/6/6.
 * 注册页面
 */

public class RegisterActivity extends BaseActivity {
    @BindView(R.id.phone_textinput)
    TextInputLayout phoneTextInput;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.pwd_textinput)
    TextInputLayout pwdTextInput;
    @BindView(R.id.et_pwd)
    EditText etPwd;
    @BindView(R.id.pwd_second_textinput)
    TextInputLayout pwdSecondTextInput;
    @BindView(R.id.et_second_pwd)
    EditText etSecondPwd;
    @BindView(R.id.code_textinput)
    TextInputLayout codeTextInput;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @BindView(R.id.et_invite)
    EditText etInvite;
    @BindView(R.id.tv_invite_err)
    TextView tvInviteErr;
    @BindView(R.id.btn_reg)
    StateButton btnReg;
    @BindView(R.id.tv_back_login)
    TextView tvBackLogin;

    private MyCountDownTimer myCountDownTimer;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    public void initData() {
        initTitleBar("");
        phoneTextInput.setHint(getString(R.string.mobile));
        pwdTextInput.setHint(getString(R.string.password));
        pwdSecondTextInput.setHint(getString(R.string.password_again));
        codeTextInput.setHint(getString(R.string.certkey));
        myCountDownTimer = new MyCountDownTimer(btnGetCode, 60000, 1000);
    }

    @OnTextChanged(value = R.id.et_phone, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterPhoneTextChanged(Editable s) {
        String phone = s.toString();
        if (!EditTextUtil.isMobile(phone) && phone.length() == 11) {
            phoneTextInput.setErrorEnabled(true);
            phoneTextInput.setError(getString(R.string.phone_err));
        } else {
            phoneTextInput.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.et_pwd, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterPwdTextChanged(Editable s) {
        String password = s.toString();
        if (!EditTextUtil.isPassWord(password) && password.length() > 6) {
            pwdTextInput.setErrorEnabled(true);
            pwdTextInput.setError(getString(R.string.pwd_err));
        } else {
            pwdTextInput.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.et_second_pwd, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterPwdSecondTextChanged(Editable s) {
        String password = s.toString();
        if (password.length() > 0) {
            if (!EditTextUtil.isPassWord(password)) {
                pwdSecondTextInput.setErrorEnabled(true);
                pwdSecondTextInput.setError(getString(R.string.pwd_err));
            } else {
                if (EditTextUtil.isPassWord(password) && password.length() < getEditString(etPwd).length()) {
                    pwdSecondTextInput.setErrorEnabled(false);
                } else {
                    if (password.equals(getEditString(etPwd))) {
                        pwdSecondTextInput.setErrorEnabled(false);
                    } else if (password.length() >= getEditString(etPwd).length() && !password.equals
                            (getEditString(etPwd))) {
                        pwdSecondTextInput.setErrorEnabled(true);
                        pwdSecondTextInput.setError(getString(R.string.second_pwd_err));
                    }
                }
            }
        } else {
            pwdSecondTextInput.setErrorEnabled(false);
        }
    }


    @Override
    public void addListener() {

    }

    @OnClick({R.id.btn_get_code, R.id.btn_reg, R.id.tv_back_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                //获取验证码showToast("请输入手机号", Toast.LENGTH_SHORT);
                if (CommonUtil.isEmpty(etPhone)) {
                    showToast("请输入手机号", Toast.LENGTH_SHORT);
                    return;
                }
                getCode();
                break;
            case R.id.btn_reg:
                //立即注册
                if (CommonUtil.isEmpty(etPhone)) {
                    showToast("请输入手机号", Toast.LENGTH_SHORT);
                    return;
                }
                if (CommonUtil.isEmpty(etPwd)) {
                    showToast("请输入密码", Toast.LENGTH_SHORT);
                    return;
                }
                if (CommonUtil.isEmpty(etSecondPwd)) {
                    showToast("请再次输入密码", Toast.LENGTH_SHORT);
                    return;
                }
                if (CommonUtil.isEmpty(etCode)) {
                    showToast("请输入验证码", Toast.LENGTH_SHORT);
                    return;
                }
                mobileregister();
                break;
            case R.id.tv_back_login:
                finish();
                break;
        }
    }

    /**
     * 获取验证码
     */
    private void getCode() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getcertkey(getEditString(etPhone)),
                new BaseObserver(getContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg, Toast.LENGTH_SHORT);
                        myCountDownTimer.start();
                    }
                }
        );
    }

    /**
     * 注册
     */
    private void mobileregister() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.MOBILE_REGISTER,
                PostParam.getRegister(getEditString(etPhone), getEditString(etCode),
                        getEditString(etPwd), getEditString(etSecondPwd), getEditString(etInvite)),
                new BaseObserver(getContext(), getString(R.string.loading_regist)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg, Toast.LENGTH_SHORT);
                        finish();
                    }
                }
        );
    }
}
