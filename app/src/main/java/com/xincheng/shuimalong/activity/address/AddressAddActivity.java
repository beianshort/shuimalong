package com.xincheng.shuimalong.activity.address;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.address.Address;
import com.xincheng.shuimalong.entity.config.District;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.view.picker.AreaPickerView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/29.
 */

public class AddressAddActivity extends BaseActivity {
    @BindView(R.id.address_user_et)
    EditText mUserEt;
    @BindView(R.id.address_mobile_et)
    EditText mMobileEt;
    @BindView(R.id.address_area_tv)
    TextView mAreaTv;
    @BindView(R.id.address_detail_et)
    EditText mDetailEt;

    private String mToken;
    private int mRegionId;
    private String mAddress;
    private String mUsername;
    private String mMobile;
    private AreaPickerView mAreaPickerView;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_address_add;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.address_add));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Address address = extras.getParcelable(IntentKey.ADDRESS);
            if (address != null) {
                mUserEt.setText(address.getName());
                mMobileEt.setText(address.getMobile());
                mRegionId = address.getRegionId();
                mAreaTv.setText(address.getProvince() + address.getCity() + address.getArea());
                mDetailEt.setText(address.getAddress());
            }
        }
        initPickerView();
    }

    private void initPickerView() {
        List<RegionData> provinceList = ConfigUtil.getRegionDataList(this, PrefKey.PROVINCE_LIST);
        mAreaPickerView = new AreaPickerView.Builder(this, new AreaPickerView.OnAreaSelectListener() {
            @Override
            public void onAreaSelect(RegionData province, RegionData city, District district, View v) {
                mRegionId = district.getMenuId();
                if(mRegionId==0){
                    mAreaTv.setText("");
                    showToast("请选择区县");
                    if (mAreaPickerView != null&&!mAreaPickerView.isShowing()) {
                        mAreaPickerView.show();
                    }
                    return;
                }
                mAreaTv.setText(province.getName() + city.getName() + district.getName());
            }
        }).setTitleText(getString(R.string.area_select)).build();
        mAreaPickerView.setPicker(provinceList);
    }

    @OnClick({R.id.address_area_tv, R.id.address_add_submit_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_area_tv:  //选择省市区
                if (mAreaPickerView != null) {
                    mAreaPickerView.show();
                }
                break;
            case R.id.address_add_submit_btn://添加收货地址
                if (checkData()) {
                    mUsername = mUserEt.getText().toString().trim();
                    mMobile = mMobileEt.getText().toString().trim();
                    mAddress = mDetailEt.getText().toString().trim();
                    addAddress();
                }
                break;
        }

    }

    private boolean checkData() {
        if (CommonUtil.isEmpty(mUserEt)) {
            showToast("真实姓名不能为空");
            return false;
        }
        if (CommonUtil.isEmpty(mMobileEt)) {
            showToast("手机号码不能为空");
            return false;
        }
        if (CommonUtil.isEmpty(mAreaTv)) {
            showToast("地区不能为空");
            return false;
        }
        if (CommonUtil.isEmpty(mDetailEt)) {
            showToast("详细地址不能为空");
            return false;
        }
        return true;
    }

    @Override
    public void addListener() {

    }

    private void addAddress() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.ADD_ADDRESS,
                PostParam.addAddress(mToken, mRegionId, mAddress, mUsername, mMobile),
                new BaseObserver(this, getString(R.string.loading_submit)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        CommonUtil.showToast(getContext(), msg);
                        if (data != null) {
                            Address address = GsonUtil.parseData(data, Address.class);
                            Intent intent = new Intent();
                            intent.putExtra(IntentKey.ADDRESS, address);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
    }
}
