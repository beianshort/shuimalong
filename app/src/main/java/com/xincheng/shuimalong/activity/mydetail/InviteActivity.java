package com.xincheng.shuimalong.activity.mydetail;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.UmengTool;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.AppConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.dialog.InviteShareDialog;
import com.xincheng.shuimalong.entity.user.ShareRegister;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;

import static com.xincheng.shuimalong.common.ShareKey.SHARE_IMG;
import static com.xincheng.shuimalong.common.ShareKey.SHARE_LINK;

/**
 * Created by 许浩 on 2017/6/19.
 */

public class  InviteActivity extends BaseActivity implements InviteShareDialog.onShareClick, UMShareListener {
    @BindView(R.id.tv_nickname)
    TextView tvNickname;
    @BindView(R.id.img_avatar)
    ImageView imgAvatar;
    @BindView(R.id.tv_share_url_link)
    TextView tvLink;
    @BindView(R.id.iv_qrcode)
    ImageView ivQrcode;
    InviteShareDialog shareDialog;
    private String mToken;
    private String shareImg, shareUrl;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_invite_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.invite));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        String nickName = PrefUtil.getString(this, PrefKey.LOGIN_NICKNAME, "");
        String face = PrefUtil.getString(this, PrefKey.LOGIN_FACE, "");
        tvNickname.setText(nickName);
        Glide.with(this).load(face).apply(GlideOptionsManager.getInstance().getRequestOptions()).into(imgAvatar);
        getShareRegister();
        if(AppConfig.IS_DEBUG){
            UmengTool.checkWx(this);
        }
    }

    @Override
    public void addListener() {
    }

    private Bitmap getBitmap() {
        Resources res = getResources();
        return BitmapFactory.decodeResource(res, R.mipmap.ic_launcher);
    }

    private UMWeb getWeb() {
        UMImage umImage = new UMImage(getContext(), getBitmap());
        UMWeb web = new UMWeb(shareUrl);
        web.setTitle(getString(R.string.app_name));//标题
        web.setThumb(umImage);  //缩略图
        web.setDescription(getString(R.string.share_desc));//描述
        return web;
    }

    private UMImage getImg() {
        UMImage umImage = new UMImage(getContext(), shareImg);
        umImage.setTitle(getString(R.string.app_name));
        umImage.setThumb(new UMImage(getContext(), getBitmap()));
        umImage.setDescription(getString(R.string.share_desc));
        return umImage;
    }

    @OnClick({R.id.tv_share_url, R.id.tv_share_img})
    public void onViewClicked(View view) {
        if (shareDialog == null) {
            shareDialog = new InviteShareDialog(getContext(), R.style.style_dialog);
            shareDialog.setShareClick(this);
        }
        switch (view.getId()) {
            case R.id.tv_share_url:
                shareDialog.setShare_type(SHARE_LINK);
                break;
            case R.id.tv_share_img:
                shareDialog.setShare_type(SHARE_IMG);
                break;
        }
        shareDialog.show();
    }

    private void getShareRegister() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.shareRegister(mToken),
                new BaseObserver(this, getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        ShareRegister shareRegister = GsonUtil.parseData(data, ShareRegister.class);
                        tvLink.setText(shareRegister.getLink());
                        shareUrl = shareRegister.getLink();
                        shareImg = shareRegister.getQrcode();
                        Glide.with(getContext()).load(shareRegister.getQrcode()).into(ivQrcode);
                    }
                });
    }

    @Override
    public void share(SHARE_MEDIA shareCode, int shareType) {
        ShareAction shareAction = new ShareAction(InviteActivity.this);
        shareAction.setPlatform(shareCode);
        shareAction.setCallback(this);
        switch (shareType) {
            case SHARE_LINK:
                shareAction.withMedia(getWeb());
                break;
            case SHARE_IMG:
                shareAction.withMedia(getImg());
                break;
        }
        shareAction.share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart(SHARE_MEDIA share_media) {

    }

    @Override
    public void onResult(SHARE_MEDIA share_media) {
        showToast("分享成功");
    }

    @Override
    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
        showToast("分享失败");
        XLog.e(throwable.getMessage());
    }

    @Override
    public void onCancel(SHARE_MEDIA share_media) {
        showToast("取消分享");
    }
}
