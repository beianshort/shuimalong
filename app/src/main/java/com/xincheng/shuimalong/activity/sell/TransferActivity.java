package com.xincheng.shuimalong.activity.sell;

import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;

/**
 * Created by 许浩 on 2017/9/3.
 */

public class TransferActivity extends BaseActivity {
    @Override
    public int getLayoutResId() {
        return R.layout.activity_transfer_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.transfer_info_title));
    }

    @Override
    public void addListener() {

    }
}
