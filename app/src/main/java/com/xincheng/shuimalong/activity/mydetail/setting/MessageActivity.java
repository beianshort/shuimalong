package com.xincheng.shuimalong.activity.mydetail.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.MessageConfig;
import com.xincheng.shuimalong.common.PrefKey;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 许浩 on 2017/9/3.
 */

public class MessageActivity extends BaseActivity {
    @BindView(R.id.tv_message_content)
    TextView tvMessageContent;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_message_layout;
    }

    @Override
    public void initData() {
        initTitleBar();
        int flag = getIntent().getIntExtra(IntentKey.INTENT_FLAG, 0);
        String message = "";
        switch (flag) {
            case MessageConfig.ABOUT_US:
                message = PrefKey.ABOUT_US;
                mTitleTv.setText("关于我们");
                break;
            case MessageConfig.CONTACTUS:
                message = PrefKey.CONTACTUS;
                mTitleTv.setText("联系我们");
                break;
            case MessageConfig.LAW:
                message = PrefKey.LAW;
                mTitleTv.setText("法律条款");
                break;
        }
        tvMessageContent.setText(PrefUtil.getString(getContext(), message, ""));
    }

    @Override
    public void addListener() {

    }
}
