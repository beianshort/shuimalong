package com.xincheng.shuimalong.activity.mydetail.wallet;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.OrderDetailActivity;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.Config;
import com.xincheng.shuimalong.entity.wallet.MoneyLogDetail;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote1988 on 2017/8/19.
 */

public class MoneyLogDetailActivity extends BaseActivity {
    @BindView(R.id.log_detail_money_tv)
    TextView mMoneyTv;
    @BindView(R.id.log_detail_bak_tv)
    TextView mBakTv;
    @BindView(R.id.log_detail_dealtype_tv)
    TextView mDealTypeTv;
    @BindView(R.id.log_detail_dealtime_tv)
    TextView mDealTimeTv;
    //BID_DATA
    @BindView(R.id.log_detail_biddata_layout)
    LinearLayout mBidDataLayout;
    @BindView(R.id.log_detail_dealorderno_tv)
    TextView mOrderNoTv;
    @BindView(R.id.log_detail_plateno_tv)
    TextView mPlateNoTv;
    @BindView(R.id.log_detail_vcimoney_tv)
    TextView mVciMoneyTv;
    @BindView(R.id.log_detail_tcimoney_tv)
    TextView mTciMoneyTv;
    @BindView(R.id.log_detail_vvtmoney_tv)
    TextView mVvtMoneyTv;
    @BindView(R.id.log_detail_vcirb_tv)
    TextView mVciRbTv;
    @BindView(R.id.log_detail_tcirb_tv)
    TextView mTciRbTv;
    //WITHDRAW
    @BindView(R.id.log_detail_withdraw_layout)
    LinearLayout mWithdrawLayout;
    @BindView(R.id.log_detail_withdraw_no_tv)
    TextView mWithdrawNoTv;
    @BindView(R.id.log_detail_withdraw_process_time_layout)
    RelativeLayout mProcessTimeLayout;
    @BindView(R.id.log_detail_withdraw_process_time_tv)
    TextView mProcessTimeTv;
    @BindView(R.id.log_detail_withdraw_process_layout)
    RelativeLayout mProcessLayout;
    @BindView(R.id.log_detail_withdraw_process_tv)
    TextView mProcessTv;
    @BindView(R.id.log_detail_withdraw_bak_layout)
    LinearLayout mWithdrawBakLayout;
    @BindView(R.id.log_detail_withdraw_bak_tv)
    TextView mWithdrawBakTv;

    private String mLogId;
    private int mLogType;
    private int mLogReduceStatus;
    private String mToken;
    private MoneyLogDetail mDetail;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_money_log_detail;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.money_log_detail_title));
        Bundle extras = getIntent().getExtras();
        mLogId = extras.getString(IntentKey.LOG_ID);
        mLogType = extras.getInt(IntentKey.LOG_TYPE);
        mLogReduceStatus = extras.getInt(IntentKey.LOG_REDUCE_STATUS);
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        getMoneyLogDetail();
    }

    private void getMoneyLogDetail() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getMoneyLogDetail(mToken, mLogId),
                new BaseObserver(this, getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            mDetail = GsonUtil.parseData(data, MoneyLogDetail.class);
                            parseDetail(mDetail);
                        }
                    }
                });
    }

    private void parseDetail(MoneyLogDetail detail) {
        switch (mLogReduceStatus) {
            case 1:
                mMoneyTv.setText("+" + detail.getAllMoney());
                break;
            case 2:
                mMoneyTv.setText("-" + detail.getAllMoney());
                break;
        }
        mBakTv.setText(detail.getBak());
        switch (mLogType) {
            case 1:
                mDealTypeTv.setText("支付保费");
                break;
            case 2:
                mDealTypeTv.setText("充值");
                break;
            case 3:
                mDealTypeTv.setText("酬金");
                break;
            case 4:
                mDealTypeTv.setText("提现");
                break;
            case 5:
                mDealTypeTv.setText("保费收入");
                break;
            case 6:
                mDealTypeTv.setText("平台奖励");
                break;
            case 99:
                mDealTypeTv.setText("其他收入");
                break;
        }
        MoneyLogDetail.BidData bidData = detail.getBidData();
        String dealTime = "";
        if (!CommonUtil.isEmpty(detail.getCreateTime())) {
            dealTime = DateUtil.parseDate(Long.parseLong(detail.getCreateTime()));
        }
        mDealTimeTv.setText(dealTime);
        if(bidData != null) {
            mBidDataLayout.setVisibility(View.VISIBLE);
            if (CommonUtil.isEmpty(detail.getCreateTime())) {
                dealTime = DateUtil.parseDate(Long.parseLong(bidData.getPayTime()));
            } else {
                dealTime = DateUtil.parseDate(Long.parseLong(detail.getCreateTime()));
            }
            mDealTimeTv.setText(dealTime);
            mOrderNoTv.setText(bidData.getOrderNo());
            mPlateNoTv.setText(bidData.getPlateNo());
            mVciMoneyTv.setText(getString(R.string.money_value, bidData.getVciMoney()));
            mTciMoneyTv.setText(getString(R.string.money_value, bidData.getTciMoney()));
            mVvtMoneyTv.setText(getString(R.string.money_value, bidData.getVvtMoney()));
            String vciRb = CommonUtil.getMoneyStr(
                    Double.parseDouble(bidData.getVciMoney()) * bidData.getVciRb() / 100);
            mVciRbTv.setText(getString(R.string.money_value, vciRb));
            String tciRb = CommonUtil.getMoneyStr(
                    Double.parseDouble(bidData.getTciMoney()) * bidData.getTciRb() / 100);
            mTciRbTv.setText(getString(R.string.money_value, tciRb));
        }
        MoneyLogDetail.WithDraw withDraw = detail.getWithdraw();
        XLog.e("withdraw=>" + withDraw);
        if(withDraw != null) {
            mWithdrawLayout.setVisibility(View.VISIBLE);
            if (CommonUtil.isEmpty(detail.getCreateTime())) {
                dealTime = DateUtil.parseDate(Long.parseLong(withDraw.getApplyTime()));
            } else {
                dealTime = DateUtil.parseDate(Long.parseLong(detail.getCreateTime()));
            }
            mDealTimeTv.setText(dealTime);
            mWithdrawNoTv.setText(withDraw.getId());
            if(withDraw.getProcess() == 1) {
                mProcessLayout.setVisibility(View.VISIBLE);
                mProcessTimeLayout.setVisibility(View.VISIBLE);
                mProcessTimeTv.setText(DateUtil.parseDate(Long.parseLong(withDraw.getProcessTime())));
                switch (withDraw.getStatus()) {
                    case 1:
                        mProcessTv.setText("已通过");
                        break;
                    case 2:
                        mProcessTv.setText("不通过");
                        break;
                    default:
                        mProcessTv.setText("处理中");
                        break;
                }
            }
            if(!CommonUtil.isEmpty(withDraw.getBak())) {
                mWithdrawBakLayout.setVisibility(View.VISIBLE);
                mWithdrawBakTv.setText(withDraw.getBak());
            }
        }
    }

    @OnClick(R.id.log_detail_dealorderno_tv)
    public void viewClick() {
        Bundle extras = new Bundle();
        extras.putInt(IntentKey.BID_ID, mDetail.getBidData().getBidId());
        String myUserId = PrefUtil.getString(this, PrefKey.LOGIN_USER_ID, "");
        if (myUserId.equals(mDetail.getBidData().getAcUserId())) {
            extras.putInt(IntentKey.ORDER_CONFIG, OrderConfig.RECEIPT);
        } else if (myUserId.equals(mDetail.getBidData().getBillUserId())) {
            extras.putInt(IntentKey.ORDER_CONFIG, OrderConfig.SELL);
        }
        extras.putInt(IntentKey.FROM, 2);
        ShowActivity.showActivity(this, OrderDetailActivity.class, extras);
    }

    @Override
    public void addListener() {

    }
}
