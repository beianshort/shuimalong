package com.xincheng.shuimalong.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.xincheng.library.widget.StateButton;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 认证成功显示页面 恭喜您认证成功
 * 提现成功 您的提现已经成功受理，\n 1-3个工作日会提现到银行卡，\n请注意查收！
 * 修改密码成功页面 保单支付成功，\n请尽快联系收单方出单
 * 返佣成功  收单方已经确认收到回单\n，佣金已经打到您的账户中！
 * Created by 许浩 on 2017/6/18.
 */

public class SucceedActivity extends BaseActivity {
    @BindView(R.id.tv_succeed_message)
    TextView tvSucceedMessage;
    @BindView(R.id.back_home_btn)
    StateButton btnBackHome;
    private String money = "";
    private int mFrom;

    @Override
    public int getLayoutResId() {
        return R.layout.succeed_layout;
    }

    @Override
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mFrom = bundle.getInt(IntentKey.FROM);
            money = bundle.getString(IntentKey.TOTAL_MONEY, "");
            initTitleBar(bundle.getString(IntentKey.TITLE_MESSAGE, ""));
            tvSucceedMessage.setText(bundle.getString(IntentKey.SUCCESS_MESSAGE, ""));
            switch (mFrom) {
                case 1:
                case 2:
                    btnBackHome.setText(getString(R.string.back_home));
                    break;
                case 3:
                    btnBackHome.setText(getString(R.string.back_wallet));
                    break;
            }
        } else {
            initTitleBar();
        }
    }

    @OnClick(R.id.back_home_btn)
    public void backHome() {
        switch (mFrom) {
            case 1:
                Intent intent = new Intent();
                intent.putExtra(IntentKey.PAY_SUCCESS, true);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case 2:
                intent = new Intent();
                intent.putExtra(IntentKey.EVALUATE, true);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case 3:
                intent = new Intent();
                intent.putExtra(IntentKey.WITHDRAW_SUCCESS, true);
                intent.putExtra(IntentKey.TOTAL_MONEY, money);
                setResult(RESULT_OK, intent);
                break;
        }
        finish();
    }

    @Override
    public void addListener() {

    }

}
