package com.xincheng.shuimalong.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.mydetail.setting.ModifyPaypwdActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.entity.user.MobileToken;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.MyCountDownTimer;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xuhao on 2017/6/7.
 * 找回密码输入验证码
 */

public class InputCodeActivity extends BaseActivity {
    @BindView(R.id.tv_retrieve_phone)
    TextView tvRetrievePhone;
    @BindView(R.id.et_input_code)
    EditText etInputCode;
    @BindView(R.id.tv_code_err)
    TextView tvCodeErr;
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @BindView(R.id.btn_next)
    StateButton btnNext;

    private MyCountDownTimer myCountDownTimer;
    private String mobile;
    private boolean modifyForPayPwd;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_input_code;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.input_code));
        myCountDownTimer = new MyCountDownTimer(btnGetCode, 60000, 1000);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mobile = bundle.getString(IntentKey.MOBILE);
            modifyForPayPwd = bundle.getBoolean(IntentKey.MODIFY_FOR_PAYPWD,false);
            tvRetrievePhone.setText(mobile);
            getCode();
        }
    }

    @Override
    public void addListener() {

    }

    @OnClick({R.id.btn_get_code, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                if (!CommonUtil.isEmpty(mobile)) {
                    getCode();
                }
                break;
            case R.id.btn_next:
                if(CommonUtil.isEmpty(etInputCode)) {
                    showToast("请输入验证码");
                    return;
                }
                checkcertkey();
                break;
        }
    }

    /**
     * 获取验证码
     */
    private void getCode() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getcertkey(mobile),
                new BaseObserver(getContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg, Toast.LENGTH_SHORT);
                        myCountDownTimer.start();
                    }
                }
        );
    }

    /**
     * 验证验证码
     */
    public void checkcertkey() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.CHECK_CERT_KEY,
                PostParam.getCheckcertkey(mobile, getEditString(etInputCode)),
                new BaseObserver(getBaseContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        //验证成功  进入设置密码页面
                        if(data != null) {
                            if(modifyForPayPwd) {
                                ShowActivity.showActivity(getContext(), ModifyPaypwdActivity.class);
                                finish();
                            } else {
                                Bundle extras = new Bundle();
                                MobileToken mobileToken = GsonUtil.parseData(data,MobileToken.class);
                                extras.putString(IntentKey.MOBILE, mobile);
                                extras.putString(IntentKey.MOBILE_TOKEN, mobileToken.getMobileToken());
                                ShowActivity.showActivity(InputCodeActivity.this,
                                        RetrieveActivity.class,extras);
                                finish();
                            }
                        }

                    }
                }
        );
    }
}
