package com.xincheng.shuimalong.activity.insurance;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.recycler.XRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.adapter.friend.NewFriendAdapter;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.ApplyFriendCommon;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.friend.ApplyFriendBean;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.view.ListItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.ContactNotificationMessage;

import static io.rong.message.ContactNotificationMessage.CONTACT_OPERATION_ACCEPT_RESPONSE;
import static io.rong.message.ContactNotificationMessage.CONTACT_OPERATION_REJECT_RESPONSE;
import static io.rong.message.ContactNotificationMessage.CONTACT_OPERATION_REQUEST;

/**
 * Created by xuhao on 2017/7/19.
 */

public class NewFriendActivity extends BaseActivity implements NewFriendAdapter.onApplyListener {
    @BindView(R.id.friend_recyclerview)
    XRecyclerView friendRecyclerview;
    NewFriendAdapter adapter;
    private List<ApplyFriendBean.data> infolist;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_new_friend_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.new_friend_title));
        infolist = new ArrayList<>();
        adapter = new NewFriendAdapter(getContext(), infolist);
        adapter.setOnApplyListener(this);
        friendRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        friendRecyclerview.addItemDecoration(new ListItemDecoration());
        friendRecyclerview.setAdapter(adapter);
        getApply();
    }

    @Override
    public void addListener() {

    }

    private void getApply() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getApplyFriend(PrefUtil.getString(getContext(), PrefKey
                        .TOKEN, "")),
                new BaseObserver(getContext(), getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        ApplyFriendBean applyFriendBean = GsonUtil.parseData(data, ApplyFriendBean.class);
                        if (applyFriendBean != null && applyFriendBean.getList() != null) {
                            infolist.clear();
                            infolist.addAll(applyFriendBean.getList());
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public void onApply(final ApplyFriendBean.data data, final int status) {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.PROCESS_APPLY_FRIENDS,
                PostParam.processapplyFriends(PrefUtil.getString(getContext(), PrefKey.TOKEN, ""), data.getId(),
                        status),
                new BaseObserver(getContext(), getString(R.string.loading_submit)) {
                    @Override
                    protected void onHandleSuccess(Object obj, String msg) {
                        setResult(RESULT_OK);
                        sendNotifiMessage(String.valueOf(data.getUser_id()), msg, status);
                        getApply();
                    }
                });
    }

    private void sendNotifiMessage(String friend_id, final String msg, int status) {
        String str = null, message = null;
        if (status == 1) {
            str = CONTACT_OPERATION_ACCEPT_RESPONSE;
            message = getString(R.string.already_friend,
                    PrefUtil.getString(getContext(), PrefKey.LOGIN_NICKNAME, ""));
        } else {
            str = CONTACT_OPERATION_REJECT_RESPONSE;
            message = getString(R.string.already_reject_friend,
                    PrefUtil.getString(getContext(), PrefKey.LOGIN_NICKNAME, ""));
        }
        ContactNotificationMessage notificationMessage = ContactNotificationMessage.obtain(str
                , PrefUtil.getString(getContext(), PrefKey.LOGIN_USER_ID, "")
                , friend_id
                , message);
        RongIMClient.getInstance().sendMessage(Conversation.ConversationType.PRIVATE,
                friend_id,
                notificationMessage,
                null,
                null,
                new IRongCallback.ISendMessageCallback() {
                    @Override
                    public void onAttached(Message message) {
                        // 消息成功存到本地数据库的回调
                    }

                    @Override
                    public void onSuccess(Message message) {
                        // 消息发送成功的回调
                        showToast(msg);
                        XLog.e("申请成功" + message.getConversationType());
                        XLog.e(message.toString());
                    }

                    @Override
                    public void onError(Message message, RongIMClient.ErrorCode errorCode) {
                        // 消息发送失败的回调
                        XLog.e(message);
                        XLog.e(errorCode);
                    }
                });
    }
}
