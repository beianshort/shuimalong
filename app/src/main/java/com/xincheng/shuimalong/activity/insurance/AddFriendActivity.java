package com.xincheng.shuimalong.activity.insurance;

import android.os.Bundle;
import android.text.Editable;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.ContactNotificationMessage;

import static io.rong.message.ContactNotificationMessage.CONTACT_OPERATION_REQUEST;

/**
 * Created by xiaote1988 on 2017/8/12.
 */

public class AddFriendActivity extends BaseActivity {
    @BindView(R.id.add_friend_avatar_iv)
    ImageView mAvatarIv;
    @BindView(R.id.add_friend_nickname_tv)
    TextView mNickNameTv;
    @BindView(R.id.add_friend_authentication_tv)
    TextView mCertTv;
    @BindView(R.id.add_friend_registerdate_tv)
    TextView mRegisterDateTv;
    @BindView(R.id.add_friend_bak_et)
    EditText mBakEt;
    @BindView(R.id.add_friend_length_tv)
    TextView mBakLengthTv;

    private String mUserId;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_add_friend;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.add_friend_title));
        Bundle extras = getIntent().getExtras();
        mUserId = extras.getString(IntentKey.USER_ID);
        String avatar = extras.getString(IntentKey.AVATAR);
        Glide.with(this).load(avatar).apply(GlideOptionsManager.getInstance().getRequestOptions())
                .into(mAvatarIv);
        String nikeName = extras.getString(IntentKey.NICK_NAME);
        mNickNameTv.setText(nikeName);
        String certMsg = extras.getString(IntentKey.USER_CERT);
        mCertTv.setText(certMsg);
        String registerDate = extras.getString(IntentKey.REGISTER_TIME);
        mRegisterDateTv.setText(registerDate);
    }

    @Override
    public void addListener() {

    }

    @OnTextChanged(value = R.id.add_friend_bak_et, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        mBakLengthTv.setText(String.valueOf(getResources().getInteger(R.integer.edit_length_default_200)
                - s.length()));
    }

    @OnClick(R.id.add_friend_submit_tv)
    public void goAddFriend() {
        String bak = mBakEt.getText().toString().trim();
        addFriend(mUserId, bak);
    }


    /**
     * 添加好友传入对方user_id
     *
     * @param friend_id
     */
    private void addFriend(final String friend_id, String bak) {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.APPLY_FRIENDS,
                PostParam.applyFriends(PrefUtil.getString(getContext(), PrefKey.TOKEN, ""),
                        friend_id, bak),
                new BaseObserver(getContext(), getString(R.string.loading_submit)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        sendNotifiMessage(friend_id, msg);
                        finish();
                    }
                }
        );
    }

    private void sendNotifiMessage(String friend_id, final String msg) {
        ContactNotificationMessage notificationMessage = ContactNotificationMessage.obtain(CONTACT_OPERATION_REQUEST
                , PrefUtil.getString(getContext(), PrefKey.LOGIN_USER_ID, "")
                , friend_id
                , getString(R.string.add_friend, PrefUtil.getString(getContext(), PrefKey.LOGIN_NICKNAME, "")));
        RongIMClient.getInstance().sendMessage(Conversation.ConversationType.PRIVATE,
                friend_id,
                notificationMessage,
                null,
                null,
                new IRongCallback.ISendMessageCallback() {
                    @Override
                    public void onAttached(Message message) {
                        // 消息成功存到本地数据库的回调
                    }

                    @Override
                    public void onSuccess(Message message) {
                        // 消息发送成功的回调
                        showToast(msg);
                        XLog.e("申请成功" + message.getConversationType());
                        XLog.e(message.toString());
                    }

                    @Override
                    public void onError(Message message, RongIMClient.ErrorCode errorCode) {
                        // 消息发送失败的回调
                        XLog.e(message);
                        XLog.e(errorCode);
                    }
                });
    }
}
