package com.xincheng.shuimalong.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.WebSettingUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.listener.MyWebChromeClient;
import com.xincheng.shuimalong.manager.OrderExitManager;
import com.xincheng.shuimalong.util.PayUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;

/**
 * Created by xiaote on 2017/7/26.
 * 支付页面
 */

public class PayActivity extends BaseActivity {
    @BindView(R.id.pay_wv)
    WebView mPayWv;
    @BindView(R.id.pay_progress_bar)
    ProgressBar mProgressBar;

    private String mPaySuccessUrl;
    private int mAddressId;
    private String bankCard;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_pay;
    }

    private void initWebView() {
        mPayWv.getSettings().setSupportZoom(true);
        mPayWv.getSettings().setBuiltInZoomControls(true);
        mPayWv.getSettings().setJavaScriptEnabled(true);
        mPayWv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        // 隐藏缩放按钮
        mPayWv.getSettings().setDisplayZoomControls(false);

        //设置WebView对h5页面的支持
        mPayWv.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mPayWv.getSettings().setDomStorageEnabled(true);
        mPayWv.getSettings().setAppCachePath(WebSettingUtil.getH5CacheDir(this));
        mPayWv.getSettings().setAllowFileAccess(true);
        mPayWv.getSettings().setAppCacheEnabled(true);
        mPayWv.getSettings().setUseWideViewPort(false);
        mPayWv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mPayWv.getSettings().setNeedInitialFocus(true);
        mPayWv.getSettings().setLoadWithOverviewMode(true);
        // 隐藏滚动条
        mPayWv.setHorizontalScrollBarEnabled(false);
        mPayWv.setVerticalScrollBarEnabled(false);
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.pay));
        initWebView();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int bidId = extras.getInt(IntentKey.BID_ID);
            mAddressId = extras.getInt(IntentKey.ADDRESS_ID);
            bankCard=extras.getString(IntentKey.BANK_CARD_NO);
            String loadUrl = PayUtil.getPayUrl(bidId, mAddressId,bankCard);
            XLog.e("loadUrl=>" + loadUrl);
            mPaySuccessUrl = PayUtil.getPaySuccessUrl(mAddressId);
            XLog.e(mPaySuccessUrl);
            mProgressBar.setVisibility(View.VISIBLE);
            mPayWv.loadUrl(loadUrl);
        }
    }

    @Override
    public void addListener() {
        mPayWv.setWebViewClient(new MyWebViewClient());
        mPayWv.setWebChromeClient(new MyWebChromeClient(mProgressBar));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_PAY:
                    Intent intent = new Intent();
                    intent.putExtra(IntentKey.PAY_SUCCESS, true);
                    setResult(RESULT_OK,intent);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (!CommonUtil.isEmpty(mPaySuccessUrl) && mPaySuccessUrl.equals(url)) {
                XLog.e("支付并且回调成功");
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 1);
                extras.putString(IntentKey.TITLE_MESSAGE, getString(R.string.pay_success));
                extras.putString(IntentKey.SUCCESS_MESSAGE, getString(R.string.pay_success_message));
                ShowActivity.showActivityForResult(getContext(), SucceedActivity.class, extras, RequestCodeKey.GO_PAY);
                return;
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            XLog.e("url=>" + url);
            return false;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            XLog.e("getUrl=>" + view.getUrl());
            handler.proceed();
        }
    }
}
