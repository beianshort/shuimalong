package com.xincheng.shuimalong.activity.sell;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.util.BitmapUtil;
import com.xincheng.shuimalong.util.MyUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/7/10.
 */

public class LookGuaranteeActivity extends BaseActivity {
    @BindView(R.id.guarantee_img_iv)
    ImageView mGuaranteeImgIv;

    private String imgUrl;

    @Override
    public int getLayoutResId() {
        return R.layout.look_guarantee_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.look_guarantee));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            imgUrl = extras.getString(IntentKey.GUARANTEE_IMAGE);
            XLog.e("imgUrl==>" + imgUrl);
            Glide.with(this).load(imgUrl).into(mGuaranteeImgIv);
        }
    }

    @OnClick(R.id.save_image_tv)
    void saveImage() {
        Glide.with(this).asBitmap().load(imgUrl).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                if (resource != null) {
                    showToast("保存成功");
                    File saveFile = MyUtil.getOutputMediaFile(getContext(), Constant.ID_CARD_PATH, "guarantee_save_");
                    BitmapUtil.saveToFile(resource, saveFile);
                }
            }
        });
    }

    @Override
    public void addListener() {

    }
}
