package com.xincheng.shuimalong.activity.login;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.util.AESUtil;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.MainActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.user.LoginInfo;
import com.xincheng.shuimalong.util.EditTextUtil;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;
import com.xincheng.shuimalong.view.ResizeLayout;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xuhao on 2017/5/26.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.li_title)
    LinearLayout liTitle;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.ed_pwd)
    EditText edPwd;
    @BindView(R.id.btn_login)
    StateButton btnLogin;
    @BindView(R.id.btn_wechat)
    ImageButton btnWechat;
    @BindView(R.id.btn_qq)
    ImageButton btnQq;
    //    @BindView(R.id.btn_sina)
//    ImageButton btnSina;
    @BindView(R.id.btn_forget_pwd)
    TextView btnForgetPwd;
    @BindView(R.id.btn_register)
    TextView btnRegister;
    @BindView(R.id.resize_layout)
    ResizeLayout resizeLayout;
    private static final int BIGGER = 1;
    private static final int SMALLER = 2;
    private static final int MSG_RESIZE = 1;
    @BindView(R.id.third_layout)
    LinearLayout thirdLayout;

    class InputHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_RESIZE: {
                    if (msg.arg1 == BIGGER) {
                        liTitle.setVisibility(View.VISIBLE);
                        thirdLayout.setVisibility(View.VISIBLE);
                    } else {
                        liTitle.setVisibility(View.GONE);
                        thirdLayout.setVisibility(View.GONE);
                    }
                }
                break;
            }
            super.handleMessage(msg);
        }
    }

    private InputHandler mHandler = new InputHandler();

    @Override
    public int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    public void initData() {
        edPhone.setText(PrefUtil.getString(this, PrefKey.LOGIN_MOBILE, ""));
        String password = PrefUtil.getString(this, PrefKey.LOGIN_PASSWORD, "");
        edPwd.setText(AESUtil.decrypt(password, Constant.PASSWORD_SECRET));
    }

    @Override
    public void addListener() {
        resizeLayout.setOnResizeListener(new ResizeLayout.OnResizeListener() {
            public void OnResize(int w, int h, int oldw, int oldh) {
                int change = BIGGER;
                if (h < oldh) {
                    change = SMALLER;
                }
                Message msg = new Message();
                msg.what = 1;
                msg.arg1 = change;
                mHandler.sendMessage(msg);
            }
        });
    }

    @OnClick({R.id.btn_login, R.id.btn_wechat, R.id.btn_qq, /*R.id.btn_sina,*/ R.id.btn_forget_pwd, R.id.btn_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (CommonUtil.isEmpty(edPhone)) {
                    showToast("请输入手机号", Toast.LENGTH_SHORT);
                    return;
                }
                if (!EditTextUtil.isMobile(getEditString(edPhone))) {
                    showToast("手机号格式不正确");
                    return;
                }
                if (CommonUtil.isEmpty(edPwd)) {
                    showToast("请输入密码", Toast.LENGTH_SHORT);
                    return;
                }
                RetroSubscrube.getInstance().postSubscrube(AppUrl.MOBILE_LOGIN,
                        PostParam.getLoginParam(getEditString(edPhone), getEditString(edPwd)),
                        new BaseObserver(getContext(), getString(R.string.loading_login)) {
                            @Override
                            protected void onHandleSuccess(Object data, String msg) {
                                showToast(msg, Toast.LENGTH_SHORT);
                                if (data != null) {
                                    LoginInfo loginInfo = GsonUtil.parseData(data, LoginInfo.class);
                                    saveLoginInfo(loginInfo);
                                    ShowActivity.showActivity(LoginActivity.this, MainActivity.class);
                                    finish();
                                }
                            }
                        }
                );
                break;
            case R.id.btn_wechat:
                showToast("微信登录", Toast.LENGTH_SHORT);
                break;
            case R.id.btn_qq:
                showToast("qq登录", Toast.LENGTH_SHORT);
                break;
//            case R.id.btn_sina:
//                showToast("新浪登录", Toast.LENGTH_SHORT);
//                break;
            case R.id.btn_forget_pwd:
                //忘记密码
                if (!CommonUtil.isEmpty(edPhone)) {
                    String mobile = getEditString(edPhone);
                    if (EditTextUtil.isMobile(mobile)) {
                        Bundle bundle = new Bundle();
                        bundle.putString(IntentKey.MOBILE, mobile);
                        ShowActivity.showActivity(LoginActivity.this, InputCodeActivity.class, bundle);
                    } else {
                        showToast("请输入正确手机号", Toast.LENGTH_SHORT);
                    }
                } else {
                    showToast("请输入手机号", Toast.LENGTH_SHORT);
                }
                break;
            case R.id.btn_register:
                //立即注册
                ShowActivity.showActivity(LoginActivity.this, RegisterActivity.class);
                break;
        }
    }

    private void saveLoginInfo(LoginInfo loginInfo) {
        PrefUtil.putBoolean(getContext(), PrefKey.IS_LOGIN, true);
        PrefUtil.putString(getContext(), PrefKey.LOGIN_MOBILE, getEditString(edPhone));
        PrefUtil.putString(getContext(), PrefKey.LOGIN_NICKNAME, loginInfo.getNick_name());
        PrefUtil.putString(getContext(), PrefKey.LOGIN_PASSWORD,
                AESUtil.encrypt(getEditString(edPwd), Constant.PASSWORD_SECRET));
        PrefUtil.putString(getContext(), PrefKey.TOKEN, loginInfo.getToken());
        PrefUtil.putString(getContext(), PrefKey.LOGIN_USER_ID, loginInfo.getUser_id());
        PrefUtil.putString(getContext(), PrefKey.LOGIN_FACE, loginInfo.getFace());
        PrefUtil.putBoolean(getContext(), PrefKey.LOGIN_CERT, loginInfo.isCert());
        if(!CommonUtil.isEmpty(loginInfo.getReason())) {
            PrefUtil.putString(getContext(), PrefKey.LOGIN_CERT_REASON, loginInfo.getReason());
        }
        PrefUtil.putBoolean(getContext(), PrefKey.LOGIN_SET_PAYPWD, loginInfo.isSetPayPwd());
    }
}
