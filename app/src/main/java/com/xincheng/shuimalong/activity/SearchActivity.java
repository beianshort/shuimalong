package com.xincheng.shuimalong.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.adapter.SearchHistoryAdapter;
import com.xincheng.shuimalong.adapter.config.CommonConfigGridAdapter;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/7/11.
 */

public class SearchActivity extends BaseActivity {
    @BindView(R.id.search_content_et)
    EditText mContentEt;
    @BindView(R.id.search_history_mrv)
    MRecyclerView mHistoryRv;

    private int mFrom;
    private Set<String> mHistoryKeywords;
    private List<String> mHistoryList;
    private SearchHistoryAdapter mAdapter;

    @Override

    public int getLayoutResId() {
        return R.layout.search_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.search));
        Bundle extras = getIntent().getExtras();
        mFrom = extras.getInt(IntentKey.FROM);
        switch (mFrom) {
            case 1:     //首页搜索
                mHistoryKeywords = PrefUtil.getSetString(this, PrefKey.HOME_SEARCH_KEYWORDS, null);
                mContentEt.setHint(getString(R.string.home_search_hint));
                break;
            case 2:    //市场搜索
                mHistoryKeywords = PrefUtil.getSetString(this, PrefKey.MARKET_SEARCH_KEYWORDS, null);
                mContentEt.setHint(getString(R.string.market_search_hint));
                break;
            case 3:   //客户管理搜索
                mHistoryKeywords = PrefUtil.getSetString(this, PrefKey.CUSTOMER_SEARCH_KEYWORDS, null);
                mContentEt.setHint(getString(R.string.management_search_hint));
                break;
        }
        mHistoryRv.setLayoutManager(new GridLayoutManager(this, 4));
        if (!CommonUtil.isEmpty(mHistoryKeywords)) {
            mHistoryList = new ArrayList<>();
            Iterator<String> iterator = mHistoryKeywords.iterator();
            while (iterator.hasNext()) {
                String text = iterator.next();
                mHistoryList.add(text);
            }
        }
        mAdapter = new SearchHistoryAdapter(this, mHistoryList);
        mHistoryRv.setAdapter(mAdapter);
    }

    @OnClick({R.id.search_content_clear_iv, R.id.search_history_delete_iv, R.id.search_complete_btn})
    public void viewClick(View view) {
        switch (view.getId()) {
            case R.id.search_content_clear_iv:
                mContentEt.setText("");
                break;
            case R.id.search_history_delete_iv:
                mHistoryList.clear();
                mHistoryKeywords.clear();
                switch (mFrom) {
                    case 1:
                        PrefUtil.removeKey(this, PrefKey.HOME_SEARCH_KEYWORDS);
                        break;
                    case 2:
                        PrefUtil.removeKey(this, PrefKey.MARKET_SEARCH_KEYWORDS);
                        break;
                    case 3:
                        PrefUtil.removeKey(this, PrefKey.CUSTOMER_SEARCH_KEYWORDS);
                        break;
                }
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.search_complete_btn:
                String keyword = mContentEt.getText().toString().trim();
                saveKeyWord(keyword);
                Intent intent = new Intent();
                intent.putExtra(IntentKey.KEYWORD, keyword);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

    private void saveKeyWord(String keyword) {
        if (mHistoryKeywords == null) {
            mHistoryKeywords = new HashSet<>();
        }
        if (!CommonUtil.isEmpty(keyword)) {
            mHistoryKeywords.add(keyword);
        }
        switch (mFrom) {
            case 1:
                PrefUtil.putSetString(this, PrefKey.HOME_SEARCH_KEYWORDS, mHistoryKeywords);
                break;
            case 2:
                PrefUtil.putSetString(this, PrefKey.MARKET_SEARCH_KEYWORDS, mHistoryKeywords);
                break;
            case 3:
                PrefUtil.putSetString(this, PrefKey.CUSTOMER_SEARCH_KEYWORDS, mHistoryKeywords);
                break;
        }
    }

    @Override
    public void addListener() {
        mHistoryRv.setOnItemClickListener(new MyOnItemClickListener());
    }

    private class MyOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            Intent intent = new Intent();
            intent.putExtra(IntentKey.KEYWORD, mAdapter.getItem(position));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
