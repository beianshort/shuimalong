package com.xincheng.shuimalong.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.UIUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.util.NetworkUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by xiaote on 2017/5/24.
 */

public abstract class BaseActivity extends AppCompatActivity {
    public ImageView mBackIv;
    public TextView mTitleTv, mRightTv;
    public TabLayout mTabLayout;
    public ImageButton mRightIv;
    private View mTitleLine;
    public ObservableTransformer<Observable, ObservableSource> composeFunction;
    private final long RETRY_TIMES = 1;

    public abstract int getLayoutResId();

    public abstract void initData();

    public abstract void addListener();

    private Unbinder mUnBinder;

    public void initTitleBar() {
        mBackIv = UIUtil.findViewById(this, R.id.common_back_iv);
        mTitleTv = UIUtil.findViewById(this, R.id.common_title_tv);
        mTabLayout = UIUtil.findViewById(this, R.id.common_tab_layout);
        mRightIv = UIUtil.findViewById(this, R.id.common_right_iv);
        mRightTv = UIUtil.findViewById(this, R.id.common_right_tv);
        mTitleLine = UIUtil.findViewById(this, R.id.common_line);
    }

    public void initTitleBar(String text) {
        initTitleBar();
        mTitleTv.setText(text);
    }

    public void initTitleBarTabLayout(String[] arr) {
        initTitleBar();
        mTitleTv.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.VISIBLE);
        for (int i = 0; i < arr.length; i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(arr[i]));
        }
    }

    public void setmTitleTvColor(int color) {
        mTitleTv.setTextColor(color);
    }

    public void setmTitleLineGone(int flag) {
        mTitleLine.setVisibility(flag);
    }

    public void setmBackIvWhiteSrc() {
        mBackIv.setImageResource(R.drawable.icon_back_white);
    }

    public void initTitleBarOfRight(String text) {
        initTitleBar(text);
        mRightIv.setVisibility(View.VISIBLE);
    }

    private void initView() {
        setContentView(getLayoutResId());
        mUnBinder = ButterKnife.bind(this);
        initData();
        addListener();
        composeFunction = new ObservableTransformer<Observable, ObservableSource>() {
            @Override
            public ObservableSource apply(Observable observable) {
                return observable.retry(RETRY_TIMES)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(new Consumer<Disposable>() {
                            @Override
                            public void accept(Disposable disposable) throws Exception {
                                if (NetworkUtil.isNetworkAvailable(BaseActivity.this)) {

                                } else {
                                    Toast.makeText(BaseActivity.this, "网络连接异常，请检查网络", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    public View getView() {
        return getWindow().getDecorView();
    }

    public String getEditString(EditText et) {
        if (TextUtils.isEmpty(et.getText())) {
            return "";
        } else {
            return et.getText().toString().trim();
        }
    }

    public void showToast(String message, int flag) {
        Toast.makeText(getBaseContext(), message, flag).show();
    }

    public void showToast(String message) {
        CommonUtil.showToast(this, message);
    }

    public Activity getContext() {
        return this;
    }

    public void back(View view) {
        finish();
    }

    public TextView getmRightTv() {
        return mRightTv;
    }

    public void setRightTv(String message) {
        if (mRightTv != null) {
            if (mRightTv.getVisibility() == View.GONE) {
                mRightTv.setVisibility(View.VISIBLE);
            }
            mRightTv.setText(message);
        }
    }

    public void right_onclick(View view) {
        rightListener();
    }

    public void rightListener() {
    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }
}
