package com.xincheng.shuimalong.activity.management;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.library.widget.alertview.AlertView;
import com.xincheng.library.widget.alertview.OnAlertItemClickListener;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.IdCardCameraActivity;
import com.xincheng.shuimalong.activity.mydetail.ReanNameAuthenticationActivity;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.customer.Customer;
import com.xincheng.shuimalong.util.BitmapUtil;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.ShowActivity;
import com.xincheng.shuimalong.util.UnitUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 许浩 on 2017/7/1.
 */

public class AddManagerActivity extends BaseActivity {
    @BindView(R.id.identity_front)
    FrameLayout identityFront;
    @BindView(R.id.identity_front_tv)
    TextView identityFrontTv;
    @BindView(R.id.img_identity_front)
    ImageView imgIdentityFront;
    @BindView(R.id.img_identity_front_clear)
    ImageView imgIdentityFrontClear;
    @BindView(R.id.identity_contrary)
    FrameLayout identityContrary;
    @BindView(R.id.identity_contrary_tv)
    TextView identityContraryTv;
    @BindView(R.id.img_identity_contrary)
    ImageView imgIdentityContrary;
    @BindView(R.id.img_identity_contrary_clear)
    ImageView imgIndentityContraryClear;
    @BindView(R.id.et_car_name)
    EditText etCarName;
    @BindView(R.id.et_id_card)
    EditText etIdCard;
    @BindView(R.id.et_phone_no)
    EditText etPhoneNo;
    @BindView(R.id.btn_next)
    StateButton btnNext;
    private Customer data;
    private String front_id_card_uri, contrary_id_card_uri;

    private AlertView mPhotoAlert;
    private int mPhotoStatus;

    @Override
    public int getLayoutResId() {
        return R.layout.manage_into_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.manager_message));
        data = new Customer();
    }

    @Override
    public void addListener() {

    }

    private void initAlertView() {
        mPhotoAlert = new AlertView(this, null, null, getString(R.string.cancel), null, Constant.CAMERA_OR_LOCAL,
                AlertView.Style.ActionSheet, new OnAlertItemClickListener() {
            @Override
            public void onItemClick(AlertView alertView, int position) {
                switch (position) {
                    case 0:
                        photoCamera();
                        break;
                    case 1:
                        photoLocal();
                        break;
                }
            }
        });
    }

    private void photoCamera() {
        switch (mPhotoStatus) {
            case 1://身份证正面
                Bundle bundle1 = new Bundle();
                bundle1.putInt(IntentKey.FROM, 1);
                ShowActivity.showActivityForResult(AddManagerActivity.this, IdCardCameraActivity.class,
                        bundle1, RequestCodeKey.INDENTIFY_FRONT);
                break;
            case 2:
                //身份证反面
                Bundle bundle2 = new Bundle();
                bundle2.putInt(IntentKey.FROM, 1);
                ShowActivity.showActivityForResult(AddManagerActivity.this, IdCardCameraActivity.class,
                        bundle2, RequestCodeKey.INDENTIFY_CONTRARY);
                break;
        }
    }


    private void photoLocal() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        try {
            startActivityForResult(Intent.createChooser(intent, "请选择一张图片"),
                    RequestCodeKey.TAKE_PHOTO_LOCAL);
        } catch (Exception e) {
            CommonUtil.showToast(this, "请安装文件管理器");
        }
    }

    @OnClick({R.id.identity_front, R.id.identity_contrary,R.id.img_identity_front_clear,
            R.id.img_identity_contrary_clear, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.identity_front:
                if(CommonUtil.isEmpty(front_id_card_uri)) {
                    mPhotoStatus = 1;
                    initAlertView();
                    if (mPhotoAlert != null) {
                        mPhotoAlert.show();
                    }
                }

                break;
            case R.id.identity_contrary:
                if(CommonUtil.isEmpty(contrary_id_card_uri)) {
                    mPhotoStatus = 2;
                    initAlertView();
                    if (mPhotoAlert != null) {
                        mPhotoAlert.show();
                    }
                }
                break;
            case R.id.img_identity_front_clear:
                identityFrontTv.setVisibility(View.VISIBLE);
                imgIdentityFront.setVisibility(View.GONE);
                imgIdentityFrontClear.setVisibility(View.GONE);
                front_id_card_uri = "";
                break;
            case R.id.img_identity_contrary_clear:
                identityContraryTv.setVisibility(View.VISIBLE);
                imgIdentityContrary.setVisibility(View.GONE);
                imgIndentityContraryClear.setVisibility(View.GONE);
                contrary_id_card_uri = "";
                break;
            case R.id.btn_next:
                if (front_id_card_uri != null) {
                    data.setIdcartd1_photo(front_id_card_uri);
                } else {
                    showToast("请拍摄" + getString(R.string.idcard_front));
                    return;
                }
                if (contrary_id_card_uri != null) {
                    data.setIdcartd2_photo(contrary_id_card_uri);
                } else {
                    showToast("请拍摄" + getString(R.string.idcard_contrary));
                    return;
                }
                if (CommonUtil.isEmpty(etCarName)) {
                    showToast(getString(R.string.input_car_name));
                    return;
                } else {
                    data.setCar_owner(getEditString(etCarName));
                }
                if (CommonUtil.isEmpty(etIdCard)) {
                    showToast(getString(R.string.input_id_card));
                    return;
                } else {
                    data.setIdcard_no(getEditString(etIdCard));
                }
                if (CommonUtil.isEmpty(etPhoneNo)) {
                    showToast(getString(R.string.input_phone_no));
                    return;
                } else {
                    data.setMobile_phone(getEditString(etPhoneNo));
                }
                Bundle bundle3 = new Bundle();
                bundle3.putParcelable(IntentKey.CUSTOMER, data);
                ShowActivity.showActivityForResult(AddManagerActivity.this, AddManagerCarActivity.class, bundle3,
                        RequestCodeKey.GO_ADD_CUSTOMER);
                break;
        }
    }

    private void showFrontImage() {
        identityFrontTv.setVisibility(View.GONE);
        imgIdentityFront.setVisibility(View.VISIBLE);
        imgIdentityFrontClear.setVisibility(View.VISIBLE);
        Glide.with(this).load(front_id_card_uri).into(imgIdentityFront);
    }

    private void showContraryImgae() {
        identityContraryTv.setVisibility(View.GONE);
        imgIdentityContrary.setVisibility(View.VISIBLE);
        imgIndentityContraryClear.setVisibility(View.VISIBLE);
        Glide.with(this).load(contrary_id_card_uri).into(imgIdentityContrary);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constant.CAMERA_SUCESS:
                switch (requestCode) {
                    case RequestCodeKey.INDENTIFY_FRONT:
                        //身份证正面
                        if (data != null && data.getData() != null) {
                            front_id_card_uri = data.getData().getPath();
                            showFrontImage();
                        }
                        break;
                    case RequestCodeKey.INDENTIFY_CONTRARY:
                        //身份证反面
                        if (data != null && data.getData() != null) {
                            contrary_id_card_uri = data.getData().getPath();
                            showContraryImgae();
                        }
                        break;
                }
                break;
            case RESULT_OK:
                switch (requestCode) {
                    case RequestCodeKey.TAKE_PHOTO_LOCAL:
                        if (data != null && data.getData() != null) {
                            Uri uri = data.getData();
                            String path = CommonUtil.getLocalImagePath(this, uri);
                            XLog.e("path==>" + path);
                            switch (mPhotoStatus) {
                                case 1:
                                    front_id_card_uri = path;
                                    showFrontImage();
                                    break;
                                case 2:
                                    contrary_id_card_uri = path;
                                    showContraryImgae();
                                    break;
                            }
                        }
                        break;
                    case RequestCodeKey.GO_ADD_CUSTOMER:
                        boolean flag = data.getBooleanExtra(IntentKey.SUCCESS, false);
                        Intent intent = new Intent();
                        intent.putExtra(IntentKey.SUCCESS, flag);
                        setResult(RESULT_OK, intent);
                        finish();
                        break;
                }
                break;
        }

    }
}
