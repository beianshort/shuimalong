package com.xincheng.shuimalong.activity.management;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.layoutmanager.FullyLinearLayoutManager;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.UploadImageActivity;
import com.xincheng.shuimalong.adapter.config.VciTypeOrderAdapter;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.entity.config.VciTypeOrder;
import com.xincheng.shuimalong.entity.customer.Customer;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.http.Query;

/**
 * Created by 许浩 on 2017/6/22.
 */

public class ManagementDetailActivity extends BaseActivity {
    @BindView(R.id.tv_car_card)
    TextView tvCarCard;
    @BindView(R.id.tv_management_name)
    TextView tvManagementName;
    @BindView(R.id.tv_management_phone)
    TextView tvManagementPhone;
    @BindView(R.id.tv_management_card)
    TextView tvManagementCard;
    @BindView(R.id.look_car_id_photo)
    TextView lookCarIdPhoto;
    @BindView(R.id.tv_car_number)
    TextView tvCarNumber;
    @BindView(R.id.tv_car_vin)
    TextView tvVin;
    @BindView(R.id.car_engine_no)
    TextView carEngineNo;
    @BindView(R.id.tv_car_register_date)
    TextView tvCarRegisterDate;
    @BindView(R.id.tv_car_type)
    TextView tvCarType;
    @BindView(R.id.tv_car_useage)
    TextView tvCarUseage;
    @BindView(R.id.tv_car_bak)
    TextView tvCarBak;
    @BindView(R.id.tv_new_car)
    TextView tvNewCar;
    @BindView(R.id.tv_transfer)
    TextView tvTransfer;
    @BindView(R.id.tv_car_weight_title)
    TextView tvCarWeightTitle;
    @BindView(R.id.tv_car_weight)
    TextView tvCarWeight;
    @BindView(R.id.insurance_history_mrv)
    RecyclerView rvInsurance;


    private Customer customer;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_management_detail;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.management_detail));
        rvInsurance.setLayoutManager(new FullyLinearLayoutManager(this));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            parseCustomer(bundle);
        }
    }

    private void parseCustomer(Bundle bundle) {
        customer = bundle.getParcelable(IntentKey.CUSTOMER);
        //车主信息
        tvManagementName.setText(customer.getCar_owner());
        tvManagementPhone.setText(customer.getMobile_phone());
        tvManagementCard.setText(customer.getIdcard_no());
        //车辆信息
        tvCarNumber.setText(customer.getPlate_no());
        tvVin.setText(customer.getVin());
        carEngineNo.setText(customer.getEngine_no());
        tvCarRegisterDate.setText(customer.getCar_register_date());
        tvCarType.setText(customer.getCartypMsg());
        tvCarUseage.setText(customer.getUseageMsg());
        tvCarBak.setText(customer.getCar_bak());
        tvNewCar.setText(customer.getNewCarMsg());
        tvTransfer.setText(customer.getTransferCarMsg());
        if(!CommonUtil.isEmpty(customer.getOutput_volume())) {
            tvCarWeightTitle.setText(getString(R.string.output_volume));
            tvCarWeight.setText(customer.getOutput_volume());
        } else if(!CommonUtil.isEmpty(customer.getCurb_weight())) {
            tvCarWeightTitle.setText(getString(R.string.curb_weight));
            tvCarWeight.setText(customer.getCurb_weight());
        }
        String vciSettings = customer.getVci_settings();
        if (!CommonUtil.isEmpty(vciSettings)) {
            List<VciTypeOrder> vciTypeList = GsonUtil.parseData(CommonUtil.getBase64DecodeStr(vciSettings),
                    new TypeToken<List<VciTypeOrder>>() {
                    }.getType());
            XLog.e("vciTypeList=>" + vciTypeList);
            if (!CommonUtil.isEmpty(vciTypeList)) {
                VciTypeOrderAdapter adapter = new VciTypeOrderAdapter(this, vciTypeList);
                rvInsurance.setAdapter(adapter);
            }
        }
    }

    @OnClick({R.id.tv_car_card, R.id.look_car_id_photo})
    public void viewClick(View view) {
        switch (view.getId()) {
            case R.id.tv_car_card: {
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 1);
                extras.putString(IntentKey.UPLOAD_IMG_PATH1, customer.getIdcard_photo1());
                extras.putString(IntentKey.UPLOAD_IMG_PATH2, customer.getIdcard_photo2());
                extras.putBoolean(IntentKey.CAN_UPDATE, false);
                ShowActivity.showActivity(this, UploadImageActivity.class, extras);
            }
            break;
            case R.id.look_car_id_photo: {
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 2);
                extras.putString(IntentKey.UPLOAD_IMG_PATH1, customer.getDl_photo1());
                extras.putString(IntentKey.UPLOAD_IMG_PATH2, customer.getDl_photo2());
                extras.putBoolean(IntentKey.CAN_UPDATE, false);
                ShowActivity.showActivity(this, UploadImageActivity.class, extras);
            }
            break;
        }
    }

    @Override
    public void addListener() {

    }

}
