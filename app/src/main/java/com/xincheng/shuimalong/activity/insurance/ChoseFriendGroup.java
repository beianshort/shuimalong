package com.xincheng.shuimalong.activity.insurance;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.XRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.adapter.friend.GroupAdapter;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.dialog.GroupDialog;
import com.xincheng.shuimalong.entity.friend.Friend;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.view.ListItemDecoration;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 许浩 on 2017/7/26.
 */

public class ChoseFriendGroup extends BaseActivity {
    @BindView(R.id.rc_group)
    XRecyclerView rcGroup;
    private List<Friend>infolist;
    private GroupAdapter adapter;
    private GroupDialog dialog;
    private ArrayList<String> ids;
    @Override
    public int getLayoutResId() {
        return R.layout.activity_friend_group;
    }

    @Override
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        ids=bundle.getStringArrayList(IntentKey.FRIEND_IDS);
        infolist=new ArrayList<>();
        adapter=new GroupAdapter(getContext(),infolist);
        rcGroup.setLayoutManager(new LinearLayoutManager(getContext()));
        rcGroup.addItemDecoration(new ListItemDecoration());
        rcGroup.setAdapter(adapter);
        getFriendList();
    }
    @Override
    public void addListener() {
        rcGroup.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerHolder holder, View view, int position) {
                moveGroup(infolist.get(position-1).getGroup_id(),position);
            }
        });
    }

    @OnClick(R.id.tv_addGroup)
    public void onViewClicked() {
        if(dialog==null){
            dialog=GroupDialog.createDialog(getContext())
                    .setConfirmListener(new GroupDialog.ConfirmListener() {
                        @Override
                        public void confirmListener(final String message) {
                            getFriendList();
                            RetroSubscrube.getInstance().postSubscrube(AppUrl.ADD_GROUP,
                                    PostParam.addGroup(PrefUtil.getString(getContext(), PrefKey.TOKEN, ""),
                                            message),
                                    new BaseObserver(getContext()) {
                                        @Override
                                        protected void onHandleSuccess(Object data, String msg) {
                                            dialog.dismiss();
                                            showToast(msg);
                                            setResult(RESULT_OK);
                                            getFriendList();
                                        }
                                    });
                        }
                    });
        }
        dialog.show();
    }
    private void getFriendList(){
        RetroSubscrube.getInstance().getSubscrube(GetParam.getFriends(PrefUtil.getString(getContext(), PrefKey.TOKEN, "")),
                new BaseObserver(getContext()) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                try {
                    List<Friend>friends=GsonUtil.parseData(data,new TypeToken<List<Friend>>() {}.getType());
                    infolist.clear();
                    infolist.addAll(friends);
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    XLog.e("friend"+e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
    private void moveGroup(String group_id,final int position){
        RetroSubscrube.getInstance().postSubscrube(AppUrl.MOVE_GROUP,
                PostParam.moveGroup(PrefUtil.getString(getContext(), PrefKey.TOKEN, ""),
                        group_id,getFriendId()),
                new BaseObserver(getContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg);
                        setResult(RESULT_OK);
                        for(Friend f:infolist){
                            f.setChecked(0);
                        }
                        infolist.get(position-1).setChecked(1);
                        adapter.notifyDataSetChanged();
                    }
                });
    }
    private String getFriendId(){
        String id=null;
        for(String str:ids){
            if(id==null){
                id=str;
            }else{
                id+=","+str;
            }
        }
        return id;
    }
}
