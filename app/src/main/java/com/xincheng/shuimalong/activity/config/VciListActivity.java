package com.xincheng.shuimalong.activity.config;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.adapter.config.VciTypeListAdapter;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.util.ConfigUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/26.
 */

public class VciListActivity extends BaseActivity {
    @BindView(R.id.vci_list_mrv)
    MRecyclerView mVicListRv;

    private VciTypeListAdapter mAdapter;
    private List<VciType> mVciTypeList;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_select_vcitype;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.vci_list_title));
        mVciTypeList = ConfigUtil.getVciTypes(this);
        ArrayList<VciType> selectList = getIntent().getExtras().getParcelableArrayList(IntentKey.VCI_TYPE_LIST);
        if (!CommonUtil.isEmpty(selectList)) {
            for (int i = 0; i < selectList.size(); i++) {
                for (int j = 0; j < mVciTypeList.size(); j++) {
                    if (selectList.get(i).getId() == mVciTypeList.get(j).getId()) {
                        mVciTypeList.get(j).setSelected(true);
                    }
                }
            }
        }
        XLog.e("vciTypeList=>" + mVciTypeList.size());
        mVicListRv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new VciTypeListAdapter(this, mVciTypeList);
        mVicListRv.setAdapter(mAdapter);
    }

    @OnClick(R.id.vci_submit_btn)
    public void selectVci() {
        ArrayList<VciType> selectedList = new ArrayList<>();
        for (int i = 0; i < mVciTypeList.size(); i++) {
            if (mVciTypeList.get(i).isSelected()) {
                VciType vciType = mVciTypeList.get(i);
                selectedList.add(vciType);
            }
        }
        Intent intent = new Intent();
        intent.putExtra(IntentKey.VCI_TYPE_LIST, selectedList);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void addListener() {
        mVicListRv.setOnItemClickListener(new MyOnClickListener());
    }

    private class MyOnClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            VciType vciType = mAdapter.getItem(position);
            vciType.selectToggle();
            mAdapter.notifyDataSetChanged();
        }
    }
}
