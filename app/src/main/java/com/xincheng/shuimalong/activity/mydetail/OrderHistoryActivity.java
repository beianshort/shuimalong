package com.xincheng.shuimalong.activity.mydetail;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.OrderDetailActivity;
import com.xincheng.shuimalong.adapter.order.OrderHistoryAdapter;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.UserConfig;
import com.xincheng.shuimalong.entity.HistoryOrder;
import com.xincheng.shuimalong.entity.Order;
import com.xincheng.shuimalong.entity.user.UserHome;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;
import com.xincheng.shuimalong.view.CardItemDecoration;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by xuhao on 2017/8/8.
 */

public class OrderHistoryActivity extends BaseActivity implements OnItemClickListener {
    @BindView(R.id.common_titlebar_layout)
    LinearLayout commonTitlebarLayout;
    @BindView(R.id.img_avatar)
    ImageView imgAvatar;
    @BindView(R.id.tv_nickname)
    TextView tvNickname;
    @BindView(R.id.tv_authentication)
    TextView tvAuthentication;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.history_recycler)
    MRecyclerView historyRecycler;

    private OrderHistoryAdapter adapter;
    private List<HistoryOrder> infolist;
    private int flag;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_history_layout;
    }

    @Override
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        flag = bundle.getInt(IntentKey.FROM, 0);
        commonTitlebarLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        adapter = new OrderHistoryAdapter(getContext());
        historyRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        historyRecycler.addItemDecoration(new CardItemDecoration());
        historyRecycler.setAdapter(adapter);
        collectMessage();

    }

    @Override
    public void addListener() {
        historyRecycler.setOnItemClickListener(this);
    }

    public void collectMessage() {
        String title = "";
        switch (flag) {
            case 0:
                title = "历史订单";
                break;
            case UserConfig.MONTH_RECEIPT_STATUS:
                title = "本月收单";
                getAcindexMessage();
                break;
            case UserConfig.MONTH_SELL_STATUS:
                title = "本月卖单";
                getSellMessage();
                break;
            case UserConfig.TOTAL_RECEIPT_STATUS:
                title = "累计收单";
                getAcindexMessage();
                break;
            case UserConfig.TOTAL_SELL_STATUS:
                title = "累计卖单";
                getSellMessage();
                break;
        }
        initTitleBar(title);
        setmTitleTvColor(getResources().getColor(R.color.white));
        setmTitleLineGone(View.GONE);
        setmBackIvWhiteSrc();
    }

    //卖单个人中心
    private void getSellMessage() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getBillindex(PrefUtil.getString(getContext(), PrefKey.TOKEN,
                "")),
                new BaseObserver(OrderHistoryActivity.this) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            UserHome mManagement = GsonUtil.parseData(data, UserHome.class);
                            setViewMessage(mManagement);
                        }
                    }
                });
    }

    private void getAcindexMessage() {
        RetroSubscrube.getInstance().getSubscrube(
                GetParam.getAcindex(PrefUtil.getString(getContext(), PrefKey.TOKEN, "")),
                new BaseObserver(OrderHistoryActivity.this) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            UserHome mManagement = GsonUtil.parseData(data, UserHome.class);
                            setViewMessage(mManagement);
                        }
                    }
                });
    }

    private void setViewMessage(UserHome management) {
        Glide.with(getContext()).load(management.getAvatar())
                .apply(GlideOptionsManager.getInstance().getRequestOptions()).into(imgAvatar);
        tvNickname.setText(management.getNickName() == null ? management.getUserName() :
                management.getNickName());
        tvBalance.setText(management.getAmount() + "元");
        tvAuthentication.setText(management.getCertMsg());
        tvAuthentication.setVisibility(View.VISIBLE);
        infolist = new ArrayList<>();
        switch (flag) {
            case UserConfig.MONTH_RECEIPT_STATUS:
            case UserConfig.MONTH_SELL_STATUS:
                if (!CommonUtil.isEmpty(management.getMonthOrderList())) {
                    infolist.addAll(management.getMonthOrderList());
                }
                break;
            case UserConfig.TOTAL_RECEIPT_STATUS:
            case UserConfig.TOTAL_SELL_STATUS:
                if (!CommonUtil.isEmpty(management.getOrderList())) {
                    infolist.addAll(management.getOrderList());
                }
                break;
        }
        adapter.refreshData(infolist);
    }

    @Override
    public void onItemClick(RecyclerHolder holder, View view, int position) {
        Bundle extras = new Bundle();
        HistoryOrder data = infolist.get(position);
        extras.putInt(IntentKey.BID_ID, data.getBid_id());
        switch (flag) {
            case UserConfig.MONTH_RECEIPT_STATUS:
            case UserConfig.TOTAL_RECEIPT_STATUS:
                extras.putInt(IntentKey.ORDER_CONFIG, OrderConfig.RECEIPT);
                break;
            case UserConfig.MONTH_SELL_STATUS:
            case UserConfig.TOTAL_SELL_STATUS:
                extras.putInt(IntentKey.ORDER_CONFIG, OrderConfig.SELL);
                break;
        }
        extras.putInt(IntentKey.FROM, 2);
        ShowActivity.showActivity(this, OrderDetailActivity.class, extras);
    }
}
