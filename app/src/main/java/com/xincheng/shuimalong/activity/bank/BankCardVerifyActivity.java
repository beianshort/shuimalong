package com.xincheng.shuimalong.activity.bank;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewDebug;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.bank.BankCard;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/30.
 */

public class BankCardVerifyActivity extends BaseActivity {
    @BindView(R.id.bankcard_no_tv)
    TextView mBankcardNoTv;
    @BindView(R.id.bankcard_user_et)
    EditText mUserEt;
    @BindView(R.id.bankcard_idcardno_et)
    EditText mIdCardNoEt;
    @BindView(R.id.bankcard_mobile_et)
    EditText mMobileEt;
    @BindView(R.id.bankcard_verify_cb)
    CheckBox mVerifyCb;

    private BankCard.Data mBankCardData;
    private String mBankCardName = "";
    private String mBankCardNo = "";
    private String mUserName = "";
    private String mIdCard = "";
    private String mMobile = "";
    private String mBankLattice="";

    @Override
    public int getLayoutResId() {
        return R.layout.activity_bankcard_verify;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.bankcard_verify_title));
        Bundle extras = getIntent().getExtras();
        mBankCardData = extras.getParcelable(IntentKey.BANK_CARD);
        if (mBankCardData == null) {
            mBankCardNo = extras.getString(IntentKey.BANK_CARD_NO);
            mBankcardNoTv.setText(mBankCardNo);
        } else {
            mBankcardNoTv.setText(mBankCardData.getBankcardNo());
            mUserEt.setText(mBankCardData.getName());
            mIdCardNoEt.setText(mBankCardData.getIdcardNo());
        }
        mBankCardName=extras.getString(IntentKey.BANK_CARD_NAME);
        mBankLattice=extras.getString(IntentKey.BANK_LATTICE);
    }

    @OnClick(R.id.bankcard_verify_next_tv)
    public void verifyNext() {
        if (checkData()) {
            mBankCardNo = mBankcardNoTv.getText().toString().trim();
            mUserName = mUserEt.getText().toString().trim();
            mIdCard = mIdCardNoEt.getText().toString().trim();
            if(!MyUtil.checkIdCard(mIdCard)) {
                showToast("身份证号不合法");
                return;
            }
            mMobile = mMobileEt.getText().toString().trim();
            Bundle extras = new Bundle();
            extras.putString(IntentKey.BANK_CARD_NO, mBankCardNo);
            extras.putString(IntentKey.BANK_CARD_NAME, mBankCardName);
            extras.putString(IntentKey.USER_NAME, mUserName);
            extras.putString(IntentKey.ID_CARD_NO, mIdCard);
            extras.putString(IntentKey.MOBILE, mMobile);
            extras.putString(IntentKey.BANK_LATTICE, mBankLattice);
            ShowActivity.showActivityForResult(this, BankCardVerifyCodeActivity.class, extras,
                    RequestCodeKey.GO_ADD_BANKCARD);
        }
    }

    private boolean checkData() {
        if (CommonUtil.isEmpty(mUserEt)) {
            showToast("请输入持卡人姓名");
            return false;
        }
        if (CommonUtil.isEmpty(mIdCardNoEt)) {
            showToast("请输入持卡人身份证号");
            return false;
        }
        if (CommonUtil.isEmpty(mMobileEt)) {
            showToast("请输入银行预留手机号");
            return false;
        }
        if (!mVerifyCb.isChecked()) {
            showToast("请选择同意银联服务协议");
            return false;
        }
        return true;
    }

    @Override
    public void addListener() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_ADD_BANKCARD:
                    Intent intent = new Intent();
                    intent.putExtra(IntentKey.SUCCESS, data.getBooleanExtra(IntentKey.SUCCESS, false));
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
