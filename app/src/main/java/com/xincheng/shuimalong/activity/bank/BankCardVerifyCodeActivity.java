package com.xincheng.shuimalong.activity.bank;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.login.InputCodeActivity;
import com.xincheng.shuimalong.activity.login.RetrieveActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.user.MobileToken;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.MyCountDownTimer;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

/**
 * Created by xiaote on 2017/7/20.
 */

public class BankCardVerifyCodeActivity extends BaseActivity {
    @BindView(R.id.verify_mobile_tv)
    TextView mMobileTv;
    @BindView(R.id.verify_code_et)
    EditText mCodeEt;
    @BindView(R.id.verify_code_get_btn)
    Button mCodeGetBtn;

    private String mBankCardName = "";
    private String mBankCardNo = "";
    private String mUserName = "";
    private String mIdCard = "";
    private String mMobile = "";
    private String mBankLattice="";
    private String mToken;

    private MyCountDownTimer myCountDownTimer;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_bankcard_verify_code;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.input_code));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        myCountDownTimer = new MyCountDownTimer(mCodeGetBtn, 60000, 1000);
        Bundle extras = getIntent().getExtras();
        mBankCardName = extras.getString(IntentKey.BANK_CARD_NAME);
        mBankCardNo = extras.getString(IntentKey.BANK_CARD_NO);
        mUserName = extras.getString(IntentKey.USER_NAME);
        mIdCard = extras.getString(IntentKey.ID_CARD_NO);
        mMobile = extras.getString(IntentKey.MOBILE);
        mBankLattice=extras.getString(IntentKey.BANK_LATTICE);
        if (!CommonUtil.isEmpty(mMobile)) {
            mMobileTv.setText(mMobile);
            getCode();
        }
    }

    @OnClick({R.id.verify_code_get_btn, R.id.verify_next_tv})
    public void viewClick(View view) {
        switch (view.getId()) {
            case R.id.verify_code_get_btn:
                if (!CommonUtil.isEmpty(mMobileTv)) {
                    getCode();
                }
                break;
            case R.id.verify_next_tv:
                if (CommonUtil.isEmpty(mCodeEt)) {
                    showToast("验证码不能为空");
                    return;
                }
                checkCertKey();
                break;
        }
    }

    @Override
    public void addListener() {

    }

    private void getCode() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getcertkey(mMobile),
                new BaseObserver(getContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg);
                        myCountDownTimer.start();
                    }
                }
        );
    }

    private void checkCertKey() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.CHECK_CERT_KEY,
                PostParam.getCheckcertkey(mMobile, getEditString(mCodeEt)),
                new BaseObserver(getBaseContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        addBankCard();
                    }
                }
        );
    }

    private void addBankCard() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.ADD_BANK,
                PostParam.addBank(mToken, mUserName, mIdCard, mBankCardName, mBankCardNo,mBankLattice),
                new BaseObserver(this,getString(R.string.loading_submit)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg);
                        Intent intent = new Intent();
                        intent.putExtra(IntentKey.SUCCESS,true);
                        setResult(RESULT_OK,intent);
                        finish();
                    }

                });
    }
}
