package com.xincheng.shuimalong.activity.address;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.library.xswipemenurecyclerview.widget.DividerListItemDecoration;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.adapter.AddressAdapter;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.address.Address;
import com.xincheng.shuimalong.entity.address.AddressStatus;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/29.
 * 收货地址管理
 */

public class AddressManagementActivity extends BaseActivity {
    @BindView(R.id.address_management_mrv)
    MRecyclerView mManagementMrv;

    private AddressAdapter mAdapter;
    private List<Address> mAddressList;
    private BaseObserver mObserver;
    private AddressStatus mStatus;
    private String mToken;
    private Address mCurrentAddress;
    private int mFrom;
    private int mAddressId;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_address_management;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.address_management));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        mManagementMrv.setLayoutManager(new LinearLayoutManager(this));
        mManagementMrv.addItemDecoration(new DividerListItemDecoration(this, R.drawable.recycler_divider_10dp));
        mAdapter = new AddressAdapter(this);
        mAdapter.setItemButtonClickListener(new MyItemButtonClickListener(this));
        mManagementMrv.setAdapter(mAdapter);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mFrom = extras.getInt(IntentKey.FROM, 0);
            mAddressId = extras.getInt(IntentKey.ADDRESS_ID, 0);
        }
        initObserver();
        getAddress();
    }

    private void setupCheckedData() {
        if (mFrom == 1 && mAddressId != 0) {
            for (int i = 0; i < mAddressList.size(); i++) {
                if (mAddressId == mAddressList.get(i).getAddressId()) {
                    mAddressList.get(i).setSelect(true);
                }
            }
        }
    }

    private void initObserver() {
        mObserver = new BaseObserver(this, getString(R.string.loading_data)) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                switch (mStatus) {
                    case GET:
                        if (data != null) {
                            try {
                                mAddressList = GsonUtil.parseData(data, new TypeToken<List<Address>>() {
                                }.getType());
                                setupCheckedData();
                                mAdapter.refreshData(mAddressList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case SET_DEFAULT:
                        getAddress();
                        break;
                    case DELETE:
                        getAddress();
                        break;
                }
            }
        };
    }

    @OnClick(R.id.address_add_btn)
    public void addressAdd() {
        ShowActivity.showActivityForResult(this, AddressAddActivity.class, RequestCodeKey.GO_ADD_ADDRESS);
    }

    @Override
    public void addListener() {
        mManagementMrv.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_ADD_ADDRESS:
                    Address address = data.getParcelableExtra(IntentKey.ADDRESS);
                    if (address != null) {
                        getAddress();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getAddress() {
        mStatus = AddressStatus.GET;
        RetroSubscrube.getInstance().getSubscrube(GetParam.getAddress(mToken), mObserver);
    }

    private void setDefaultAddress() {
        mStatus = AddressStatus.SET_DEFAULT;
        RetroSubscrube.getInstance().postSubscrube(AppUrl.SET_DEFAULT_ADDRESS,
                PostParam.setDefaultAddress(mToken, mCurrentAddress.getAddressId()), mObserver);
    }

    private void deleteAddress() {
        mStatus = AddressStatus.DELETE;
        RetroSubscrube.getInstance().postSubscrube(AppUrl.DELETE_ADDRSS,
                PostParam.deleteAddress(mToken, mCurrentAddress.getAddressId()), mObserver);
    }

    private class MyItemButtonClickListener implements ItemButtonClickListener {

        private Activity activity;

        public MyItemButtonClickListener(Activity activity) {
            this.activity = activity;
        }

        public void buttonClick(int clickId, int position) {
            mCurrentAddress = mAdapter.getItem(position);
            switch (clickId) {
                case R.id.address_default_ctv://设置为默认地址
                    if (!mCurrentAddress.isDefault()) {
                        setDefaultAddress();
                    }
                    break;
                case R.id.address_edit_tv://编辑收货地址
                    Bundle extras = new Bundle();
                    extras.putParcelable(IntentKey.ADDRESS, mCurrentAddress);
                    ShowActivity.showActivityForResult(activity, AddressAddActivity.class, extras,
                            RequestCodeKey.GO_ADD_ADDRESS);
                    break;
                case R.id.address_delete_tv://删除收货地址
                    deleteAddress();
                    break;
            }
        }
    }

    private class MyOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            mCurrentAddress = mAdapter.getItem(position);
            XLog.e("position==>" + position);
            if (mFrom == 1) {
                XLog.e("position==>" + position);
                Intent intent = new Intent();
                intent.putExtra(IntentKey.ADDRESS, mCurrentAddress);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    public void back(View view) {
        if(mFrom == 1 && CommonUtil.isEmpty(mAddressList)) {
            Intent intent = new Intent();
            setResult(RESULT_OK,intent);
        }
        super.back(view);
    }
}
