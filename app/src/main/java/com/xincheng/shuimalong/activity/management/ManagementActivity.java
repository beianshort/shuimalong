package com.xincheng.shuimalong.activity.management;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xswipemenurecyclerview.Mode;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.listener.OnLoadingListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.XRecyclerView;
import com.xincheng.library.xswipemenurecyclerview.widget.ProgressStyle;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.SearchActivity;
import com.xincheng.shuimalong.adapter.ManagementAdapter;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RefreshStatus;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.customer.Customer;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.ShowActivity;
import com.xincheng.shuimalong.view.ListItemDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 许浩 on 2017/6/20.
 */

public class ManagementActivity extends BaseActivity {
    @BindView(R.id.management_search_tv)
    TextView mSearchTv;
    @BindView(R.id.rv_management)
    XRecyclerView rvManagement;

    ManagementAdapter adapter;
    private List<Customer> infolist;
    private String mToken;
    private int mPage = 1;
    private String mKeyword;
    private RefreshStatus mRefreshStatus;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_management;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.management));
        setRightTv("添加");
        mToken = PrefUtil.getString(getContext(), PrefKey.TOKEN, "");
        mPage = 1;
        rvManagement.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        rvManagement.addItemDecoration(new ListItemDecoration());
        rvManagement.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        rvManagement.setLoadingMoreProgressStyle(ProgressStyle.BallRotate);
        adapter = new ManagementAdapter(this);
        rvManagement.setAdapter(adapter);
        rvManagement.setMode(Mode.BOTH);
        mRefreshStatus = RefreshStatus.LOAD_DEFAULT;
        getManagement();
    }

    @OnClick(R.id.management_search_layout)
    public void viewClick(View view) {
        switch (view.getId()) {
            case R.id.management_search_layout:
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 3);
                ShowActivity.showActivityForResult(this, SearchActivity.class, extras, RequestCodeKey.GO_SEARCH);
                break;
        }
    }

    @Override
    public void addListener() {
        rvManagement.setOnItemClickListener(new MyOnItemClickListener());
        rvManagement.setOnLoadingListener(new MyOnLoadingListener());
    }

    private void getManagement() {
        BaseObserver observer = new BaseObserver(getContext()) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                rvManagement.setMode(Mode.BOTH);
                if (data != null) {
                    try {
                        List<Customer> list = GsonUtil.parseData(data, new TypeToken<List<Customer>>() {
                        }.getType());
                        switch (mRefreshStatus) {
                            case LOAD_DEFAULT:
                                infolist = list;
                                break;
                            case PULL_TO_REFRESH:
                                rvManagement.refreshComplete();
                                infolist = list;
                                break;
                            case LOAD_MORE:
                                rvManagement.loadMoreComplete();
                                if (!CommonUtil.isEmpty(list)) {
                                    infolist.addAll(list);
                                }
                                break;
                        }
                        if (MyUtil.isDataLengthLess(list)) {
                            rvManagement.setMode(Mode.PULL_TO_REFRESH);
                        }
                        adapter.refreshData(infolist);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        if (mRefreshStatus == RefreshStatus.LOAD_DEFAULT) {
            observer.initProgressDialog(this, getString(R.string.loading_data));
        }
        RetroSubscrube.getInstance().getSubscrube(GetParam.getCustomer(mToken, mPage, mKeyword), observer);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_ADD_CUSTOMER:
                    boolean flag = data.getBooleanExtra(IntentKey.SUCCESS, false);
                    if (flag) {
                        getManagement();
                    }
                    break;
                case RequestCodeKey.GO_SEARCH:
                    mKeyword = data.getStringExtra(IntentKey.KEYWORD);
                    mPage = 1;
                    mRefreshStatus = RefreshStatus.LOAD_DEFAULT;
                    getManagement();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void rightListener() {
        ShowActivity.showActivityForResult(ManagementActivity.this, AddManagerActivity.class,
                RequestCodeKey.GO_ADD_CUSTOMER);
    }

    private class MyOnItemClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(IntentKey.CUSTOMER, infolist.get(position - Constant.ITEM_POSITION_MINUS));
            ShowActivity.showActivity(ManagementActivity.this, ManagementDetailActivity.class, bundle);
        }
    }

    private class MyOnLoadingListener implements OnLoadingListener {

        @Override
        public void onRefresh() {
            mPage = 1;
            mRefreshStatus = RefreshStatus.PULL_TO_REFRESH;
            getManagement();
        }

        @Override
        public void onLoadMore() {
            mPage++;
            mRefreshStatus = RefreshStatus.LOAD_MORE;
            getManagement();
        }
    }
}
