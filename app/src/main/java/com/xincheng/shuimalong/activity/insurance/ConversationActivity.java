package com.xincheng.shuimalong.activity.insurance;

import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;

/**
 * Created by xuhao on 2017/7/31.
 */

public class ConversationActivity extends BaseActivity {
    @Override
    public int getLayoutResId() {
        return R.layout.activity_conversation;
    }

    @Override
    public void initData() {
        initTitleBar(getIntent().getData().getQueryParameter("title"));
    }

    @Override
    public void addListener() {

    }
}
