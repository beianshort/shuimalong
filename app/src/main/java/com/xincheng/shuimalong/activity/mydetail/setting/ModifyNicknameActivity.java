package com.xincheng.shuimalong.activity.mydetail.setting;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

/**
 * Created by xiaote on 2017/7/18.
 */

public class ModifyNicknameActivity extends BaseActivity {
    @BindView(R.id.modify_nickname_et)
    EditText mNicknameEt;

    private String mNickname;
    private String mToken;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_modify_nickname;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.modify_nickname));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String nickName = extras.getString(IntentKey.NICK_NAME);
            if (!CommonUtil.isEmpty(nickName)) {
                mNicknameEt.setText(nickName);
                mNicknameEt.setSelection(nickName.length());
            }
        }
    }

    @OnClick(R.id.modify_nickname_btn)
    public void modifyNickName() {
        if (CommonUtil.isEmpty(mNicknameEt)) {
            showToast("昵称不能为空");
            return;
        }
        mNickname = mNicknameEt.getText().toString().trim();
        changeUserInfo();
    }

    private void changeUserInfo() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.CHANGE_USER_INFO,
                PostParam.changeUserInfo(mToken, mNickname),
                new BaseObserver(this, getString(R.string.loading_modify)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg);
                        PrefUtil.putString(getContext(), PrefKey.LOGIN_NICKNAME, mNickname);
                        Intent intent = new Intent();
                        intent.putExtra(IntentKey.NICK_NAME,mNickname);
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                });
    }

    @Override
    public void addListener() {

    }
}
