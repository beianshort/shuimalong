package com.xincheng.shuimalong.activity.recept;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.VciTypeOrder;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.DisplayUtil;
import com.xincheng.shuimalong.util.GsonUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.annotations.NonNull;

/**
 * Created by xuhao on 2017/8/9.
 */

public class ChargeBackActivity extends BaseActivity {
    @BindView(R.id.radio_chargeback)
    RadioGroup radioChargeback;
    @BindView(R.id.et_input_chargeback)
    EditText etInputChargeback;
    @BindView(R.id.tv_input_number)
    TextView tvInputNumber;

    private String mToken;
    private int mBidId;
    private int mRefuseReason;
    private String mRefuseBak;
    private List<CommonConfig> refuseReason;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_chargeback_layout;
    }

    @Override
    public void initData() {
        initTitleBar("退回原因");
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mBidId = extras.getInt(IntentKey.BID_ID);
        }
        refuseReason = ConfigUtil.getCommonConfigList(this, PrefKey.REFUSE_REASON);
        if (refuseReason != null) {
            for (int i = 0; i < refuseReason.size(); i++) {
                CommonConfig config = refuseReason.get(i);
                RadioButton radioButton = getRadioButton(config);
                if (i == 0) {
                    mRefuseReason = config.getId();
                    radioButton.setChecked(true);
                }
                radioChargeback.addView(radioButton);
            }
        }
    }

    public RadioButton getRadioButton(CommonConfig commonConfig) {
        RadioButton radioButton = (RadioButton) LayoutInflater.from(getContext()).inflate(R.layout
                .radio_button_laout, null);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                DisplayUtil.dip2px(getContext(), 59));
        radioButton.setLayoutParams(layoutParams);
        radioButton.setId(commonConfig.getId());
        radioButton.setText(commonConfig.getName());
        return radioButton;
    }

    @OnClick(R.id.chargeback_btn)
    public void chargeBack() {
        mRefuseBak = etInputChargeback.getText().toString().trim();
        RetroSubscrube.getInstance().postSubscrube(AppUrl.REFUSE_BID, PostParam.refuseBid(mToken, mBidId,
                mRefuseReason, mRefuseBak), new BaseObserver(this, getString(R.string.loading_submit)) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                showToast(msg);
                Intent intent = new Intent();
                intent.putExtra(IntentKey.REFUSE_SUCCESS, true);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void addListener() {
        radioChargeback.setOnCheckedChangeListener(new MyOnCheckedChangedListener());
    }


    @OnTextChanged(value = R.id.et_input_chargeback, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable editable) {
        tvInputNumber.setText(String.valueOf(getResources().getInteger(R.integer.edit_length_default_500)
                - editable.length()));
    }

    private class MyOnCheckedChangedListener implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkId) {
            mRefuseReason = checkId;
        }
    }
}
