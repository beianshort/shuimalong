package com.xincheng.shuimalong.activity.config;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.adapter.config.CommonConfigGridAdapter;
import com.xincheng.shuimalong.adapter.config.CommonConfigListAdapter;
import com.xincheng.shuimalong.adapter.config.RegionDataListAdapter;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.GsonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by xiaote on 2017/6/23.
 * 保险公司选择
 */

public class InsuranceCompanyActivity extends BaseActivity {
    @BindView(R.id.insurance_current_tv)
    TextView mCurrentTv;
    @BindView(R.id.insurance_company_select_rv)
    MRecyclerView mCompanySelectRv;
    @BindView(R.id.insurance_company_list_rv)
    MRecyclerView mCompanyListRv;

    private CommonConfigListAdapter mAdapter;

    private List<CommonConfig> mCompanySelectList;
    private CommonConfigGridAdapter mGridAdapter;
    private int mFrom;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_select_insurance_company;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.insurance_company));
        initCompanyList();
        initSelectCompanyList();
    }

    private void initCompanyList() {
        List<CommonConfig> companyList = ConfigUtil.getCommonConfigList(this, PrefKey.COMPANY_LIST);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mFrom = extras.getInt(IntentKey.FROM, 0);
            CommonConfig company = extras.getParcelable(IntentKey.INSURANCE_COMPANY);
            if (mFrom == 1) {
                CommonConfig commonConfig = new CommonConfig();
                commonConfig.setId(0);
                commonConfig.setName("全部");
                companyList.add(0, commonConfig);
            }
            if (company != null) {
                mCurrentTv.setText(company.getName());
            } else {
                mCurrentTv.setText(companyList.get(mFrom == 1 ? 1 : 0).getName());
            }
        } else {
            mCurrentTv.setText(companyList.get(0).getName());
        }
        mCompanyListRv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new CommonConfigListAdapter(this, companyList);
        mCompanyListRv.setAdapter(mAdapter);
    }

    private void initSelectCompanyList() {
        String companySave = PrefUtil.getString(this, PrefKey.COMPANY_SAVE, "");
        if (!CommonUtil.isEmpty(companySave)) {
            mCompanySelectList = ConfigUtil.getCommonConfigList(this, PrefKey.COMPANY_SAVE);
        } else {
            mCompanySelectList = new ArrayList<>();
        }
        mCompanySelectRv.setLayoutManager(new GridLayoutManager(this, Constant.COMPANY_SELECT_ROW_ITEMS_SIZE));
        mGridAdapter = new CommonConfigGridAdapter(this, mCompanySelectList);
        mCompanySelectRv.setAdapter(mGridAdapter);
    }

    @Override
    public void addListener() {
        mCompanySelectRv.setOnItemClickListener(new MyOnGridItemClickListener());
        mCompanyListRv.setOnItemClickListener(new MyOnItemClickListener());
    }

    private class MyOnGridItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            Intent intent = new Intent();
            intent.putExtra(IntentKey.INSURANCE_COMPANY, mGridAdapter.getItem(position));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private class MyOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(RecyclerHolder holder, View view, int position) {
            CommonConfig company = mAdapter.getItem(position);
            saveCompany(company);
            Intent intent = new Intent();
            intent.putExtra(IntentKey.INSURANCE_COMPANY, company);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void saveCompany(CommonConfig company) {
        if(company.getId() == 0) {
            return;
        }
        if (mCompanySelectList == null) {
            mCompanySelectList = new ArrayList<>();
            mCompanySelectList.add(company);
        } else {
            for (int i = 0; i < mCompanySelectList.size(); i++) {
                if (company.getId() == mCompanySelectList.get(i).getId()) {
                    return;
                }
            }
            if (mCompanySelectList.size() < Constant.COMPANY_SELECT_ROW_ITEMS_SIZE) {
                mCompanySelectList.add(company);
            } else {
                mCompanySelectList.remove(0);
                mCompanySelectList.add(company);
            }
        }
        PrefUtil.putString(this, PrefKey.COMPANY_SAVE, GsonUtil.toJson(mCompanySelectList));
    }
}
