package com.xincheng.shuimalong.activity.mydetail;

import android.os.Bundle;
import android.widget.TextView;

import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.UserConfig;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/7/19.
 * 历史收益
 */

public class HistoryEarningActivity extends BaseActivity {
    @BindView(R.id.issuing_total_tv)
    TextView mIssuingTotalTv;
    @BindView(R.id.issuing_money_total_tv)
    TextView mIssuingMoneyTotalTv;
    @BindView(R.id.vci_performance_tv)
    TextView mVciPerformanceTv;
    @BindView(R.id.tci_performance_tv)
    TextView mTciPerformanceTv;
    @BindView(R.id.num_total_tv)
    TextView mNumTotalTv;
    @BindView(R.id.vci_title_tv)
    TextView mVciTitleTv;
    @BindView(R.id.tci_title_tv)
    TextView mTciTitleTv;
    private int from;
    private int mNum;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_history_earnings;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.history_earnings));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String totalNum = extras.getString(IntentKey.ISSUING_NUMBER, "0");
            mNum = Integer.parseInt(totalNum);
            mIssuingTotalTv.setText(totalNum);
            mIssuingMoneyTotalTv.setText(extras.getString(IntentKey.TOTAL_MONEY, "0"));
            mVciPerformanceTv.setText(extras.getString(IntentKey.VCI_PERFORMANCE, "0"));
            mTciPerformanceTv.setText(extras.getString(IntentKey.TCI_PERFORMANCE, "0"));
            from = extras.getInt(IntentKey.FROM);
            switch (from) {
                case 1:
                    mNumTotalTv.setText(getString(R.string.receipt_total));
                    mVciTitleTv.setText(getString(R.string.vci_performance));
                    mTciTitleTv.setText(getString(R.string.tci_performance));
                    break;
                case 2:
                    mNumTotalTv.setText(getString(R.string.sell_total));
                    mVciTitleTv.setText(getString(R.string.commercial_insurance_promotion_fee));
                    mTciTitleTv.setText(getString(R.string.compulsory_insurance_promotion_fee));
                    break;
            }
        }
    }

    @Override
    public void addListener() {

    }

    @OnClick(R.id.order_history_id)
    public void onViewClicked() {
        if(mNum == 0) {
            return;
        }
        Bundle extras = new Bundle();
        switch (from) {
            case UserConfig.MONTH_RECEIPT_STATUS:
                extras.putInt(IntentKey.FROM, UserConfig.TOTAL_RECEIPT_STATUS);
                break;
            case UserConfig.MONTH_SELL_STATUS:
                extras.putInt(IntentKey.FROM, UserConfig.TOTAL_SELL_STATUS);
                break;
        }
        ShowActivity.showActivity(this, OrderHistoryActivity.class, extras);
    }
}
