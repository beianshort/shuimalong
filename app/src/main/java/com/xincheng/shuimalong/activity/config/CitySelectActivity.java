package com.xincheng.shuimalong.activity.config;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.fragment.config.CityFragment;
import com.xincheng.shuimalong.fragment.config.ProvinceFragment;

/**
 * Created by xiaote on 2017/6/16.
 */

public class CitySelectActivity extends BaseActivity {
    private RegionData province;

    public void setProvince(RegionData province) {
        this.province = province;
    }

    public RegionData getProvince() {
        return province;
    }

    public TabLayout getTabLayoyt() {
        return mTabLayout;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_select_city;
    }

    @Override
    public void initData() {
        initTitleBarTabLayout(getResources().getStringArray(R.array.city_title_arr));
        mTabLayout.getTabAt(0).select();
        ProvinceFragment provinceFragment = new ProvinceFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.province_city_layout, provinceFragment).commitAllowingStateLoss();
    }


    @Override
    public void addListener() {
        mTabLayout.addOnTabSelectedListener(new MyOnTabSelectedListener());
    }

    private class MyOnTabSelectedListener implements TabLayout.OnTabSelectedListener {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            Fragment fragment = null;
            switch (tab.getPosition()) {
                case 0:
                    fragment = new ProvinceFragment();
                    break;
                case 1:
                    fragment = new CityFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.province_city_layout, fragment).commitAllowingStateLoss();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    }
}
