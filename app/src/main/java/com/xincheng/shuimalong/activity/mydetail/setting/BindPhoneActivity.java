package com.xincheng.shuimalong.activity.mydetail.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.login.InputCodeActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.util.MyCountDownTimer;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 许浩 on 2017/6/19.
 */

public class BindPhoneActivity extends BaseActivity {
    @BindView(R.id.tv_phone)
    TextView tvMobile;
    @BindView(R.id.et_input_code)
    EditText etInputCode;
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @BindView(R.id.change_pwd_layout)
    RelativeLayout changePwdLayout;
    @BindView(R.id.et_input_name)
    EditText etInputName;
    @BindView(R.id.et_input_idcard)
    EditText etInputIdcard;
    @BindView(R.id.change_pay_pwd_layout)
    LinearLayout changePayPwdLayout;

    private String mMobile;
    private MyCountDownTimer myCountDownTimer;
    private boolean modifyForPayPwd;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_bind_phone_layout;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.change_pwd));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            modifyForPayPwd = extras.getBoolean(IntentKey.MODIFY_FOR_PAYPWD);
        }
        XLog.e("modifyForPayPwd=>" + modifyForPayPwd);
        myCountDownTimer = new MyCountDownTimer(btnGetCode, 60000, 1000);
        mMobile = PrefUtil.getString(this, PrefKey.LOGIN_MOBILE, "");
        tvMobile.setText(mMobile);
        if(modifyForPayPwd){
            changePayPwdLayout.setVisibility(View.VISIBLE);
            changePwdLayout.setVisibility(View.GONE);
        }else{
            changePayPwdLayout.setVisibility(View.GONE);
            changePwdLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addListener() {

    }

    @OnClick({R.id.btn_get_code, R.id.tv_pwd_bind,R.id.next_step_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                getCode();
                break;
            case R.id.tv_pwd_bind:
                Bundle extras = new Bundle();
                extras.putBoolean(IntentKey.MODIFY_FOR_PAYPWD, modifyForPayPwd);
                extras.putString(IntentKey.MOBILE, mMobile);
                ShowActivity.showActivity(this, InputCodeActivity.class, extras);
                finish();
                break;
            case R.id.next_step_tv:
                if (CommonUtil.isEmpty(etInputCode)) {
                    CommonUtil.showToast(this, "请输入验证码！");
                    return;
                }
                if(modifyForPayPwd){
                    if (CommonUtil.isEmpty(etInputName)) {
                        CommonUtil.showToast(this, "请输入真实名字！");
                        return;
                    }
                    if (CommonUtil.isEmpty(etInputIdcard)) {
                        CommonUtil.showToast(this, "请输入身份证号码！");
                        return;
                    }else{
                        if (!MyUtil.checkIdCard(getEditString(etInputIdcard))) {
                            showToast("身份证格式不合法");
                            return;
                        }
                    }
                }
                checkcertkey();
                break;
        }
    }

    private void getCode() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getcertkey(mMobile),
                new BaseObserver(getContext()) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg, Toast.LENGTH_SHORT);
                        myCountDownTimer.start();
                    }
                }
        );
    }

    public void checkcertkey() {
        if (modifyForPayPwd) {
            String token = PrefUtil.getString(this,PrefKey.TOKEN,"");
            RetroSubscrube.getInstance().postSubscrube(AppUrl.VERIFY_PAY_PWD,
                    PostParam.getPwdkey(token,mMobile, getEditString(etInputCode),
                            getEditString(etInputName), getEditString(etInputIdcard)),
                    new BaseObserver(getBaseContext()) {
                        @Override
                        protected void onHandleSuccess(Object data, String msg) {
                            //验证成功  进入设置密码页面
                            ShowActivity.showActivity(getContext(), ModifyPaypwdActivity.class);
                            finish();
                        }
                    }
            );
        }else{
            RetroSubscrube.getInstance().postSubscrube(AppUrl.CHECK_CERT_KEY,
                    PostParam.getCheckcertkey(mMobile, getEditString(etInputCode)),
                    new BaseObserver(getBaseContext()) {
                        @Override
                        protected void onHandleSuccess(Object data, String msg) {
                            //验证成功  进入设置密码页面
                            ShowActivity.showActivity(getContext(), ChangePwdActivity.class);
                            finish();
                        }
                    }
            );
        }

    }
}
