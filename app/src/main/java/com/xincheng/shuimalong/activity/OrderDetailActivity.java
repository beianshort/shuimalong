package com.xincheng.shuimalong.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideOption;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.ClipBoardUtil;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.util.UIUtil;
import com.xincheng.library.widget.StateButton;
import com.xincheng.library.widget.alertview.AlertView;
import com.xincheng.library.widget.alertview.OnAlertItemClickListener;
import com.xincheng.library.xlog.LogUtils;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.layoutmanager.FullyLinearLayoutManager;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.recept.ChargeBackActivity;
import com.xincheng.shuimalong.activity.recept.ReceiptQuoteConfirmActivity;
import com.xincheng.shuimalong.activity.sell.OrderFillInsuranceActivity;
import com.xincheng.shuimalong.activity.sell.PayOrderActivity;
import com.xincheng.shuimalong.adapter.config.VciTypeOrderAdapter;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.common.RequestCodeKey;
import com.xincheng.shuimalong.entity.OrderDetail;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.CompanyLogo;
import com.xincheng.shuimalong.entity.config.VciTypeOrder;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.manager.OrderExitManager;
import com.xincheng.shuimalong.util.BitmapUtil;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/29.
 */

public class OrderDetailActivity extends BaseActivity {
    @BindView(R.id.order_detail_status_tv)
    TextView mStatusTv;
    @BindView(R.id.order_detail_orderno_layout)
    LinearLayout mOrderNoLayout;
    @BindView(R.id.order_detail_orderno_tv)
    TextView mOrderNoTv;
    @BindView(R.id.order_detail_updatetime_tv)
    TextView mUpdateTimeTv;
    @BindView(R.id.order_detail_inquirydate_tv)
    TextView mInquiryDateTv;
    @BindView(R.id.order_detail_vcidate_tv)
    TextView mVciDateTv;
    @BindView(R.id.order_detail_tcidate_tv)
    TextView mTciDateTv;
    @BindView(R.id.order_detail_carowner_tv)
    TextView mCarOwnerTv;
    @BindView(R.id.order_detail_idcard_tv)
    TextView mIdCardTv;
    @BindView(R.id.order_detail_plateno_tv)
    TextView mPlateNoTv;
    @BindView(R.id.order_detail_carbak_tv)
    TextView mCarBakTv;
    @BindView(R.id.order_detail_vin_tv)
    TextView mVinTv;
    @BindView(R.id.order_detail_engineno_tv)
    TextView mEngineNoTv;
    @BindView(R.id.order_detail_registerdate_tv)
    TextView mRegisterDateTv;
    @BindView(R.id.order_detail_useage_tv)
    TextView mUserAgeTv;
    @BindView(R.id.order_detail_cartype_tv)
    TextView mCarTypeTv;
    @BindView(R.id.order_detail_newcar_tv)
    TextView mNewCarTv;
    @BindView(R.id.order_detail_transfer_tv)
    TextView mTransferTv;
    @BindView(R.id.order_detail_company_tv)
    TextView mCompanyTv;
    @BindView(R.id.order_detail_vcitype_rv)
    RecyclerView mVciTypeRv;
    @BindView(R.id.order_detail_bak_tv)
    TextView mBakTv;
    @BindView(R.id.order_detail_seatnum_tv)
    TextView mSeatNumTv;
    @BindView(R.id.order_detail_carweight_title_tv)
    TextView mCarWeightTitleTv;
    @BindView(R.id.order_detail_carweight_tv)
    TextView mCarWeightTv;

    //立即报价
    @BindView(R.id.order_detail_quote_layout)
    LinearLayout mQuoteLayout;

    //车险金额
    @BindView(R.id.order_detail_money_layout)
    LinearLayout mMoneyLayout;
    @BindView(R.id.order_detail_vcimoney_tv)
    TextView mVciMoneyTv;
    @BindView(R.id.order_detail_tcimoney_tv)
    TextView mTciMoneyTv;
    @BindView(R.id.order_detail_vvtmoney_tv)
    TextView mVvtMoneyTv;
    @BindView(R.id.order_detail_summoney_tv)
    TextView mSumMoneyTv;
    @BindView(R.id.order_detail_vcifeemoney_tv)
    TextView mVciFeeMoneyTv;
    @BindView(R.id.order_detail_tcifeemoney_tv)
    TextView mTciFeeMoneyTv;
    @BindView(R.id.order_detail_sumfeemoney_tv)
    TextView mSumFeeMoneyTv;

    //呼叫车主
    @BindView(R.id.order_detail_call_layout)
    LinearLayout mCallLayout;
    @BindView(R.id.order_detail_carownermobile_tv)
    TextView mCarOwnerMobileTv;

    //确认支付
    @BindView(R.id.order_detail_pay_layout)
    LinearLayout mPayLayout;
    @BindView(R.id.order_detail_totalmoney_tv)
    TextView mTotalMoneyTv;
    //名称 头像 手机号码 保险logo
    @BindView(R.id.order_detail_head_layout)
    LinearLayout layoutHead;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.tv_order_name)
    TextView tvOrderName;
    @BindView(R.id.tv_order_phone)
    TextView tvOrderPhone;
    @BindView(R.id.iv_insurance_img)
    ImageView ivInsuranceImg;
    @BindView(R.id.tv_back_title)
    TextView tvBackTitle;
    @BindView(R.id.tv_back_message)
    TextView tvBackMessage;
    @BindView(R.id.lv_back_layout)
    LinearLayout lvBackLayout;
    @BindView(R.id.order_detail_modification_btn)
    StateButton orderDetailModificationBtn;

    private String mToken;
    private int mBidId;
    private OrderDetail mDetail;
    private int mOrderConfig;
    private boolean mCanUpdate;
    public int refuseStatus, refuseReason;
    private String refuseBak;

    private String avatar, nickname, mobile;
    private int mFrom;
    private boolean mIsDisabled;
    private boolean mIsComment;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_order_detail;
    }

    @Override
    public void initData() {
        OrderExitManager.getInstance().addActivity(this);
        initTitleBar();
        mToken = PrefUtil.getString(this, PrefKey.TOKEN, "");
        Bundle extras = getIntent().getExtras();
        mFrom = getIntent().getIntExtra(IntentKey.FROM, 0);
        refuseStatus = extras.getInt(IntentKey.REFUSE_STATUS);
        refuseReason = extras.getInt(IntentKey.REFUSE_REASON, 0);
        mIsDisabled = extras.getBoolean(IntentKey.IS_DISABLED, false);
        mIsComment = extras.getBoolean(IntentKey.IS_COMMENT, false);
        mVciTypeRv.setLayoutManager(new FullyLinearLayoutManager(this));
        switch (mFrom) {
            case 0:
                layoutHead.setVisibility(View.VISIBLE);
                mCanUpdate = true;
                mTitleTv.setText(getString(R.string.order_detail1));
                mBidId = extras.getInt(IntentKey.BID_ID, 0);
                mOrderConfig = extras.getInt(IntentKey.ORDER_CONFIG);
                mOrderNoTv.setText(getString(R.string.order_no, ""));
                avatar = extras.getString(IntentKey.AVATAR);
                nickname = extras.getString(IntentKey.NICK_NAME);
                mobile = extras.getString(IntentKey.MOBILE);
                tvOrderName.setText(nickname);
                tvOrderPhone.setText(mobile);
                Glide.with(getContext()).load(avatar).apply(GlideOptionsManager.getInstance()
                        .getRequestOptions()).into(ivAvatar);
                if (mOrderConfig == OrderConfig.SELL) {
                    if (refuseStatus == 1) {
                        orderDetailModificationBtn.setVisibility(View.VISIBLE);
                        lvBackLayout.setVisibility(View.VISIBLE);
                        refuseBak = extras.getString(IntentKey.REFUSE_BAK);
                        if (refuseBak != null && !refuseBak.equals("")) {
                            tvBackMessage.setText(refuseBak);
                        }
                        List<CommonConfig> commonConfigs = GsonUtil.parseData(PrefUtil.getString(getContext(), PrefKey
                                        .REFUSE_REASON, ""),
                                new TypeToken<List<CommonConfig>>() {
                                }.getType());
                        for (CommonConfig config : commonConfigs) {
                            if (config.getId() == refuseReason) {
                                tvBackTitle.setText(config.getName());
                            }
                        }
                    } else {
                        orderDetailModificationBtn.setVisibility(View.GONE);
                        lvBackLayout.setVisibility(View.GONE);
                    }
                }
                getOrderDetail();
                break;
            case 1:
                layoutHead.setVisibility(View.GONE);
                mCanUpdate = false;
                mTitleTv.setText(getString(R.string.order_detail2));
                mDetail = extras.getParcelable(IntentKey.ORDER_DETAIL);
                mOrderNoLayout.setVisibility(View.GONE);
                mMoneyLayout.setVisibility(View.VISIBLE);
                parseDetail();
                break;
            case 2:
                mCanUpdate = false;
                mTitleTv.setText(getString(R.string.order_detail2));
                layoutHead.setVisibility(View.GONE);
                mOrderNoTv.setText(getString(R.string.order_no, ""));
                mBidId = extras.getInt(IntentKey.BID_ID, 0);
                mOrderConfig = extras.getInt(IntentKey.ORDER_CONFIG);
                mMoneyLayout.setVisibility(View.VISIBLE);
                mOrderNoLayout.setVisibility(View.GONE);
                getOrderDetail();
                break;
        }
    }

    @OnClick({R.id.order_detail_carowneridentify_tv, R.id.order_detail_caridentify_tv, R.id.order_detail_quote_tv,
            R.id.order_detail_pay_btn, R.id.order_detail_call_btn, R.id.quote_back_btn, R.id
            .order_detail_modification_btn, R.id.tv_order_phone})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.order_detail_carowneridentify_tv: {//查看车主证件
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 1);
                extras.putInt(IntentKey.BID_ID, mDetail.getBidId());
                extras.putBoolean(IntentKey.CAN_UPDATE, mCanUpdate);
                extras.putString(IntentKey.UPLOAD_IMG_PATH1, mDetail.getIdcardPhoto1());
                extras.putString(IntentKey.UPLOAD_IMG_PATH2, mDetail.getIdcardPhoto2());
                ShowActivity.showActivityForResult(this, UploadImageActivity.class, extras,
                        RequestCodeKey.GO_UPLOAD_IMG);
            }
            break;
            case R.id.order_detail_caridentify_tv: {//查看车辆证件
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.FROM, 2);
                extras.putInt(IntentKey.BID_ID, mDetail.getBidId());
                extras.putBoolean(IntentKey.CAN_UPDATE, mCanUpdate);
                extras.putString(IntentKey.UPLOAD_IMG_PATH1, mDetail.getDlPhoto1());
                extras.putString(IntentKey.UPLOAD_IMG_PATH2, mDetail.getDlPhoto2());
                ShowActivity.showActivityForResult(this, UploadImageActivity.class, extras,
                        RequestCodeKey.GO_UPLOAD_IMG);
            }
            break;
            case R.id.quote_back_btn: { //退回报价
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.BID_ID, mBidId);
                ShowActivity.showActivityForResult(this, ChargeBackActivity.class, extras,
                        RequestCodeKey.GO_RECEIPT_QUOTE);
            }
            break;
            case R.id.order_detail_quote_tv: {//立即报价
                XLog.e("报价...");
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.BID_ID, mDetail.getBidId());
                extras.putInt(IntentKey.VCI_RB, mDetail.getVciRb());
                extras.putInt(IntentKey.TCI_RB, mDetail.getTciRb());
                ShowActivity.showActivityForResult(this, ReceiptQuoteConfirmActivity.class, extras,
                        RequestCodeKey.GO_RECEIPT_QUOTE);
            }
            break;
            case R.id.order_detail_pay_btn: { //确认支付
                if (!mDetail.isCanPay()) {
                    showToast("身份证和行驶证照片未上传，不能进行支付！");
                    return;
                }
                Bundle extras = new Bundle();
                extras.putInt(IntentKey.CITY, mDetail.getCityId());
                extras.putInt(IntentKey.PROVINCE, mDetail.getProvinceId());
                extras.putInt(IntentKey.INSURANCE_COMPANY, mDetail.getCompanyId());
                extras.putString(IntentKey.PLATE_NO, mDetail.getPlateNo());
                extras.putString(IntentKey.TOTAL_MONEY, mDetail.getTotalMoney());
                extras.putInt(IntentKey.BID_ID, mDetail.getBidId());
                ShowActivity.showActivityForResult(this, PayOrderActivity.class, extras, RequestCodeKey.GO_PAY);
            }
            break;
            case R.id.tv_order_phone:
                if (CommonUtil.isEmpty(tvOrderPhone)) {
                    showToast("抱歉，没有联系方式");
                    return;
                }
                String mobile_phone = tvOrderPhone.getText().toString().trim();
                showCallAlert(mobile_phone);
                break;
            case R.id.order_detail_call_btn: {//联系车主
                if (CommonUtil.isEmpty(mCarOwnerMobileTv)) {
                    showToast("抱歉，没有车主的联系方式");
                    return;
                }
                String mobile = mCarOwnerMobileTv.getText().toString().trim();
                showCallAlert(mobile);
            }
            break;
            case R.id.order_detail_modification_btn:
                //修改订单
                Bundle extras = new Bundle();
                extras.putString(IntentKey.NICK_NAME, nickname);
                extras.putString(IntentKey.AVATAR, avatar);
                extras.putString(IntentKey.MOBILE, mobile);
                extras.putParcelable(IntentKey.ORDER_DETAIL, mDetail);
                ShowActivity.showActivityForResult(this, OrderFillInsuranceActivity.class, extras,
                        RequestCodeKey.GO_REPUBISH_BILL);
                break;
        }
    }

    private void showCallAlert(final String mobile) {
        new AlertView(this, null, null, getString(R.string.cancel), null, Constant.CALL_WAY,
                AlertView.Style.ActionSheet, new OnAlertItemClickListener() {
            @Override
            public void onItemClick(AlertView alertView, int position) {
                switch (position) {
                    case 0: //呼叫
                        ShowActivity.callPhone(getContext(), mobile);
                        break;
                    case 1: //复制号码
                        ClipBoardUtil.copyText(getContext(), mobile);
                        break;
                    case 2: //添加到通讯录
                        ShowActivity.addContact(getContext(), mobile);
                        break;
                }
            }
        }).show();
    }

    @Override
    public void addListener() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.GO_RECEIPT_QUOTE:
                    XLog.e("quoteSuccess=>" + data.getBooleanExtra(IntentKey.QUOTE_SUCCESS, false));
                    Intent intent = new Intent();
                    intent.putExtra(IntentKey.REFUSE_SUCCESS, data.getBooleanExtra(IntentKey.REFUSE_SUCCESS, false));
                    intent.putExtra(IntentKey.QUOTE_SUCCESS, data.getBooleanExtra(IntentKey.QUOTE_SUCCESS, false));
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case RequestCodeKey.GO_REPUBISH_BILL:
                    intent = new Intent();
                    intent.putExtra(IntentKey.REPUBLISH_SUCCESS, true);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case RequestCodeKey.GO_UPLOAD_IMG:
                    boolean flag = data.getBooleanExtra(IntentKey.SUCCESS, false);
                    if (flag) {
                        getOrderDetail();
                    }
                    break;
                case RequestCodeKey.GO_PAY:
                    Intent intent1 = new Intent();
                    intent1.putExtra(IntentKey.PAY_SUCCESS, true);
                    setResult(RESULT_OK, intent1);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getOrderDetail() {
        BaseObserver observer = new BaseObserver(this, getString(R.string.loading_data)) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                mDetail = GsonUtil.parseData(data, OrderDetail.class);
                if (mFrom == 0) {
                    if (mIsDisabled) {
                        mCanUpdate = false;
                        mStatusTv.setText("已失效");
                        mMoneyLayout.setVisibility(View.VISIBLE);
                    } else {
                        if (mIsComment) {
                            mCanUpdate = false;
                            mStatusTv.setText("已完成");
                            mMoneyLayout.setVisibility(View.VISIBLE);
                        } else {
                            switch (mOrderConfig) {
                                case OrderConfig.RECEIPT: {
                                    mCanUpdate = false;
                                    switch (mDetail.getStatus()) {
                                        case "待报价":
                                            mStatusTv.setText("待报价");
                                            mQuoteLayout.setVisibility(View.VISIBLE);
                                            break;
                                        case "待付款":
                                            mStatusTv.setText("待付款");
                                            mMoneyLayout.setVisibility(View.VISIBLE);
                                            break;
                                    }
                                }
                                break;
                                case OrderConfig.SELL: {
                                    mCanUpdate = true;
                                    switch (mDetail.getStatus()) {
                                        case "待报价":
                                            if (refuseReason == 1) {
                                                mStatusTv.setText("已驳回");
                                            } else {
                                                mStatusTv.setText("待报价");
                                            }
                                            mCallLayout.setVisibility(View.VISIBLE);
                                            break;
                                        case "待付款":
                                            mStatusTv.setText("待付款");
                                            mMoneyLayout.setVisibility(View.VISIBLE);
                                            mPayLayout.setVisibility(View.VISIBLE);
                                            break;
                                    }
                                }
                                break;
                            }
                        }

                    }
                }
                parseDetail();
            }
        };
        switch (mOrderConfig) {
            case OrderConfig.RECEIPT:
                RetroSubscrube.getInstance().getSubscrube(GetParam.viewAcOrder(mToken, mBidId), observer);
                break;
            case OrderConfig.SELL:
                RetroSubscrube.getInstance().getSubscrube(GetParam.viewBillOrder(mToken, mBidId), observer);
                break;
        }
    }

    private void parseDetail() {
        mOrderNoTv.setText(getString(R.string.order_no, mDetail.getOrderNo()));
        mUpdateTimeTv.setText(mDetail.getUpdateTime());
        mInquiryDateTv.setText(mDetail.getCreateTime());
        mVciDateTv.setText(mDetail.getVciDate());
        mTciDateTv.setText(mDetail.getTciDate());
        mCarOwnerTv.setText(mDetail.getCarOwner());
        mIdCardTv.setText(mDetail.getIdcardNo());
        mPlateNoTv.setText(mDetail.getPlateNo());
        mCarBakTv.setText(mDetail.getCarBak());
        mVinTv.setText(mDetail.getVin());
        mEngineNoTv.setText(mDetail.getEngineNo());
        mRegisterDateTv.setText(mDetail.getCarRegisterDate());
        mUserAgeTv.setText(mDetail.getUseAgeMsg());
        mNewCarTv.setText(mDetail.getNewCarMsg());
        mSeatNumTv.setText(mDetail.getSeatNum());
        switch (mDetail.getCarType()) {
            case 1:
                mCarWeightTitleTv.setText(getString(R.string.output_volume));
                mCarWeightTv.setText(mDetail.getOutputVolume());
                break;
            case 2:
                mCarWeightTitleTv.setText(getString(R.string.curb_weight));
                mCarWeightTv.setText(mDetail.getCurbWeight());
                break;
        }
        if (!CommonUtil.isEmpty(mDetail.getBak())) {
            mBakTv.setVisibility(View.VISIBLE);
            mBakTv.setText(getString(R.string.bak_value, mDetail.getBak()));
        }
        CommonConfig carType = ConfigUtil.getCommonConfigById(this, PrefKey.CAR_TYPE, mDetail.getCarType());
        mCarTypeTv.setText(carType.getName());
        CommonConfig newCar = ConfigUtil.getCommonConfigById(this, PrefKey.NEW_CAR_TYPE, mDetail.getNewCar());
        if (newCar != null) {
            mNewCarTv.setText(newCar.getName());
        }
        switch (mDetail.getTransferCar()) {
            case 1:
                mTransferTv.setText("是");
                break;
            case 2:
                mTransferTv.setText("否");
                break;
        }
        CommonConfig company = ConfigUtil.getCommonConfigById(this, PrefKey.COMPANY_LIST, mDetail.getCompanyId());
        if (company != null) {
            mCompanyTv.setText(company.getName());
        }
        CompanyLogo companyLogo = ConfigUtil.getCompanyLogoById(this, mDetail.getCompanyId());
        XLog.e("companyLogo=>" + companyLogo);
        final int screenWith = UIUtil.getDisplaySize(this)[0];
        if (companyLogo != null) {
            Glide.with(this).asBitmap().load(companyLogo.getLogo()).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    Bitmap bitmap = BitmapUtil.zoomImg(resource, screenWith / 3);
                    ivInsuranceImg.setImageBitmap(bitmap);
                }
            });
        }

        if (!CommonUtil.isEmpty(mDetail.getVciSettings())) {
            List<VciTypeOrder> vciTypeList = GsonUtil.parseData(
                    CommonUtil.getBase64DecodeStr(mDetail.getVciSettings()),
                    new TypeToken<List<VciTypeOrder>>() {
                    }.getType());
            if (!CommonUtil.isEmpty(vciTypeList)) {
                VciTypeOrderAdapter adapter = new VciTypeOrderAdapter(this, vciTypeList);
                mVciTypeRv.setAdapter(adapter);
            }
        }
        //显示车险金额
        mVciMoneyTv.setText(getString(R.string.money_value, mDetail.getVciMoney()));
        mTciMoneyTv.setText(getString(R.string.money_value, mDetail.getTciMoney()));
        mVvtMoneyTv.setText(getString(R.string.money_value, mDetail.getVvtMoney()));
        mSumMoneyTv.setText(getString(R.string.money_value, mDetail.getTotalMoney()));
        mVciFeeMoneyTv.setText(getString(R.string.money_value, mDetail.getVciReturnMoney()));
        mTciFeeMoneyTv.setText(getString(R.string.money_value, mDetail.getTciReturnMoney()));
        mSumFeeMoneyTv.setText(getString(R.string.money_value, mDetail.getFeeTotalMoney()));
        //显示待支付金额
        mTotalMoneyTv.setText(getString(R.string.money_value, mDetail.getTotalMoney()));
        //显示车主电话
        mCarOwnerMobileTv.setText(mDetail.getMobilePhone());
        if (mIsDisabled) {
            if (CommonUtil.isEmpty(mDetail.getVciMoney()) || Double.parseDouble(mDetail.getVciMoney()) == 0) {
                mMoneyLayout.setVisibility(View.GONE);
            }
        }
    }
}