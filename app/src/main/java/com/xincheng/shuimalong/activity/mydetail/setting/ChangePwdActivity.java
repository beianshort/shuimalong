package com.xincheng.shuimalong.activity.mydetail.setting;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.xincheng.library.manager.ExitManager;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.activity.login.LoginActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.util.ShowActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

/**
 * Created by 许浩 on 2017/6/19.
 */

public class ChangePwdActivity extends BaseActivity {
    @BindView(R.id.et_input_password)
    EditText etInputPassword;
    @BindView(R.id.et_again_input_password)
    EditText etAgainInputPassword;

    private String mPassword,mAgainPassword;

    private String mToken;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_setting_pwd;
    }

    @Override
    public void initData() {
        initTitleBar(getString(R.string.change_pwd));
        mToken = PrefUtil.getString(this, PrefKey.TOKEN,"");
    }

    @Override
    public void addListener() {

    }

    private boolean checkData() {
        if(CommonUtil.isEmpty(etInputPassword)) {
            showToast("请输入新密码");
            return false;
        }
        if(CommonUtil.isEmpty(etAgainInputPassword)) {
            showToast("请输入确认密码");
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_commit_pwd)
    public void onViewClicked() {
        if(checkData()) {
            mPassword = etInputPassword.getText().toString().trim();
            mAgainPassword = etAgainInputPassword.getText().toString().trim();
            changePwd();
        }
    }

    private void changePwd() {
        RetroSubscrube.getInstance().postSubscrube(AppUrl.CHANGE_PWD,
                PostParam.getChangepwd(mToken, mPassword, mAgainPassword),
                new BaseObserver(this,getString(R.string.retrieve_loading)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        showToast(msg);
                        PrefUtil.removeKey(getContext(), PrefKey.IS_LOGIN);
                        ExitManager.getInstance().exitActivities();
                        ShowActivity.showActivity(getContext(), LoginActivity.class);
                        finish();
                    }
                });
    }
}
