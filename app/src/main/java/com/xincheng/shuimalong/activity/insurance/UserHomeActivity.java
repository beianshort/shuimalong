package com.xincheng.shuimalong.activity.insurance;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.widget.alertview.AlertView;
import com.xincheng.library.widget.alertview.OnAlertItemClickListener;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.recycler.MRecyclerView;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.BaseActivity;
import com.xincheng.shuimalong.adapter.friend.CommenyAdapter;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.friend.FriendUserInfo;
import com.xincheng.shuimalong.entity.user.InsuranceCompany;
import com.xincheng.shuimalong.entity.user.UserHome;
import com.xincheng.shuimalong.entity.user.UserInfo;
import com.xincheng.shuimalong.manager.GlideOptionsManager;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.rong.imkit.RongIM;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.ContactNotificationMessage;

import static io.rong.message.ContactNotificationMessage.CONTACT_OPERATION_REQUEST;

/**
 * Created by xiaote1988 on 2017/7/30.
 */

public class UserHomeActivity extends BaseActivity {
    @BindView(R.id.user_home_title_tv)
    TextView mTitleTv;
    @BindView(R.id.user_home_add_tv)
    TextView mAddTv;
    @BindView(R.id.user_home_avatar_iv)
    ImageView mAvatarIv;
    @BindView(R.id.user_home_nickname_tv)
    TextView mNicknameTv;
    @BindView(R.id.user_home_authentication_tv)
    TextView mAuthenticationTv;
    @BindView(R.id.user_home_registerdate_tv)
    TextView mRegisterDateTv;
    @BindView(R.id.user_home_numtotal_tv)
    TextView mNumTotalTv;
    @BindView(R.id.user_home_nummonth_tv)
    TextView mNumMonthTv;
    @BindView(R.id.user_home_moneytotal_tv)
    TextView mMoneyTotalTv;
    @BindView(R.id.user_home_performance_layout)
    LinearLayout mPerformanceLayout;
    @BindView(R.id.user_home_vcimoney_tv)
    TextView mVciMoneyTv;
    @BindView(R.id.user_home_tcimoney_tv)
    TextView mTciMoneyTv;
    @BindView(R.id.vci_linearlayout)
    LinearLayout vciLinearlayout;
    @BindView(R.id.tv_credit)
    TextView tvCredit;
    @BindView(R.id.tv_pod)
    TextView tvPod;
    @BindView(R.id.rv_pingfen)
    MRecyclerView rvPingfen;
    @BindView(R.id.li_min_pod)
    LinearLayout li_Minlod;
    @BindView(R.id.tv_min_pod)
    TextView tvMinpod;
    @BindView(R.id.user_home_carnum_layout)
    LinearLayout mCarNumLayout;
    @BindView(R.id.user_home_car_num_tv)
    TextView mCarNumTv;
    @BindView(R.id.user_home_truck_num_tv)
    TextView mTruckNumTv;
    private String mUserId;
    private FriendUserInfo mUserHome;
    private int mFrom; //1-收单方 2-卖单方

    private CommenyAdapter commenyAdapter;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_user_home;
    }

    @Override
    public void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mUserId = extras.getString(IntentKey.USER_ID, "");
            mFrom = extras.getInt(IntentKey.FROM, 0);
        }
        switch (mFrom) {
            case 1:
                mTitleTv.setText(getString(R.string.receipt_home_title));
                li_Minlod.setVisibility(View.VISIBLE);
                mCarNumLayout.setVisibility(View.VISIBLE);
                collectMessage(mUserId);
                break;
            case 2:
                mTitleTv.setText(getString(R.string.sell_home_title));
                mPerformanceLayout.setVisibility(View.VISIBLE);
                li_Minlod.setVisibility(View.GONE);
                sellMessage(mUserId);
                break;
        }
    }

    @OnClick({R.id.user_home_back_iv, R.id.user_home_add_tv,
            R.id.user_home_send_call_btn, R.id.user_home_send_msg_btn})
    public void viewClick(View view) {
        switch (view.getId()) {
            case R.id.user_home_back_iv:
                finish();
                break;
            case R.id.user_home_add_tv:
                Bundle extras = new Bundle();
                extras.putString(IntentKey.USER_ID, mUserHome.getUser_id());
                extras.putString(IntentKey.AVATAR, mUserHome.getAvatar());
                extras.putString(IntentKey.USER_CERT, mUserHome.getUser_cert());
                extras.putString(IntentKey.REGISTER_TIME, mUserHome.getRegister_time());
                extras.putString(IntentKey.NICK_NAME, mUserHome.getNick_name());
                ShowActivity.showActivity(this, AddFriendActivity.class, extras);
                break;
            case R.id.user_home_send_call_btn:
                if (mUserHome != null) {
                    showCallAlert();
                }
                break;
            case R.id.user_home_send_msg_btn:
                if (mUserHome != null) {
                    toChatActivity(mUserHome);
                }
                break;
        }
    }

    @Override
    public void addListener() {

    }


    private void parseUserInfo(FriendUserInfo userInfo) {
        mNicknameTv.setText(userInfo.getNick_name());
        mAuthenticationTv.setText(userInfo.getUser_cert());
        mRegisterDateTv.setText(userInfo.getRegister_time());
        DecimalFormat fnum = new DecimalFormat("0.0");
        switch (mFrom) {
            case 1:
                mCarNumTv.setText(String.valueOf(userInfo.getSmall_car_num()));
                mTruckNumTv.setText(String.valueOf(userInfo.getBig_car_num()));
                tvCredit.setText(fnum.format(userInfo.getStar()));
                tvPod.setText(userInfo.getAccept_issue());
                break;
            case 2:
                tvCredit.setText(fnum.format(userInfo.getBuyer_star()));
                tvPod.setText(userInfo.getBuyer_issue());
        }
        mAddTv.setVisibility(userInfo.getIsFriend() ? View.GONE : View.VISIBLE);
        rvPingfen.setLayoutManager(new LinearLayoutManager(getContext()));
        commenyAdapter = new CommenyAdapter(getContext(), mFrom);
        rvPingfen.setAdapter(commenyAdapter);
        commenyAdapter.refreshData(userInfo.getCommenylist());
        for (InsuranceCompany c : userInfo.getCompanyNum()) {
            vciLinearlayout.addView(getInviteView(c));
        }
    }

    private View getInviteView(InsuranceCompany company) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_invite_layout, null);
        TextView name = (TextView) view.findViewById(R.id.invite_name);
        TextView number = (TextView) view.findViewById(R.id.invite_number);
        name.setText(company.getCompany());
        number.setText(company.getNum() + "单");
        return view;
    }

    /**
     * 获取收单方个人中心
     *
     * @param user_id
     */
    public void collectMessage(String user_id) {
        RetroSubscrube.getInstance().getSubscrube(
                GetParam.getViewacuser(PrefUtil.getString(getContext(), PrefKey.TOKEN, ""), user_id),
                new BaseObserver(getContext(), getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            mUserHome = GsonUtil.parseData(data, FriendUserInfo.class);
                            if (mUserHome != null) {
                                tvMinpod.setText(mUserHome.getAccept_price());
                                parseUserInfo(mUserHome);
                                parseData(mUserHome);
                            }
                        }
                    }
                });
    }

    /**
     * 获取卖单方个人中心
     */
    public void sellMessage(String user_id) {
        RetroSubscrube.getInstance().getSubscrube(
                GetParam.getViewbilluser(PrefUtil.getString(getContext(), PrefKey.TOKEN, ""), user_id),
                new BaseObserver(getContext(), getString(R.string.loading_data)) {
                    @Override
                    protected void onHandleSuccess(Object data, String msg) {
                        if (data != null) {
                            mUserHome = GsonUtil.parseData(data, FriendUserInfo.class);
                            if (mUserHome != null) {
                                parseUserInfo(mUserHome);
                                parseData(mUserHome);
                            }
                        }
                    }
                });
    }

    private void parseData(FriendUserInfo userHome) {
        Glide.with(this).load(userHome.getAvatar())
                .apply(GlideOptionsManager.getInstance().getRequestOptions()).into(mAvatarIv);
        mNumTotalTv.setText(String.valueOf(userHome.getNum()));
        mNumMonthTv.setText(String.valueOf(userHome.getMonth_num()));
        mMoneyTotalTv.setText(userHome.getMonth_totalMoney());
        if (mFrom == 2) {
            mVciMoneyTv.setText(userHome.getMonth_vciMoney());
            mTciMoneyTv.setText(userHome.getMonth_tciMoney());
        }
    }

    private void showCallAlert() {
        new AlertView(this, null, "是否呼叫" + mUserHome.getUser_name(), getString(R.string.cancel),
                new String[]{getString(R.string.confirm)}, null, AlertView.Style.Alert, new OnAlertItemClickListener() {
            @Override
            public void onItemClick(AlertView alertView, int position) {
                switch (position) {
                    case 0:
                        ShowActivity.callPhone(getContext(), mUserHome.getUser_name());
                        break;
                }
            }
        }).show();
    }


    /**
     * 发起聊天
     *
     * @param UserHome
     */
    private void toChatActivity(FriendUserInfo UserHome) {
        RongIM.getInstance().startPrivateChat(getContext(), String.valueOf(UserHome.getUser_id()), UserHome
                .getNick_name());
    }
}
