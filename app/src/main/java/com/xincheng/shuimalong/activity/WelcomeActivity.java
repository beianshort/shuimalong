package com.xincheng.shuimalong.activity;

import android.os.Handler;

import com.google.gson.Gson;
import com.xincheng.library.util.AESUtil;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.login.LoginActivity;
import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.PostParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.user.LoginInfo;
import com.xincheng.shuimalong.entity.config.Config;
import com.xincheng.shuimalong.entity.config.Region;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

/**
 * Created by xiaote on 2017/6/21.
 */

public class WelcomeActivity extends BaseActivity {

    @Override
    public int getLayoutResId() {
        return R.layout.activity_welcome;
    }

    @Override
    public void initData() {
        getConfig();
    }

    @Override
    public void addListener() {

    }

    private void getConfig() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getConfig(), new BaseObserver(this) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                if (data != null) {
                    Config config = GsonUtil.parseData(data, Config.class);
                    Gson gson = new Gson();
                    PrefUtil.putString(getContext(), PrefKey.EXPRESS_COMPANYS, gson.toJson(config.getExpressCompanys()));
                    PrefUtil.putString(getContext(), PrefKey.EXPRESS_TYPE, gson.toJson(config.getExpressType()));
                    PrefUtil.putString(getContext(), PrefKey.COMPANY_LIST, gson.toJson(config.getCompanyList()));
                    PrefUtil.putString(getContext(), PrefKey.VCI_TYPES, gson.toJson(config.getVciTypes()));
                    PrefUtil.putString(getContext(), PrefKey.CAR_TYPE, gson.toJson(config.getCarType()));
                    PrefUtil.putString(getContext(), PrefKey.TRUCK_WEIGHT_TYPES, gson.toJson(config.getTruckWeightTypes()));
                    PrefUtil.putString(getContext(), PrefKey.NEW_CAR_TYPE, gson.toJson(config.getNewCarType()));
                    PrefUtil.putString(getContext(), PrefKey.ACCEPT_PRICE, gson.toJson(config.getAcceptPrice()));
                    PrefUtil.putString(getContext(), PrefKey.ACCEPT_ISSUE, gson.toJson(config.getAcceptIssue()));
                    PrefUtil.putString(getContext(), PrefKey.BUYER_ISSUE, gson.toJson(config.getBuyerIssue()));
                    PrefUtil.putString(getContext(), PrefKey.HOT_CITYS, gson.toJson(config.getHotCitys()));
                    PrefUtil.putString(getContext(), PrefKey.USAGE, gson.toJson(config.getUsage()));
                    PrefUtil.putString(getContext(), PrefKey.FILE_STATUS, gson.toJson(config.getFileStatus()));
                    PrefUtil.putString(getContext(), PrefKey.REFUSE_REASON, gson.toJson(config.getRefuseReason()));
                    PrefUtil.putString(getContext(), PrefKey.COMPANY_LOGO_LIST, gson.toJson(config.getCompanyLogoList()));
                    PrefUtil.putString(getContext(),PrefKey.ABOUT_US,config.getAboutus());
                    PrefUtil.putString(getContext(),PrefKey.LAW,config.getLaw());
                    PrefUtil.putString(getContext(),PrefKey.CONTACTUS,config.getContactus());
                }
                getRegion();
            }

            @Override
            protected void onHandleError(String msg) {
                goToRank();
            }
        });
    }

    private void getRegion() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getRegion(), new BaseObserver(this) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                if (data != null) {
                    Region region = GsonUtil.parseData(data, Region.class);
                    Gson gson = new Gson();
                    PrefUtil.putString(getContext(), PrefKey.CITY_LIST, gson.toJson(region.getCityList()));
                    PrefUtil.putString(getContext(), PrefKey.PROVINCE_LIST, gson.toJson(region.getProvinceList()));
                }
                goToRank();
            }

            @Override
            protected void onHandleError(String msg) {
                goToRank();
            }
        });
    }

    private void goToRank() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ShowActivity.showActivity(getContext(), RankActivity.class);
                finish();
            }
        }, 1000);
    }

}
