package com.xincheng.shuimalong.api;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 许浩 on 2017/6/12.
 */

public class GetParam {
    private static Map<String, String> getMap(String op) {
        Map<String, String> map = new HashMap<>();
        map.put(ParamsKey.ACTION_TYPE, "api");
        map.put(ParamsKey.OP, op);
        return map;
    }

    private static Map<String, String> getTokenMap(String op, String token) {
        Map<String, String> map = getMap(op);
        map.put(ParamsKey.TOKEN, token);
        return map;
    }

    /**
     * 获取验证码
     *
     * @param phone
     * @return
     */
    public static Map<String, String> getcertkey(String phone) {
        Map<String, String> map = getMap(AppUrl.GET_CERT_KEY);
        map.put(ParamsKey.MOBILE, phone);
        return map;
    }

    /**
     * 获取全局配置
     */
    public static Map<String, String> getConfig() {
        return getMap(AppUrl.GET_CONFIG);
    }

    /**
     * 获取地区
     */
    public static Map<String, String> getRegion() {
        return getMap(AppUrl.GET_REGION);
    }

    /**
     * 获取我的收单
     */
    public static Map<String, String> getMyAcceptance(String token) {
        return getTokenMap(AppUrl.MY_ACCEPTANCE, token);
    }

    public static Map<String, String> getAcindex(String token) {
        return getTokenMap(AppUrl.ACINDEX, token);
    }

    public static Map<String, String> getAcindex(String token, String user_id) {
        Map<String, String> map = getTokenMap(AppUrl.ACINDEX, token);
        map.put(ParamsKey.USER_ID, user_id);
        return map;
    }

    public static Map<String, String> getViewacuser(String token, String user_id) {
        Map<String, String> map = getTokenMap(AppUrl.VIEWACUSER, token);
        map.put(ParamsKey.USER_ID, user_id);
        return map;
    }

    public static Map<String, String> getBillindex(String token) {
        return getTokenMap(AppUrl.BILLINDEX, token);
    }

    public static Map<String, String> getBillindex(String token, String user_id) {
        Map<String, String> map = getTokenMap(AppUrl.BILLINDEX, token);
        map.put(ParamsKey.USER_ID, user_id);
        return map;
    }

    public static Map<String, String> getViewbilluser(String token, String user_id) {
        Map<String, String> map = getTokenMap(AppUrl.VIEWBILLUSER, token);
        map.put(ParamsKey.USER_ID, user_id);
        return map;
    }

    /**
     * 删除我的收单
     */
    public static Map<String, String> delMyAcceptance(String token, int acId) {
        Map<String, String> map = getTokenMap(AppUrl.DEL_ACCEPTANCE, token);
        map.put(ParamsKey.ID, String.valueOf(acId));
        return map;
    }

    /**
     * 搜索收单
     */
    public static Map<String, String> searchAcceptance(int cityId, int provinceId, int carTypeId, int companyId,
                                                       int minReturn, int maxReturn, String order, int page,
                                                       boolean showNewCar, int newCar, int useAge, String keyword) {
        Map<String, String> map = getMap(AppUrl.SEARCH_ACCEPTANCE);
        map.put(ParamsKey.CITY, String.valueOf(cityId));
        map.put(ParamsKey.PROVINCE, String.valueOf(provinceId));
        map.put(ParamsKey.CAR_TYPE, String.valueOf(carTypeId));
        if (companyId != 0) {
            map.put(ParamsKey.COMPANY, String.valueOf(companyId));
        }
        if (minReturn != -1) {
            map.put(ParamsKey.MIN_RETURN, String.valueOf(minReturn));
        }
        if (maxReturn != -1) {
            map.put(ParamsKey.MAX_RETURN, String.valueOf(maxReturn));
        }
        map.put(ParamsKey.PAGE, String.valueOf(page));
        if (!CommonUtil.isEmpty(order)) {
            map.put(ParamsKey.ORDER, order);
        }
        if (showNewCar) {
            map.put(ParamsKey.NEW_CAR, String.valueOf(newCar));
        }
        if (useAge != 0) {
            map.put(ParamsKey.USAGE, String.valueOf(useAge));
        }
        if (!CommonUtil.isEmpty(keyword)) {
            map.put(ParamsKey.KEYWORD, keyword);
        }
        return map;
    }


    public static Map<String, String> myBillOrder(String token, int status, int fileStatus,
                                                  int toComment, String statusName, int city,
                                                  int province, int carType, int newCar, int usage,
                                                  int company, String keyword, int page) {
        Map<String, String> map = getTokenMap(AppUrl.MY_BILL_ORDER, token);
        map.put(ParamsKey.STATUS, String.valueOf(status));
        map.put(ParamsKey.PAGE, String.valueOf(page));
        map.put(ParamsKey.FILE_STATUS, String.valueOf(fileStatus));
        map.put(ParamsKey.TO_COMMENT, String.valueOf(toComment));
        if (city != 0) {
            map.put(ParamsKey.CITY, String.valueOf(city));
        }
        if (province != 0) {
            map.put(ParamsKey.PROVINCE, String.valueOf(province));
        }
        if (carType != 0) {
            map.put(ParamsKey.CAR_TYPE, String.valueOf(carType));
        }
        if (newCar != 0) {
            map.put(ParamsKey.NEW_CAR, String.valueOf(newCar));
        }
        if (usage != 0) {
            map.put(ParamsKey.USAGE, String.valueOf(usage));
        }
        if (company != 0) {
            map.put(ParamsKey.COMPANY, String.valueOf(company));
        }
        if (!CommonUtil.isEmpty(keyword)) {
            map.put(ParamsKey.KEYWORD, String.valueOf(keyword));
        }
        map.put(ParamsKey.STATUS_NAME, statusName);
        return map;
    }

    /**
     * 获取卖单方订单详情
     */
    public static Map<String, String> viewBillOrder(String token, int bidId) {
        Map<String, String> map = getTokenMap(AppUrl.VIEW_BILL_ORDER, token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        return map;
    }

    /**
     * 获取收单方订单列表
     */
    public static Map<String, String> myAcOrder(String token, int status, int fileStatus,
                                                int toComment, String statusName, int city,
                                                int province, int carType, int newCar, int usage,
                                                int company, String keyword, int page) {
        Map<String, String> map = getTokenMap(AppUrl.MY_AC_ORDER, token);
        map.put(ParamsKey.STATUS, String.valueOf(status));
        map.put(ParamsKey.FILE_STATUS, String.valueOf(fileStatus));
        map.put(ParamsKey.STATUS_NAME, statusName);
        map.put(ParamsKey.TO_COMMENT, String.valueOf(toComment));
        if (city != 0) {
            map.put(ParamsKey.CITY, String.valueOf(city));
        }
        if (province != 0) {
            map.put(ParamsKey.PROVINCE, String.valueOf(province));
        }
        if (carType != 0) {
            map.put(ParamsKey.CAR_TYPE, String.valueOf(carType));
        }
        if (newCar != 0) {
            map.put(ParamsKey.NEW_CAR, String.valueOf(newCar));
        }
        if (usage != 0) {
            map.put(ParamsKey.USAGE, String.valueOf(usage));
        }
        if (company != 0) {
            map.put(ParamsKey.COMPANY, String.valueOf(company));
        }
        if (!CommonUtil.isEmpty(keyword)) {
            map.put(ParamsKey.KEYWORD, String.valueOf(keyword));
        }
        map.put(ParamsKey.PAGE, String.valueOf(page));
        return map;
    }

    /**
     * 获取收单方订单详情
     */
    public static Map<String, String> viewAcOrder(String token, int bidId) {
        Map<String, String> map = getTokenMap(AppUrl.VIEW_AC_ORDER, token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        return map;
    }

    /**
     * 根据城市ID获取区域
     */
    public static Map<String, String> getAreaByCityId(int cityId) {
        Map<String, String> map = getMap(AppUrl.GET_AREA);
        map.put(ParamsKey.CITY_ID, String.valueOf(cityId));
        return map;
    }

    /**
     * 获取收货地址
     */
    public static Map<String, String> getAddress(String token) {
        return getTokenMap(AppUrl.GET_ADDRESS, token);
    }

    /**
     * 获取客户列表
     */
    public static Map<String, String> getCustomer(String token, int page, String keyword) {
        Map<String, String> map = getTokenMap(AppUrl.MYCUSTOMER, token);
        map.put(ParamsKey.PAGE, String.valueOf(page));
        if (!CommonUtil.isEmpty(keyword)) {
            map.put(ParamsKey.KEYWORD, keyword);
        }
        return map;
    }

    /**
     * 邀请好友
     */
    public static Map<String, String> shareRegister(String token) {
        return getTokenMap(AppUrl.SHARE_REGISTER, token);
    }

    /**
     * 获取我的银行卡
     */
    public static Map<String, String> getBank(String token) {
        return getTokenMap(AppUrl.GET_BANK, token);
    }

    /**
     * 根据银行卡号搜索信息
     */
    public static Map<String, String> searchBank(String token, String bankCardNo) {
        Map<String, String> map = getTokenMap(AppUrl.SEARCH_BANK, token);
        map.put(ParamsKey.BANKCARD_NO, bankCardNo);
        return map;
    }

    /**
     * 获取我的钱包
     */
    public static Map<String, String> myMoneyLog(String token, int type, String startDate, String endDate) {
        Map<String, String> map = getTokenMap(AppUrl.MY_MONEY_LOG, token);
        if (type != 0) {
            map.put(ParamsKey.TYPE, String.valueOf(type));
        }
        if (!CommonUtil.isEmpty(startDate)) {
            map.put(ParamsKey.START_DATE, startDate);
        }
        if (!CommonUtil.isEmpty(endDate)) {
            map.put(ParamsKey.END_DATE, endDate);
        }
        return map;
    }

    /**
     * 获取融云TOKEN
     *
     * @param token
     * @return
     */
    public static Map<String, String> getImToken(String token) {
        return getTokenMap(AppUrl.GET_CHAT_TOKEN, token);
    }

    /**
     * 好友列表
     *
     * @param token
     * @return
     */
    public static Map<String, String> getFriends(String token) {
        return getTokenMap(AppUrl.MY_FRIENDS, token);
    }

    /**
     * 好友列表
     *
     * @param token
     * @return
     */
    public static Map<String, String> getApplyFriend(String token) {
        return getTokenMap(AppUrl.MY_APPLY_FRIENDS, token);
    }

    /**
     * 获取排行
     */
    public static Map<String, String> getRank(String token) {
        return getTokenMap(AppUrl.GET_RANK, token);
    }

    /**
     * 获取用户信息
     */
    public static Map<String, String> getUserInfo(String token, String uid) {
        Map<String, String> map = getTokenMap(AppUrl.GET_USERINFO, token);
        map.put(ParamsKey.UID, uid);
        return map;
    }

    /**
     * 获取钱包详情
     */
    public static Map<String, String> getMoneyLogDetail(String token, String logId) {
        Map<String, String> map = getTokenMap(AppUrl.MONEY_LOG_DETAIL, token);
        map.put(ParamsKey.LOG_ID, logId);
        return map;
    }
}
