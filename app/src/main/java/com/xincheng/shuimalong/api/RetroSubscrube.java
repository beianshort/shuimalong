package com.xincheng.shuimalong.api;

import com.xincheng.library.retrofit.RetrofitManager;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.entity.BaseEntity;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by xuhao on 2017/6/13.
 */

public class RetroSubscrube {
    public static RetroSubscrube retroSubscrube;

    public static RetroSubscrube getInstance() {
        if (retroSubscrube == null) {
            retroSubscrube = new RetroSubscrube();
        }
        return retroSubscrube;
    }

    /**
     * 从服务器获取数据
     *
     * @param map
     * @param os
     */
    public void getSubscrube(Map<String, String> map, BaseObserver os) {
        os.onStart();
        Observable<BaseEntity> observable = RetroFactory.getInstance().get(map);
        observable.compose(RxSchedulers.compose()).subscribe(os);
    }

    /**
     * 提交数据到服务器
     *
     * @param map
     * @param os
     */
    public void postSubscrube(String op, Map<String, String> map, BaseObserver os) {
        os.onStart();
        Observable<BaseEntity> observable = RetroFactory.getInstance().post(op, map);
        observable.compose(RxSchedulers.compose()).subscribe(os);
        Set set = map.keySet();
        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            XLog.e(key);
        }
    }

}
