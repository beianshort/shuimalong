package com.xincheng.shuimalong.api;


import com.xincheng.shuimalong.entity.BaseEntity;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by xuhao on 2017/6/12.
 */
public interface RetrofitService {
    @GET("api.php")
    Observable<BaseEntity> get(@QueryMap Map<String,String> map);

    @POST("api.php?action_type=api")
    @FormUrlEncoded
    Observable<BaseEntity> post(@Query("op") String op, @FieldMap Map<String,String> map);
}