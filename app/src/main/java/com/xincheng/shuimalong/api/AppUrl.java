package com.xincheng.shuimalong.api;

/**
 * Created by 许浩 on 2017/6/12.
 */

public class AppUrl {
    //"http://demo.webzing.cn/"->测试地址; //"http://api.taxiqi.com"->正式地址;
    public static final String BASE_URL = "http://api.taxiqi.com";
    //全局配置
    public static final String GET_CONFIG = "getconfig";//获取全局配置
    public static final String GET_REGION = "getregion"; //获取地区
    public static final String GET_AREA = "getarea";//根据城市ID获取区域
    //排行
    public static final String GET_RANK = "getrank";//获取排行信息
    //用户相关(注册、登录、密码、验证码、个人中心)
    public static final String GET_CERT_KEY = "getcertkey";//获取验证码
    public static final String MOBILE_REGISTER = "mobileregister";//手机快速注册
    public static final String MOBILE_LOGIN = "mobilelogin";  //手机登录
    public static final String CHECK_CERT_KEY = "checkcertkey";//验证验证码
    public static final String VERIFY_PAY_PWD = "verifypaypwd";//验证验证码
    public static final String CHANGE_PWD = "changepwd";//修改密码
    public static final String REALCERT = "realcert";//实名认证
    public static final String SHARE_REGISTER = "shareregister";//分享
    public static final String GET_PWD = "getpwd";//获取登录密码
    public static final String CHANGE_AVATAR = "changeavatar";//修改头像
    public static final String CHANGE_USER_INFO = "changeuserinfo";//修改昵称
    public static final String SET_PAY_PWD = "setpaypwd";//设置提现密码
    public static final String APPLY_WITHDRAW = "applywithdraw";//申请提现
    public static final String GET_USERINFO = "getuserinfo";
    //收单相关
    public static final String MY_ACCEPTANCE = "myacceptance";//我的收单
    public static final String PUBLISH_ACCEPTANCE = "publishacceptance";//发布收单
    public static final String DEL_ACCEPTANCE = "delacceptance";//删除收单
    public static final String SEARCH_ACCEPTANCE = "searchacceptance";//搜索收单
    public static final String MY_AC_ORDER = "myacorder";   //收单方订单
    public static final String VIEW_AC_ORDER = "viewacorder";//收单方订单详情
    public static final String SEND_PRICE = "sendprice";//报价
    public static final String ACINDEX = "acindex";//收单方主页
    public static final String GIVE_ISSUE = "giveissue";//出单
    public static final String AC_COMMENT = "accomment";//评价
    public static final String REFUSE_BID = "refusebid";//退回报价
    public static final String CONFIRM_ACCEPT = "confirmaccept";//接受出单
    /**
     * 卖单相关
     */
    public static final String BILLINDEX="billindex";//卖单方个人主页
    public static final String PUBLISH_BILL = "publishbill";//发布卖单
    public static final String MY_BILL_ORDER = "mybillorder";//卖单方订单
    public static final String VIEW_BILL_ORDER = "viewbillorder"; //卖单方订单详情
    public static final String PAY_ORDER = "payorder";//支付回调
    public static final String PAY_OFFLINE = "offlinepay";//线下支付
    public static final String CONFRIM_RETURN = "confirmreturn";//确认回单
    public static final String BILL_COMMENT = "billcomment";//评价
    public static final String FILE_CONFIRM = "confirmfile";//签收保单
    public static final String EDIT_BILL = "editbill";//单独上传身份证图片和行驶证图片
    public static final String REPUBLISH_BILL="republishbill";//重新申请卖单
    /**
     * 收货地址
     */
    public static final String GET_ADDRESS = "getaddress";//获取收货地址
    public static final String ADD_ADDRESS = "addaddress";//添加收货地址
    public static final String SET_DEFAULT_ADDRESS = "setdefaultaddress";//设置默认收货地址
    public static final String DELETE_ADDRSS = "deleteaddress";//删除收货地址

    /**
     * 客户管理
     */
    public static final String MYCUSTOMER = "mycustomer";
    public static final String ADD_CUSTOMER = "addcustomer";

    /**
     * 银行卡
     */
    public static final String GET_BANK = "getbank";
    public static final String DEL_BANK = "delbank";
    public static final String ADD_BANK = "addbank";
    public static final String SEARCH_BANK = "searchbank";
    public static final String SET_DEFAULT_BANK = "setdefaultbank";

    /**
     * 钱包
     */
    public static final String MY_MONEY_LOG = "mymoneylog";
    public static final String AMOUNT_PAY="amountpay";
    public static final String MONEY_LOG_DETAIL = "moneylogdetail";
    /**
     * 保友线
     */
    public static final String GET_CHAT_TOKEN="getchattoken";
    public static final String MY_FRIENDS="myfriends";
    public static final String APPLY_FRIENDS="applyfriends";
    public static final String MY_APPLY_FRIENDS="myapplyfriends";
    public static final String PROCESS_APPLY_FRIENDS="processapplyfriends";
    public static final String ADD_GROUP="addgroup";
    public static final String MOVE_GROUP="movegroup";
    public static final String DEL_GROUP ="delgroup";
    public static final String DEL_FRIENDS="delfriends";
    public static final String SEARCHUSE="searchuser";

    public static final String VIEWACUSER = "viewacuser";//收单方主页
    public static final String VIEWBILLUSER = "viewbilluser";//卖单方主页

}
