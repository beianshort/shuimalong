package com.xincheng.shuimalong.api;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.adapter.SearchHistoryAdapter;
import com.xincheng.shuimalong.entity.Order;
import com.xincheng.shuimalong.entity.OrderDetail;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.util.GsonUtil;
import com.xincheng.shuimalong.util.JsonUtil;
import com.xincheng.shuimalong.util.MyUtil;
import com.xincheng.shuimalong.util.PayUtil;
import com.xincheng.shuimalong.util.ShowActivity;

import java.io.StringBufferInputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by xiaote on 2017/6/23.
 */

public class PostParam {
    private static Map<String, String> getTokenMap(String token) {
        Map<String, String> map = new HashMap<>();
        map.put(ParamsKey.TOKEN, token);
        return map;
    }


    /**
     * 注册
     *
     * @param mobile     手机号
     * @param cert_key   验证码
     * @param password   密码
     * @param retypepwd  再次输入密码
     * @param inviteuser 邀请人账号(可不传)
     * @return
     */
    public static Map<String, String> getRegister(String mobile, String cert_key, String password, String retypepwd,
                                                  String inviteuser) {
        Map<String, String> map = new HashMap<>();
        map.put(ParamsKey.MOBILE, mobile);
        map.put(ParamsKey.CERT_KEY, cert_key);
        map.put(ParamsKey.PASSWORD, password);
        map.put(ParamsKey.RETYPE_PWD, retypepwd);
        if (!inviteuser.equals("")) {
            map.put(ParamsKey.INVITE_USER, inviteuser);
        }
        return map;
    }

    /**
     * 手机登陆
     *
     * @param mobile   手机号
     * @param password 密码
     * @return
     */
    public static Map<String, String> getLoginParam(String mobile, String password) {
        Map<String, String> map = new HashMap<>();
        map.put(ParamsKey.MOBILE, mobile);
        map.put(ParamsKey.PASSWORD, password);
        return map;
    }

    /**
     * 验证验证码
     *
     * @param mobile
     * @param cert_key
     * @return
     */
    public static Map<String, String> getCheckcertkey(String mobile, String cert_key) {
        Map<String, String> map = new HashMap<>();
        map.put(ParamsKey.MOBILE, mobile);
        map.put(ParamsKey.CERT_KEY, cert_key);
        return map;
    }

    /**
     * 验证验证码
     *
     * @param mobile
     * @param cert_key
     * @return
     */
    public static Map<String, String> getPwdkey(String token, String mobile, String cert_key,
                                                String real_name, String id_card) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.MOBILE, mobile);
        map.put(ParamsKey.CERT_KEY, cert_key);
        map.put(ParamsKey.REAL_NAME, real_name);
        map.put(ParamsKey.ID_CARD_NO, id_card);
        return map;
    }

    /**
     * 修改密码
     *
     * @param password
     * @param retypepwd
     * @return
     */
    public static Map<String, String> getChangepwd(String token, String password, String retypepwd) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.PASSWORD, password);
        map.put(ParamsKey.RETYPE_PWD, retypepwd);
        return map;
    }

    /**
     * 找回密码
     *
     * @param password
     * @param retypepwd
     * @return
     */
    public static Map<String, String> getPwd(String mobile, String password, String retypepwd, String mobileToken) {
        Map<String, String> map = new HashMap<>();
        map.put(ParamsKey.USER_NAME, mobile);
        map.put(ParamsKey.PASSWORD, password);
        map.put(ParamsKey.RETYPE_PWD, retypepwd);
        map.put(ParamsKey.MOBILE_TOKEN, mobileToken);
        return map;
    }

    /**
     * 实名认证
     *
     * @param token      用户token
     * @param realName   真实姓名
     * @param id_card_no 身份证号
     * @param id_card    身份证正面
     * @param id_card_v  身份证反面
     * @return
     */
    public static Map<String, String> realcert(String token, String realName, String id_card_no, String id_card,
                                               String id_card_v) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.REAL_NAME, realName);
        map.put(ParamsKey.ID_CARD_NO, id_card_no);
        map.put(ParamsKey.ID_CARD, id_card);
        map.put(ParamsKey.ID_CRAD_V, id_card_v);
        return map;
    }

    /**
     * 发布收单
     */
    public static Map<String, String> publishAcceptance(String token, int city, int company, int carType, int newCar,
                                                        int usAge, int tciRb, int vciRb, String startDate,
                                                        String endDate, int carWeight, String bak, int longTerm) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.CITY, String.valueOf(city));
        map.put(ParamsKey.COMPANY, String.valueOf(company));
        map.put(ParamsKey.CAR_TYPE, String.valueOf(carType));
        map.put(ParamsKey.NEW_CAR, String.valueOf(newCar));
        map.put(ParamsKey.USAGE, String.valueOf(usAge));
        map.put(ParamsKey.TCI_RB, String.valueOf(tciRb));
        map.put(ParamsKey.VCI_RB, String.valueOf(vciRb));
        map.put(ParamsKey.START_DATE, startDate);
        map.put(ParamsKey.END_DATE, endDate);
        map.put(ParamsKey.CAR_WEIGHT, String.valueOf(carWeight));
        map.put(ParamsKey.BAK, bak);
        map.put(ParamsKey.LONG_TERM, String.valueOf(longTerm));
        return map;
    }

    /**
     * 发布卖单
     */
    public static Map<String, String> publishBill(String token, int acId, String plateNo, String carOwner,
                                                  String mobilePhone, String idCardNo,
                                                  String carRegisterDate, int transferCar, int tciStatus,
                                                  String tciDate, String vciDate, List<VciType> vciTypes,
                                                  String idCardPhoto1, String idCardPhoto2, String dlPhoto1,
                                                  String dlPhoto2, String orgPhoto, String orgNo,
                                                  String outputVolume, String curbWeight, String vin,
                                                  String engineNo, int usAge, String mCarVehicle,
                                                  String bak, String seatNum) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.AC_ID, String.valueOf(acId));
        map.put(ParamsKey.PLATE_NO, plateNo.toUpperCase());
        map.put(ParamsKey.CAR_OWNER, carOwner);
        map.put(ParamsKey.MOBILE_PHONE, mobilePhone);
        map.put(ParamsKey.CAR_REGISTER_DATE, carRegisterDate);
        map.put(ParamsKey.TRANSFER_CAR, String.valueOf(transferCar));
        map.put(ParamsKey.TCI_STATUS, String.valueOf(tciStatus));
        map.put(ParamsKey.TCI_DATE, MyUtil.getBillVciOrTciSubmitDate(tciDate));
        map.put(ParamsKey.VCI_DATE, MyUtil.getBillVciOrTciSubmitDate(vciDate));
        map.put(ParamsKey.VCI_SETTINGS, CommonUtil.getBase64EncodeStr(JsonUtil.getVciTypeSettings(vciTypes)));
        map.put(ParamsKey.ID_CARD_NO, idCardNo);
        map.put(ParamsKey.ID_CARD_PHOTO1, idCardPhoto1);
        map.put(ParamsKey.ID_CARD_PHOTO2, idCardPhoto2);
        map.put(ParamsKey.DL_PHOTO1, dlPhoto1);
        map.put(ParamsKey.DL_PHOTO2, dlPhoto2);
        map.put(ParamsKey.ORG_PHOTO, orgPhoto);
        map.put(ParamsKey.ORG_NO, orgNo);
        map.put(ParamsKey.OUTPUT_VOLUME, outputVolume);
        map.put(ParamsKey.CURB_WEIGHT, curbWeight);
        map.put(ParamsKey.VIN, vin.toUpperCase());
        map.put(ParamsKey.ENGINE_NO, engineNo.toUpperCase());
        map.put(ParamsKey.USEAGE, String.valueOf(usAge));
        map.put(ParamsKey.CAR_VEHICLE, mCarVehicle);
        map.put(ParamsKey.BAK, bak);
        map.put(ParamsKey.SEAT_NUM, seatNum);
        return map;
    }

    /**
     * 发布卖单
     */
    public static Map<String, String> republishbill(String token, int bin_id, String plateNo, String carOwner,
                                                    String mobilePhone, String idCardNo,
                                                    String carRegisterDate, int transferCar, int tciStatus,
                                                    String tciDate, String vciDate, List<VciType> vciTypes,
                                                    String idCardPhoto1, String idCardPhoto2, String dlPhoto1,
                                                    String dlPhoto2, String orgPhoto, String orgNo,
                                                    String outputVolume, String curbWeight, String vin,
                                                    String engineNo, int usAge, String mCarVehicle,
                                                    String bak, String seatNum) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bin_id));
        map.put(ParamsKey.PLATE_NO, plateNo.toUpperCase());
        map.put(ParamsKey.CAR_OWNER, carOwner);
        map.put(ParamsKey.MOBILE_PHONE, mobilePhone);
        map.put(ParamsKey.CAR_REGISTER_DATE, carRegisterDate);
        map.put(ParamsKey.TRANSFER_CAR, String.valueOf(transferCar));
        map.put(ParamsKey.TCI_STATUS, String.valueOf(tciStatus));
        map.put(ParamsKey.TCI_DATE, MyUtil.getBillVciOrTciSubmitDate(tciDate));
        map.put(ParamsKey.VCI_DATE, MyUtil.getBillVciOrTciSubmitDate(vciDate));
        map.put(ParamsKey.VCI_SETTINGS, CommonUtil.getBase64EncodeStr(JsonUtil.getVciTypeSettings(vciTypes)));
        map.put(ParamsKey.ID_CARD_NO, idCardNo);
        map.put(ParamsKey.ID_CARD_PHOTO1, idCardPhoto1);
        map.put(ParamsKey.ID_CARD_PHOTO2, idCardPhoto2);
        map.put(ParamsKey.DL_PHOTO1, dlPhoto1);
        map.put(ParamsKey.DL_PHOTO2, dlPhoto2);
        map.put(ParamsKey.ORG_PHOTO, orgPhoto);
        map.put(ParamsKey.ORG_NO, orgNo);
        map.put(ParamsKey.OUTPUT_VOLUME, outputVolume);
        map.put(ParamsKey.CURB_WEIGHT, curbWeight);
        map.put(ParamsKey.VIN, vin.toUpperCase());
        map.put(ParamsKey.ENGINE_NO, engineNo.toUpperCase());
        map.put(ParamsKey.USEAGE, String.valueOf(usAge));
        map.put(ParamsKey.CAR_VEHICLE, mCarVehicle);
        map.put(ParamsKey.BAK, bak);
        map.put(ParamsKey.SEAT_NUM, seatNum);
        return map;
    }

    /**
     * 报价
     */
    public static Map<String, String> sendPrice(String token, int bidId, String tciMoney, String vciMoney,
                                                String vvtMoney) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.TCI_MONEY, tciMoney);
        map.put(ParamsKey.VCI_MONEY, vciMoney);
        map.put(ParamsKey.VVT_MONEY, vvtMoney);
        return map;
    }

    /**
     * 添加收货地址
     */
    public static Map<String, String> addAddress(String token, int regionId, String address, String name,
                                                 String mobile) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.REGION_ID, String.valueOf(regionId));
        map.put(ParamsKey.ADDRESS, address);
        map.put(ParamsKey.NAME, name);
        map.put(ParamsKey.MOBILE, mobile);
        return map;
    }

    /**
     * 设置默认收货地址
     */
    public static Map<String, String> setDefaultAddress(String token, int addressId) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.ID, String.valueOf(addressId));
        return map;
    }

    /**
     * delete
     * 删除收货地址
     */
    public static Map<String, String> deleteAddress(String token, int addressId) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.ID, String.valueOf(addressId));
        return map;
    }

    /**
     * 支付回调
     */
    public static Map<String, String> payOrder(String token, int bidId, int addressId) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.ADDRESS_ID, String.valueOf(addressId));
        return map;
    }

    /**
     * 支付回调
     */
    public static Map<String, String> payOrder(String token, int bidId, int addressId, String pay_pwd) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.ADDRESS_ID, String.valueOf(addressId));
        map.put(ParamsKey.PAY_PWD, pay_pwd);
        return map;
    }

    /**
     * 收单方出单
     */
    public static Map<String, String> giveIssue(String token, int bidId, String tciCt, String vciCt,
                                                String tciNo, String vciNo, int shippingTypeId,
                                                int shippingCompanyId, String shippingNo) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.TCI_CT, tciCt);
        map.put(ParamsKey.VCI_CT, vciCt);
        map.put(ParamsKey.TCI_NO, tciNo);
        map.put(ParamsKey.VCI_NO, vciNo);
        map.put(ParamsKey.SHIPPING_TYPE, String.valueOf(shippingTypeId));
        if (shippingCompanyId != 0) {
            map.put(ParamsKey.SHIPPING_COMPANY, String.valueOf(shippingCompanyId));
            map.put(ParamsKey.SHIPPING_NO, shippingNo);
        } else {
            map.put(ParamsKey.SHIPPING_COMPANY, "");
            map.put(ParamsKey.SHIPPING_NO, "");
        }
        return map;
    }

    /**
     * 卖单方回单
     */
    public static Map<String, String> confirmReturn(String token, int bidId, int shippingTypeId, int shippingCompanyId,
                                                    String shippingNo) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.SHIPPING_TYPE, String.valueOf(shippingTypeId));
        map.put(ParamsKey.SHIPPING_COMPANY, String.valueOf(shippingCompanyId));
        map.put(ParamsKey.SHIPPING_NO, shippingNo);
        return map;
    }

    /**
     * 收单方评价
     */
    public static Map<String, String> acComment(String token, int bidId, int buyerStar, int buyerIssue,
                                                String buyerNote, int hideAcUser) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.BUYER_STAR, String.valueOf(buyerStar));
        map.put(ParamsKey.BUYER_ISSUE, String.valueOf(buyerIssue));
        map.put(ParamsKey.BUYER_NOTE, buyerNote);
        map.put(ParamsKey.HIDE_AC_USER, String.valueOf(hideAcUser));
        return map;
    }

    /**
     * 卖单方评价
     */
    public static Map<String, String> billComment(String token, int bidId, int acceptStar, int acceptPrice,
                                                  int acceptIssue, String acceptNote, int hideBillUser) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.ACCEPT_STAR, String.valueOf(acceptStar));
        map.put(ParamsKey.ACCEPT_PRICE, String.valueOf(acceptPrice));
        map.put(ParamsKey.ACCEPT_ISSUE, String.valueOf(acceptIssue));
        map.put(ParamsKey.ACCEPT_NOTE, acceptNote);
        map.put(ParamsKey.HIDE_BILL_USER, String.valueOf(hideBillUser));
        return map;
    }

    /**
     * 卖单方签收保单和收单方确认签收卖单方回单
     */
    public static Map<String, String> fileConfirm(String token, int bidId, int fileStatus) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.FILE_STATUS, String.valueOf(fileStatus));
        return map;
    }

    /**
     * 删除银行卡
     */
    public static Map<String, String> delBankcard(String token, String id) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.ID, id);
        return map;
    }

    /**
     * 添加银行卡
     */
    public static Map<String, String> addBank(String token, String name, String idCardNo, String bank,
                                              String bankCardNo, String lattice_point) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.NAME, name);
        map.put(ParamsKey.ID_CARD_NO, idCardNo);
        map.put(ParamsKey.BANK, bank);
        map.put(ParamsKey.BANKCARD_NO, bankCardNo);
        map.put(ParamsKey.LATTICE_POINT, lattice_point);
        return map;
    }

    /**
     * 设置默认银行卡
     */
    public static Map<String, String> setDefalutBank(String token, String bankId) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.ID, bankId);
        return map;
    }

    /**
     * 添加客户
     */
    public static Map<String, String> addCustomer(String token, String plateNo, String carOwner, String mobilePhone,
                                                  String carRegisterDate, int transferCar, int tciStatus,
                                                  String tciDate, String vciDate, List<VciType> vciTypes,
                                                  String idCardPhoto1, String idCardPhoto2,
                                                  String dlPhoto1, String dlPhoto2, String orgPhoto, String orgNo,
                                                  int carType, int newCar, String outputVolume, String curbWeight,
                                                  String carBak, int useAge, String engineNo, String vin) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.PLATE_NO, plateNo.toUpperCase());
        map.put(ParamsKey.CAR_OWNER, carOwner);
        map.put(ParamsKey.MOBILE_PHONE, mobilePhone);
        map.put(ParamsKey.CAR_REGISTER_DATE, carRegisterDate);
        map.put(ParamsKey.TRANSFER_CAR, String.valueOf(transferCar));
        map.put(ParamsKey.TCI_STATUS, String.valueOf(tciStatus));
        map.put(ParamsKey.TCI_DATE, MyUtil.getBillVciOrTciSubmitDate(tciDate));
        map.put(ParamsKey.VCI_DATE, MyUtil.getBillVciOrTciSubmitDate(vciDate));
        map.put(ParamsKey.VCI_SETTINGS, CommonUtil.getBase64EncodeStr(GsonUtil.toJson(vciTypes)));
        map.put(ParamsKey.ID_CARD_PHOTO1, idCardPhoto1);
        map.put(ParamsKey.ID_CARD_PHOTO2, idCardPhoto2);
        map.put(ParamsKey.DL_PHOTO1, dlPhoto1);
        map.put(ParamsKey.DL_PHOTO2, dlPhoto2);
        map.put(ParamsKey.ORG_PHOTO, orgPhoto);
        map.put(ParamsKey.ORG_NO, orgNo);
        map.put(ParamsKey.CAR_TYPE, String.valueOf(carType));
        map.put(ParamsKey.NEW_CAR, String.valueOf(newCar));
        if (!CommonUtil.isEmpty(outputVolume)) {
            map.put(ParamsKey.OUTPUT_VOLUME, outputVolume);
        }
        if (!CommonUtil.isEmpty(curbWeight)) {
            map.put(ParamsKey.CURB_WEIGHT, curbWeight);
        }
        if (!CommonUtil.isEmpty(carBak)) {
            map.put(ParamsKey.CAR_VEHICLE, carBak);
        }
        map.put(ParamsKey.USEAGE, String.valueOf(useAge));
        if (!CommonUtil.isEmpty(engineNo)) {
            map.put(ParamsKey.ENGINE_NO, engineNo);
        }
        if (!CommonUtil.isEmpty(vin)) {
            map.put(ParamsKey.VIN, vin);
        }
        return map;
    }

    /**
     * 修改头像
     */
    public static Map<String, String> changeAvatar(String token, String avatar) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.AVATAR, avatar);
        return map;
    }

    /**
     * 修改昵称
     */
    public static Map<String, String> changeUserInfo(String token, String nickname) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.NICK_NAME, nickname);
        return map;
    }

    /**
     * 申请添加好友
     *
     * @param token
     * @param friend_user_id
     * @return
     */
    public static Map<String, String> applyFriends(String token, String friend_user_id, String bak) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.FRIEND_USER_ID, friend_user_id);
        if (!CommonUtil.isEmpty(bak)) {
            map.put(ParamsKey.BAK, bak);
        }
        return map;
    }

    /**
     * 处理申请好友
     *
     * @param token
     * @param friend_user_id
     * @return
     */
    public static Map<String, String> processapplyFriends(String token, int friend_user_id, int status) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.ID, String.valueOf(friend_user_id));
        map.put(ParamsKey.STATUS, String.valueOf(status));
        return map;
    }

    /**
     * 上传行驶证和身份证图片
     */
    public static Map<String, String> editBill(String token, int bidId, String idCardPhoto1, String idCardPhoto2,
                                               String dlPhoto1, String dlPhoto2) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        if (!CommonUtil.isEmpty(idCardPhoto1)) {
            map.put(ParamsKey.ID_CARD_PHOTO1, idCardPhoto1);
        }
        if (!CommonUtil.isEmpty(idCardPhoto2)) {
            map.put(ParamsKey.ID_CARD_PHOTO2, idCardPhoto2);
        }
        if (!CommonUtil.isEmpty(dlPhoto1)) {
            map.put(ParamsKey.DL_PHOTO1, dlPhoto1);
        }
        if (!CommonUtil.isEmpty(dlPhoto2)) {
            map.put(ParamsKey.DL_PHOTO2, dlPhoto2);
        }
        return map;
    }

    /**
     * 设置交易密码
     */
    public static Map<String, String> setPayPwd(String token, String password, String retypepwd) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.PASSWORD, password);
        map.put(ParamsKey.RETYPE_PWD, retypepwd);
        return map;
    }

    /**
     * 申请提现
     */
    public static Map<String, String> applyWithdraw(String token, String money, String bankId, String payPwd) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.MONEY, money);
        map.put(ParamsKey.BANK_Id, bankId);
        map.put(ParamsKey.PAY_PWD, payPwd);
        return map;
    }

    /**
     * 添加分组
     *
     * @param token
     * @param group_name
     * @return
     */
    public static Map<String, String> addGroup(String token, String group_name) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.GROUP_NAME, group_name);
        return map;
    }

    /**
     * 移动至分组
     *
     * @param token
     * @param
     * @return
     */
    public static Map<String, String> moveGroup(String token, String group_id, String user_ids) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.GROUP_ID, group_id);
        map.put(ParamsKey.USER_IDS, user_ids);
        return map;
    }

    /**
     * 删除分组
     *
     * @param token
     * @param group_id
     * @return
     */
    public static Map<String, String> delGroup(String token, String group_id) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.GROUP_ID, group_id);
        return map;
    }

    /**
     * 删除好友
     *
     * @param token
     * @param friend_id
     * @return
     */
    public static Map<String, String> delFriends(String token, String friend_id) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.FRIEND_ID, friend_id);
        return map;
    }

    /**
     * a搜索用户
     *
     * @param token
     * @param keyword
     * @return
     */
    public static Map<String, String> searchus(String token, String keyword) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.KEYWORD, keyword);
        return map;
    }

    /**
     * 退回报价
     */
    public static Map<String, String> refuseBid(String token, int bidId, int refuseReason, String refuseBak) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        map.put(ParamsKey.REFUSE_REASON, String.valueOf(refuseReason));
        map.put(ParamsKey.REFUSE_BAK, refuseBak);
        return map;
    }

    /**
     * 接受出单
     */
    public static Map<String, String> confirmAccept(String token, int bidId) {
        Map<String, String> map = getTokenMap(token);
        map.put(ParamsKey.BID_ID, String.valueOf(bidId));
        return map;
    }

    /**
     * 余额支付
     */
    public static Map<String,String> amoutPay(String mToken, int bidId, int addressId, String pwd) {
        Map<String,String> map = getTokenMap(mToken);
        map.put(ParamsKey.BID_ID,String.valueOf(bidId));
        map.put(ParamsKey.ADDRESS_ID,String.valueOf(addressId));
        map.put(ParamsKey.PAY_PWD,pwd);
        return map;
    }
}
