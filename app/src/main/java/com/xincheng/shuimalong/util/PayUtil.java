package com.xincheng.shuimalong.util;

import com.xincheng.shuimalong.api.AppUrl;
import com.xincheng.shuimalong.api.ParamsKey;

import java.text.MessageFormat;

/**
 * Created by xiaote on 2017/7/26.
 */

public class PayUtil {
    private static final String BASE_PAY_URL = AppUrl.BASE_URL + "/index.php?r=pay/sandypay" ;
    public static String getPayUrl(int bidId, int addressId,String cardno) {
        StringBuffer buf = new StringBuffer();
        buf.append(BASE_PAY_URL).append("&").append(ParamsKey.BID_ID).append("=").append(bidId)
                .append("&").append(ParamsKey.ADDRESS_ID).append("=").append(addressId)
                .append("&").append(ParamsKey.CARDNO).append("=").append(cardno);

        return buf.toString();
    }

    public static final String getPaySuccessUrl(int addressId) {
        String successUrl = AppUrl.BASE_URL + "/pay/return/address_id/{0}/";
        return MessageFormat.format(successUrl, addressId);
    }
}
