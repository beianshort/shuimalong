package com.xincheng.shuimalong.util;

import com.xincheng.shuimalong.common.Constant;

import java.util.Calendar;

/**
 * Created by xiaote on 2017/7/20.
 */

public class CalenderUtil {
    public static Calendar getRangeStartDate() {
        Calendar startDate = Calendar.getInstance();
        startDate.set(Constant.YEAR_BEGIN, 0, 1);
        return startDate;
    }

    public static Calendar getRangeEndDate() {
        Calendar endDate = Calendar.getInstance();
        endDate.set(Constant.YEAR_END, 11, 31);
        return endDate;
    }
}
