package com.xincheng.shuimalong.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.WindowManager;

import com.xincheng.library.xlog.XLog;

import java.io.File;

/**
 * Created by xuhao on 2017/6/26.
 */

public class UnitUtils {
    public static UnitUtils unitUtils;
    public Activity ac;

    public static UnitUtils getInstance(Activity activity) {
        if (unitUtils == null) {
            unitUtils = new UnitUtils(activity);
        }
        return unitUtils;
    }

    public UnitUtils(Activity activity) {
        this.ac = activity;
    }

    public static int getDisplayWidth(Activity activity) {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getWidth();
    }

    public static int getDisplayHeight(Activity activity) {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getHeight();
    }

    public int dip2px(int dipValue) {
        float scale = ac.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static Bitmap getDiskBitmap(Uri pathString) {
        String path = pathString.getPath();
        Bitmap bitmap = null;
        try {
            File file = new File(path);
            if (file.exists()) {
                bitmap = BitmapFactory.decodeFile(path);
            } else {
                XLog.e("图片打开失败");
            }
        } catch (Exception e) {
            // TODO: handle exception
            XLog.e(e.getMessage());
        }
        return bitmap;
    }

    public static Bitmap getLocalBitmap(String path) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            bitmap = BitmapFactory.decodeFile(path,options);
        } catch (Exception e) {
            // TODO: handle exception
            XLog.e(e.getMessage());
        }
        return bitmap;
    }
}
