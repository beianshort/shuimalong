package com.xincheng.shuimalong.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by xuhao on 2017/5/26.
 */

public class ShowActivity {
    public static Intent intent = null;

    public static void showActivity(Activity activity, Class<?> c) {
        intent = new Intent(activity, c);
        startActivity(activity, intent);
    }
    public static void showActivity(Context context, Class<?> c, Bundle bundle) {
        intent = new Intent(context, c);
        intent.putExtras(bundle);
        startActivity(context, intent);
    }
    public static void showActivity(Activity activity, Class<?> c, Bundle bundle) {
        intent = new Intent(activity, c);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(activity, intent);
    }

    public static void showActivityForResult(Activity activity, Class<?> c, int code) {
        intent = new Intent(activity, c);
        startActivityForResult(activity, intent, code);
    }

    public static void showActivityForResult(Activity activity, Class<?> c, Bundle bundle, int code) {
        intent = new Intent(activity, c);
        intent.putExtras(bundle);
        startActivityForResult(activity, intent, code);
    }

    /**
     * 直接拨打电话
     */
    public static void callPhone(Context context, String mobile) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + mobile));
        context.startActivity(intent);
    }

    /**
     * 添加手机号到系统通讯录
     */
    public static void addContact(Context context, String mobile) {
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setData(Uri.parse("content://com.android.contacts/contacts/" + "1"));
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, mobile);
        context.startActivity(intent);
    }

    /**
     * 打开拨号键盘
     */
    public static void callBoard(Context context, String mobile) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobile));
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private static void startActivity(Activity activity, Intent intent) {
        activity.startActivity(intent);
        if (intent != null) {
            intent = null;
        }
    }
    private static void startActivity(Context context, Intent intent) {
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        if (intent != null) {
            intent = null;
        }
    }
    private static void startActivityForResult(Activity activity, Intent intent, int code) {
        activity.startActivityForResult(intent, code);
        if (intent != null) {
            intent = null;
        }
    }
}
