package com.xincheng.shuimalong.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.widget.TextView;

import com.xincheng.shuimalong.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xuhao on 2017/6/13.
 */

public class EditTextUtil {

    /**
     * 验证手机格式
     */
    public static boolean isMobile(String number) {
    /*
    移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188、147、170
    联通：130、131、132、152、155、156、185、186、176
    电信：133、153、180、189、（1349卫通）
    总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
    */
        String num = "[1][34578]\\d{9}";
        if (TextUtils.isEmpty(number)) {
            return false;
        } else {
            //matches():字符串是否在给定的正则表达式匹配
            return number.matches(num);
        }
    }

    /**
     * 是否是字母数字组合的密码
     * @param password
     * @return
     */
    public static boolean isPassWord(String password){
        String pwd="^[A-Za-z0-9]+$";
        if (TextUtils.isEmpty(password)) {
            return false;
        } else {
            return password.matches(pwd);
        }
    }

    public static SpannableString getContent(Context context,String source) {
        SpannableString spannableString = new SpannableString(source);
        int rescode[]=context.getResources().getIntArray(R.array.rc_emoji_code);
//        String regex="";
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(spannableString);
//        while (matcher.find()) {
//            String emojiStr = matcher.group();
//            int start = matcher.start();
//            Integer res = Constants.getEmojiIconMaps().get(emojiStr);
//            if (res != null) {
//                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resid[]);
////                bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
//                ImageSpan imageSpan = new ImageSpan(context, bitmap);
//                spannableString.setSpan(imageSpan, start, start + emojiStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//            }
//        }
        return spannableString;
    }
}
