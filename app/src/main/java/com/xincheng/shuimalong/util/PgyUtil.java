package com.xincheng.shuimalong.util;

import android.content.Context;

import com.pgyersdk.crash.PgyCrashManager;

/**
 * Created by xiaote1988 on 2017/7/14.
 */

public class PgyUtil {
    public static void registerCrash(Context context, boolean isDebug) {
        if (isDebug) {
            PgyCrashManager.register(context);
        }
    }
}
