package com.xincheng.shuimalong.util;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.CompanyLogo;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.entity.config.RegionData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiaote on 2017/6/22.
 */

public class ConfigUtil {
    public static List<CommonConfig> getCommonConfigList(Context context, String key) {
        String jsonStr = PrefUtil.getString(context, key, "");
        List<CommonConfig> list = GsonUtil.parseData(jsonStr, new TypeToken<List<CommonConfig>>() {
        }.getType());
        XLog.e("list->" + list);
        return list;
    }

    public static List<VciType> getVciTypes(Context context) {
        String jsonStr = PrefUtil.getString(context, PrefKey.VCI_TYPES, "");
        List<VciType> list = GsonUtil.parseData(jsonStr, new TypeToken<List<VciType>>() {
        }.getType());
        return list;
    }

    public static List<RegionData> getRegionDataList(Context context, String key) {
        String jsonStr = PrefUtil.getString(context, key, "");
        List<RegionData> list = GsonUtil.parseData(jsonStr, new TypeToken<List<RegionData>>() {
        }.getType());
        return list;
    }

    public static List<RegionData> getRegionDataListByProvinceId(Context context, String key, int provinceId) {
        String jsonStr = PrefUtil.getString(context, key, "");
        List<RegionData> list = GsonUtil.parseData(jsonStr, new TypeToken<List<RegionData>>() {
        }.getType());
        List<RegionData> cityList = new ArrayList<>();
        if (!CommonUtil.isEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                if (provinceId == list.get(i).getParentId()) {
                    cityList.add(list.get(i));
                }
            }
        }
        return cityList;
    }

    public static CommonConfig getCommonConfigById(Context context, String key, int id) {
        List<CommonConfig> list = getCommonConfigList(context, key);
        for (int i = 0; i < list.size(); i++) {
            CommonConfig commonConfig = list.get(i);
            if (id == commonConfig.getId()) {
                return commonConfig;
            }
        }
        return null;
    }

    public static VciType getVciTypeById(Context context, int id) {
        List<VciType> list = getVciTypes(context);
        for (int i = 0; i < list.size(); i++) {
            VciType vciType = list.get(i);
            if (id == vciType.getId()) {
                return vciType;
            }
        }
        return null;
    }

    public static RegionData getRegionDataById(Context context, String key, int id) {
        List<RegionData> list = getRegionDataList(context, key);
        for (int i = 0; i < list.size(); i++) {
            RegionData regionData = list.get(i);
            if (id == regionData.getId()) {
                return regionData;
            }
        }
        return null;
    }

    public static CommonConfig getCommonConfigById(List<CommonConfig> list, int id) {
        for (int i = 0; i < list.size(); i++) {
            CommonConfig commonConfig = list.get(i);
            if (id == commonConfig.getId()) {
                return commonConfig;
            }
        }
        return null;
    }

    public static VciType getVciTypeById(List<VciType> list, int id) {
        for (int i = 0; i < list.size(); i++) {
            VciType vciType = list.get(i);
            if (id == vciType.getId()) {
                return vciType;
            }
        }
        return null;
    }

    public static RegionData getRegionDataById(List<RegionData> list, int id) {
        for (int i = 0; i < list.size(); i++) {
            RegionData regionData = list.get(i);
            if (id == regionData.getId()) {
                return regionData;
            }
        }
        return null;
    }

    public static RegionData getCity(Context context, int id) {
        List<RegionData> list = getRegionDataList(context, PrefKey.CITY_LIST);
        for (int i = 0; i < list.size(); i++) {
            RegionData regionData = list.get(i);
            if (id == regionData.getId()) {
                return regionData;
            }
        }
        return null;
    }

    public static RegionData getProvince(Context context, int id) {
        List<RegionData> list = getRegionDataList(context, PrefKey.PROVINCE_LIST);
        for (int i = 0; i < list.size(); i++) {
            RegionData regionData = list.get(i);
            if (id == regionData.getId()) {
                return regionData;
            }
        }
        return null;
    }

    public static List<CompanyLogo> getComanyLogList(Context context) {
        String jsonStr = PrefUtil.getString(context, PrefKey.COMPANY_LOGO_LIST, "");
        List<CompanyLogo> list = GsonUtil.parseData(jsonStr, new TypeToken<List<CompanyLogo>>() {
        }.getType());
        return list;
    }

    public static CompanyLogo getCompanyLogoById(Context context, int id) {
        List<CompanyLogo> logoList = getComanyLogList(context);
        for (int i = 0; i < logoList.size(); i++) {
            CompanyLogo companyLogo = logoList.get(i);
            if (id == companyLogo.getId()) {
                return companyLogo;
            }
        }
        return null;
    }

}
