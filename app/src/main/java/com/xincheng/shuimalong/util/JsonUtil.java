package com.xincheng.shuimalong.util;

import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xlog.formatter.message.object.ObjectFormatter;
import com.xincheng.shuimalong.api.ParamsKey;
import com.xincheng.shuimalong.entity.config.VciType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.net.ssl.X509KeyManager;

/**
 * Created by xiaote on 2017/6/26.
 */

public class JsonUtil {
    public static String getVciTypeSettings(List<VciType> list) {
        JSONArray array = new JSONArray();
        try {
            for (int i = 0; i < list.size(); i++) {
                VciType vciType = list.get(i);
                JSONObject object = new JSONObject();
                object.put("id", String.valueOf(vciType.getId()));
                object.put("title", vciType.getTitle());
                object.put("ndis", String.valueOf(vciType.isNdis()));
                object.put("option", vciType.getOption());
                array.put(object);
            }
        } catch (JSONException e) {
            XLog.e(e.getMessage());
        }
        return array.toString();
    }
}
