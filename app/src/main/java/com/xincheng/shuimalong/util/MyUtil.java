package com.xincheng.shuimalong.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.request.RequestOptions;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.entity.address.Address;

import org.w3c.dom.Text;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by xiaote on 2017/6/30.
 */

public class MyUtil {
    public static Address getDefaultAddress(List<Address> list) {
        for (int i = 0; i < list.size(); i++) {
            Address address = list.get(i);
            if (address.isDefault()) {
                return address;
            }
        }
        return list.get(0);
    }

    public static boolean isDataLengthLess(List<?> dataList) {
        if (CommonUtil.isEmpty(dataList) || dataList.size() < Constant.DATA_LENGTH_DEFAULT) {
            return true;
        }
        return false;
    }


    public static File getOutputBaseFile(Context context,String fileDirName) {
        File mediaStorageDir;
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), fileDirName);
            if(!mediaStorageDir.exists()) {
                mediaStorageDir = new File(Environment.getExternalStorageDirectory(),fileDirName);
            }
        } else {
            CommonUtil.showToast(context, "没有sd卡");
            return null;
        }
        return mediaStorageDir;
    }

    public static File getOutputMediaFile(Context context, String fileDirName, String fileNamePrefix) {
        File file = getOutputBaseFile(context,fileDirName);
        if(file != null) {
            if(!file.exists()) {
                file.mkdirs();
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile = new File(file.getPath() + File.separator + fileNamePrefix + timeStamp + ".jpg");
            return mediaFile;
        }
        return null;
    }

    public static boolean checkVIN(String vin) {
        Map<Integer, Integer> vinMapWeighting = null;
        Map<Character, Integer> vinMapValue = null;
        vinMapWeighting = new HashMap<>();
        vinMapValue = new HashMap<>();
        vinMapWeighting.put(1, 8);
        vinMapWeighting.put(2, 7);
        vinMapWeighting.put(3, 6);
        vinMapWeighting.put(4, 5);
        vinMapWeighting.put(5, 4);
        vinMapWeighting.put(6, 3);
        vinMapWeighting.put(7, 2);
        vinMapWeighting.put(8, 10);
        vinMapWeighting.put(9, 0);
        vinMapWeighting.put(10, 9);
        vinMapWeighting.put(11, 8);
        vinMapWeighting.put(12, 7);
        vinMapWeighting.put(13, 6);
        vinMapWeighting.put(14, 5);
        vinMapWeighting.put(15, 4);
        vinMapWeighting.put(16, 3);
        vinMapWeighting.put(17, 2);

        vinMapValue.put('0', 0);
        vinMapValue.put('1', 1);
        vinMapValue.put('2', 2);
        vinMapValue.put('3', 3);
        vinMapValue.put('4', 4);
        vinMapValue.put('5', 5);
        vinMapValue.put('6', 6);
        vinMapValue.put('7', 7);
        vinMapValue.put('8', 8);
        vinMapValue.put('9', 9);
        vinMapValue.put('A', 1);
        vinMapValue.put('B', 2);
        vinMapValue.put('C', 3);
        vinMapValue.put('D', 4);
        vinMapValue.put('E', 5);
        vinMapValue.put('F', 6);
        vinMapValue.put('G', 7);
        vinMapValue.put('H', 8);
        vinMapValue.put('J', 1);
        vinMapValue.put('K', 2);
        vinMapValue.put('M', 4);
        vinMapValue.put('L', 3);
        vinMapValue.put('N', 5);
        vinMapValue.put('P', 7);
        vinMapValue.put('R', 9);
        vinMapValue.put('S', 2);
        vinMapValue.put('T', 3);
        vinMapValue.put('U', 4);
        vinMapValue.put('V', 5);
        vinMapValue.put('W', 6);
        vinMapValue.put('X', 7);
        vinMapValue.put('Y', 8);
        vinMapValue.put('Z', 9);
        boolean reultFlag = false;
        String uppervin = vin.toUpperCase();
        //排除字母O、I
        if (vin == null || uppervin.indexOf("O") >= 0 || uppervin.indexOf("I") >= 0) {
            reultFlag = false;
        } else {
            //1:长度为17
            if (vin.length() == 17) {
                char[] vinArr = uppervin.toCharArray();
                int amount = 0;
                for (int i = 0; i < vinArr.length; i++) {
                    //VIN码从从第一位开始，码数字的对应值×该位的加权值，计算全部17位的乘积值相加
                    amount += vinMapValue.get(vinArr[i]) * vinMapWeighting.get(i + 1);
                }
                //乘积值相加除以11、若余数为10，即为字母Ｘ
                if (amount % 11 == 10) {
                    if (vinArr[8] == 'X') {
                        reultFlag = true;
                    } else {
                        reultFlag = false;
                    }
                } else {
                    //VIN码从从第一位开始，码数字的对应值×该位的加权值，
                    //计算全部17位的乘积值相加除以11，所得的余数，即为第九位校验值
                    if (amount % 11 != vinMapValue.get(vinArr[8])) {
                        reultFlag = false;
                    } else {
                        reultFlag = true;
                    }
                }
            }
            //1:长度不为17
            if (!vin.equals("") && vin.length() != 17) {
                reultFlag = false;
            }
        }
        return reultFlag;
    }

    public static String getMoneyValueNew(double money, int max) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        if (money < max) {
            return decimalFormat.format(money);
        } else {
            return decimalFormat.format(money / 10000) + "万";
        }
    }

    public static boolean checkIdCard(String idCard) {
        // 对身份证号进行长度等简单判断
        if (TextUtils.isEmpty(idCard) || idCard.length() != 18 || !idCard.matches("\\d{17}[0-9X]")) {
            return false;
        }
        // 1-17位相乘因子数组
        int[] factor = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        // 18位随机码数组
        char[] random = "10X98765432".toCharArray();
        // 计算1-17位与相应因子乘积之和
        int total = 0;
        for (int i = 0; i < 17; i++) {
            total += Character.getNumericValue(idCard.charAt(i)) * factor[i];
        }
        // 判断随机码是否相等
        return random[total % 11] == idCard.charAt(17);
    }

    public static void showGlideImage(Context context, String imgUrl, ImageView imageView,
                                      RequestOptions requestOptions) {
        Glide.with(context).load(imgUrl).apply(requestOptions).into(imageView);
    }

    public static String getBillVciOrTciSubmitDate(String dateStr) {
        if(dateStr.endsWith("时")) {
            return dateStr.replace("时",":00:00");
        }
        return dateStr;
    }
}
