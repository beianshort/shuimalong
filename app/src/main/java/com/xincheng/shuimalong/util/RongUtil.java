package com.xincheng.shuimalong.util;

import android.app.ActivityManager;
import android.content.Context;

import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.entity.friend.ChatMessage;
import com.xincheng.shuimalong.listener.MyReceiveMessageListener;
import com.xincheng.shuimalong.listener.OnMessageListener;
import com.xincheng.shuimalong.listener.OnSystemListener;

import io.rong.imkit.RongIM;
import io.rong.imlib.AnnotationNotFoundException;
import io.rong.imlib.RongIMClient;

/**
 * Created by xiaote1988 on 2017/7/17.
 */

public class RongUtil {
    private static RongUtil instances;
    public static RongUtil getInstances(){
        if(instances==null){
            synchronized (RongUtil.class){
                if(instances==null){
                    instances=new RongUtil();
                }
            }
        }
        return instances;
    }

    public  void initRongImClient(Context context) {
        if (isCurrentProcess(context)) {
            RongIM.init(context, Constant.RONG_APPKEY);
//            try {
//                RongIMClient.registerMessageType(ChatMessage.class);
//            } catch (AnnotationNotFoundException e) {
//                e.printStackTrace();
//            }
        }
    }
    public  void setMessageListener(OnMessageListener onMessageListener){
    }
    public  void setSystemMessage(OnSystemListener onSystemListener){
    }
    public  boolean isCurrentProcess(Context context) {
        if (context.getApplicationInfo().packageName.equals(
                getCurProcessName(context.getApplicationContext()))) {
           return true;
        }
        return false;
    }

    private  String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
}
