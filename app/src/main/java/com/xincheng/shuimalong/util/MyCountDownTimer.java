package com.xincheng.shuimalong.util;

import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.widget.Button;

import com.xincheng.shuimalong.R;

/**
 * Created by 许浩 on 2017/6/12.
 */

public class MyCountDownTimer extends CountDownTimer {
    private Button btn_djs;

    public MyCountDownTimer(Button btn, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
        this.btn_djs = btn;
    }

    //计时过程
    @Override
    public void onTick(long tick) {
        //防止计时过程中重复点击
        btn_djs.setClickable(false);
        btn_djs.setText("(" + (tick / 1000) + ")重发");
        btn_djs.setTextColor(ContextCompat.getColor(btn_djs.getContext(), R.color.black_99));
    }

    //计时完毕的方法
    @Override
    public void onFinish() {
        //重新给Button设置文字
        btn_djs.setText("重新获取");
        btn_djs.setTextColor(ContextCompat.getColor(btn_djs.getContext(), R.color.red1));
        //设置可点击
        btn_djs.setClickable(true);
    }
}
