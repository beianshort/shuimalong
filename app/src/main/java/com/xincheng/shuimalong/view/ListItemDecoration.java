package com.xincheng.shuimalong.view;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.xincheng.library.util.UIUtil;

/**
 * Created by xiaote on 2017/6/16.
 */

public class ListItemDecoration extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int size = UIUtil.dip2px(1);
        outRect.set(0, size, 0, size);
    }
}
