package com.xincheng.shuimalong.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by xuhao on 2017/6/6.
 */

public class ResizeLayout extends LinearLayout {
    private OnResizeListener mListener;
    public interface OnResizeListener {
        void OnResize(int w, int h, int oldw, int oldh);
    }
    public void setOnResizeListener(OnResizeListener l) {
        mListener = l;
    }
    public ResizeLayout(Context context) {
        super(context);
    }

    public ResizeLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ResizeLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mListener != null) {
            mListener.OnResize(w, h, oldw, oldh);
        }
    }
}
