package com.xincheng.shuimalong.view.picker;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;

import com.google.gson.reflect.TypeToken;
import com.xincheng.library.R;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.widget.pickerview.adapter.ArrayWheelAdapter;
import com.xincheng.library.widget.pickerview.lib.WheelView;
import com.xincheng.library.widget.pickerview.listener.OnItemSelectedListener;
import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.api.BaseObserver;
import com.xincheng.shuimalong.api.GetParam;
import com.xincheng.shuimalong.api.RetroSubscrube;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.District;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.GsonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.annotations.NonNull;

public class AreaOptions {
    private Context context;
    private View view;
    private WheelView mProvinceWv;
    private WheelView mCityWv;
    private WheelView mDistictWv;
    private List<RegionData> cityAllList;
    private List<RegionData> mProvinceList;
    private List<RegionData> mCityList;
    private RegionData mCurrentCity;
    private List<District> mDistrictList;
    private boolean linkage;
    private OnItemSelectedListener wheelListener_option1;
    private OnItemSelectedListener wheelListener_option2;

    //文字的颜色和分割线的颜色
    int textColorOut;
    int textColorCenter;
    int dividerColor;

    private boolean showDistrict = true;

    private WheelView.DividerType dividerType;

    // 条目间距倍数
    float lineSpacingMultiplier = 1.6F;

    private BaseObserver mObserver;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public AreaOptions(Context context, View view, Boolean linkage) {
        super();
        this.context = context;
        this.linkage = linkage;
        this.view = view;
        mProvinceWv = (WheelView) view.findViewById(R.id.options1);// 初始化时显示的数据
        mCityWv = (WheelView) view.findViewById(R.id.options2);
        mDistictWv = (WheelView) view.findViewById(R.id.options3);
        cityAllList = ConfigUtil.getRegionDataList(context, PrefKey.CITY_LIST);
        mObserver = new BaseObserver(context) {
            @Override
            protected void onHandleSuccess(Object data, String msg) {
                if (data != null) {
                    try {
                        mDistrictList = GsonUtil.parseData(data, new TypeToken<List<District>>() {
                        }.getType());
                        if (!CommonUtil.isEmpty(mDistrictList)) {
                            mDistictWv.setAdapter(new ArrayWheelAdapter(mDistrictList));
                            mDistictWv.setCurrentItem(0);
                        }
                    } catch (Exception e) {

                    }
                }
            }
        };
    }

    public void setShowDistrict(boolean showDistrict) {
        this.showDistrict = showDistrict;
    }

    public void setPicker(List<RegionData> provinceList) {
        this.mProvinceList = provinceList;
        int len = ArrayWheelAdapter.DEFAULT_LENGTH;
        // 选项1
        mProvinceWv.setAdapter(new ArrayWheelAdapter(mProvinceList, len));// 设置显示数据
        mProvinceWv.setCurrentItem(0);// 初始化时显示的数据
        // 联动监听器
        wheelListener_option1 = new OnItemSelectedListener() {

            @Override
            public void onItemSelected(int index) {
                RegionData province = mProvinceList.get(index);
                XLog.e("selectProvince==>" + province.toString());
                if (mCityList == null) {
                    mCityList = new ArrayList<>();
                } else {
                    mCityList.clear();
                }
                for (int i = 0; i < cityAllList.size(); i++) {
                    if (province.getId() == cityAllList.get(i).getParentId()) {
                        mCityList.add(cityAllList.get(i));
                    }
                }
                if (!CommonUtil.isEmpty(mCityList)) {
                    mCityWv.setAdapter(new ArrayWheelAdapter(mCityList));
                    mCityWv.setCurrentItem(0);
                    wheelListener_option2.onItemSelected(0);
                }
            }
        };
        wheelListener_option2 = new OnItemSelectedListener() {

            @Override
            public void onItemSelected(int index) {
                if(showDistrict) {
                    mCurrentCity = mCityList.get(index);
                    getDistrictData();
                }
            }
        };
        mProvinceWv.setOnItemSelectedListener(wheelListener_option1);
        mCityWv.setOnItemSelectedListener(wheelListener_option2);
        wheelListener_option1.onItemSelected(0);
        mDistictWv.setVisibility(showDistrict ? View.VISIBLE : View.GONE);
    }

    private void getDistrictData() {
        RetroSubscrube.getInstance().getSubscrube(GetParam.getAreaByCityId(mCurrentCity.getId()), mObserver);
    }

    public void setTextContentSize(int textSize) {
        mProvinceWv.setTextSize(textSize);
        mCityWv.setTextSize(textSize);
        mDistictWv.setTextSize(textSize);
    }

    private void setTextColorOut() {
        mProvinceWv.setTextColorOut(textColorOut);
        mCityWv.setTextColorOut(textColorOut);
        mDistictWv.setTextColorOut(textColorOut);

    }

    private void setTextColorCenter() {
        mProvinceWv.setTextColorCenter(textColorCenter);
        mCityWv.setTextColorCenter(textColorCenter);
        mDistictWv.setTextColorCenter(textColorCenter);
    }

    private void setDividerColor() {
        mProvinceWv.setDividerColor(dividerColor);
        mCityWv.setDividerColor(dividerColor);
        mDistictWv.setDividerColor(dividerColor);
    }

    private void setDividerType() {
        mProvinceWv.setDividerType(dividerType);
        mCityWv.setDividerType(dividerType);
        mDistictWv.setDividerType(dividerType);
    }

    private void setLineSpacingMultiplier() {
        mProvinceWv.setLineSpacingMultiplier(lineSpacingMultiplier);
        mCityWv.setLineSpacingMultiplier(lineSpacingMultiplier);
        mDistictWv.setLineSpacingMultiplier(lineSpacingMultiplier);
    }

    /**
     * 设置选项的单位
     *
     * @param label1 单位
     * @param label2 单位
     * @param label3 单位
     */
    public void setLabels(String label1, String label2, String label3) {
        if (label1 != null)
            mProvinceWv.setLabel(label1);
        if (label2 != null)
            mCityWv.setLabel(label2);
        if (label3 != null)
            mDistictWv.setLabel(label3);
    }

    /**
     * 设置是否循环滚动
     *
     * @param cyclic 是否循环
     */
    public void setCyclic(boolean cyclic) {
        mProvinceWv.setCyclic(cyclic);
        mCityWv.setCyclic(cyclic);
        mDistictWv.setCyclic(cyclic);
    }

    /**
     * 设置字体样式
     *
     * @param font 系统提供的几种样式
     */
    public void setTypeface(Typeface font) {
        mProvinceWv.setTypeface(font);
        mCityWv.setTypeface(font);
        mDistictWv.setTypeface(font);
    }

    /**
     * 分别设置第一二三级是否循环滚动
     *
     * @param cyclic1,cyclic2,cyclic3 是否循环
     */
    public void setCyclic(boolean cyclic1, boolean cyclic2, boolean cyclic3) {
        mProvinceWv.setCyclic(cyclic1);
        mCityWv.setCyclic(cyclic2);
        mDistictWv.setCyclic(cyclic3);
    }


    /**
     * 返回当前选中的结果对应的位置数组 因为支持三级联动效果，分三个级别索引，0，1，2。
     * 在快速滑动未停止时，点击确定按钮，会进行判断，如果匹配数据越界，则设为0，防止index出错导致崩溃。
     *
     * @return 索引数组
     */
    public int[] getCurrentItems() {
        int[] currentItems = new int[3];
        currentItems[0] = mProvinceWv.getCurrentItem();
        currentItems[1] = mCityWv.getCurrentItem();
        currentItems[2] = mDistictWv.getCurrentItem();
        return currentItems;
    }

    public RegionData getProvince() {
        if (!CommonUtil.isEmpty(mProvinceList)) {
            return mProvinceList.get(mProvinceWv.getCurrentItem());
        }
        return null;
    }

    public RegionData getCity() {
        if (!CommonUtil.isEmpty(mCityList)) {
            return mCityList.get(mCityWv.getCurrentItem());
        }
        return null;
    }

    public District getDistrict() {
        if (!CommonUtil.isEmpty(mDistrictList)) {
            return mDistrictList.get(mDistictWv.getCurrentItem());
        }
        return null;
    }

    public void setCurrentItems(int option1, int option2, int option3) {
        if (linkage) {
            itemSelected(option1, option2, option3);
        }
        mProvinceWv.setCurrentItem(option1);
        mCityWv.setCurrentItem(option2);
        mDistictWv.setCurrentItem(option3);
    }

    private void itemSelected(int opt1Select, int opt2Select, int opt3Select) {
        if (mProvinceList != null) {
            mProvinceWv.setAdapter(new ArrayWheelAdapter(mProvinceList));
            mProvinceWv.setCurrentItem(opt1Select);
        }
        if (mCityList != null) {
            mCityWv.setAdapter(new ArrayWheelAdapter(mCityList));
            mCityWv.setCurrentItem(opt2Select);
        }
        if (mDistrictList != null) {
            mDistictWv.setAdapter(new ArrayWheelAdapter(mDistrictList));
            mDistictWv.setCurrentItem(opt3Select);
        }
    }

    /**
     * 设置间距倍数,但是只能在1.2-2.0f之间
     *
     * @param lineSpacingMultiplier
     */
    public void setLineSpacingMultiplier(float lineSpacingMultiplier) {
        this.lineSpacingMultiplier = lineSpacingMultiplier;
        setLineSpacingMultiplier();
    }

    /**
     * 设置分割线的颜色
     *
     * @param dividerColor
     */
    public void setDividerColor(int dividerColor) {
        this.dividerColor = dividerColor;
        setDividerColor();
    }

    /**
     * 设置分割线的类型
     *
     * @param dividerType
     */
    public void setDividerType(WheelView.DividerType dividerType) {
        this.dividerType = dividerType;
        setDividerType();
    }

    /**
     * 设置分割线之间的文字的颜色
     *
     * @param textColorCenter
     */
    public void setTextColorCenter(int textColorCenter) {
        this.textColorCenter = textColorCenter;
        setTextColorCenter();
    }

    /**
     * 设置分割线以外文字的颜色
     *
     * @param textColorOut
     */
    public void setTextColorOut(int textColorOut) {
        this.textColorOut = textColorOut;
        setTextColorOut();
    }

    /**
     * Label 是否只显示中间选中项的
     *
     * @param isCenterLabel
     */

    public void isCenterLabel(Boolean isCenterLabel) {
        mProvinceWv.isCenterLabel(isCenterLabel);
        mCityWv.isCenterLabel(isCenterLabel);
        mDistictWv.isCenterLabel(isCenterLabel);
    }

}
