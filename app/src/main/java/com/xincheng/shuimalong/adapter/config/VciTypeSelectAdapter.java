package com.xincheng.shuimalong.adapter.config;

import android.content.Context;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/27.
 */

public class VciTypeSelectAdapter extends BaseListRecyclerAdapter<VciType,
        VciTypeSelectAdapter.VciTypeSelectViewHolder> {
    private ItemButtonClickListener listener;

    public VciTypeSelectAdapter(Context context) {
        super(context);
    }

    public VciTypeSelectAdapter(Context context, List<VciType> vciTypeList) {
        super(context, vciTypeList);
    }

    public void setItemButtonClickListener(ItemButtonClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_vci_type_select;
    }

    @Override
    public VciTypeSelectViewHolder createViewHolder(View itemView, int viewType) {
        VciTypeSelectViewHolder holder = new VciTypeSelectViewHolder(itemView);
        holder.setItemButtonClickListener(listener);
        return holder;
    }

    @Override
    public void convert(VciTypeSelectViewHolder holder, VciType vciType, int position) {
        holder.nameTv.setText(vciType.getTitle());
        holder.checkedTv.setVisibility(vciType.canNdis() ? View.VISIBLE : View.GONE);
        holder.checkedTv.setChecked(vciType.isNdis());
        holder.optionTv.setText(vciType.getOption());
    }

    static class VciTypeSelectViewHolder extends RecyclerHolder {
        @BindView(R.id.vci_type_select_name_tv)
        TextView nameTv;
        @BindView(R.id.vci_type_select_checked_tv)
        CheckedTextView checkedTv;
        @BindView(R.id.vci_type_select_option_tv)
        TextView optionTv;
        private ItemButtonClickListener listener;

        public void setItemButtonClickListener(ItemButtonClickListener listener) {
            this.listener = listener;
        }

        public VciTypeSelectViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.vci_type_select_checked_tv, R.id.vci_type_select_option_tv})
        public void selectOption(View view) {
            if (listener != null) {
                listener.buttonClick(view.getId(), getAdapterPosition());
            }
        }
    }

}
