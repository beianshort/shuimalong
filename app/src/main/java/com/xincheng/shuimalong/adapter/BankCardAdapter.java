package com.xincheng.shuimalong.adapter;

import android.view.View;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListSwipeMenuRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.bank.BankCard;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/7/11.
 */

public class BankCardAdapter extends BaseListSwipeMenuRecyclerAdapter<BankCard.Data,BankCardAdapter.BankCardViewHolder> {


    public BankCardAdapter() {
        super();
    }

    @Override
    public int getLayoutResId(int viewType) {
        return R.layout.item_bankcard;
    }

    @Override
    public BankCardViewHolder onCompatCreateViewHolder(View realContentView, int viewType) {
        return new BankCardViewHolder(realContentView);
    }

    @Override
    public void onCompatBindViewHolder(BankCardViewHolder holder, int position) {
        holder.bankcardNameTv.setText(dataList.get(position).getBank());
        holder.usernameTv.setText(dataList.get(position).getName());
        holder.bankcardNoTv.setText(dataList.get(position).getBankcardNo());
    }

    static class BankCardViewHolder extends RecyclerHolder {
        @BindView(R.id.bankcard_name_tv)
        TextView bankcardNameTv;
        @BindView(R.id.bankcard_username_tv)
        TextView usernameTv;
        @BindView(R.id.bankcard_no_tv)
        TextView bankcardNoTv;

        public BankCardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
