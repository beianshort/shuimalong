package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.friend.ChatMessage;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import java.util.List;

import io.rong.imlib.model.Message;
import io.rong.imlib.model.UserInfo;
import io.rong.message.TextMessage;

/**
 * Created by 许浩 on 2017/7/1.
 */

public class ChatAdapter extends BaseAdapter {
    private List<ChatMessage>infolist;
    private LayoutInflater inflaterl;
    private Context mcontext;
    private boolean is_me;
    public ChatAdapter(Context context, List<ChatMessage>info){
        this.inflaterl=LayoutInflater.from(context);
        this.infolist=info;
        this.mcontext=context;
    }
    @Override
    public int getCount() {
        return infolist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        //文本消息
            ChatMessage chatMessage= infolist.get(position);
            if( chatMessage.getUserInfo().equals(PrefUtil.getString(mcontext, PrefKey.LOGIN_USER_ID,""))){
                view=inflaterl.inflate(R.layout.item_chat_layout,null);
                is_me=true;
            }else{
                view=inflaterl.inflate(R.layout.item_chat_left_layout,null);
                is_me=false;
            }
            ChatHolder holder=new ChatHolder(view,is_me);
            holder.message.setText(chatMessage.getContent());
            Glide.with(mcontext).load(chatMessage.getFace()).apply(
                    GlideOptionsManager.getInstance().getRequestOptions()).into(holder.avatar);
        return view;
    }
    private class ChatHolder{
        ImageView avatar;
        TextView message;
        public ChatHolder(View view,boolean isSend){
            if(isSend){
                this.message= (TextView) view.findViewById(R.id.tv_message_content);
                this.avatar= (ImageView) view.findViewById(R.id.iv_send_avatar);
            }else{
                this.message= (TextView) view.findViewById(R.id.tv_from_message);
                this.avatar= (ImageView) view.findViewById(R.id.iv_from_avatar);
            }
        }
    }
}
