package com.xincheng.shuimalong.adapter.config;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.ListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.config.RegionData;

import java.util.List;

/**
 * Created by xiaote on 2017/6/19.
 */

public class CityAdapter extends ListRecyclerAdapter<RegionData> {

    public CityAdapter(Context context) {
        super(context);
    }

    public CityAdapter(Context context, List<RegionData> dataList) {
        super(context, dataList);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_city;
    }

    @Override
    public void convert(RecyclerHolder holder, RegionData cityData, int position) {
        holder.setText(R.id.city_name_tv, cityData.getName());
        String currentLetter = cityData.getLetter();
        String previousLetter = position >= 1 ? getDataList().get(position - 1).getLetter() : "";
        holder.setText(R.id.city_letter_tv, currentLetter);
        holder.setVisibility(R.id.city_letter_tv,
                TextUtils.equals(currentLetter, previousLetter) ? View.GONE : View.VISIBLE);
    }

    public int getLetterPosition(String letter) {
        if (!CommonUtil.isEmpty(getDataList())) {
            for (int i = 0; i < getDataList().size(); i++) {
                if (letter.equals(getDataList().get(i).getLetter())) {
                    return i;
                }
            }
        }
        return -1;
    }
}
