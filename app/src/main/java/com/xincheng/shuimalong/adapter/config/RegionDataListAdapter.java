package com.xincheng.shuimalong.adapter.config;

import android.content.Context;

import com.xincheng.library.xswipemenurecyclerview.adapter.ListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.config.RegionData;

import java.util.List;

/**
 * Created by xiaote on 2017/6/22.
 */

public class RegionDataListAdapter extends ListRecyclerAdapter<RegionData> {

    public RegionDataListAdapter(Context context) {
        super(context);
    }

    public RegionDataListAdapter(Context context, List<RegionData> dataList) {
        super(context, dataList);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_config_list;
    }

    @Override
    public void convert(RecyclerHolder holder, RegionData regionData, int position) {
        holder.setText(R.id.config_name_tv, regionData.getName());
    }
}
