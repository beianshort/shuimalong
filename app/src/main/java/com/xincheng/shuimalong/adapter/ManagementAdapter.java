package com.xincheng.shuimalong.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.customer.Customer;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 许浩 on 2017/6/20.
 */

public class ManagementAdapter extends BaseListRecyclerAdapter<Customer, ManagementAdapter.ManagementHolder> {

    public ManagementAdapter(Context context) {
        super(context);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_management;
    }

    @Override
    public ManagementHolder createViewHolder(View itemView, int viewType) {
        ManagementHolder holder = new ManagementHolder(itemView);
        return holder;
    }

    @Override
    public void convert(ManagementHolder holder, Customer data, int position) {
        holder.tvName.setText(data.getCar_owner());
        holder.tvCarNumber.setText(data.getPlate_no());
        holder.tvDate.setText(data.getCar_register_date());
        holder.tvPhone.setText(data.getMobile_phone());
    }


    class ManagementHolder extends RecyclerHolder {
        @BindView(R.id.img_management_avatar)
        ImageView imgManagementAvatar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_car_number)
        TextView tvCarNumber;
        @BindView(R.id.tv_phone)
        TextView tvPhone;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public ManagementHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
