package com.xincheng.shuimalong.adapter.market;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.market.MarketSort;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/7/7.
 */

public class MarketSortAdapter extends BaseListRecyclerAdapter<MarketSort, MarketSortAdapter.MarketSortViewHolder> {


    public MarketSortAdapter(Context context, List<MarketSort> dataList) {
        super(context, dataList);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.market_sort_item;
    }

    @Override
    public MarketSortViewHolder createViewHolder(View itemView, int viewType) {
        return new MarketSortViewHolder(itemView);
    }

    @Override
    public void convert(MarketSortViewHolder holder, MarketSort marketSort, int position) {
        holder.nameTv.setText(marketSort.getName());
        holder.nameTv.setTextColor(ContextCompat.getColor(getContext(),
                marketSort.isSelected() ? R.color.blue1 : R.color.black_66));
        holder.selectIv.setVisibility(marketSort.isSelected() ? View.VISIBLE : View.GONE);
    }

    public void changeSelect(int position) {
        for(int i = 0; i< dataList.size();i++) {
            if(i == position) {
                dataList.get(i).setSelected(true);
            } else {
                dataList.get(i).setSelected(false);
            }
        }
        this.notifyDataSetChanged();
    }

    public static class MarketSortViewHolder extends RecyclerHolder {
        @BindView(R.id.sort_name_tv)
        TextView nameTv;
        @BindView(R.id.sort_select_iv)
        ImageView selectIv;

        public MarketSortViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
