package com.xincheng.shuimalong.adapter;

import android.content.Context;

import com.xincheng.library.xswipemenurecyclerview.adapter.ListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;

import java.util.List;

/**
 * Created by xiaote on 2017/7/22.
 */

public class SearchHistoryAdapter extends ListRecyclerAdapter<String> {

    public SearchHistoryAdapter(Context context, List<String> dataList) {
        super(context, dataList);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_cofig_grid;
    }

    @Override
    public void convert(RecyclerHolder holder, String str, int position) {
        holder.setText(R.id.config_grid_name_tv, str);
    }
}
