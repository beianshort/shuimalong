package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.ApplyFriendCommon;
import com.xincheng.shuimalong.entity.friend.ApplyFriendBean;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xuhao on 2017/7/18.
 */

public class SearchFriendAdapter extends BaseListRecyclerAdapter<ApplyFriendBean.data, SearchFriendAdapter.FriendHolder> {
    private Context mcontext;
    private onApplyListener onApplyListener;

    public SearchFriendAdapter(Context context, List<ApplyFriendBean.data> infolist) {
        super(context, infolist);
        this.mcontext = context;
    }

    public void setOnApplyListener(onApplyListener applyListener) {
        this.onApplyListener = applyListener;
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_search_friend;
    }

    @Override
    public FriendHolder createViewHolder(View itemView, int viewType) {
        FriendHolder holder = new FriendHolder(itemView);
        holder.setListener(onApplyListener);
        return holder;
    }

    @Override
    public void convert(FriendHolder holder, ApplyFriendBean.data data, int position) {
        holder.tvName.setText(data.getNick_name());
        if (data.getIs_friend() == 1) {
            holder.tvAddfriend.setVisibility(View.GONE);
            holder.tvChat.setVisibility(View.VISIBLE);
            holder.tvChat.setTag(data);
        }else{
            holder.tvChat.setVisibility(View.GONE);
            holder.tvAddfriend.setVisibility(View.VISIBLE);
            holder.tvAddfriend.setTag(data);
        }

        Glide.with(mcontext).load(data.getAvatar()).apply(
                GlideOptionsManager.getInstance().getRequestOptions()).into(holder.itemAvatar);

    }

    static class FriendHolder extends RecyclerHolder {
        private onApplyListener listener;
        @BindView(R.id.item_avatar)
        ImageView itemAvatar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_addfriend)
        TextView tvAddfriend;
        @BindView(R.id.tv_chat)
        TextView tvChat;

        public void setListener(SearchFriendAdapter.onApplyListener listener) {
            this.listener = listener;
        }

        public FriendHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.tv_chat, R.id.tv_addfriend})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.tv_chat:
                    if (listener != null) {
                        listener.onApply((ApplyFriendBean.data) view.getTag(), ApplyFriendCommon.CHAT);
                    }
                    break;
                case R.id.tv_addfriend:
                    if (listener != null) {
                        listener.onApply((ApplyFriendBean.data) view.getTag(), ApplyFriendCommon.FRIEND);
                    }
                    break;
            }
        }
    }

    public interface onApplyListener {
        void onApply(ApplyFriendBean.data data, int status);
    }
}
