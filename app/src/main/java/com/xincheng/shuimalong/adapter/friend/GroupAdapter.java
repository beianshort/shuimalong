package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.library.xswipemenurecyclerview.listener.OnItemClickListener;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.friend.Friend;
import com.xincheng.shuimalong.entity.friend.HistoryChat;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 许浩 on 2017/7/26.
 */

public class GroupAdapter extends BaseListRecyclerAdapter<Friend, GroupAdapter.GroupHolder> {
    public GroupAdapter(Context context) {
        super(context);
    }

    public GroupAdapter(Context context, List<Friend> dataList) {
        super(context, dataList);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_group_layout;
    }

    @Override
    public GroupHolder createViewHolder(View itemView, int viewType) {
        GroupHolder groupHolder = new GroupHolder(itemView);
        return groupHolder;
    }

    @Override
    public void convert(GroupHolder holder, final Friend friend, int position) {
        holder.tvGroupName.setText(friend.getGroup_name());
        if(friend.getChecked()){
            holder.ivGroupSelect.setVisibility(View.VISIBLE);
        }else{
            holder.ivGroupSelect.setVisibility(View.GONE);
        }
    }

    public static class GroupHolder extends RecyclerHolder {
        @BindView(R.id.tv_group_name)
        TextView tvGroupName;
        @BindView(R.id.iv_group_select)
        ImageView ivGroupSelect;
        public GroupHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
