package com.xincheng.shuimalong.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xincheng.library.widget.CheckImageView;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListSwipeMenuRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.bank.BankCard;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/7/11.
 */

public class PayBankCardAdapter extends BaseListRecyclerAdapter<BankCard.Data, PayBankCardAdapter
        .BankCardViewHolder> {


    public PayBankCardAdapter(Context context) {
        super(context);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_pay_layout;
    }

    @Override
    public BankCardViewHolder createViewHolder(View itemView, int viewType) {
        return new BankCardViewHolder(itemView);
    }

    @Override
    public void convert(BankCardViewHolder holder, BankCard.Data data, int position) {
        holder.tvWalletMoney.setVisibility(View.GONE);
        switch (data.getStatus()) {
            case 0:
                holder.tvBankName.setText(data.getBank());
                holder.imvCheckPay.setVisibility(View.INVISIBLE);
                holder.imvPayBank.setVisibility(View.INVISIBLE);
                holder.tvWalletMoney.setVisibility(View.GONE);
                break;
            case 1:
                holder.imvPayBank.setVisibility(View.VISIBLE);
                holder.imvCheckPay.setVisibility(View.VISIBLE);
                holder.tvWalletMoney.setVisibility(View.VISIBLE);
                holder.tvWalletMoney.setText(data.getMoney());
                holder.tvBankName.setText(data.getBank());
                holder.imvCheckPay.setChecked(data.getIsSelect());
                break;
            case 2:
                holder.tvWalletMoney.setVisibility(View.GONE);
                holder.imvPayBank.setVisibility(View.VISIBLE);
                holder.imvCheckPay.setVisibility(View.VISIBLE);
                holder.tvBankName.setText(data.getBank() + "(" + data.getBankcardNoLast() + ")");
                holder.imvCheckPay.setChecked(data.getIsSelect());
                break;
        }
    }

    public void changeSelected(int position) {
        for (int i = 0; i < dataList.size(); i++) {
            if (i == position) {
                dataList.get(i).setIsSelect(1);
            } else {
                dataList.get(i).setIsSelect(0);
            }
        }
        this.notifyDataSetChanged();
    }

    static class BankCardViewHolder extends RecyclerHolder {
        @BindView(R.id.tv_bank_name)
        TextView tvBankName;
        @BindView(R.id.imv_check_pay)
        CheckImageView imvCheckPay;
        @BindView(R.id.imv_pay_bank)
        ImageView imvPayBank;
        @BindView(R.id.tv_wallet_money)
        TextView tvWalletMoney;

        public BankCardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
