package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.friend.HistoryChat;
import com.xincheng.shuimalong.util.MyDateUtil;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xuhao on 2017/7/18.
 */

public class ChatHistoryAdapter extends BaseListRecyclerAdapter<HistoryChat,ChatHistoryAdapter.HistoryHolder> {
   private Context mcontext;
    OnItemLongClickListener mOnItemLongClickListener;
    public ChatHistoryAdapter(Context context, List<HistoryChat>infolist) {
        super(context,infolist);
        this.mcontext=context;
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_chathistory_layout;
    }

    @Override
    public HistoryHolder createViewHolder(View itemView, int viewType) {
        ChatHistoryAdapter.HistoryHolder holder = new ChatHistoryAdapter.HistoryHolder(itemView);
        return holder;
    }

    @Override
    public void convert(final HistoryHolder holder, final HistoryChat conversation, final int position) {
        holder.tvLastMessage.setText(conversation.getLasstmessage());
        holder.tvName.setText(conversation.getNick_name());
        holder.tvLastTime.setText(MyDateUtil.getNewChatTime(conversation.getTime()));
        if(conversation.getUnbumber()>0){
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.tvNumber.setText(conversation.getUnbumber()+"");
        }else{
            holder.tvNumber.setVisibility(View.INVISIBLE);
        }

        Glide.with(mcontext).load(conversation.getFace()).apply(
                GlideOptionsManager.getInstance().getRequestOptions()).into(holder.itemAvatar);
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return mOnItemLongClickListener.onItemLongClick(holder,conversation, position);
            }
        });
    }

   public static class HistoryHolder extends RecyclerHolder{
        @BindView(R.id.item_avatar)
        ImageView itemAvatar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_last_message)
        TextView tvLastMessage;
        @BindView(R.id.tv_number)
        TextView tvNumber;
        @BindView(R.id.tv_last_time)
        TextView tvLastTime;
        public HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface OnItemLongClickListener {
        boolean onItemLongClick(HistoryHolder viewHolder, HistoryChat conversation, int position);
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        mOnItemLongClickListener = onItemLongClickListener;
    }
}
