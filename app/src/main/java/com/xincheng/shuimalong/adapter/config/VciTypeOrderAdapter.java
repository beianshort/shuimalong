package com.xincheng.shuimalong.adapter.config;

import android.content.Context;
import android.view.View;

import com.xincheng.library.xswipemenurecyclerview.adapter.ListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.entity.config.VciTypeOrder;

import java.util.List;

/**
 * Created by xiaote on 2017/6/28.
 */

public class VciTypeOrderAdapter extends ListRecyclerAdapter<VciTypeOrder> {

    public VciTypeOrderAdapter(Context context, List<VciTypeOrder> dataList) {
        super(context, dataList);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_vci_type_order;
    }

    @Override
    public void convert(RecyclerHolder holder, VciTypeOrder vciType, int position) {
        holder.setText(R.id.vci_type_name_tv, vciType.getTitle());
        holder.setVisibility(R.id.vci_type_ndis_tv, vciType.isNdis() ? View.VISIBLE : View.GONE);
        holder.setText(R.id.vci_type_option_tv, vciType.getOption());
    }
}
