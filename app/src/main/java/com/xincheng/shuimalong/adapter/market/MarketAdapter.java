package com.xincheng.shuimalong.adapter.market;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xincheng.library.glide.GlideCircleTransform;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.entity.market.Market;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;
import com.xincheng.shuimalong.util.ConfigUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/16.
 */

public class MarketAdapter extends BaseListRecyclerAdapter<Market.Data, MarketAdapter.MarketViewHolder> {
    private ItemButtonClickListener listener;
    private List<RegionData> mCityList;
    private List<RegionData> mProvinceList;
    private List<CommonConfig> newCarTypeList;
    private List<CommonConfig> companyList;
    private List<CommonConfig> carTypeList;
    private RequestOptions options;

    public MarketAdapter(Context context, RequestOptions options) {
        super(context);
        initConfigData(context);
        this.options = options;
    }

    private void initConfigData(Context context) {
        mCityList = ConfigUtil.getRegionDataList(context, PrefKey.CITY_LIST);
        mProvinceList = ConfigUtil.getRegionDataList(context, PrefKey.PROVINCE_LIST);
        newCarTypeList = ConfigUtil.getCommonConfigList(context, PrefKey.NEW_CAR_TYPE);
        companyList = ConfigUtil.getCommonConfigList(context, PrefKey.COMPANY_LIST);
    }

    public void setItemButtonClickListener(ItemButtonClickListener listener) {
        this.listener = listener;
    }

    public void setCarTypeList(List<CommonConfig> carTypeList) {
        this.carTypeList = carTypeList;
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_market;
    }

    @Override
    public MarketViewHolder createViewHolder(View itemView, int viewType) {
        MarketViewHolder holder = new MarketViewHolder(itemView);
        if (listener != null) {
            holder.setItemButtonClickListener(listener);
        }
        return holder;
    }

    @Override
    public void convert(MarketViewHolder holder, Market.Data market, int position) {
        Glide.with(getContext()).load(market.getAvatar()).apply(options).into(holder.headIconIv);
        holder.usernameTv.setText(market.getNickName());
        String cityName = ConfigUtil.getRegionDataById(mCityList, market.getCityId()).getName();
        String provinceName = ConfigUtil.getRegionDataById(mProvinceList, market.getProvinceId()).getName();
        holder.cityTv.setText(provinceName + cityName);
        holder.bidNumTv.setText(market.getBidNumStr());
        holder.cartypeTv.setText(ConfigUtil.getCommonConfigById(carTypeList, market.getCarTypeId()).getName());
        holder.newoldTv.setText(ConfigUtil.getCommonConfigById(newCarTypeList, market.getNewCarId()).getName());
        holder.useAgeTv.setText(market.getUseAgeStr());
        CommonConfig company = ConfigUtil.getCommonConfigById(companyList, market.getCompanyId());
        if(company != null) {
            holder.insuranceCompanyTv.setText(company.getName());
        }
        holder.vciRbTv.setText(market.getVciRb() + "%");
        holder.tciRbTv.setText(market.getTciRb() + "%");
    }

    static class MarketViewHolder extends RecyclerHolder {
        @BindView(R.id.item_market_headicon_iv)
        ImageView headIconIv;
        @BindView(R.id.item_market_username_tv)
        TextView usernameTv;
        @BindView(R.id.item_market_city_tv)
        TextView cityTv;
        @BindView(R.id.item_market_bid_num_tv)
        TextView bidNumTv;
        @BindView(R.id.item_market_quotation_tv)
        TextView quotationTv;
        @BindView(R.id.item_market_cartype_tv)
        TextView cartypeTv;
        @BindView(R.id.item_market_newold_tv)
        TextView newoldTv;
        @BindView(R.id.item_market_useage_tv)
        TextView useAgeTv;
        @BindView(R.id.item_market_insurance_company_tv)
        TextView insuranceCompanyTv;
        @BindView(R.id.item_market_vcirb_tv)
        TextView vciRbTv;
        @BindView(R.id.item_market_tcirb_tv)
        TextView tciRbTv;

        private ItemButtonClickListener listener;

        public void setItemButtonClickListener(ItemButtonClickListener listener) {
            this.listener = listener;
        }

        public MarketViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.item_market_headicon_iv,R.id.item_market_quotation_tv})
        public void marketClick(View view) {
            if (listener != null) {
                listener.buttonClick(view.getId(), getAdapterPosition());
            }
        }
    }


}
