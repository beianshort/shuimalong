package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.activity.insurance.UserHomeActivity;
import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.util.ShowActivity;

import io.rong.imkit.model.UIMessage;
import io.rong.imkit.widget.adapter.MessageListAdapter;
import io.rong.imlib.model.UserInfo;

/**
 * Created by 许浩 on 2017/7/31.
 */

public class ChatMessageAdapter extends MessageListAdapter {
    public ChatMessageAdapter(Context context) {
        super(context);
    }
    OnAvatarClick onAvatarClick;

    public void setOnAvatarClick(OnAvatarClick onAvatarClick) {
        this.onAvatarClick = onAvatarClick;
    }

    @Override
    protected void bindView(View v, int position, final UIMessage data) {
        super.bindView(v, position, data);
        v.findViewById(R.id.rc_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onAvatarClick!=null){
                    onAvatarClick.onAvatarClick(false);
                }
            }
        });
        v.findViewById(R.id.rc_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onAvatarClick!=null){
                    onAvatarClick.onAvatarClick(true);
                }
            }
        });
    }
    public interface OnAvatarClick{
        void onAvatarClick(boolean isMyself);
    }
}
