package com.xincheng.shuimalong.adapter.friend;



import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xincheng.library.adpater.ViewHolder;
import com.xincheng.library.view.TreeItem;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.friend.Friend;
import com.xincheng.shuimalong.manager.GlideOptionsManager;


/**
 */
public class ChildrenItem extends TreeItem<Friend.Data> {


    @Override
    public int initLayoutId() {
        return R.layout.item_friend_item;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder) {
        holder.setText(R.id.tv_friend_name, data.getNick_name());
        holder.setText(R.id.tv_location, data.getLocation());
        holder.setText(R.id.tv_deal_num, data.getDealNum());
        if(getParentItem().getContext()!=null){
            ImageView img=holder.getView(R.id.imv_friend_avatar);
            Glide.with(getParentItem().getContext()).load(data.getFace()).apply(
                    GlideOptionsManager.getInstance().getRequestOptions()).into(img);
        }
    }
}
