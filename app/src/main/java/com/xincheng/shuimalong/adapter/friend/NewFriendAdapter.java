package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.ApplyFriendCommon;
import com.xincheng.shuimalong.entity.friend.ApplyFriendBean;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xuhao on 2017/7/18.
 */

public class NewFriendAdapter extends BaseListRecyclerAdapter<ApplyFriendBean.data, NewFriendAdapter.FriendHolder> {
    private Context mcontext;
    private onApplyListener onApplyListener;
    public NewFriendAdapter(Context context, List<ApplyFriendBean.data> infolist) {
        super(context, infolist);
        this.mcontext = context;
    }
    public void setOnApplyListener(onApplyListener applyListener){
        this.onApplyListener=applyListener;
    }
    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_addfriend_layout;
    }

    @Override
    public FriendHolder createViewHolder(View itemView, int viewType) {
        FriendHolder holder = new FriendHolder(itemView);
        holder.setListener(onApplyListener);
        return holder;
    }

    @Override
    public void convert(FriendHolder holder, ApplyFriendBean.data data, int position) {
        holder.tvName.setText(data.getNick_name());
        holder.tvAgree.setTag(data);
        holder.tvReject.setTag(data);
        Glide.with(mcontext).load(data.getAvatar()).apply(
                GlideOptionsManager.getInstance().getRequestOptions()).into(holder.itemAvatar);

    }

    static class FriendHolder extends RecyclerHolder {
        private onApplyListener listener;
        @BindView(R.id.item_avatar)
        ImageView itemAvatar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_reject)
        TextView tvReject;
        @BindView(R.id.tv_agree)
        TextView tvAgree;
        public void setListener(NewFriendAdapter.onApplyListener listener) {
            this.listener = listener;
        }

        public FriendHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        @OnClick({R.id.tv_reject, R.id.tv_agree})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.tv_reject:
                    if(listener!=null){
                        listener.onApply(( ApplyFriendBean.data)view.getTag(), ApplyFriendCommon.REJECT);
                    }
                    break;
                case R.id.tv_agree:
                    if(listener!=null){
                        listener.onApply(( ApplyFriendBean.data)view.getTag(), ApplyFriendCommon.AGREE);
                    }
                    break;
            }
        }
    }
    public interface onApplyListener{
        void onApply( ApplyFriendBean.data data,int status);
    }
}
