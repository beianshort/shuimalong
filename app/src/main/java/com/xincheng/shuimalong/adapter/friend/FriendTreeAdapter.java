package com.xincheng.shuimalong.adapter.friend;

import com.xincheng.library.adpater.TreeRecyclerAdapter;
import com.xincheng.library.view.TreeItem;
import com.xincheng.library.view.TreeItemGroup;

/**
 * Created by 许浩 on 2017/7/29.
 */

public class FriendTreeAdapter extends TreeRecyclerAdapter {
    public void setChecked(int position ){
        int itemPosition = getCheckItem().getAfterCheckingPosition(position);
        TreeItem treeItem=getData(itemPosition);
        if(treeItem!=null){
            TreeItemGroup group=((TreeItemGroup) treeItem);
            group.setExpand(false);
            expandOrCollapse(group);
        }
    }
}
