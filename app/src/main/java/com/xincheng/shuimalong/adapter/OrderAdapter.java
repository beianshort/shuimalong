package com.xincheng.shuimalong.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xincheng.library.glide.GlideCircleTransform;
import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.OrderConfig;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.Order;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;
import com.xincheng.shuimalong.util.ConfigUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/28.
 */

public class OrderAdapter extends BaseListRecyclerAdapter<Order.Data,
        OrderAdapter.AcceptQuoteViewHolder> {
    private List<CommonConfig> carTypeList;
    private List<CommonConfig> companyList;
    private int orderConfig;
    private ItemButtonClickListener listener;
    private RequestOptions options;

    public OrderAdapter(Context context, int orderConfig, RequestOptions options) {
        super(context);
        this.orderConfig = orderConfig;
        this.options = options;
        initConfigData(context);
    }

    private void initConfigData(Context context) {
        carTypeList = ConfigUtil.getCommonConfigList(context, PrefKey.CAR_TYPE);
        companyList = ConfigUtil.getCommonConfigList(context, PrefKey.COMPANY_LIST);
    }

    public void setItemButtonClickListener(ItemButtonClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_order;
    }

    @Override
    public AcceptQuoteViewHolder createViewHolder(View itemView, int viewType) {
        AcceptQuoteViewHolder holder = new AcceptQuoteViewHolder(itemView);
        holder.setItemButtonClickListener(listener);
        return holder;
    }

    @Override
    public void convert(AcceptQuoteViewHolder holder, Order.Data data, int position) {
        Glide.with(getContext()).load(data.getAvatar()).apply(options).into(holder.headIconIv);
        holder.usernameTv.setText(data.getNickName());
        holder.carOwnerTv.setText(data.getCarOwner());
        holder.plateNoTv.setText(data.getPlateNo());
        holder.carVehicleTv.setText(data.getCarBak());
        if (data.getCarTypeId() != 0) {
            holder.carTypeTv.setText(ConfigUtil.getCommonConfigById(carTypeList,
                    data.getCarTypeId()).getName());
        }
        if (data.getCompanyId() != 0) {
            CommonConfig company = ConfigUtil.getCommonConfigById(companyList,
                    data.getCompanyId());
            if (company != null) {
                holder.companyTv.setText(company.getName());
            }
        }
        holder.effectDateTv.setText(data.getTakeEffectDate());
        holder.updateTimeTv.setText(DateUtil.parseDate(data.getUpdateTime()));
        holder.updateTime2Tv.setText(DateUtil.parseDate(data.getUpdateTime()));
        if (data.getDisabled() == 1) {
            holder.statusTv.setText("已失效");
            if(!CommonUtil.isEmpty(data.getVciMoney()) &&
                    Double.parseDouble(data.getVciMoney()) != 0){
                holder.totalMoneyTv.setVisibility(View.VISIBLE);
                holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                        data.getTotalMoney()));
                holder.putinMoneyTv.setVisibility(View.VISIBLE);
                holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                        data.getPromotionMoney()));
            }
        } else {
            switch (data.getStatus()) {
                case 0: //待报价
                    holder.statusTv.setText(data.getStatusMsg(orderConfig));
                    break;
                case 1: //待付款
                    switch (orderConfig) {
                        case OrderConfig.RECEIPT:
                            holder.statusTv.setText(data.getStatusMsg(OrderConfig.RECEIPT));
                            holder.totalMoneyTv.setVisibility(View.VISIBLE);
                            holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                    data.getTotalMoney()));
                            holder.putinMoneyTv.setVisibility(View.VISIBLE);
                            holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                    data.getPromotionMoney()));
                            break;
                        case OrderConfig.SELL:
                            holder.statusTv.setText(data.getStatusMsg(OrderConfig.SELL));
                            holder.totalMoneyTv.setVisibility(View.VISIBLE);
                            holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                    data.getTotalMoney()));
                            holder.putinMoneyTv.setVisibility(View.VISIBLE);
                            holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                    data.getPromotionMoney()));
                            holder.updateTimeTv.setVisibility(View.GONE);
                            holder.orderBtn.setText(R.string.pay_now);
                            holder.orderBtn.setVisibility(View.VISIBLE);
                            holder.updateTime2Tv.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
                case 2://已付款
                    switch (orderConfig) {
                        case OrderConfig.RECEIPT:
                            switch (data.getFileStatus()) {
                                case 0://待出单
                                    holder.statusTv.setText(data.getStatusMsg(OrderConfig.RECEIPT));
                                    holder.totalMoneyTv.setVisibility(View.VISIBLE);
                                    holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                            data.getTotalMoney()));
                                    holder.putinMoneyTv.setVisibility(View.VISIBLE);
                                    holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                            data.getPromotionMoney()));
                                    holder.updateTimeTv.setVisibility(View.GONE);
                                    break;
                                case 1: //待签收(出单卖单方未签收)
                                case 2: //待回单(出单卖单方已签收未回单)
                                case 3://回单已寄出(出单卖单方已回单)
                                    holder.statusTv.setText(data.getStatusMsg(OrderConfig.RECEIPT));
                                    holder.totalMoneyTv.setVisibility(View.VISIBLE);
                                    holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                            data.getTotalMoney()));
                                    holder.putinMoneyTv.setVisibility(View.VISIBLE);
                                    holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                            data.getPromotionMoney()));
                                    break;
                                case 4://待评价
                                    holder.statusTv.setText(data.getStatusMsg(OrderConfig.RECEIPT));
                                    holder.totalMoneyTv.setVisibility(View.VISIBLE);
                                    holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                            data.getTotalMoney()));
                                    holder.putinMoneyTv.setVisibility(View.VISIBLE);
                                    holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                            data.getPromotionMoney()));
                                    if (data.getAcComment() == 0) {
                                        holder.updateTimeTv.setVisibility(View.GONE);
                                        holder.orderBtn.setVisibility(View.VISIBLE);
                                        holder.orderBtn.setText(getContext().getString(R.string.evaluate));
                                        holder.updateTime2Tv.setVisibility(View.VISIBLE);
                                    }
                                    break;
                            }
                            break;
                        case OrderConfig.SELL:
                            switch (data.getFileStatus()) {
                                case 0://待签收(已付款收单方未出单)
                                case 1://待签收(已付款收单方已出单)
                                    holder.statusTv.setText(data.getStatusMsg(OrderConfig.SELL));
                                    holder.totalMoneyTv.setVisibility(View.VISIBLE);
                                    holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                            data.getTotalMoney()));
                                    holder.putinMoneyTv.setVisibility(View.VISIBLE);
                                    holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                            data.getPromotionMoney()));
                                    holder.updateTimeTv.setVisibility(View.GONE);
                                    holder.orderBtn.setText(getContext().getString(R.string.sing_for_confirm));
                                    holder.orderBtn.setVisibility(View.VISIBLE);
                                    holder.updateTime2Tv.setVisibility(View.VISIBLE);
                                    break;
                                case 2://待回单(已签收)
                                case 3://待回单(已签收收单方未回单)
                                    holder.statusTv.setText(data.getStatusMsg(OrderConfig.SELL));
                                    holder.totalMoneyTv.setVisibility(View.VISIBLE);
                                    holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                            data.getTotalMoney()));
                                    holder.putinMoneyTv.setVisibility(View.VISIBLE);
                                    holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                            data.getPromotionMoney()));
                                    break;
                                case 4://待评价
                                    holder.statusTv.setText(data.getStatusMsg(OrderConfig.SELL));
                                    holder.totalMoneyTv.setVisibility(View.VISIBLE);
                                    holder.totalMoneyTv.setText(getContext().getString(R.string.order_total_money,
                                            data.getTotalMoney()));
                                    holder.putinMoneyTv.setVisibility(View.VISIBLE);
                                    holder.putinMoneyTv.setText(getContext().getString(R.string.order_putin_money,
                                            data.getPromotionMoney()));
                                    if (data.getBillComment() == 0) {
                                        holder.updateTimeTv.setVisibility(View.GONE);
                                        holder.orderBtn.setVisibility(View.VISIBLE);
                                        holder.orderBtn.setText(getContext().getString(R.string.evaluate));
                                        holder.updateTime2Tv.setVisibility(View.VISIBLE);
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
            }

        }
    }

    static class AcceptQuoteViewHolder extends RecyclerHolder {
        @BindView(R.id.order_headicon_iv)
        ImageView headIconIv;
        @BindView(R.id.order_username_tv)
        TextView usernameTv;
        @BindView(R.id.order_status_tv)
        TextView statusTv;
        @BindView(R.id.order_carowner_tv)
        TextView carOwnerTv;
        @BindView(R.id.order_plateno_tv)
        TextView plateNoTv;
        @BindView(R.id.order_cartype_tv)
        TextView carTypeTv;
        @BindView(R.id.order_company_tv)
        TextView companyTv;
        @BindView(R.id.order_carvehicle_tv)
        TextView carVehicleTv;
        @BindView(R.id.order_effectdate_tv)
        TextView effectDateTv;
        @BindView(R.id.order_updatetime_tv)
        TextView updateTimeTv;
        @BindView(R.id.order_updatetime2_tv)
        TextView updateTime2Tv;
        @BindView(R.id.order_totalmoney_tv)
        TextView totalMoneyTv;
        @BindView(R.id.order_putinmoney_tv)
        TextView putinMoneyTv;
        @BindView(R.id.order_button_tv)
        TextView orderBtn;

        private ItemButtonClickListener listener;

        public void setItemButtonClickListener(ItemButtonClickListener listener) {
            this.listener = listener;
        }

        public AcceptQuoteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.order_headicon_iv, R.id.order_button_tv})
        public void orderButtonClick(View view) {
            if (listener != null) {
                listener.buttonClick(view.getId(), getAdapterPosition());
            }
        }
    }
}
