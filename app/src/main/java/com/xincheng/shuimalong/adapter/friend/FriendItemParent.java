package com.xincheng.shuimalong.adapter.friend;

import android.util.Log;
import android.widget.CheckBox;

import com.xincheng.library.adpater.ViewHolder;
import com.xincheng.library.factory.ItemHelperFactory;
import com.xincheng.library.view.TreeItem;
import com.xincheng.library.view.TreeItemGroup;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.friend.Friend;

import java.util.List;


/**
 */
public class FriendItemParent extends TreeItemGroup<Friend> {

    @Override
    public List<TreeItem> initChildsList(Friend data) {
        return ItemHelperFactory.createTreeItemList(data.getFriends(), ChildrenItem.class, this,getContext());
    }


    @Override
    public int initLayoutId() {
        return R.layout.item_friend_group;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder) {
        holder.setText(R.id.tv_group_name, getData().getGroup_name());
        holder.setChecked(R.id.cb_friend,isExpand());
        holder.getView(R.id.cb_friend).setEnabled(false);
    }

}
