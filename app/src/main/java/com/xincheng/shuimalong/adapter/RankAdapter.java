package com.xincheng.shuimalong.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xincheng.library.util.UIUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.entity.rank.RankData;
import com.xincheng.shuimalong.util.ConfigUtil;
import com.xincheng.shuimalong.util.MyUtil;

import java.util.List;

/**
 * Created by xiaote1988 on 2017/7/29.
 */

public class RankAdapter extends BaseListRecyclerAdapter<RankData, RankAdapter.RankViewHolder> {
    private RequestOptions options;
    private List<RegionData> cityList;

    public RankAdapter(Context context, RequestOptions options) {
        super(context);
        this.options = options;
        cityList = ConfigUtil.getRegionDataList(context, PrefKey.CITY_LIST);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        switch (viewType) {
            case RankData.TYPE_TITLE:
                return R.layout.item_rank_title;
            case RankData.TYPE_CONTENT:
                return R.layout.item_rank;
        }
        return 0;
    }

    @Override
    public RankViewHolder createViewHolder(View itemView, int viewType) {
        return new RankViewHolder(itemView, viewType);
    }

    @Override
    public void convert(RankViewHolder holder, RankData rankData, int position) {
        switch (holder.getItemViewType()) {
            case RankData.TYPE_TITLE:
                holder.titleTv.setText(rankData.getTitle());
                holder.topTv.setText(String.valueOf(rankData.getTop()));
                break;
            case RankData.TYPE_CONTENT:
                Glide.with(getContext()).load(rankData.getData().getAvatar()).apply(options)
                        .into(holder.avatarIv);
                holder.nameTv.setText(rankData.getData().getNickName());
                if (rankData.getData().getRegionId() != 0) {
                    RegionData city = ConfigUtil.getRegionDataById(cityList,
                            rankData.getData().getRegionId());
                    holder.cityTv.setText(city.getName());
                }
                holder.numTv.setText(String.valueOf(rankData.getData().getNum()));
                double money = Double.parseDouble(rankData.getData().getMoney());
                holder.moneyTv.setText(MyUtil.getMoneyValueNew(money, 100000));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return dataList.get(position).getType();
    }

    public static class RankViewHolder extends RecyclerHolder {
        //Title
        private TextView titleTv;
        private TextView topTv;
        //Content
        private ImageView avatarIv;
        private TextView nameTv;
        private TextView cityTv;
        private TextView numTv;
        private TextView moneyTv;

        public RankViewHolder(View itemView, int viewType) {
            super(itemView);
            switch (viewType) {
                case RankData.TYPE_TITLE:
                    titleTv = UIUtil.findViewById(itemView, R.id.rank_item_title_tv);
                    topTv = UIUtil.findViewById(itemView, R.id.rank_item_top_tv);
                    break;
                case RankData.TYPE_CONTENT:
                    avatarIv = UIUtil.findViewById(itemView, R.id.rank_item_avatar_iv);
                    nameTv = UIUtil.findViewById(itemView, R.id.rank_item_name_tv);
                    cityTv = UIUtil.findViewById(itemView, R.id.rank_item_city_tv);
                    numTv = UIUtil.findViewById(itemView, R.id.rank_item_num_tv);
                    moneyTv = UIUtil.findViewById(itemView, R.id.rank_item_money_tv);
                    break;
            }
        }
    }
}
