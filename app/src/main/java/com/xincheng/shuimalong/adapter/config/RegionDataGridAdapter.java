package com.xincheng.shuimalong.adapter.config;

import android.content.Context;

import com.xincheng.library.xswipemenurecyclerview.adapter.ListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.RegionData;

import java.util.List;

/**
 * Created by xiaote on 2017/6/22.
 */

public class RegionDataGridAdapter extends ListRecyclerAdapter<RegionData> {

    public RegionDataGridAdapter(Context context) {
        super(context);
    }

    public RegionDataGridAdapter(Context context, List<RegionData> dataList) {
        super(context, dataList);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_cofig_grid;
    }

    @Override
    public void convert(RecyclerHolder holder, RegionData regionData, int position) {
        holder.setText(R.id.config_grid_name_tv, regionData.getName());
    }
}
