package com.xincheng.shuimalong.adapter.friend;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.friend.Commeny;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/6/28.
 */

public class CommenyAdapter extends BaseListRecyclerAdapter<Commeny,CommenyAdapter.ViewHolder> {
    private Context mcontext;
    private int from;

    public CommenyAdapter(Context context,int from) {
        super(context);
        this.mcontext=context;
        this.from = from;
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_evaluate_layout;
    }

    @Override
    public ViewHolder createViewHolder(View itemView, int viewType) {
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void convert(ViewHolder holder, Commeny data, int position) {
        holder.tvName.setText(data.getNick_name());
        Glide.with(mcontext).load(data.getAvatar()).apply(
                GlideOptionsManager.getInstance().getRequestOptions()).into(holder.imvAvatar);
        switch (from) {
            case 1:
                holder.tvTime.setText(DateUtil.parseDate(data.getBill_comment_time()));
                holder.evaluateRatingBar.setRating(data.getAccept_star());
                holder.evaluateContent.setText(data.getAccept_note());
                break;
            case 2:
                holder.tvName.setText(DateUtil.parseDate(data.getAc_comment_time()));
                holder.evaluateRatingBar.setRating(data.getBuyer_star());
                holder.evaluateContent.setText(data.getBuyer_note());
                break;
        }

    }

    static class ViewHolder extends RecyclerHolder {
        @BindView(R.id.imv_avatar)
        ImageView imvAvatar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.evaluate_rating_bar)
        AppCompatRatingBar evaluateRatingBar;
        @BindView(R.id.evaluate_content)
        TextView evaluateContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
