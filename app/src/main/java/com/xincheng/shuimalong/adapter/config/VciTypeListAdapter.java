package com.xincheng.shuimalong.adapter.config;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.adapter.ListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.config.VciType;
import com.xincheng.shuimalong.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by xiaote on 2017/6/26.
 */

public class VciTypeListAdapter extends ListRecyclerAdapter<VciType> {

    public VciTypeListAdapter(Context context, List<VciType> dataList) {
        super(context, dataList);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.item_vci_type;
    }

    @Override
    public void convert(RecyclerHolder holder, VciType vciType, int position) {
        TextView nameTv = holder.getView(R.id.item_vci_type_tv);
        nameTv.setText(vciType.getTitle());
        if (vciType.isSelected()) {
            nameTv.setTextColor(ContextCompat.getColor(getContext(), R.color.black_33));
        } else {
            nameTv.setTextColor(ContextCompat.getColor(getContext(), R.color.black_d9));
        }
    }
}
