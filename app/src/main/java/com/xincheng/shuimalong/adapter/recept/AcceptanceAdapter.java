package com.xincheng.shuimalong.adapter.recept;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xlog.XLog;
import com.xincheng.library.xswipemenurecyclerview.swipe.SwipeMenuLayout;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.common.Constant;
import com.xincheng.shuimalong.common.PrefKey;
import com.xincheng.shuimalong.entity.config.CommonConfig;
import com.xincheng.shuimalong.entity.config.Config;
import com.xincheng.shuimalong.entity.config.RegionData;
import com.xincheng.shuimalong.entity.recept.Acceptance;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;
import com.xincheng.shuimalong.recyclerview.BaseViewHolder;
import com.xincheng.shuimalong.recyclerview.CardSwipeMenuRecyclerAdapter;
import com.xincheng.shuimalong.util.ConfigUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/20.
 */

public class AcceptanceAdapter extends CardSwipeMenuRecyclerAdapter<Acceptance.Data,
        AcceptanceAdapter.AcceptanceViewHolder> {
    private List<CommonConfig> carTypeList;
    private List<CommonConfig> carWeightList;
    private List<CommonConfig> companyList;
    private List<CommonConfig> newCarTypeList;
    private List<CommonConfig> useAgeList;
    private List<RegionData> cityList;

    public AcceptanceAdapter(Context context) {
        super(context);
        initConfigData(context);
    }

    private void initConfigData(Context context) {
        carTypeList = ConfigUtil.getCommonConfigList(context, PrefKey.CAR_TYPE);
        carWeightList = ConfigUtil.getCommonConfigList(context, PrefKey.TRUCK_WEIGHT_TYPES);
        companyList = ConfigUtil.getCommonConfigList(context, PrefKey.COMPANY_LIST);
        newCarTypeList = ConfigUtil.getCommonConfigList(context, PrefKey.NEW_CAR_TYPE);
        useAgeList = ConfigUtil.getCommonConfigList(context, PrefKey.USAGE);
        cityList = ConfigUtil.getRegionDataList(context, PrefKey.CITY_LIST);
    }

    @Override
    public int getLayoutResId(int viewType) {
        return R.layout.item_receipt_bill;
    }

    @Override
    public AcceptanceViewHolder onCompatCreateViewHolder(View realContentView, int viewType) {
        AcceptanceViewHolder holder = new AcceptanceViewHolder(realContentView);
        return holder;
    }

    @Override
    public void onCompatBindViewHolder(AcceptanceViewHolder holder, Acceptance.Data acceptance, int position) {
        RegionData city = ConfigUtil.getRegionDataById(cityList,acceptance.getCityId());
        CommonConfig carType = ConfigUtil.getCommonConfigById(carTypeList, acceptance.getCarTypeId());
        CommonConfig carWeight = ConfigUtil.getCommonConfigById(carWeightList, acceptance.getCarWeight());
        CommonConfig insuranceCompany = ConfigUtil.getCommonConfigById(companyList, acceptance.getCompanyId());
        CommonConfig newCarType = ConfigUtil.getCommonConfigById(newCarTypeList, acceptance.getNewCarId());
        CommonConfig useAge = ConfigUtil.getCommonConfigById(useAgeList, acceptance.getUsage());
        holder.activityDurationTv.setText(acceptance.getActivityDuration());
        if (carType != null) {
            switch (carType.getId()) {
                case 1:
                    holder.carTypeTv.setText(carType.getName() + "  " + context.getString(R.string.seat_nums));
                    break;
                case 2:
                    if (carWeight != null) {
                        holder.carTypeTv.setText(carType.getName() + "  " + (carWeight.getId() == 0 ? "" : carWeight
                                .getName()));
                    }
                    break;
            }
        }
        holder.insuranceCompanyTv.setText(city.getName() + "  " +
                (insuranceCompany != null ? insuranceCompany.getName() : ""));
        holder.newOldCarTv.setText(newCarType.getName() + "  " + useAge.getName());
        holder.vciRbTv.setText(Constant.getPercentData(String.valueOf(acceptance.getVciRb())));
        holder.tciRbTv.setText(Constant.getPercentData(String.valueOf(acceptance.getTciRb())));
    }

    static class AcceptanceViewHolder extends BaseViewHolder {
        @BindView(R.id.item_bill_car_type_tv)
        TextView carTypeTv;
        @BindView(R.id.item_bill_insurance_company_tv)
        TextView insuranceCompanyTv;
        @BindView(R.id.item_bill_new_old_car_tv)
        TextView newOldCarTv;
        @BindView(R.id.item_bill_activity_duration_tv)
        TextView activityDurationTv;
        @BindView(R.id.item_bill_vcirb_tv)
        TextView vciRbTv;
        @BindView(R.id.item_bill_tcirb_tv)
        TextView tciRbTv;

        public AcceptanceViewHolder(View itemView) {
            super(itemView);
        }

    }
}
