package com.xincheng.shuimalong.adapter;

import android.content.Context;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.address.Address;
import com.xincheng.shuimalong.listener.ItemButtonClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xiaote on 2017/6/30.
 */

public class AddressAdapter extends BaseListRecyclerAdapter<Address, AddressAdapter.AddressViewHolder> {
    private ItemButtonClickListener listener;

    public AddressAdapter(Context context) {
        super(context);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_address;
    }

    public void setItemButtonClickListener(ItemButtonClickListener listener) {
        this.listener = listener;
    }

    @Override
    public AddressViewHolder createViewHolder(View itemView, int viewType) {
        AddressViewHolder holder = new AddressViewHolder(itemView);
        holder.setItemButtonClickListener(listener);
        return holder;
    }

    @Override
    public void convert(AddressViewHolder holder, Address address, int position) {
        holder.usernameTv.setText(address.getName());
        holder.mobileTv.setText(address.getMobile());
        holder.detailTv.setText(address.getDetailAddress());
        holder.selectIv.setVisibility(address.isSelect() ? View.VISIBLE : View.GONE);
        holder.defaultTv.setChecked(address.isDefault());
    }

    static class AddressViewHolder extends RecyclerHolder {
        @BindView(R.id.address_username_tv)
        TextView usernameTv;
        @BindView(R.id.address_mobile_tv)
        TextView mobileTv;
        @BindView(R.id.address_select_iv)
        ImageView selectIv;
        @BindView(R.id.address_detail_tv)
        TextView detailTv;
        @BindView(R.id.address_default_ctv)
        CheckedTextView defaultTv;

        private ItemButtonClickListener listener;

        public void setItemButtonClickListener(ItemButtonClickListener listener) {
            this.listener = listener;
        }

        public AddressViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.address_default_ctv, R.id.address_edit_tv, R.id.address_delete_tv})
        public void itemButtonClick(View view) {
            if (listener != null) {
                listener.buttonClick(view.getId(), getAdapterPosition());
            }
        }
    }
}
