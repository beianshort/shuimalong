package com.xincheng.shuimalong.adapter.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xincheng.shuimalong.common.IntentKey;
import com.xincheng.shuimalong.common.OrderConfig;

import java.util.List;

/**
 * Created by xiaote on 2017/6/21.
 */

public class HomeViewPagerAdapter extends FragmentPagerAdapter {
    private String[] titles;
    private List<Fragment> fragments;
    private int orderConfig;
    private int provinceId;
    private int cityId;
    private String keyword;

    public HomeViewPagerAdapter(FragmentManager fm, String[] titles, List<Fragment> fragments,int orderConfig,
                                int provinceId, int cityId,String keyword) {
        super(fm);
        this.titles = titles;
        this.fragments = fragments;
        this.orderConfig = orderConfig;
        this.provinceId = provinceId;
        this.cityId = cityId;
        this.keyword = keyword;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = fragments.get(position);
        Bundle extras = new Bundle();
        extras.putInt(IntentKey.ORDER_CONFIG, orderConfig);
        extras.putInt(IntentKey.STATE, OrderConfig.STATE_ARR[position]);
        extras.putInt("provinceId",provinceId);
        extras.putInt("cityId",cityId);
        extras.putString("keyword",keyword);
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public int getCount() {
        return fragments == null ? 0 : fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
