package com.xincheng.shuimalong.adapter.wallet;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.wallet.WalletInquiry;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/7/20.
 */

public class WalletInquiryAdapter extends BaseListRecyclerAdapter<WalletInquiry,WalletInquiryAdapter.WalletInquiryViewHolder> {

    public WalletInquiryAdapter(Context context, List<WalletInquiry> dataList) {
        super(context, dataList);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.market_sort_item;
    }

    @Override
    public WalletInquiryViewHolder createViewHolder(View itemView, int viewType) {
        return new WalletInquiryViewHolder(itemView);
    }

    @Override
    public void convert(WalletInquiryViewHolder holder, WalletInquiry walletInquiry, int position) {
        holder.nameTv.setText(walletInquiry.getName());
        holder.nameTv.setTextColor(ContextCompat.getColor(getContext(),
               walletInquiry.isSelected() ? R.color.blue1 : R.color.black_66));
        holder.selectIv.setVisibility(walletInquiry.isSelected() ? View.VISIBLE : View.GONE);
    }

    public void changeSelect(int position) {
        for(int i = 0; i< dataList.size();i++) {
            if(i == position) {
                dataList.get(i).setSelected(true);
            } else {
                dataList.get(i).setSelected(false);
            }
        }
        this.notifyDataSetChanged();
    }

    static class WalletInquiryViewHolder extends RecyclerHolder {
        @BindView(R.id.sort_name_tv)
        TextView nameTv;
        @BindView(R.id.sort_select_iv)
        ImageView selectIv;

        public WalletInquiryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
