package com.xincheng.shuimalong.adapter.wallet;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.wallet.MoneyLog;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/7/20.
 */

public class MoneyLogAdapter extends BaseListRecyclerAdapter<MoneyLog.Data, MoneyLogAdapter.LogViewHolder> {

    public MoneyLogAdapter(Context context) {
        super(context);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_money_log;
    }

    @Override
    public LogViewHolder createViewHolder(View itemView, int viewType) {
        return new LogViewHolder(itemView);
    }

    @Override
    public void convert(LogViewHolder holder, MoneyLog.Data data, int position) {
        if(CommonUtil.isEmpty(data.getDateTime())) {
            holder.dateTimeTv.setText("今天\n18:00");
            holder.dateTimeTv.setVisibility(View.INVISIBLE);
        } else {
            holder.dateTimeTv.setVisibility(View.VISIBLE);
            holder.dateTimeTv.setText(data.getDateTime());
        }
        holder.typeMoneyTv.setText(data.getTypeMsg());
        holder.bakTv.setText(data.getBak());
        holder.statusTv.setText(data.getStatusMsg());
        holder.reduceTv.setText(data.getReduceMsg());
        holder.lineView1.setVisibility(position == 0 ? View.INVISIBLE : View.VISIBLE);
        holder.lineView2.setVisibility(position == dataList.size() - 1 ? View.INVISIBLE: View.VISIBLE);
    }

    class LogViewHolder extends RecyclerHolder {
        @BindView(R.id.money_log_line1)
        View lineView1;
        @BindView(R.id.money_log_line2)
        View lineView2;
        @BindView(R.id.log_date_time_tv)
        TextView dateTimeTv;
        @BindView(R.id.log_type_money_tv)
        TextView typeMoneyTv;
        @BindView(R.id.log_bak_tv)
        TextView bakTv;
        @BindView(R.id.log_reduce_tv)
        TextView reduceTv;
        @BindView(R.id.log_status_tv)
        TextView statusTv;

        public LogViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
