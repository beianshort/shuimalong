package com.xincheng.shuimalong.adapter.order;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xincheng.library.util.DateUtil;
import com.xincheng.library.xswipemenurecyclerview.adapter.BaseListRecyclerAdapter;
import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;
import com.xincheng.shuimalong.R;
import com.xincheng.shuimalong.entity.HistoryOrder;
import com.xincheng.shuimalong.manager.GlideOptionsManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xiaote on 2017/6/28.
 */

public class OrderHistoryAdapter extends BaseListRecyclerAdapter<HistoryOrder,
        OrderHistoryAdapter.AcceptQuoteViewHolder> {
    public OrderHistoryAdapter(Context context) {
        super(context);
    }

    @Override
    public int getViewLayoutId(int viewType) {
        return R.layout.item_order_history;
    }

    @Override
    public AcceptQuoteViewHolder createViewHolder(View itemView, int viewType) {
        AcceptQuoteViewHolder holder = new AcceptQuoteViewHolder(itemView);
        return holder;
    }

    @Override
    public void convert(AcceptQuoteViewHolder holder, HistoryOrder data, int position) {
        holder.tvOrderHistoryAmount.setText(data.getTotal_money());
        holder.tvOrderHistoryName.setText(data.getNick_name());
        holder.tvOrderHistoryMobile.setText(data.getMobile());
        holder.tvOrderHistoryCarOwner.setText(data.getCar_owner());
        holder.tvOrderHistoryCarNumber.setText(data.getPlate_no());
        holder.tvOrderHistoryDate.setText(DateUtil.parseDate(data.getUpdate_time()));
        holder.tvOrderHistoryId.setText(data.getOrder_no());
        Glide.with(getContext()).load(data.getAvatar()).apply(GlideOptionsManager.getInstance().getRequestOptions())
                .into(holder.imvOrderHistoryAvatar);
    }

    static class AcceptQuoteViewHolder extends RecyclerHolder {
        @BindView(R.id.tv_order_history_name)
        TextView tvOrderHistoryName;
        @BindView(R.id.imv_order_history_avatar)
        ImageView imvOrderHistoryAvatar;
        @BindView(R.id.tv_order_history_mobile)
        TextView tvOrderHistoryMobile;
        @BindView(R.id.tv_order_history_car_owner)
        TextView tvOrderHistoryCarOwner;
        @BindView(R.id.tv_order_history_car_number)
        TextView tvOrderHistoryCarNumber;
        @BindView(R.id.tv_order_history_date)
        TextView tvOrderHistoryDate;
        @BindView(R.id.tv_order_history_id)
        TextView tvOrderHistoryId;
        @BindView(R.id.tv_order_history_amount)
        TextView tvOrderHistoryAmount;

        public AcceptQuoteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
