package com.xincheng.shuimalong.listener;




import com.xincheng.library.xlog.XLog;
import com.xincheng.shuimalong.entity.friend.ChatMessage;
import com.xincheng.shuimalong.entity.friend.HistoryChat;
import com.xincheng.shuimalong.util.GreenUtil;

import java.util.Timer;
import java.util.TimerTask;

import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.message.ContactNotificationMessage;
import io.rong.message.NotificationMessage;

/**
 * Created by xuhao on 2017/7/17.
 */

public class MyReceiveMessageListener implements RongIMClient.OnReceiveMessageListener {
    private OnMessageListener messageListener;
    private OnSystemListener systemListener;
    public void setMessageListener(OnMessageListener listener) {
        this.messageListener = listener;
    }
    public void setSystemListener(OnSystemListener listener){
        this.systemListener=listener;
    }
    /**
     * 收到消息的处理。
     * @param message 收到的消息实体。
     * @param left 剩余未拉取消息数目。
     * @return
     */
    @Override
    public boolean onReceived( Message message, int left) {
        //开发者根据自己需求自行处理,根据消息类型调用不同监听
        return true;
    }
}