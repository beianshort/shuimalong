package com.xincheng.shuimalong.listener;

import io.rong.imlib.model.Message;

/**
 * Created by xuhao on 2017/7/19.
 */

public interface OnSystemListener {
    void onMessage(Message message, int left);
}
