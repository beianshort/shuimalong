package com.xincheng.shuimalong.listener;

import com.xincheng.shuimalong.entity.friend.ChatMessage;


/**
 * Created by xuhao on 2017/7/19.
 */

public interface OnMessageListener {
    void onMessage(ChatMessage message, int left);
}
