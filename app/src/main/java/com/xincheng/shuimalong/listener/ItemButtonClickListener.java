package com.xincheng.shuimalong.listener;

/**
 * Created by xiaote on 2017/6/19.
 */

public interface ItemButtonClickListener {
    void buttonClick(int clickId, int position);
}
