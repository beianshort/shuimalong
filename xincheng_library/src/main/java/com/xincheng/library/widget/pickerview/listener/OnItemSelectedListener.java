package com.xincheng.library.widget.pickerview.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
