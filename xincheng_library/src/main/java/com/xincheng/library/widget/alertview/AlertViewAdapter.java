package com.xincheng.library.widget.alertview;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xincheng.library.R;

import java.util.List;

/**
 * Created by Sai on 15/8/9.
 */
public class AlertViewAdapter extends BaseAdapter {
    private List<String> mDatas;
    private List<String> mDestructive;

    public AlertViewAdapter(List<String> datas, List<String> destructive) {
        this.mDatas = datas;
        this.mDestructive = destructive;
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String data = mDatas.get(position);
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.alertview_item_alertbutton, null);
            holder = creatViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.updateUI(parent.getContext(), data, position);
        return convertView;
    }

    public ViewHolder creatViewHolder(View view) {
        return new ViewHolder(view);
    }

    class ViewHolder {
        private TextView tvAlert;
        private View tvDivider;

        public ViewHolder(View view) {
            tvAlert = (TextView) view.findViewById(R.id.tvAlert);
            tvDivider = (View) view.findViewById(R.id.tvDivider);
        }

        public void updateUI(Context context, String data, int position) {
            if(position == 0) {
                tvDivider.setVisibility(View.GONE);
            } else {
                tvDivider.setVisibility(View.VISIBLE);
            }
            tvAlert.setText(data);
            if (mDestructive != null && mDestructive.contains(data)) {
                tvAlert.setTextColor(ContextCompat.getColor(context, R.color.textColor_alert_button_destructive));
            } else {
                tvAlert.setTextColor(ContextCompat.getColor(context,R.color.textColor_alert_button_others));
            }
        }
    }
}