package com.xincheng.library.widget.alertview;


/**
 * Created by Sai on 15/8/9.
 */
public interface OnAlertDismissListener {
    void onDismiss(AlertView alertView);
}
