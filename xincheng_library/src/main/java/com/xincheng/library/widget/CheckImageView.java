package com.xincheng.library.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.xincheng.library.R;

/**
 * Created by xiaote on 2016/3/17.
 */
public class CheckImageView extends AppCompatImageView {
    private int checked_normal;
    private int checked_selected;
    private int enabled_false;
    private int enabled_true;

    public CheckImageView(Context context) {
        this(context,null);
    }

    public CheckImageView(Context context, AttributeSet attrs) {
        super(context,attrs);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckImageView);
        checked_normal = typedArray.getResourceId(R.styleable.CheckImageView_checked_normal, R.drawable.checkbox);
        checked_selected = typedArray.getResourceId(R.styleable.CheckImageView_checked_selected,
                R.drawable.checkbox_selected);
        enabled_false = typedArray.getResourceId(R.styleable.CheckImageView_enabled_false, R.color.white);
        enabled_true = typedArray.getResourceId(R.styleable.CheckImageView_enabled_true, R.color.black);
    }

    public void setChecked(boolean checked) {
        if (checked) {
            setImageResource(checked_selected);
        } else {
            setImageResource(checked_normal);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        if(enabled) {
            setImageResource(enabled_true);
        } else{
            setImageResource(enabled_false);
        }
        super.setEnabled(enabled);
    }

}
