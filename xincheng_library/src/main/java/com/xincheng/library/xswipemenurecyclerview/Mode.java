package com.xincheng.library.xswipemenurecyclerview;

/**
 * Created by xiaote on 2017/4/13.
 */

public enum Mode {
    NONE, PULL_TO_REFRESH, LOAD_MORE, BOTH
}
