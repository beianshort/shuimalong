package com.xincheng.library.xswipemenurecyclerview.listener;

import android.view.View;

import com.xincheng.library.xswipemenurecyclerview.holder.RecyclerHolder;

public interface OnItemClickListener {
    void onItemClick(RecyclerHolder holder, View view, int position);
}
