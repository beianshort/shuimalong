package com.xincheng.library.xswipemenurecyclerview.listener;

/**
 * Created by xiaote on 2017/4/14.
 */
public interface OnLoadingListener {
    void onRefresh();

    void onLoadMore();
}
