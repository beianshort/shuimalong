package com.xincheng.library.common;

/**
 * Created by xiaote on 2017/5/24.
 */

public class BaseConfig {
    public static final int CONNECT_TIMEOUT = 30;
    public static final int READ_TIMEOUT = 30;
    public static final int WRITE_TIMEOUT = 30;
}
