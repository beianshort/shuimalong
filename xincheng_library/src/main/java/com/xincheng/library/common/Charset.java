package com.xincheng.library.common;

/**
 * Created by xiaote on 2016/4/12.
 */
public class Charset {
    public static final String UTF8 = "UTF-8";
    public static final String GBK = "GBK";
    public static final String GB2312 = "GB2312";
}
