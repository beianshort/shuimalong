package com.xincheng.library.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by xiaote on 2017/6/23.
 */

public class DateUtil {
    public static String parseDateDefault(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public static String parseDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    public static String getDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
        return format.format(date);
    }

    public static String parseDate2(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH时");
        return format.format(date);
    }

    public static String parseDate(long time) {
        Date date = new Date(time * 1000L);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }

    public static String parseDate2(long time) {
        Date date = new Date(time * 1000L);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    public static long getTodayZeroTime() {
        Calendar c = Calendar.getInstance();
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        return c.getTimeInMillis();
    }

    public static String getHourMinute(long timeInMillis) {
        Date date = new Date(timeInMillis);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(date);
    }

    public static String getMonthDay(long timeInMillis) {
        Date date = new Date(timeInMillis);
        SimpleDateFormat format = new SimpleDateFormat("MM-dd");
        return format.format(date);
    }

    public static String getWeekDay(Calendar c) {
        if (c == null) {
            return "周一";
        }
        if (Calendar.MONDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周一";
        }
        if (Calendar.TUESDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周二";
        }
        if (Calendar.WEDNESDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周三";
        }
        if (Calendar.THURSDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周四";
        }
        if (Calendar.FRIDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周五";
        }
        if (Calendar.SATURDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周六";
        }
        if (Calendar.SUNDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return "周日";
        }
        return "周一";
    }

    public static Calendar getTomrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH) + 1,
                0,0,0);
        return calendar;
    }

    public static boolean isBefore(Date date) {
        Date now = new Date();
        return date.before(now);
    }

    public static long getTimeInMillis(String str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(str).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    public static long getTimeInMillis2(String str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH时");
        try {
            return format.parse(str).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }
}
