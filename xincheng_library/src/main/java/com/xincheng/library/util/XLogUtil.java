package com.xincheng.library.util;

import com.xincheng.library.xlog.LogConfiguration;
import com.xincheng.library.xlog.LogLevel;
import com.xincheng.library.xlog.XLog;

/**
 * Created by xiaote on 2017/5/24.
 */

public class XLogUtil {
    public static void init(String tag, boolean isDebug, boolean isShowBoard) {
        XLog.init(new LogConfiguration.Builder().tag(tag).logLevel(isDebug ? LogLevel.ALL : LogLevel.NONE)
                .showBoarder(isShowBoard).build());
    }
}
