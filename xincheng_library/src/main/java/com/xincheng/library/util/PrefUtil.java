package com.xincheng.library.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.Set;

public class PrefUtil {
    private static String PREF_TAG = "pref_tag";

    public static void setPrefTag(String tag) {
        PREF_TAG = tag;
    }

    /**
     * 保存字符串数据
     *
     * @param context   上下文对象
     * @param key       PREF_KEY
     * @param value    PREF_VALUE
     */
    public static void putString(Context context, String key, String value) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }


    /**
     * 获取字符串数据
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static String getString(Context context, String key, String defValue) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return pref.getString(key, defValue);
    }


    /**
     * 保存整型数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putInt(Context context, String key, int value) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * 保存整型数据
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return pref.getInt(key, defValue);
    }


    /**
     * 保存长整型数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putLong(Context context, String key, long value) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * 保存长整型数据
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static long getLong(Context context, String key, long defValue) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return pref.getLong(key, defValue);
    }

    /**
     * 保存布尔值数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * 保存布尔值数据
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getBoolean(Context context, String key, boolean defValue) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return pref.getBoolean(key, defValue);
    }


    /**
     * 保存浮点型数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putFloat(Context context, String key, float value) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    /**
     * 保存浮点型数据
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static float getFloat(Context context, String key, float defValue) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return pref.getFloat(key, defValue);
    }

    /**
     * 保存String Set集合数据
     * @param context
     * @param key
     * @param value
     */
    public static void putSetString(Context context, String key, Set<String> value) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putStringSet(key,value);
        editor.commit();
    }

    public static Set<String> getSetString(Context context, String key, Set<String> defValue) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return pref.getStringSet(key,defValue);
    }

    public static void removeKey(Context context, String key) {
        SharedPreferences pref = context
                .getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.remove(key);
        editor.commit();
    }
}
