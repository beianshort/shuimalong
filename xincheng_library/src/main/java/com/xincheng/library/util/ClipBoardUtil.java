package com.xincheng.library.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

/**
 * Created by xiaote1988 on 2017/7/20.
 * 系统文本编辑器工具类
 */

public class ClipBoardUtil {
    public static final String LABEL = "content";

    /**
     * 复制文本到剪切板
     */
    public static void copyText(Context context, String content) {
        //获取剪切板对象
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(LABEL,content);
        cmb.setPrimaryClip(clipData);
    }

    /**
     * 实现粘贴功能
     */
    public static String pasteText(Context context) {
        //获取剪切板对象
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = cmb.getPrimaryClip();
        String text = clipData.getItemAt(0).getText().toString();
        return text;
    }
}
