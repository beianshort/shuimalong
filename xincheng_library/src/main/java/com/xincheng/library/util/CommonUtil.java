package com.xincheng.library.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.widget.PopupWindowCompat;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.xincheng.library.common.Charset;
import com.xincheng.library.xlog.XLog;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by xiaote on 2017/6/16.
 */

public class CommonUtil {
    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, int textResId) {
        Toast.makeText(context, textResId, Toast.LENGTH_SHORT).show();
    }

    public static boolean isEmpty(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(Object[] arr) {
        if (arr == null || arr.length == 0) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(List<?> list) {
        if (list == null || list.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(Set<?> set) {
        if (set == null || set.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(Map<?, ?> map) {
        if (map == null || map.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(EditText et) {
        if (et == null || "".equals(et.getText().toString().trim())) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(TextView tv) {
        if (tv == null || "".equals(tv.getText().toString().trim())) {
            return true;
        }
        return false;
    }

    public static final String getURLEncodeStr(String source) {
        try {
            return URLEncoder.encode(source, Charset.UTF8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return source;
        }
    }

    public static String getBase64EncodeStr(String source) {
        try {
            byte[] bytes = source.getBytes(Charset.UTF8);
            return Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (Exception e) {
            return source;
        }
    }

    public static String getBase64DecodeStr(String source) {
        XLog.d("source==>" + source);
        try{
            byte[] bytes = Base64.decode(source, Base64.DEFAULT);
            String decodeStr = new String(bytes);
            XLog.d("base64DecodeStr==>" + decodeStr);
            return decodeStr;
        }catch (Exception e) {
            return null;
        }
    }

    public static String getMoneyStr(double money) {
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(money);
    }

    public static PopupWindow createPopupWindow(View view) {
        PopupWindow popupWindow = new PopupWindow(view);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#66FFFFFF"));
        popupWindow.setBackgroundDrawable(colorDrawable);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setClippingEnabled(false);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        return popupWindow;
    }

    public static String getLocalImagePath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) { // 忽略大小写
            // String[] projection = { "_data" };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, null, null,
                        null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static int getStatusBarHeight() {
        int statusBarHeight = 50;
        int resId = Resources.getSystem().getIdentifier("status_bar_height", "dimen", "android");
        if(resId > 0){
            statusBarHeight = Resources.getSystem().getDimensionPixelSize(resId);
        }
        return statusBarHeight;
    }

    public static int getStatusBarHeight(Activity activity) {
        int statusHeight = 0;
        Rect localRect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);
        statusHeight = localRect.top;
        if (0 == statusHeight) {
            Class<?> localClass;
            try {
                localClass = Class.forName("com.android.internal.R$dimen");
                Object localObject = localClass.newInstance();
                int i5 = Integer.parseInt(localClass.getField("status_bar_height").get(localObject).toString());
                statusHeight = activity.getResources().getDimensionPixelSize(i5);
            } catch (Exception e) {
            }
        }
        return statusHeight;
    }

    public static final int getVersionCode(Context context, String packageName) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            return info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static final String getVersionName(Context context, String packageName) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            return info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
