package com.xincheng.library.util;

import android.app.Activity;

/**
 * Created by xiaote on 2017/7/26.
 */

public class WebSettingUtil {
    public static String getH5CacheDir(Activity activity) {
        return activity.getApplicationContext().getCacheDir().getAbsolutePath();
    }
}
