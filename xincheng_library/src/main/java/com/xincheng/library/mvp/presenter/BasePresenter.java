package com.xincheng.library.mvp.presenter;

/**
 * Created by xiaote on 2016/10/26.
 */

public interface BasePresenter<V> {
    void attachView(V view);

    void detachView();

    void requestData(int code);
}
