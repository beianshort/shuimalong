package com.xincheng.library.mvp.listener;

/**
 * Created by xiaote on 2016/5/4.
 */
public interface BaseModelListener<T> {

    void onStart(int code);

    void onFinish(int code);

    void onSuccess(int code, T t);

    void onFailed(int code, String errorMsg);
}
