package com.xincheng.library.mvp.model;

import com.xincheng.library.mvp.listener.BaseModelListener;

import io.reactivex.disposables.Disposable;


/**
 * Created by xiaote on 2016/5/4.
 * E->表示请求时需要提交的封装类
 * T->表示响应回调成功的封装类
 */
public interface BaseModel<E,T> {
    Disposable getData(int code, E e, BaseModelListener<T> listener);
}
