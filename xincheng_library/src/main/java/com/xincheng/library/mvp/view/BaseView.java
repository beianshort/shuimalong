package com.xincheng.library.mvp.view;

/**
 * Created by xiaote on 2016/10/26.
 */

public interface BaseView {
    void showLoading();

    void hideLoading();

    void showToastMsg(String msg);

    void sendFailed(String errorMsg);
}
