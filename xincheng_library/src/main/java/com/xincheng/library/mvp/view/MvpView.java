package com.xincheng.library.mvp.view;

/**
 * Created by xiaote on 2016/5/4.
 */
public interface MvpView<E,T> extends BaseView {
    E getRequestObj();

    void sendSuccess(T t);

}
