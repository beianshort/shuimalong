package com.xincheng.library.mvp.presenter;

import com.xincheng.library.mvp.model.MvpModel;

import io.reactivex.disposables.Disposable;


/**
 * Created by xiaote on 2016/5/4.
 */
public abstract class MvpPresenter<V> implements BasePresenter<V> {
    protected MvpModel mvpModel;
    public V mvpView;
    private Disposable disposable;

    public MvpPresenter(MvpModel mvpModel) {
        this.mvpModel = mvpModel;
    }

    @Override
    public void attachView(V view) {
        this.mvpView = view;
    }

    @Override
    public void detachView() {
        this.mvpView = null;
        onUnSubscribe();
    }

    //RxJava取消注册，以避免内存泄露
    public void onUnSubscribe() {
        if(disposable != null) {
            disposable.dispose();
        }
    }

    public void addDisposable(Disposable disposable) {
        this.disposable = disposable;
    }

    public void unSubscriber() {
        if(mvpModel != null) {
            mvpModel.unSubscriber();
        }
    }

}
