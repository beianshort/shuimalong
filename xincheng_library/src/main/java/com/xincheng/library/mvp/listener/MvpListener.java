package com.xincheng.library.mvp.listener;

import com.xincheng.library.mvp.view.MvpView;

/**
 * Created by xiaote on 2016/11/3.
 */

public class MvpListener<T> implements BaseModelListener<T> {
    protected MvpView view;

    public MvpListener(MvpView view) {
        this.view = view;
    }

    @Override
    public void onStart(int code) {
        view.showLoading();
    }

    @Override
    public void onFinish(int code) {
        view.hideLoading();
    }

    @Override
    public void onSuccess(int code, T t) {
        view.sendSuccess(t);
    }

    @Override
    public void onFailed(int code,String errorMsg) {
        view.sendFailed(errorMsg);
    }
}
