package com.xincheng.library.view;

import android.content.Context;
import android.support.annotation.Nullable;

import com.xincheng.library.base.BaseItem;


/**
 * TreeRecyclerAdapter的item
 */
public abstract class TreeItem<D> extends BaseItem<D> {
    private TreeItemGroup parentItem;
    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setParentItem(TreeItemGroup parentItem) {
        this.parentItem = parentItem;
    }
    /**
     * 获取当前item的父级
     *
     * @return
     */
    @Nullable
    public TreeItemGroup getParentItem() {
        return parentItem;
    }

    public String getItemName() {
        return "TreeItem";
    }
}
