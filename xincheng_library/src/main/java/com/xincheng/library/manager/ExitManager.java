package com.xincheng.library.manager;

import android.app.Activity;

import com.xincheng.library.util.CommonUtil;
import com.xincheng.library.util.PrefUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiaote on 2017/7/6.
 */

public class ExitManager {
    private static ExitManager instance;

    private List<Activity> activityList;

    private ExitManager() {
        activityList = new ArrayList<>();
    }

    public static ExitManager getInstance() {
        if (instance == null) {
            synchronized (ExitManager.class) {
                if (instance == null) {
                    instance = new ExitManager();
                }
            }
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        this.activityList.add(activity);
    }

    public void exitActivities() {
        if (!CommonUtil.isEmpty(activityList)) {
            for (int i = 0; i < activityList.size(); i++) {
                activityList.get(i).finish();
            }
            activityList.clear();
        }
    }

    public void clearActivies() {
        if(!CommonUtil.isEmpty(activityList)) {
            activityList.clear();
        }
    }
}
